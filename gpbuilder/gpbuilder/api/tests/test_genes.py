from fastapi.testclient import TestClient

from gpbuilder.app import app

client = TestClient(app)


def test_search_gene_by_hgnc_id_int():
    response = client.get("/api/v1/genenames/hgnc/1100")
    assert response.status_code == 200


def test_search_gene_by_hgnc_name():
    response = client.get("/api/v1/genenames/hgnc/INS")
    assert response.status_code == 422


def test_get_batch_genes():
    response = client.post("/api/v1/genenames/hgnc_ids", json=[1100, 1101])
    assert response.status_code == 200


def test_search_gene_by_pattern_with_letters():
    response = client.get("/api/v1/genenames/search/BRC")
    assert response.status_code == 200


def test_search_gene_by_pattern_with_ints():
    response = client.get("/api/v1/genenames/search/1100")
    assert response.status_code == 200
