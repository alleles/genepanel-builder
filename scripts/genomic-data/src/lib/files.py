import gzip
import sys
from pathlib import Path


def decompress_file(gz_file, bed_file):
    with open(gz_file, "rb") as inf, open(bed_file, "w", encoding="utf8") as tof:
        decom_str = gzip.decompress(inf.read()).decode("utf-8")
        tof.write(decom_str)


def get_decompressed_files(gz_files: list[Path]) -> list[Path]:
    """
    Decompress each sample file: .gz --> .bed

    Validate file exists and has the expected columns: chromosome (str), start(+int),
    end(+int), number of reads(+int)

    Parameters:
    gz_files: pathlib.Path, list of bed.gz file names

    Returns:
    list of pathlib.Path for the files that were extracted

    """
    print("Decompressing bed.gz files...")
    bed_files = []
    for gz_file in gz_files:
        if not gz_file.suffix == ".gz":
            print(f"File {gz_file} is not compressed.", file=sys.stderr)
            sys.exit(-1)

        if gz_file.exists():
            bed_file = Path(gz_file.resolve().parent / gz_file.stem)
            if not bed_file.exists():
                decompress_file(gz_file, bed_file)
            bed_files.append(bed_file)
            print(f"Decompressed {bed_file}")
        else:
            print(f"File {gz_file} not found.", file=sys.stderr)

    return bed_files
