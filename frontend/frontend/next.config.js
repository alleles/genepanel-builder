/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  webpack: (config) => {
    config.experiments.topLevelAwait = true
    return config
  },
  output: 'standalone',
}

module.exports = nextConfig
