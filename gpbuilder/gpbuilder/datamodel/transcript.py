from functools import cached_property
from typing import ClassVar, List, Literal, Optional

from pydantic import root_validator
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm.exc import UnmappedInstanceError
from sqlmodel import (
    Column,
    Enum,
    Field,
    Identity,
    Integer,
    Relationship,
    Session,
    UniqueConstraint,
    col,
    or_,
    select,
)

from gpbuilder.datamodel.base import GenomicFeature, SQLModelBase
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.utils.types import CodingStrand


class Transcript(GenomicFeature, table=True):
    id: Optional[int] = Field(sa_column=Column(Integer, Identity(), primary_key=True))
    name: str = Field(index=True)
    strand: CodingStrand = Field(sa_column=Column(Enum("+", "-", name="strand")))
    source: str = Field(default="custom")
    hgnc_id: int = Field(foreign_key="gene.hgnc_id")
    coding_start: Optional[int]
    coding_end: Optional[int]
    exon_starts: List[int] = Field(sa_column=Column(postgresql.ARRAY(Integer)))
    exon_ends: List[int] = Field(sa_column=Column(postgresql.ARRAY(Integer)))
    update_id: Optional[int] = Field(default=None, foreign_key="update.id")

    gene: "Gene" = Relationship()

    name_sep: ClassVar[str] = "__"

    __tableargs__ = (UniqueConstraint("ref_genome", "name"),)

    @root_validator
    def validate_coding_region(cls, values):
        _coding_start = values["coding_start"]
        _coding_end = values["coding_end"]
        _exon_starts = values.get("exon_starts", [])
        _exon_ends = values.get("exon_ends", [])
        assert _exon_starts == sorted(_exon_starts), "exon starts are not sorted."
        assert _exon_ends == sorted(_exon_ends), "exon ends are not sorted."
        if (_coding_start and not _coding_end) or (_coding_end and not _coding_start):
            raise ValueError(f"open coding region [{_coding_start}, {_coding_end}].")
        if _coding_start and _coding_end and _coding_start > _coding_end:
            raise ValueError(f"invalid coding region [{_coding_start}, {_coding_end}].")
        if len(_exon_starts) != len(_exon_ends):
            raise ValueError(
                "exon splicing mismatch: (%d <> %d)"
                % (len(_exon_starts), len(_exon_ends))
            )
        is_valid_coding_region = False
        for _exon_start, _exon_end in zip(_exon_starts, _exon_ends):
            if _exon_start < 0:
                raise ValueError("invalid exon start position %d" % _exon_start)
            if _exon_end < 0:
                raise ValueError("invalid exon end position %d" % _exon_end)
            if _exon_start > _exon_end:
                raise ValueError(
                    "invalid exon region [%d, %d]." % (_exon_start, _exon_end)
                )
            if _coding_start and _coding_end:
                if _exon_start < _coding_end and _exon_end > _coding_start:
                    is_valid_coding_region = True
        if (
            _coding_start
            and _coding_end
            and _exon_starts
            and _exon_ends
            and not is_valid_coding_region
        ):
            raise ValueError(
                (
                    f"the coding region [{_coding_start}, {_coding_end}] contains no exons.",
                    str(list(zip(_exon_starts, _exon_ends))),
                )
            )
        return values

    @property
    def is_coding(self):
        return self.coding_start is not None

    @cached_property
    def cds(self) -> List[Region]:
        """Return chromosome, coding start and end position tuples"""
        rlist = []
        if self.coding_start and self.coding_end:
            coding_region = Region.construct(
                chromosome=self.chromosome,
                start=self.coding_start,
                end=self.coding_end,
                ref_genome=self.ref_genome,
            )
            for exon in self.exons:
                rlist.extend(list(exon.intersect([coding_region])))
        return rlist

    @cached_property
    def cds_names(self):
        """Return the (1-based) cds names."""
        cds_names = []
        for exon, name in zip(self.exons, self.exon_names):
            # This is equivalent to (and signficantly faster) than `list(exon.intersect(self.cds))`
            # Not noticeable for a single transcript, but for a large number of transcripts this
            # can be a bottleneck.
            if next(exon.intersect(self.cds), False):
                cds_names.append(name)
        assert len(self.cds) == len(cds_names)
        return cds_names

    @cached_property
    def exon_names(self):
        """Return the (1-based) exon names."""
        exon_names = []
        features = list(zip(self.exon_starts, self.exon_ends))
        for n in range(len(features)):
            exon_names.append(
                self.__class__.name_sep.join(self.name_tuple + (f"exon{n+1}",))
            )
        if self.strand == "-":
            exon_names.reverse()
        assert len(self.exons) == len(exon_names)
        return exon_names

    @cached_property
    def intron_names(self):
        """Return the (1-based) intron names."""
        intron_names = []
        features = list(zip(self.exon_starts, self.exon_ends))
        for n in range(len(features) - 1):
            intron_names.append(
                self.__class__.name_sep.join(self.name_tuple + (f"intron{n+1}",))
            )
        if self.strand == "-":
            intron_names.reverse()
        assert len(self.introns) == len(intron_names)
        return intron_names

    @cached_property
    def exons(self):
        """List of exonic chromosome, start positions and end positions in the transcript."""
        features = zip(self.exon_starts, self.exon_ends)
        return list(
            Region.construct(
                chromosome=self.chromosome,
                start=start,
                end=end,
                ref_genome=self.ref_genome,
                # name=self.exon_names[n],
                strand=self.strand,
            )
            for n, (start, end) in enumerate(features)
        )

    @cached_property
    def introns(self):
        """List of intronic chromosome, start positions and end positions in the transcript."""
        features = zip(self.exon_ends[:-1], self.exon_starts[1:])
        return list(
            Region.construct(
                chromosome=self.chromosome,
                start=end,
                end=start,
                ref_genome=self.ref_genome,
                # name=self.intron_names[n],
                strand=self.strand,
            )
            for n, (end, start) in enumerate(features)
        )

    @cached_property
    def utr3(self):
        """Return 3' untranslated regions"""
        if self.coding_start or self.coding_end:
            utr = Region.construct(
                chromosome=self.chromosome,
                start=self.coding_end,
                end=self.read_end,
                ref_genome=self.ref_genome,
                strand=self.strand,
            )
            if self.strand == "-":
                utr = Region.construct(
                    chromosome=self.chromosome,
                    start=self.read_start,
                    end=self.coding_start,
                    ref_genome=self.ref_genome,
                    strand=self.strand,
                )
            utr3_regions = self._exons_in_regions([utr])
            assert len(utr3_regions) == 1, "multiple 3'UTRs in '{}':".format(repr(self))
            return utr3_regions[0]
        return None

    @cached_property
    def utr5(self):
        """Return 5' untranslated regions"""
        if self.coding_start and self.coding_end:
            utr = Region.construct(
                chromosome=self.chromosome,
                start=self.read_start,
                end=self.coding_start,
                ref_genome=self.ref_genome,
                strand=self.strand,
            )
            if self.strand == "-":
                utr = Region.construct(
                    chromosome=self.chromosome,
                    start=self.coding_end,
                    end=self.read_end,
                    ref_genome=self.ref_genome,
                    strand=self.strand,
                )
            utr5_regions = self._exons_in_regions([utr])
            assert len(utr5_regions) == 1, "multiple 5'UTRs in '{}':".format(repr(self))
            return utr5_regions[0]
        return None

    @cached_property
    def name_tuple(self):
        tup = (str(self.hgnc_id), str(self.name))
        # HACK: trying accessing the `gene` Relationship of a `Transcript` instance that is not
        #       associated to any database results in `UnmappedInstanceError`.
        try:
            if self.gene:
                tup = (self.gene.symbol,) + tup
        except UnmappedInstanceError as e:
            print(f"Warning: transcript {str(self)} isn't mapped to any gene: {str(e)}")
        return tup

    @property
    def length_coding_frame(self):
        if not self.is_coding:
            return 0

        return sum(
            max(min(end, self.coding_end) - max(start, self.coding_start), 0)  # type: ignore
            for (start, end) in zip(self.exon_starts, self.exon_ends)
        )

    @property
    def length_transcription_frame(self):
        return sum([abs(ee - es) for es, ee in zip(self.exon_starts, self.exon_ends)])

    @property
    def length_whole_coding_frame(self):
        if not self.is_coding:
            return 0
        return abs(self.coding_end - self.coding_start)  # type: ignore

    @property
    def length_whole_transcription_frame(self):
        return abs(self.read_end - self.read_start)

    @property
    def read_start(self):
        return self.start

    @property
    def read_end(self):
        return self.end

    def slopped(self, upstream, downstream):
        """Return a copy of self with slopped coding features."""
        eff_upstream = upstream
        eff_downstream = downstream
        if self.strand == "-":
            eff_upstream, eff_downstream = downstream, upstream
        slopped_exons = list(e.slop(eff_upstream, eff_downstream) for e in self.exons)
        slopped_coding_start = None
        slopped_coding_end = None
        if self.coding_start:
            slopped_coding_start = self.coding_start - eff_upstream
        if self.coding_end:
            slopped_coding_end = self.coding_end + eff_downstream
        return Transcript.construct(
            ref_genome=self.ref_genome,
            chromosome=self.chromosome,
            start=self.read_start,
            end=self.read_end,
            name=self.name,
            source=self.source,
            strand=self.strand,
            hgnc_id=self.hgnc_id,
            coding_start=slopped_coding_start,
            coding_end=slopped_coding_end,
            exon_starts=list(e.start for e in slopped_exons),
            exon_ends=list(e.end for e in slopped_exons),
        )

    @classmethod
    def _find_longest_frames(cls, transcripts):
        """
        :param transcripts:  list of Transcripts
        :return:             Transcripts with longest frame
        """

        if not transcripts or len(transcripts) < 1:
            return list()

        def _cmptuple(tx: Transcript):
            return (
                tx.is_coding,
                tx.length_coding_frame,
                tx.length_transcription_frame,
                tx.length_whole_coding_frame,
                tx.length_whole_transcription_frame,
            )

        comparators = [_cmptuple(tx) for tx in transcripts]
        maxtuple = max(comparators)
        return [tx for tx, cmp in zip(transcripts, comparators) if cmp == maxtuple]

    @staticmethod
    def get_longest_transcripts_by_hgnc_ids(
        session: Session, hgnc_ids: list[int]
    ) -> dict[int, list["Transcript"]]:
        """Return default transcripts for the given HGNC IDs."""
        # Get all available transcripts
        available_transcripts = Transcript.get_by_hgnc_ids(session, hgnc_ids)
        return {
            hgnc_id: Transcript._find_longest_frames(
                available_transcripts.get(hgnc_id, [])
            )
            for hgnc_id in available_transcripts
        }

    @classmethod
    def get_by_hgnc_ids(
        cls, session: Session, hgnc_ids: list[int]
    ) -> dict[int, list["Transcript"]]:
        """Return all transcripts for the given HGNC IDs."""
        all_transcripts = session.exec(
            select(Transcript).where(col(Transcript.hgnc_id).in_(hgnc_ids))
        ).all()

        transcripts_by_hgnc_id: dict[int, list["Transcript"]] = {
            hgnc_id: [] for hgnc_id in hgnc_ids
        }

        for tx in all_transcripts:
            transcripts_by_hgnc_id[tx.hgnc_id].append(tx)
        return transcripts_by_hgnc_id

    def _exons_in_regions(self, regions, return_all=False):
        """List of exonic chromosome, start and end positions in a list of regions."""
        return self._items_in_regions(self.exons, regions, return_all)

    def _introns_in_regions(self, regions, return_all=False):
        """List of intronic chromosome, start and end positions in a list of regions."""
        return self._items_in_regions(self.introns, regions, return_all)

    def _items_in_regions(self, features, regions, return_all=False):
        """List of features chromosome, start and end positions intersecting regions."""
        rlist = list()
        for r in features:
            rlist.extend(
                (rx.chromosome, rx.start, rx.end)
                for rx in r.intersect(regions, return_all)
            )
        return rlist


class ManeSelectTranscript(SQLModelBase, table=True):
    id: Optional[int] = Field(sa_column=Column(Integer, Identity(), primary_key=True))
    transcript_id: int = Field(foreign_key="transcript.id", unique=True)
    transcript: Transcript = Relationship(sa_relationship_kwargs={"viewonly": True})
    status: Literal["MANE Select"] | Literal["MANE Plus Clinical"] = Field(
        sa_column=Column(Enum("MANE Select", "MANE Plus Clinical", name="mane_status"))
    )
    update_id: int = Field(default=None, foreign_key="update.id")

    @classmethod
    def get_transcripts_by_hgnc_ids(
        cls,
        session: Session,
        hgnc_ids: list[int],
        include_mane_select: bool = True,
        include_mane_plus_clinical: bool = True,
    ) -> dict[int, list[Transcript]]:
        """Return all transcripts with MANE Select or MANE Plus Clinical entries for the given HGNC IDs."""

        filter_clause = []
        if include_mane_select:
            filter_clause.append(col(ManeSelectTranscript.status) == "MANE Select")

        if include_mane_plus_clinical:
            filter_clause.append(
                col(ManeSelectTranscript.status) == "MANE Plus Clinical"
            )

        assert filter_clause, "At least one of include_mane_select or include_mane_plus_clinical must be True"

        all_transcripts = session.exec(
            select(Transcript)
            .join(
                ManeSelectTranscript,
                Transcript.id == ManeSelectTranscript.transcript_id,
            )
            .where(
                col(Transcript.hgnc_id).in_(hgnc_ids),
                or_(*filter_clause),
            )
        ).all()

        transcripts_by_hgnc_id: dict[int, list[Transcript]] = {
            tx.hgnc_id: [] for tx in all_transcripts
        }
        for transcript in all_transcripts:
            transcripts_by_hgnc_id[transcript.hgnc_id].append(transcript)

        return transcripts_by_hgnc_id
