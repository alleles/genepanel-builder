import React, {
  ReactNode,
  Context,
  useState,
  createContext,
  useContext,
} from 'react'
import API from 'api'
import { useRouter } from 'next/router'

type User = {
  user: string | null
  name: string | null
  email: string | null
}

interface SessionContext {
  userinfo: User
  logout: () => void
  validateSession: () => Promise<boolean>
  getUserInfo: () => void
}

const isDev = process.env.NODE_ENV === 'development'
const defaultUserInfo = isDev
  ? { user: 'test', name: 'Test User', email: 'test@test.com' }
  : { user: null, name: null, email: null }

function useSessionContext(): SessionContext {
  const router = useRouter()

  const [session, setSession] = useState({
    userinfo: defaultUserInfo,
  })

  const validateSession = async () => {
    if (isDev) return Promise.resolve(true)

    const response = await API.AuthVerify.get()
      .then(() => {
        return true
      })
      .catch(() => {
        router.push('/api/auth/login')
        return false
      })
    return response
  }

  const getUserInfo = () => {
    if (session.userinfo.user === null) {
      API.AuthInfo.get().then((response) =>
        setSession({ ...session, userinfo: response.data })
      )
    }
  }

  const logout = async () => {
    if (isDev) {
      router.push('/')
    } else {
      await API.AuthLogout.get()
      router.push('/api/auth/login')
    }
  }
  return {
    userinfo: session.userinfo,
    logout,
    validateSession,
    getUserInfo,
  }
}
let sessionContext: Context<SessionContext>

export function SessionProvider({ children }: { children: ReactNode }) {
  sessionContext = createContext<SessionContext>(useSessionContext())
  return (
    <sessionContext.Provider value={useSessionContext()}>
      {children}
    </sessionContext.Provider>
  )
}

export function useSession() {
  return useContext(sessionContext)
}
