import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios'; 

declare namespace Components {
    namespace Schemas {
        /**
         * ConfidenceLevel
         * An enumeration.
         */
        export type ConfidenceLevel = 1 | 2 | 3;
        /**
         * Gene
         * SQLModel with enforced validation
         */
        export interface Gene {
            /**
             * Hgnc Id
             */
            hgnc_id: number;
            /**
             * Symbol
             */
            symbol: string;
            /**
             * Aliases
             */
            aliases?: string[];
            /**
             * Previous Symbols
             */
            previous_symbols?: string[];
            /**
             * Withdrawn
             */
            withdrawn?: boolean;
            /**
             * Chromosome
             */
            chromosome?: string;
            /**
             * New Hgnc Id
             */
            new_hgnc_id?: number;
            /**
             * Update Id
             */
            update_id?: number;
        }
        /**
         * GenePanel
         * SQLModel with enforced validation
         */
        export interface GenePanel {
            /**
             * Id
             */
            id?: number;
            /**
             * Name
             */
            name: string;
            /**
             * Shortname
             */
            shortname: string;
            /**
             * Description
             */
            description?: string;
            /**
             * Version
             */
            version: string;
            /**
             * Created
             */
            created?: string; // date-time
            /**
             * Updated
             */
            updated?: string; // date-time
            /**
             * Inactive
             */
            inactive?: boolean;
            /**
             * Masterpanel Id
             */
            masterpanel_id?: number;
        }
        /**
         * GenePanelConfig
         */
        export interface GenePanelConfig {
            /**
             * Hgnc Ids
             */
            hgnc_ids: number[];
            /**
             * Panelapp Panels
             */
            panelapp_panels: /* PanelAppInput */ PanelAppInput[];
            /**
             * Regions
             */
            regions: /**
             * GenePanelConfigRegion
             * Feature coordinates are 0-based half-open: [start, end)
             */
            GenePanelConfigRegion[];
            /**
             * Id
             */
            id?: number;
            /**
             * Name
             */
            name: string;
            /**
             * Shortname
             */
            shortname: string;
            /**
             * Description
             */
            description?: string;
            /**
             * Version
             */
            version?: string;
            /**
             * Diff
             */
            diff: {
                [name: string]: /* GenePanelConfigGene */ GenePanelConfigGene;
            };
        }
        /**
         * GenePanelConfigGene
         */
        export interface GenePanelConfigGene {
            /**
             * Inheritance
             */
            inheritance?: string;
            /**
             * Transcripts
             */
            transcripts?: string[];
            /**
             * Exclusion Reasons
             */
            exclusion_reasons?: string[];
            /**
             * Comment
             */
            comment?: string;
        }
        /**
         * GenePanelConfigRegion
         * Feature coordinates are 0-based half-open: [start, end)
         */
        export interface GenePanelConfigRegion {
            /**
             * Ref Genome
             */
            ref_genome: /* Ref Genome */ ("GRCh37") | ("GRCh38");
            /**
             * Chromosome
             */
            chromosome: /* Chromosome */ ("1") | ("2") | ("3") | ("4") | ("5") | ("6") | ("7") | ("8") | ("9") | ("10") | ("11") | ("12") | ("13") | ("14") | ("15") | ("16") | ("17") | ("18") | ("19") | ("20") | ("21") | ("22") | ("X") | ("Y") | ("XY") | ("MT");
            /**
             * Start
             */
            start: number;
            /**
             * End
             */
            end: number;
            /**
             * Id
             */
            id?: number;
            /**
             * Name
             */
            name?: string;
            /**
             * Strand
             */
            strand?: /* Strand */ ("+") | ("-");
            /**
             * Comment
             */
            comment?: string;
            /**
             * Inheritance
             */
            inheritance?: string;
        }
        /**
         * GenePanelItem
         * SQLModel with enforced validation
         */
        export interface GenePanelItem {
            /**
             * Id
             */
            id?: number;
            /**
             * Name
             */
            name: string;
            /**
             * Shortname
             */
            shortname: string;
            /**
             * Description
             */
            description?: string;
            /**
             * Version
             */
            version: string;
            /**
             * Created
             */
            created?: string; // date-time
            /**
             * Updated
             */
            updated?: string; // date-time
            /**
             * Inactive
             */
            inactive?: boolean;
            /**
             * Masterpanel Id
             */
            masterpanel_id?: number;
            /**
             * Gene Count
             */
            gene_count: number;
        }
        /**
         * GenePanelSelectRequestModel
         */
        export interface GenePanelSelectRequestModel {
            /**
             * Hgnc Ids
             */
            hgnc_ids: number[];
            /**
             * Panelapp Panels
             */
            panelapp_panels: /* PanelAppInput */ PanelAppInput[];
            /**
             * Regions
             */
            regions: /**
             * Region
             * Feature coordinates are 0-based half-open: [start, end)
             */
            Region[];
        }
        /**
         * GenePanelSelectResponse
         */
        export interface GenePanelSelectResponse {
            /**
             * Genes
             */
            genes: /* GeneResponseItem */ GeneResponseItem[];
            /**
             * Regions
             */
            regions: /* RegionResponseItem */ RegionResponseItem[];
        }
        /**
         * GeneResponseItem
         */
        export interface GeneResponseItem {
            /**
             * Hgnc Id
             */
            hgnc_id: number;
            /**
             * Symbol
             */
            symbol: string;
            /**
             * Default Transcripts
             */
            default_transcripts?: string[];
            /**
             * Inheritance
             */
            inheritance?: string;
            /**
             * Available Transcripts
             */
            available_transcripts?: string[];
            /**
             * Transcript Origin
             */
            transcript_origin?: {
                [name: string]: string[];
            };
            /**
             * Coverage
             */
            coverage?: {
                [name: string]: number;
            };
            /**
             * Segdup
             */
            segdup?: number;
            /**
             * Origin
             */
            origin?: /* OriginItem */ OriginItem[];
        }
        /**
         * HTTPValidationError
         */
        export interface HTTPValidationError {
            /**
             * Detail
             */
            detail?: /* ValidationError */ ValidationError[];
        }
        /**
         * OriginItem
         */
        export interface OriginItem {
            /**
             * Panelapp Id
             */
            panelapp_id?: number;
            conf_level?: /**
             * ConfidenceLevel
             * An enumeration.
             */
            ConfidenceLevel;
        }
        /**
         * PanelAppInput
         */
        export interface PanelAppInput {
            /**
             * Panelapp Id
             */
            panelapp_id: number;
            /**
             * Confidence Levels
             */
            confidence_levels: {
                [name: string]: boolean;
            };
        }
        /**
         * PanelAppPanel
         * SQLModel with enforced validation
         */
        export interface PanelAppPanel {
            /**
             * Id
             */
            id: number;
            /**
             * Name
             */
            name: string;
            /**
             * Version
             */
            version: string;
            /**
             * Version Created
             */
            version_created: string; // date-time
            /**
             * Disease Group
             */
            disease_group: string;
            /**
             * Disease Sub Group
             */
            disease_sub_group: string;
            /**
             * Relevant Disorders
             */
            relevant_disorders?: string[];
            /**
             * Update Id
             */
            update_id?: number;
        }
        /**
         * Region
         * Feature coordinates are 0-based half-open: [start, end)
         */
        export interface Region {
            /**
             * Ref Genome
             */
            ref_genome: /* Ref Genome */ ("GRCh37") | ("GRCh38");
            /**
             * Chromosome
             */
            chromosome: /* Chromosome */ ("1") | ("2") | ("3") | ("4") | ("5") | ("6") | ("7") | ("8") | ("9") | ("10") | ("11") | ("12") | ("13") | ("14") | ("15") | ("16") | ("17") | ("18") | ("19") | ("20") | ("21") | ("22") | ("X") | ("Y") | ("XY") | ("MT");
            /**
             * Start
             */
            start: number;
            /**
             * End
             */
            end: number;
            /**
             * Id
             */
            id?: number;
            /**
             * Name
             */
            name?: string;
            /**
             * Strand
             */
            strand?: /* Strand */ ("+") | ("-");
        }
        /**
         * RegionResponseItem
         */
        export interface RegionResponseItem {
            region: /**
             * Region
             * Feature coordinates are 0-based half-open: [start, end)
             */
            Region;
            /**
             * Coverage
             */
            coverage?: {
                [name: string]: number;
            };
            /**
             * Segdup
             */
            segdup?: number;
        }
        /**
         * ValidationError
         */
        export interface ValidationError {
            /**
             * Location
             */
            loc: (string | number)[];
            /**
             * Message
             */
            msg: string;
            /**
             * Error Type
             */
            type: string;
        }
    }
}
declare namespace Paths {
    namespace AuthCallback {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace AuthInfo {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace AuthLogin {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace AuthLogout {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace AuthVerify {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace GeneNamesGetHgncId {
        namespace Parameters {
            /**
             * Hgnc Id
             */
            export type HgncId = number;
        }
        export interface PathParameters {
            hgnc_id: /* Hgnc Id */ Parameters.HgncId;
        }
        namespace Responses {
            export type $200 = /**
             * Gene
             * SQLModel with enforced validation
             */
            Components.Schemas.Gene;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GeneNamesGetHgncIds {
        namespace Parameters {
            /**
             * Ensure All
             */
            export type EnsureAll = boolean;
        }
        export interface QueryParameters {
            ensure_all?: /* Ensure All */ Parameters.EnsureAll;
        }
        /**
         * Hgnc Ids
         */
        export type RequestBody = number[];
        namespace Responses {
            /**
             * Response Get Hgnc Ids Api V1 Genenames Hgnc Ids Post
             */
            export type $200 = /**
             * Gene
             * SQLModel with enforced validation
             */
            Components.Schemas.Gene[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GeneNamesSearchGenes {
        namespace Parameters {
            /**
             * Pattern
             */
            export type Pattern = string;
        }
        export interface PathParameters {
            pattern: /* Pattern */ Parameters.Pattern;
        }
        namespace Responses {
            /**
             * Response Search Genes Api V1 Genenames Search  Pattern  Get
             */
            export type $200 = /**
             * Gene
             * SQLModel with enforced validation
             */
            Components.Schemas.Gene[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsConfigure {
        export type RequestBody = /* GenePanelConfig */ Components.Schemas.GenePanelConfig;
        namespace Responses {
            /**
             * Response Configure Api V1 Genepanels Configure Put
             */
            export interface $201 {
                [name: string]: number;
            }
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsDeleteGenepanel {
        namespace Parameters {
            /**
             * Id
             */
            export type Id = number;
        }
        export interface PathParameters {
            id: /* Id */ Parameters.Id;
        }
        namespace Responses {
            export type $200 = any;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsExportGenepanel {
        namespace Parameters {
            /**
             * Id
             */
            export type Id = number;
        }
        export interface PathParameters {
            id: /* Id */ Parameters.Id;
        }
        namespace Responses {
            export type $200 = any;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsGetGenepanel {
        namespace Parameters {
            /**
             * Id
             */
            export type Id = number;
        }
        export interface PathParameters {
            id: /* Id */ Parameters.Id;
        }
        namespace Responses {
            export type $200 = /* GenePanelConfig */ Components.Schemas.GenePanelConfig;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsGetGenepanels {
        namespace Responses {
            /**
             * Response Get Genepanels Api V1 Genepanels  Get
             */
            export type $200 = /**
             * GenePanelItem
             * SQLModel with enforced validation
             */
            Components.Schemas.GenePanelItem[];
        }
    }
    namespace GenePanelsGetGenepanelsByGene {
        namespace Parameters {
            /**
             * Hgnc Id
             */
            export type HgncId = number;
        }
        export interface PathParameters {
            hgnc_id: /* Hgnc Id */ Parameters.HgncId;
        }
        namespace Responses {
            /**
             * Response Get Genepanels By Gene Api V1 Genepanels Genes  Hgnc Id  Get
             */
            export type $200 = /**
             * GenePanel
             * SQLModel with enforced validation
             */
            Components.Schemas.GenePanel[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsSearchPanels {
        namespace Parameters {
            /**
             * Pattern
             */
            export type Pattern = string;
        }
        export interface PathParameters {
            pattern: /* Pattern */ Parameters.Pattern;
        }
        namespace Responses {
            /**
             * Response Search Panels Api V1 Genepanels Search  Pattern  Get
             */
            export type $200 = /**
             * GenePanel
             * SQLModel with enforced validation
             */
            Components.Schemas.GenePanel[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsSelectPanel {
        export type RequestBody = /* GenePanelSelectRequestModel */ Components.Schemas.GenePanelSelectRequestModel;
        namespace Responses {
            export type $200 = /* GenePanelSelectResponse */ Components.Schemas.GenePanelSelectResponse;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenePanelsSubmit {
        namespace Parameters {
            /**
             * Id
             */
            export type Id = number;
            /**
             * Version
             */
            export type Version = string;
        }
        export interface PathParameters {
            id: /* Id */ Parameters.Id;
        }
        export interface QueryParameters {
            version: /* Version */ Parameters.Version;
        }
        namespace Responses {
            /**
             * Response Submit Api V1 Genepanels  Id  Submit Post
             */
            export interface $200 {
                [name: string]: number;
            }
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace GenomicStatsInitCoverage {
        namespace Responses {
            export type $200 = any;
        }
    }
    namespace GenomicStatsTranscriptCoverage {
        namespace Parameters {
            /**
             * D4 Target
             */
            export type D4Target = string;
        }
        export interface QueryParameters {
            d4_target: /* D4 Target */ Parameters.D4Target;
        }
        /**
         * Tx Names
         */
        export type RequestBody = string[];
        namespace Responses {
            /**
             * Response Transcript Coverage Api V1 Coverage Transcript Post
             */
            export interface $200 {
                [name: string]: number;
            }
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace PanelAppGetPanel {
        namespace Parameters {
            /**
             * Id
             */
            export type Id = number;
            /**
             * Site
             */
            export type Site = "ENG";
        }
        export interface PathParameters {
            site: /* Site */ Parameters.Site;
            id: /* Id */ Parameters.Id;
        }
        namespace Responses {
            export type $200 = /**
             * PanelAppPanel
             * SQLModel with enforced validation
             */
            Components.Schemas.PanelAppPanel;
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace PanelAppGetPanels {
        namespace Parameters {
            /**
             * Site
             */
            export type Site = "ENG";
        }
        export interface PathParameters {
            site: /* Site */ Parameters.Site;
        }
        /**
         * Ids
         */
        export type RequestBody = number[];
        namespace Responses {
            /**
             * Response Get Panels Api V1 Panelapp  Site  Post
             */
            export type $200 = /**
             * PanelAppPanel
             * SQLModel with enforced validation
             */
            Components.Schemas.PanelAppPanel[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace PanelAppSearchPanels {
        namespace Parameters {
            /**
             * Pattern
             */
            export type Pattern = string;
            /**
             * Site
             */
            export type Site = "ENG";
        }
        export interface PathParameters {
            site: /* Site */ Parameters.Site;
            pattern: /* Pattern */ Parameters.Pattern;
        }
        namespace Responses {
            /**
             * Response Search Panels Api V1 Panelapp  Site  Search  Pattern  Get
             */
            export type $200 = /**
             * PanelAppPanel
             * SQLModel with enforced validation
             */
            Components.Schemas.PanelAppPanel[];
            export type $422 = /* HTTPValidationError */ Components.Schemas.HTTPValidationError;
        }
    }
    namespace SettingsGetVersion {
        namespace Responses {
            export type $200 = any;
        }
    }
}

export interface OperationMethods {
  /**
   * Gene_names_get_hgnc_id - Get Hgnc Id
   */
  'Gene_names_get_hgnc_id'(
    parameters?: Parameters<Paths.GeneNamesGetHgncId.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GeneNamesGetHgncId.Responses.$200>
  /**
   * Gene_names_get_hgnc_ids - Get Hgnc Ids
   */
  'Gene_names_get_hgnc_ids'(
    parameters?: Parameters<Paths.GeneNamesGetHgncIds.QueryParameters> | null,
    data?: Paths.GeneNamesGetHgncIds.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GeneNamesGetHgncIds.Responses.$200>
  /**
   * Gene_names_search_genes - Search Genes
   */
  'Gene_names_search_genes'(
    parameters?: Parameters<Paths.GeneNamesSearchGenes.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GeneNamesSearchGenes.Responses.$200>
  /**
   * Gene_panels_get_genepanels - Get Genepanels
   */
  'Gene_panels_get_genepanels'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsGetGenepanels.Responses.$200>
  /**
   * Gene_panels_get_genepanels_by_gene - Get Genepanels By Gene
   */
  'Gene_panels_get_genepanels_by_gene'(
    parameters?: Parameters<Paths.GenePanelsGetGenepanelsByGene.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsGetGenepanelsByGene.Responses.$200>
  /**
   * Gene_panels_search_panels - Search Panels
   */
  'Gene_panels_search_panels'(
    parameters?: Parameters<Paths.GenePanelsSearchPanels.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsSearchPanels.Responses.$200>
  /**
   * Gene_panels_select_panel - Select Panel
   */
  'Gene_panels_select_panel'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.GenePanelsSelectPanel.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsSelectPanel.Responses.$200>
  /**
   * Gene_panels_configure - Configure
   * 
   * Create or update a gene panel
   */
  'Gene_panels_configure'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.GenePanelsConfigure.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsConfigure.Responses.$201>
  /**
   * Gene_panels_submit - Submit
   */
  'Gene_panels_submit'(
    parameters?: Parameters<Paths.GenePanelsSubmit.PathParameters & Paths.GenePanelsSubmit.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsSubmit.Responses.$200>
  /**
   * Gene_panels_get_genepanel - Get Genepanel
   */
  'Gene_panels_get_genepanel'(
    parameters?: Parameters<Paths.GenePanelsGetGenepanel.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsGetGenepanel.Responses.$200>
  /**
   * Gene_panels_delete_genepanel - Delete Genepanel
   */
  'Gene_panels_delete_genepanel'(
    parameters?: Parameters<Paths.GenePanelsDeleteGenepanel.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsDeleteGenepanel.Responses.$200>
  /**
   * Gene_panels_export_genepanel - Export Genepanel
   */
  'Gene_panels_export_genepanel'(
    parameters?: Parameters<Paths.GenePanelsExportGenepanel.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenePanelsExportGenepanel.Responses.$200>
  /**
   * PanelApp_get_panel - Get Panel
   */
  'PanelApp_get_panel'(
    parameters?: Parameters<Paths.PanelAppGetPanel.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PanelAppGetPanel.Responses.$200>
  /**
   * PanelApp_get_panels - Get Panels
   */
  'PanelApp_get_panels'(
    parameters?: Parameters<Paths.PanelAppGetPanels.PathParameters> | null,
    data?: Paths.PanelAppGetPanels.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PanelAppGetPanels.Responses.$200>
  /**
   * PanelApp_search_panels - Search Panels
   */
  'PanelApp_search_panels'(
    parameters?: Parameters<Paths.PanelAppSearchPanels.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PanelAppSearchPanels.Responses.$200>
  /**
   * Genomic_stats_transcript_coverage - Transcript Coverage
   */
  'Genomic_stats_transcript_coverage'(
    parameters?: Parameters<Paths.GenomicStatsTranscriptCoverage.QueryParameters> | null,
    data?: Paths.GenomicStatsTranscriptCoverage.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenomicStatsTranscriptCoverage.Responses.$200>
  /**
   * Genomic_stats_init_coverage - Init Coverage
   */
  'Genomic_stats_init_coverage'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GenomicStatsInitCoverage.Responses.$200>
  /**
   * Settings_get_version - Get Version
   */
  'Settings_get_version'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SettingsGetVersion.Responses.$200>
  /**
   * Auth_login - Login
   */
  'Auth_login'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AuthLogin.Responses.$200>
  /**
   * Auth_callback - Callback
   */
  'Auth_callback'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AuthCallback.Responses.$200>
  /**
   * Auth_verify - Verify
   */
  'Auth_verify'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AuthVerify.Responses.$200>
  /**
   * Auth_info - Info
   */
  'Auth_info'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AuthInfo.Responses.$200>
  /**
   * Auth_logout - Logout
   */
  'Auth_logout'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AuthLogout.Responses.$200>
}

export interface PathsDictionary {
  ['/api/v1/genenames/hgnc/{hgnc_id}']: {
    /**
     * Gene_names_get_hgnc_id - Get Hgnc Id
     */
    'get'(
      parameters?: Parameters<Paths.GeneNamesGetHgncId.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GeneNamesGetHgncId.Responses.$200>
  }
  ['/api/v1/genenames/hgnc_ids']: {
    /**
     * Gene_names_get_hgnc_ids - Get Hgnc Ids
     */
    'post'(
      parameters?: Parameters<Paths.GeneNamesGetHgncIds.QueryParameters> | null,
      data?: Paths.GeneNamesGetHgncIds.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GeneNamesGetHgncIds.Responses.$200>
  }
  ['/api/v1/genenames/search/{pattern}']: {
    /**
     * Gene_names_search_genes - Search Genes
     */
    'get'(
      parameters?: Parameters<Paths.GeneNamesSearchGenes.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GeneNamesSearchGenes.Responses.$200>
  }
  ['/api/v1/genepanels/']: {
    /**
     * Gene_panels_get_genepanels - Get Genepanels
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsGetGenepanels.Responses.$200>
  }
  ['/api/v1/genepanels/genes/{hgnc_id}']: {
    /**
     * Gene_panels_get_genepanels_by_gene - Get Genepanels By Gene
     */
    'get'(
      parameters?: Parameters<Paths.GenePanelsGetGenepanelsByGene.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsGetGenepanelsByGene.Responses.$200>
  }
  ['/api/v1/genepanels/search/{pattern}']: {
    /**
     * Gene_panels_search_panels - Search Panels
     */
    'get'(
      parameters?: Parameters<Paths.GenePanelsSearchPanels.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsSearchPanels.Responses.$200>
  }
  ['/api/v1/genepanels/select']: {
    /**
     * Gene_panels_select_panel - Select Panel
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.GenePanelsSelectPanel.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsSelectPanel.Responses.$200>
  }
  ['/api/v1/genepanels/configure']: {
    /**
     * Gene_panels_configure - Configure
     * 
     * Create or update a gene panel
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.GenePanelsConfigure.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsConfigure.Responses.$201>
  }
  ['/api/v1/genepanels/{id}/submit']: {
    /**
     * Gene_panels_submit - Submit
     */
    'post'(
      parameters?: Parameters<Paths.GenePanelsSubmit.PathParameters & Paths.GenePanelsSubmit.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsSubmit.Responses.$200>
  }
  ['/api/v1/genepanel/{id}']: {
    /**
     * Gene_panels_get_genepanel - Get Genepanel
     */
    'get'(
      parameters?: Parameters<Paths.GenePanelsGetGenepanel.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsGetGenepanel.Responses.$200>
    /**
     * Gene_panels_delete_genepanel - Delete Genepanel
     */
    'delete'(
      parameters?: Parameters<Paths.GenePanelsDeleteGenepanel.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsDeleteGenepanel.Responses.$200>
  }
  ['/api/v1/genepanel/{id}/export']: {
    /**
     * Gene_panels_export_genepanel - Export Genepanel
     */
    'get'(
      parameters?: Parameters<Paths.GenePanelsExportGenepanel.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenePanelsExportGenepanel.Responses.$200>
  }
  ['/api/v1/panelapp/{site}/{id}']: {
    /**
     * PanelApp_get_panel - Get Panel
     */
    'get'(
      parameters?: Parameters<Paths.PanelAppGetPanel.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PanelAppGetPanel.Responses.$200>
  }
  ['/api/v1/panelapp/{site}']: {
    /**
     * PanelApp_get_panels - Get Panels
     */
    'post'(
      parameters?: Parameters<Paths.PanelAppGetPanels.PathParameters> | null,
      data?: Paths.PanelAppGetPanels.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PanelAppGetPanels.Responses.$200>
  }
  ['/api/v1/panelapp/{site}/search/{pattern}']: {
    /**
     * PanelApp_search_panels - Search Panels
     */
    'get'(
      parameters?: Parameters<Paths.PanelAppSearchPanels.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PanelAppSearchPanels.Responses.$200>
  }
  ['/api/v1/coverage/transcript']: {
    /**
     * Genomic_stats_transcript_coverage - Transcript Coverage
     */
    'post'(
      parameters?: Parameters<Paths.GenomicStatsTranscriptCoverage.QueryParameters> | null,
      data?: Paths.GenomicStatsTranscriptCoverage.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenomicStatsTranscriptCoverage.Responses.$200>
  }
  ['/api/v1/coverage/init']: {
    /**
     * Genomic_stats_init_coverage - Init Coverage
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GenomicStatsInitCoverage.Responses.$200>
  }
  ['/api/v1/version']: {
    /**
     * Settings_get_version - Get Version
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SettingsGetVersion.Responses.$200>
  }
  ['/api/auth/login']: {
    /**
     * Auth_login - Login
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AuthLogin.Responses.$200>
  }
  ['/api/auth/callback']: {
    /**
     * Auth_callback - Callback
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AuthCallback.Responses.$200>
  }
  ['/api/auth/verify']: {
    /**
     * Auth_verify - Verify
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AuthVerify.Responses.$200>
  }
  ['/api/auth/userinfo']: {
    /**
     * Auth_info - Info
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AuthInfo.Responses.$200>
  }
  ['/api/auth/logout']: {
    /**
     * Auth_logout - Logout
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AuthLogout.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
