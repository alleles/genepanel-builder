import json
import logging
import os
from datetime import date
from io import TextIOWrapper
from pathlib import Path
from typing import List

import typer
from sqlalchemy.orm import selectinload
from sqlmodel import Session, Text, cast, col, func, select

from gpbuilder.cli.shared import init_log, safe_str
from gpbuilder.datamodel.crud.genepanel import create_panel
from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.genepanel import GenePanel, GenePanelGene
from gpbuilder.datamodel.phenotype import PhenotypeGene
from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.datamodel.utils.records import RegionRecord, TranscriptRecord

files_legend_basename = "files_legend.txt"
logger = logging.getLogger(__name__)


app = typer.Typer()


def export_panel(panel: GenePanel, session: Session, outputdir: str):
    targetdir = setup_export(panel, outputdir)
    region_records, transcript_records = panel.prepare_for_export()
    write_panel(session, panel, targetdir, region_records, transcript_records)
    logger.info(f"Exported {panel} to {targetdir}")
    return targetdir


def setup_export(panel: GenePanel, outputdir: str):
    targetdir = os.path.abspath(
        os.path.join(outputdir, panel.shortname + "_v" + panel.version)
    )
    if not os.path.exists(targetdir):
        os.makedirs(targetdir)
    with open(os.path.join(targetdir, files_legend_basename), "w") as lh:
        lh.write(f"# Gene panel: {panel.name}-v{panel.version} -- File legend\n")
    with open(os.path.join(targetdir, "report_config.txt"), "w") as rh:
        rh.writelines(
            "%s\n" % s
            for s in (
                "[DEFAULT]",
                "# Used in attachment sent to doctor and internal web",
                "  title={name}".format(name=panel.name),
                "  version=v{version}".format(version=panel.version),
                "  coverage_threshold=100",
                "  coverage_description=",
                "",
                "[Web publishing - table]",
                "# The values (not the keys) are printed line by line before the gene table.",
                "  legend = [",
                "        ]",
            )
        )
    return targetdir


def summarize_build(outputdir: Path, rfh: TextIOWrapper):
    """
    Generate a summary of the export in the given folder
    """

    def counts(transcripts_file: Path):
        genes = set()
        transcripts = set()
        custom_regions = set()
        hgnc_id_field = 7
        gene_transcript_field = 3
        with transcripts_file.open("r") as f:
            for l in f.readlines():
                if l.startswith("#"):
                    continue
                ls = l.split("\t")
                if ls[gene_transcript_field].startswith("CR_"):
                    custom_regions.add(ls[hgnc_id_field])
                else:
                    genes.add(ls[hgnc_id_field])
                    transcripts.add(ls[gene_transcript_field])

        return len(genes), len(transcripts), len(custom_regions)

    transcripts_file = outputdir / (outputdir.name + "_genes_transcripts.tsv")
    regions_file = outputdir / (outputdir.name + "_regions.bed")
    assert transcripts_file.is_file(), f"{transcripts_file} does not exist"
    assert regions_file.is_file(), f"{regions_file} does not exist"
    num_genes, num_transcripts, num_custom_regions = counts(transcripts_file)
    with regions_file.open("r") as f:
        num_variant_calling_regions = len(
            [x for x in f.readlines() if not x.startswith("#")]
        )

    rfh.write(f"\nPanel {outputdir.name}\n")
    rfh.write("-" * 40 + "\n")
    rfh.write(f"Number of genes in panel:                {num_genes}\n")
    rfh.write(f"Number of regions in panel:              {num_custom_regions}\n")
    rfh.write(f"Number of transcripts in panel:          {num_transcripts}\n")
    rfh.write(
        f"Number of variant calling regions:       {num_variant_calling_regions}\n"
    )
    rfh.write("-" * 40 + "\n")


def verify_path(path: str) -> str:
    pathdir = os.path.dirname(os.path.abspath(path))
    assert os.path.isdir(pathdir), f"Directory '{pathdir}' does not exist."

    return path


def write_panel(
    session: Session,
    panel: GenePanel,
    targetdir: str,
    region_records: set[RegionRecord],
    transcript_records: set[TranscriptRecord],
):
    write_phenotypes(session, panel, targetdir)
    write_regions(panel, region_records, targetdir)
    write_transcripts(panel, transcript_records, targetdir)
    write_genes(transcript_records, targetdir)


def write_phenotypes(session: Session, panel: GenePanel, targetdir: str):
    """
    Writes phenotype information to a file, one line for each phenotype. Gene symbols can have
    multiple phenotypes. Gene symbols without any phenotypes are printed last, one line per symbol.
    :param panel:          gene panel object;
    :param targetdir:      output directory;
    :return:
    """

    log = logging.getLogger(__name__ + ".phenotypes_writer")

    real_file_name = f"{panel.shortname}_v{panel.version}_phenotypes.tsv"
    real_file_path = os.path.join(targetdir, real_file_name)
    temp_file_name = real_file_name[: -len(".tsv")] + "__draft.tsv"
    temp_file_path = os.path.join(targetdir, temp_file_name)

    with open(temp_file_path, "w") as fh:
        record_fields = (
            ("HGNC id", "gene_id"),
            ("HGNC symbol", "gene_symbol"),
            ("inheritance", "inheritance_mode"),
            ("phenotype MIM number", "phenotype_id"),
            ("phenotype", "phenotype_titles"),
            ("PMID", "pmid"),
            ("inheritance info", "info"),
            ("comment", "comment"),
        )
        record_fields_dict = dict(record_fields)
        record_names = tuple(r[0] for r in record_fields)
        fh.write(
            f"# Gene panel: {panel.name}-v{panel.version} -- Date: {date.today()}\n"
        )
        fh.write("\t".join(record_names) + "\n")

        text_entry = set()

        hgnc_ids = GenePanel.hgnc_ids_by_panel_id(session, [panel.id]).get(panel.id, [])  # type: ignore[arg-type]

        # Load all phenotypes at once for performance
        # Also selectinload the phenotype relationship to avoid N+1 queries
        # (accessed in PhenotypeGene.phenotype_titles)
        all_phenotypes = session.exec(
            select(PhenotypeGene)
            .options(selectinload(PhenotypeGene.phenotype))
            .where(PhenotypeGene.gene_id.in_(hgnc_ids))
            .order_by(PhenotypeGene.gene_id)
        ).all()
        for phenotype in all_phenotypes:
            items = list(
                getattr(phenotype, attr_name, "")
                for attr_name in record_fields_dict.values()
            )
            assert len(record_names) == len(items), "record/header mismatch."
            line = "\t".join(map(safe_str, items)) + "\n"
            log.debug(f"Adding line '{line}' to '{temp_file_path}'")
            text_entry.add(line)

        fh.writelines(sorted(text_entry))

        # rename file
        os.rename(temp_file_path, real_file_path)
        log.debug(f"Saved phenotypes file '{real_file_path}' to '{targetdir}'")

        # add file to legend
        file_description = "gene phenotypes/inheritance annotation"
        lh = open(os.path.join(targetdir, files_legend_basename), "a")
        lh.write(f"- {real_file_name}:  {file_description}\n")
        lh.close()


def write_regions(
    panel: GenePanel,
    records: set[RegionRecord],
    targetdir: str,
    all_transcripts: bool = False,
):
    """
    Write full and normalized transcripts files corresponding to the given records
    :param panel:      GenePanel instance
    :param records:    set of gene panel transcript records
    :param targetdir:  output directory
    """

    files_legend = []

    date_tag = str(date.today())

    log = logging.getLogger(__name__ + ".regions_writer")

    # select report fallback strategy (greedy, select)
    fallback_addendum = "longest frame fallback strategy"
    if all_transcripts:
        fallback_addendum = "all frames fallback strategy"

    real_file_name = f"{panel.shortname}_v{panel.version}_regions.bed"
    real_file_path = os.path.join(targetdir, real_file_name)
    temp_file_name = real_file_name[: -len(".bed")] + "__draft.bed"
    temp_file_path = os.path.join(targetdir, temp_file_name)

    with open(temp_file_path, "w") as fh:
        fh.write(f"# Gene panel: {panel.name}-v{panel.version} -- Date: {date_tag}\n")
        fh.write(
            "\t".join(
                field.alias for (field_name, field) in RegionRecord.__fields__.items()
            )
            + "\n"
        )

        for record in sorted(records):
            line = "\t".join(safe_str(entry) for entry in dict(record).values())
            log.debug(f"Adding line '{line}' to '{temp_file_path}'")
            fh.write(line + "\n")

    # rename file
    os.rename(temp_file_path, real_file_path)
    log.debug(f"Saved regions file '{real_file_path}'")
    # add file to legend
    files_legend.append(f"- {real_file_name}:  exonic regions [{fallback_addendum}]\n")

    # TODO?: add problematic regions
    problematic_regions = list()

    if problematic_regions:
        p_scope_description = (
            "eventual exonic regions overlapping segmental duplications"
        )

        real_file_name = f"{panel.shortname}_v{panel.version}_problematic_regions.bed"
        real_file_path = os.path.join(targetdir, real_file_name)
        temp_file_name = real_file_name[: -len(".bed")] + "__draft.bed"
        temp_file_path = os.path.join(targetdir, temp_file_name)

        with open(temp_file_path, "w") as fh:
            fh.write(
                f"# Gene panel: {panel.name}-v{panel.version} -- Date: {date_tag}\n"
            )
            fh.write(
                "\t".join(
                    tuple(field.alias for field in RegionRecord.__fields__.values())
                    + ("segDup", "fracMatch")
                )
                + "\n"
            )

            log.debug("Intersecting refseqs with problematic regions..")

        # rename file
        os.rename(temp_file_path, real_file_path)
        log.debug(f"Saved problematic regions file '{real_file_path}'")
        # add file to legend
        files_legend.append(
            f"- {real_file_name}:  {p_scope_description} "
            f"[{fallback_addendum}, fracMatch = duplicated sequence identity]\n"
        )

    lh = open(os.path.join(targetdir, files_legend_basename), "a")
    lh.writelines(sorted(files_legend))
    lh.close()


def write_transcripts(
    panel: GenePanel,
    records: set[TranscriptRecord],
    targetdir: str,
    all_transcripts: bool = False,
):
    """
    Write full and normalized transcripts files corresponding to the given records
    :param panel:      GenePanel instance
    :param records:    set of gene panel transcript records
    :param targetdir:  output directory
    """

    date_tag = str(date.today())

    log = logging.getLogger(__name__ + ".transcripts_writer")

    real_file_name = f"{panel.shortname}_v{panel.version}_genes_transcripts.tsv"
    real_normal_file_name = real_file_name[: -len("_genes_transcripts.tsv")] + ".json"
    real_file_path = os.path.join(targetdir, real_file_name)
    real_normal_file_path = os.path.join(targetdir, real_normal_file_name)
    temp_file_name = real_file_name[: -len(".tsv")] + "__draft.tsv"
    temp_file_path = os.path.join(targetdir, temp_file_name)
    temp_normal_file_name = real_normal_file_name[: -len(".json")] + "__draft.json"
    temp_normal_file_path = os.path.join(targetdir, temp_normal_file_name)

    genepanel_dictionary = {
        "date": date_tag,
        "description": panel.description,
        "custom-regions": [],
        "refseq-transcripts": [],
    }

    with open(temp_file_path, "w") as fh:
        fh.write(f"# Gene panel: {panel.name}-v{panel.version} -- Date: {date_tag}\n")
        fh.write(
            "\t".join(
                field.alias
                for (field_name, field) in TranscriptRecord.__fields__.items()
            )
            + "\n"
        )

        for record in sorted(records):
            line = "\t".join(safe_str(entry) for entry in dict(record).values())
            log.debug(f"Adding line '{line}' to '{temp_file_path}'")
            fh.write(line + "\n")
            if "Custom region" in safe_str(record.tags):
                genepanel_dictionary["custom-regions"].append(
                    {
                        "chr": record.chromosome,
                        "start": record.start,
                        "end": record.end,
                        "name": record.name,
                        "inheritance": record.inheritance,
                    }
                )
            else:
                genepanel_dictionary["refseq-transcripts"].append(
                    {
                        "id": record.name,
                        "tags": record.tags,
                        "inheritance": record.inheritance,
                    }
                )

        with open(temp_normal_file_path, "w") as fh:
            json.dump(genepanel_dictionary, fh)

        # rename files
        os.rename(temp_file_path, real_file_path)
        log.debug(f"Saved transcripts file '{real_file_path}'")
        os.rename(temp_normal_file_path, real_normal_file_path)
        log.debug(f"Saved normalized gene panel file '{real_normal_file_path}'")

        # add file to legend
        file_description = (
            "gene transcripts information [longest frame fallback strategy]"
        )
        if all_transcripts:
            file_description = (
                "gene transcripts information [all frames fallback strategy]"
            )
        lh = open(os.path.join(targetdir, files_legend_basename), "a")
        lh.write(f"- {real_file_name}:  {file_description}\n")
        lh.write(f"- {real_normal_file_name}:  normalized panel file\n")
        lh.close()


def write_genes(records: set[TranscriptRecord], targetdir: str):
    """
    Write simple tab separated file with list of genes in this panel
    """
    genes = set(
        (record.hgnc_id, record.hgnc_symbol) for record in records if record.hgnc_id
    )
    with open(os.path.join(targetdir, "genes.csv"), "w") as f:
        f.write("HGNC id,HGNC symbol\n")
        for hgnc_symbol, hgnc_id in sorted(genes):
            f.write(f"{hgnc_id},{hgnc_symbol}\n")


@app.command("panels")
def panels(
    ctx: typer.Context,
    logfile: str = typer.Option(
        "gene-panels_export_log_" + str(date.today()) + ".txt",
        "--log-file",
        callback=verify_path,
        help="Name of the master log file (general export process log)",
    ),
    outputdir: str = typer.Option(
        ".",
        "-o",
        "--output-dir",
        callback=verify_path,
        help="Directory to save output files in (will overwrite existing files)",
    ),
    reportfile: str = typer.Option(
        "gene-panels_build_summary_" + str(date.today()) + ".txt",
        "--report-file",
        callback=verify_path,
        help="Name of the file to write the build summary to",
    ),
    include_drafts: bool = typer.Option(
        False, help="Include drafts of panels (only relevant if no ids are provided)"
    ),
    panel_ids: List[int] = typer.Argument(
        None, help="List of panel ids", show_default=False
    ),
):
    """
    Export gene panels.
    If no panel ids are specified, _all_ panels will be exported

    See docs for detail on what is exported.
    """
    with get_session() as session:
        init_log("export-panels", logfile, ctx.obj.verbose)
        with open(reportfile, "w") as rfh:
            unsubmitted_panels = []
            rfh.write("===========  BUILD  SUMMARY  ===========\n")
            rfh.flush()
            if not panel_ids:
                panel_ids = GenePanel.active_panel_ids(
                    session, include_drafts=include_drafts
                )
                if not include_drafts:
                    unsubmitted_panels = session.exec(
                        select(
                            GenePanel.shortname,
                            func.array_agg(cast(GenePanel.version, Text)),
                        )
                        .where(~GenePanel.inactive)
                        .group_by(GenePanel.shortname)
                        .having(
                            func.get_semver_prerelease(func.min(GenePanel.version))
                            == "draft"
                        )
                    ).all()
            panels = session.exec(
                select(GenePanel)
                .where(col(GenePanel.id).in_(panel_ids))
                .order_by(GenePanel.name, GenePanel.version)
            ).all()
            for panel in panels:
                targetdir = export_panel(panel, session, outputdir)
                summarize_build(Path(targetdir), rfh)
            for panel, versions in unsubmitted_panels:
                logger.warning(
                    f"Panel {panel} has no submitted (active) versions, and will not be "
                    f"included in the export (available versions: {versions})"
                )


@app.command()
def genes(
    shortname: str,
    version: str,
    ctx: typer.Context,
    outputdir: str = typer.Option(
        ".",
        "-o",
        "--output-dir",
        callback=verify_path,
        help="Directory to save output files in (will overwrite existing files)",
    ),
    logfile: str = typer.Option(
        "genes_export_log_" + str(date.today()) + ".txt",
        "--log-file",
        callback=verify_path,
        help="Name of the master log file (general export process log)",
    ),
    include_mtdna: bool = typer.Option(
        False,
        "--include-mtdna/--exclude-mtdna",
        help="Include mitochondrial DNA genes",
    ),
    coding_only: bool = typer.Option(
        False,
        "--coding-only",
        help="Export only coding transcripts",
    ),
):
    """
    Generate and export a transient gene panel including all genes with a transcript
    """
    logger = init_log("export-genes", logfile, ctx.obj.verbose)
    with get_session() as session:
        if coding_only:
            description = "Transient panel with all genes with a coding transcript"
        else:
            description = "Transient panel with all genes with a transcript"
        panel = create_panel(shortname, shortname, description)
        panel.version = version

        tx_filter = []
        if coding_only:
            tx_filter.append(~col(Transcript.coding_start).is_(None))
        if not include_mtdna:
            tx_filter.append(Transcript.chromosome != "MT")
        all_hgnc_ids = session.exec(
            select(col(Transcript.hgnc_id).distinct()).where(*tx_filter)
        ).all()
        session.add(panel)
        session.flush()
        assert panel.id is not None
        gpgs = [
            GenePanelGene(hgnc_id=hgnc_id, genepanel_id=panel.id)
            for hgnc_id in all_hgnc_ids
        ]
        session.add_all(gpgs)
        session.flush()
        logger.info(f"Panel created with id {panel.id}")
        export_panel(panel, session, outputdir)
        session.rollback()
