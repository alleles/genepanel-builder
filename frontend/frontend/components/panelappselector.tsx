import API from 'api'
import React, { useState } from 'react'
import AsyncSelect from 'react-select/async'
import { asyncDebounce } from 'utils'

import { Components } from 'api-types'
import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import PanelsListItem from 'components/panelslistitem'
import { ConfidenceLevel } from 'types'

const searchCallback = asyncDebounce(async (searchTerm) => {
  const response = await API.PanelAppSearch.search(searchTerm)
  return response.data.map((doc) => ({
    value: doc,
    label: `${doc.name} - v${doc.version}`,
  }))
}, 300)

export default function PanelAppSelector(): JSX.Element {
  const [selected, setSelected] = useState<{
    value: Components.Schemas.PanelAppPanel
    label: string
  }>(null)

  const {
    panelAppPanels,
    addPanelAppPanel,
    removePanelAppPanel,
    setPanelAppPanelLevels,
  } = useGenePanel()

  return (
    <React.Fragment>
      <div
        id="search-panelapp"
        className="grid grid-cols-5 items-start space-x-2"
      >
        <AsyncSelect
          className="col-span-4"
          placeholder="Search PanelApp panels …"
          // @ts-ignore
          loadOptions={searchCallback}
          inputId="search-panelapp-input"
          value={selected}
          onChange={(selected) => setSelected(selected)}
        />
        <div className="w-32 justify-self-end" id="panelapp-add">
          <Button
            color="confirm"
            disabled={!selected}
            onClick={() =>
              addPanelAppPanel({
                ...selected.value,
                confidence_levels: {
                  [ConfidenceLevel.green]: true,
                  [ConfidenceLevel.amber]: false,
                  [ConfidenceLevel.red]: false,
                },
              })
            }
          >
            Add
          </Button>
        </div>
      </div>
      <div className="mt-3">
        <ul>
          {panelAppPanels.map((panel) => (
            <PanelsListItem
              panel={panel}
              onChange={(levels) => setPanelAppPanelLevels(panel.id, levels)}
              key={panel.id}
            >
              <Button
                color="danger"
                onClick={() => removePanelAppPanel(panel.id)}
              >
                Remove
              </Button>
            </PanelsListItem>
          ))}
        </ul>
      </div>
    </React.Fragment>
  )
}
