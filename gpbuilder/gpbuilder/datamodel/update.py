from datetime import datetime
from typing import Literal, Optional

from sqlmodel import Column, DateTime, Enum, Field, Integer

from gpbuilder.datamodel.base import SQLModelBase

UpdateType = Literal[
    "gene",
    "phenotype",
    "ncbi_transcript",
    "mane_transcript",
    "panelapp",
]

update_types = UpdateType.__args__


class Update(SQLModelBase, table=True):
    id: Optional[int] = Field(
        default=None,
        unique=True,
        sa_column=Column(Integer, primary_key=True, autoincrement=True),
    )
    update_type: UpdateType = Field(
        sa_column=Column(Enum(*update_types, name="update_type"))
    )
    date: datetime = Field(sa_column=Column(DateTime(timezone=True)))
