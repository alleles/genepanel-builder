import { GenePanelProvider } from 'components/genepanelcontext'

export default function Layout({ children }) {
  return (
    <>
      <GenePanelProvider>{children}</GenePanelProvider>
    </>
  )
}
