import pytest

from gpbuilder.api.tests.mock_data import (
    create_genes,
    create_panelapp_panels,
    link_panelapp_panel_gene,
)
from gpbuilder.datamodel.panelapp_panel import ConfidenceLevel, PanelAppPanel


def test_from_panelapp_ids(session):
    panelapp_panels = create_panelapp_panels(3, session)
    session.add_all(panelapp_panels)
    session.flush()
    panelapp_ids = [x.id for x in panelapp_panels]
    res = PanelAppPanel.from_panelapp_ids(session, panelapp_ids)
    assert len(res) == 3
    assert set(x.id for x in res) == set(panelapp_ids)

    with pytest.raises(ValueError):
        PanelAppPanel.from_panelapp_ids(session, [9999999])  # ID does not exist
    res = PanelAppPanel.from_panelapp_ids(
        session, [9999999], ensure_all=False
    )  # ID does not exist, but it shouldn't raise
    assert len(res) == 0


def test_get_genes_from_confidence_level(session):
    (panelapp_panel,) = create_panelapp_panels(1, session)
    green_gene, amber_gene, red_gene = create_genes(3, session)
    session.flush()

    link_panelapp_panel_gene(
        [green_gene], panelapp_panel, ConfidenceLevel.green, session
    )
    link_panelapp_panel_gene(
        [amber_gene], panelapp_panel, ConfidenceLevel.amber, session
    )
    link_panelapp_panel_gene([red_gene], panelapp_panel, ConfidenceLevel.red, session)
    session.flush()

    db_panelapp_genes = panelapp_panel.get_genes_from_confidence_levels(
        session,
        {
            ConfidenceLevel.green: True,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.red: False,
        },
    )
    assert len(db_panelapp_genes) == 1
    db_panelapp_gene = db_panelapp_genes[0]
    assert db_panelapp_gene.hgnc_id == green_gene.hgnc_id
    assert db_panelapp_gene.confidence_level == ConfidenceLevel.green

    db_panelapp_genes = panelapp_panel.get_genes_from_confidence_levels(
        session,
        {
            ConfidenceLevel.green: False,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.red: False,
        },
    )

    assert len(db_panelapp_genes) == 1
    db_panelapp_gene = db_panelapp_genes[0]
    assert db_panelapp_gene.hgnc_id == amber_gene.hgnc_id
    assert db_panelapp_gene.confidence_level == ConfidenceLevel.amber

    db_panelapp_genes = panelapp_panel.get_genes_from_confidence_levels(
        session,
        {
            ConfidenceLevel.green: False,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.red: True,
        },
    )

    assert len(db_panelapp_genes) == 1
    db_panelapp_gene = db_panelapp_genes[0]
    assert db_panelapp_gene.hgnc_id == red_gene.hgnc_id
    assert db_panelapp_gene.confidence_level == ConfidenceLevel.red

    db_panelapp_genes = panelapp_panel.get_genes_from_confidence_levels(
        session,
        {
            ConfidenceLevel.green: True,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.red: False,
        },
    )

    assert len(db_panelapp_genes) == 2
    db_panelapp_gene = db_panelapp_genes[0]
    assert db_panelapp_gene.hgnc_id == green_gene.hgnc_id
    assert db_panelapp_gene.confidence_level == ConfidenceLevel.green

    db_panelapp_gene = db_panelapp_genes[1]
    assert db_panelapp_gene.hgnc_id == amber_gene.hgnc_id
    assert db_panelapp_gene.confidence_level == ConfidenceLevel.amber
