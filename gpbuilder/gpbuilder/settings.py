"""
Module for reading the settings file in environment variable GENERAL_SETTINGS

The settings file is expected to contain the information to initialize a data structure of
D4NumpyReaders, one for each target.
"""

import os
from pathlib import Path
from typing import Literal

from pydantic import BaseModel

from gpbuilder.coverage.D4Reader import D4NumpyReader


class CoverageConfig(BaseModel):
    slop: dict[Literal["upstream", "downstream"], int]
    use_cds_if_available: bool

    def __hash__(self):
        return hash(
            (
                self.slop.get("upstream"),
                self.slop.get("downstream"),
                self.use_cds_if_available,
            )
        )


class D4Config(BaseModel):
    long_name: str
    path: Path
    threshold: int


class Settings(BaseModel):
    coverage: CoverageConfig
    d4: dict[str, D4Config]


_D4_READERS: dict[str, D4NumpyReader] = dict()


_GENERAL_SETTINGS = Settings.parse_raw(
    Path(os.environ["GENERAL_SETTINGS"]).read_bytes()
)


def get_settings():
    """Get the settings from the environment variable GENERAL_SETTINGS"""
    return _GENERAL_SETTINGS


def get_d4_reader(key) -> D4NumpyReader:
    """Get a D4NumpyReader from the settings"""
    d4_config = get_settings().d4[key]
    if key not in _D4_READERS:
        _D4_READERS[key] = D4NumpyReader(d4_config.path, d4_config.threshold)
    reader = _D4_READERS[key]
    assert reader.d4_path == d4_config.path
    assert reader.threshold == d4_config.threshold
    return _D4_READERS[key]
