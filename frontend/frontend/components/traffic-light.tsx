import { useState } from 'react'

const colors = {
  circle: {
    red: { clicked: '#ef4444', unclicked: 'grey' },
    amber: { clicked: '#e6ad54', unclicked: 'grey' },
    green: { clicked: '#5aab4a', unclicked: 'grey' },
  },
  letter: {
    red: { clicked: '#f4dddc', unclicked: 'black' },
    amber: { clicked: '#faeedc', unclicked: 'black' },
    green: { clicked: '#deeeda', unclicked: 'black' },
  },
}

interface TrafficLightProps {
  color: string
  cursor?: string
  size?: string | number
  clickable?: boolean
}

export default function TrafficLight({
  color,
  cursor,
  size,
  clickable = true,
}: TrafficLightProps) {
  const [clicked, setClicked] = useState(!clickable)
  return (
    <div
      className={`${cursor ? 'cursor-' + cursor : 'cursor-default'}`}
      style={{ width: size ? size : 16 }}
      onClick={() => {
        clickable ? setClicked(!clicked) : null
      }}
    >
      <svg viewBox="0 0 33.866666 33.866666">
        <g id="layer1">
          <circle
            style={{
              fill: clicked
                ? colors.circle[color].clicked
                : colors.circle[color].unclicked,
              fillRule: 'evenodd',
              stroke: clicked
                ? colors.circle[color].clicked + 'bf'
                : colors.circle[color].unclicked + 'bf',
              strokeWidth: 0.192613,
              strokeOpacity: 1,
            }}
            cx="-16.933332"
            cy="16.933332"
            transform="scale(-1,1)"
            r="16.307859"
          />
          {
            {
              red: (
                <path
                  style={{
                    strokeWidth: 0.264583,
                    fill: clicked
                      ? colors.letter[color].clicked
                      : colors.letter[color].unclicked,
                  }}
                  d="m 10.300145,25.881624 h 3.745508 v -6.486425 h 2.815332 q 0.04961,0 0.09922,0 0.04961,0 0.08682,0 l 3.33623,6.486425 h 4.241602 l -3.770313,-7.044531 q 1.488282,-0.58291 2.356446,-1.95957 0.880566,-1.37666 0.880566,-3.137793 v -0.02481 q 0,-1.79834 -0.768945,-3.075781 Q 22.566063,9.3493003 21.127391,8.6671714 19.701122,7.9850425 17.691942,7.9850425 h -7.391797 z m 3.745508,-9.326562 v -5.643066 h 3.187402 q 1.389063,0 2.207617,0.756542 0.818555,0.756543 0.818555,2.046387 v 0.02481 q 0,1.314648 -0.79375,2.071191 -0.781348,0.744141 -2.182812,0.744141 z"
                ></path>
              ),
              amber: (
                <path
                  style={{
                    strokeWidth: 0.264583,
                    fill: clicked
                      ? colors.letter[color].clicked
                      : colors.letter[color].unclicked,
                  }}
                  d="m 8.4873365,25.881624 h 3.9315425 l 4.477246,-14.423925 h 1.141016 V 7.9850425 h -3.299023 z m 3.3610355,-4.34082 h 10.182324 l -0.917774,-2.80293 h -8.346777 z m 9.599414,4.34082 h 3.931542 L 19.14095,7.9850425 h -2.17041 v 3.4726565 z"
                ></path>
              ),
              green: (
                <path
                  className={`${
                    clicked
                      ? colors.circle[color].clicked
                      : colors.circle[color].clicked
                  }`}
                  style={{
                    strokeWidth: 0.264583,
                    fill: clicked
                      ? colors.letter[color].clicked
                      : colors.letter[color].unclicked,
                  }}
                  d="m 17.274397,26.191683 q 1.860351,0 3.33623,-0.533301 1.475879,-0.533301 2.492871,-1.550293 1.029395,-1.016992 1.562696,-2.468066 0.545703,-1.451074 0.545703,-3.274219 v -2.058789 h -7.739063 v 2.790527 h 4.092774 l -0.0124,0.26045 q -0.04961,1.103808 -0.595312,1.934765 -0.533301,0.818555 -1.475879,1.277441 -0.930176,0.446485 -2.145606,0.446485 -1.11621,0 -2.021582,-0.42168 -0.892968,-0.42168 -1.53789,-1.215429 -0.63252,-0.79375 -0.967383,-1.934766 Q 12.47469,18.29139 12.47469,16.840316 v -0.0124 q 0,-1.897558 0.558105,-3.237011 0.570508,-1.339453 1.624707,-2.033985 1.066602,-0.706933 2.542481,-0.706933 1.513086,0 2.554882,0.719336 1.041797,0.719336 1.488282,2.021582 l 0.04961,0.124023 h 3.75791 l -0.03721,-0.173633 Q 24.715803,11.854574 23.674006,10.527523 22.644612,9.2004721 20.9951,8.4439292 19.345588,7.6749839 17.18758,7.6749839 q -2.616894,0 -4.539257,1.1038085 Q 10.738362,9.882601 9.6965648,11.94139 8.654768,13.987777 8.654768,16.86512 v 0.0124 q 0,2.182812 0.5953125,3.906738 0.5953125,1.723926 1.7115235,2.939355 1.116211,1.203028 2.703711,1.835547 1.599902,0.63252 3.609082,0.63252 z"
                ></path>
              ),
            }[color]
          }
        </g>
      </svg>
    </div>
  )
}
