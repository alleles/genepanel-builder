import os
import subprocess

from behave import given, when

PG_HOST = os.environ["PG_HOST"]
PG_USER = os.environ["PG_USER"]
PG_PASSWORD = os.environ["PG_PASSWORD"]
PG_DB = os.environ["PG_DB"]
PG_PORT = os.environ["PG_PORT"]

MAINTENANCE_DB = f"postgres://{PG_USER}:{PG_PASSWORD}@{PG_HOST}:{PG_PORT}/template1"

@when(u'I save the state')
@when(u'I save the state as "{name}"')
def save_state(context, name="behaving"):
    """Save the state of the database, using a template database."""
    with open(os.devnull, "w") as devnull:
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname IS NOT null AND pid <> pg_backend_pid();"], stdout=devnull)
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", f"DROP DATABASE IF EXISTS {name} WITH (FORCE)"], stdout=devnull)
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", f"CREATE DATABASE {name} WITH TEMPLATE {PG_DB}"], stdout=devnull)

@given(u'initial state')
@given(u'state "{name}"')
def restore_state(context, name=None):
    if not name:
        name = f"{os.environ['PG_DB']}_initial"
    """Restore the state of the database from template database."""
    with open(os.devnull) as devnull:
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname IS NOT null AND pid <> pg_backend_pid();"], stdout=devnull)
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", f"DROP DATABASE IF EXISTS {PG_DB} WITH (FORCE)"], stdout=devnull)
        subprocess.check_call(["psql", MAINTENANCE_DB, "-c", f"CREATE DATABASE {PG_DB} WITH TEMPLATE {name}"], stdout=devnull)


