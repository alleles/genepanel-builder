"""
Module to find and download RefSeq GFF files from NCBI FTP server.
"""

import ftplib
import logging
from fnmatch import fnmatch
from pathlib import Path

from gpbuilder.datamodel.utils.types import ReferenceGenome

logger = logging.getLogger(__name__)


def download_refseq_gff_files(
    build: ReferenceGenome, latest_only=False
) -> list[tuple[Path, str]]:
    """
    Iterates NCBI ftp server to search for RefSeq GFF files, and downloads them.
    Returns a list of tuples, where the first element is the path to the downloaded file,
    and the second is the release number.
    """
    pattern = f"*{build}*"

    NCBI_ANNOTATION_ROOT = "/genomes/all/annotation_releases/9606/"

    fc = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
    fc.login()
    fc.cwd(NCBI_ANNOTATION_ROOT)

    releases = sorted([x for x in fc.nlst()])

    def traverse(path):
        logger.debug(f"Traversing {path}")
        for subpath, attrs in fc.mlsd(path):
            if attrs["type"] == "dir" and fnmatch(subpath, pattern):
                yield from traverse(f"{path}/{subpath}")
            elif (
                attrs["type"] == "file"
                and fnmatch(subpath, pattern)
                and subpath.endswith("genomic.gff.gz")
            ):
                yield f"{fc.pwd()}/{path}/{subpath}"

    gff_remote = []
    for release in releases:
        gff_remote.extend(list((gff_file, release) for gff_file in traverse(release)))

    if latest_only:
        gff_remote = gff_remote[-1:]

    gff_local = []
    for gff_file, release in gff_remote:
        fc.cwd(NCBI_ANNOTATION_ROOT)
        filename = Path(__file__).parent / f"ncbi-refseq-{release}.gff.gz"
        if filename.exists():
            logger.info(f"{filename} already exists, will not download")
            gff_local.append((filename, release))
            continue
        logger.info(f"Downloading {gff_file} to {filename}")
        with open(filename, "wb") as f:
            fc.retrbinary(f"RETR {gff_file}", f.write)
        gff_local.append((filename, release))

    return gff_local


if __name__ == "__main__":
    print(download_refseq_gff_files("GRCh37", latest_only=True))
