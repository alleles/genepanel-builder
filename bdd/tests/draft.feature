Feature: Draft a new or modified gene panel

    As a user I should be able to save ongoing work when creating a new or modifying an existing gene panel.
    I should also be able to delete a draft.

    Background:
        Given initial state
        Given a browser

    Scenario: Draft a new panel

        # Create a new draft with testuser1
        Given "testuser1" as the persona
        When I visit "/"
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I fill in "description" with "Test description"
        And I press "Continue (Select)"

        ## Check that draft is created for other user
        Given "testuser2" as the persona
        When I visit "/"
        Then I should see "Test gene panel" within 5 seconds
        And I should see "testpanel"
        And I should see "Test description"
        And the version of "testpanel" should be "0.0.1-draft"

        # Continue with testuser1: populate gene panel
        Given "testuser1" as the persona

        ## Test that choices are not automatically saved on reload
        When I add panel "Parathyroid Cancer"
        And I reload
        Then I should not see "Parathyroid Cancer"

        ## Test that choices are saved when clicking Save
        When I add panel "Familial breast cancer"
        And I press "Save"
        And I wait for 1 seconds
        And I reload
        Then I should see "Familial breast cancer" within 5 seconds

        When I add gene "COPA"
        And I add region "chr7:1000-10000"

        ## Check that choices are saved when navigating away
        And I press "Previous (Name)"
        And I wait for 1 seconds
        And I press "Continue (Select)"
        Then I should see "Familial breast cancer" within 5 seconds
        And I should see "COPA"
        And I should see "chr7:1000-10000"

        # Continue and perform some edits in gene table
        When I press "Continue (Edit)"
        Then I should see "1100" within 15 seconds

        ## Test that choices are not automatically saved on reload
        When I change the inheritance of gene "1101" to "AD"
        And I reload
        Then field "inheritance-1101" should have the value "AD/AR" within 5 seconds

        ## Test that choices are saved when clicking Save
        When I change the inheritance of gene "1101" to "AD"
        And I press "Save"
        And I reload
        Then field "inheritance-1101" should have the value "AD" within 5 seconds

        When I select the transcript "NM_007297.4" for gene "1100"
        And I wait for 1 seconds
        And I exclude gene "795" with reason "Insufficient evidence"
        And I fill in "comment-2230" with "Manual"
        ## TODO: include a change in the region table

        ## Check that edits are saved when navigating away
        And I press "Previous (Select)"
        And I wait for 1 seconds
        And I press "Continue (Edit)"
        Then I should see "Inheritance" within 5 seconds
        And field "inheritance-1101" should have the value "AD"
        And field "transcript-1100[0]" should have the value "NM_007294.4"
        And field "exclusion_reasons-795" should have the value "Insufficient evidence"
        And field "comment-2230" should have the value "Manual"

        # Check that panel is populated for other user
        Given "testuser2" as the persona
        When I press "Test gene panel"
        ## Check in read-only view
        Then I should see "Familial breast cancer" within 5 seconds
        And I should see "HGNC:2230"
        And I should see "chr7:1000-10000"
        And field "inheritance-1101" should have the value "AD"
        And the selected transcripts for gene "1100" should be "NM_007297.4, NM_007294.4"
        And field "exclusion_reasons-795" should have the value "Insufficient evidence"
        And field "comment-2230" should have the value "Manual"

        # Check that panel can be edited and submitted by testuser2
        When I press "Modify Draft"
        Then I should see "Create or modify a gene panel" within 5 seconds
        When I wait for 1 seconds
        Then field "name" should have the value "Test gene panel"

        ## Test that version stays the same for testuser1: ongoing draft panels should not be incremented
        Given "testuser1" as the persona
        When I visit "/"
        Then I should see "All gene panels" within 5 seconds
        And the version of "testpanel" should be "0.0.1-draft"

        ## Continue as testuser2
        Given "testuser2" as the persona
        When I press "Continue (Select)"
        And I wait for 1 seconds
        And I press "Continue (Edit)"
        Then I should see "1100" within 15 seconds

        When I fill in "comment-9823" with "Testuser2 edit"
        And I submit the gene panel with version "1.0.0"

        Then I should see "All gene panels" within 5 seconds
        And I should see "Test gene panel" within 5 seconds
        And I should see "Test description"
        And the version of "testpanel" should be "1.0.0"

        # Check that edit from testuser2 is visible in submitted panel.
        Given "testuser1" as the persona
        When I visit "/"
        And I press "Test gene panel"
        Then I should see "1100" within 15 seconds
        And field "comment-9823" should have the value "Testuser2 edit"

    Scenario: Create a new draft from a submitted panel

        Given "testuser1" as the persona

        # First submit a new panel
        When I visit "/"
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I press "Continue (Select)"
        And I add panel "Breast cancer pertinent cancer susceptibility"
        And I add gene "COPA"
        And I press "Continue (Edit)"
        Then I should see "BRCA1" within 15 seconds

        When I submit the gene panel with version "1.0.0"

        # Create a new draft from the submitted panel as another user and make some changes
        Given "testuser2" as the persona
        When I visit "/"
        Then I should see "Test gene panel" within 5 seconds
        And the version of "testpanel" should be "1.0.0"

        When I press "Test gene panel"
        Then I should see "Test gene panel [testpanel]" within 5 seconds

        When I wait for 1 seconds
        And I press "Create New Draft"
        Then I should see "Create or modify a gene panel" within 5 seconds

        ## Check that draft is not created if nothing is changed
        When I press "Continue (Select)"
        Then I should see "Select genes and regions for: testpanel" within 5 seconds

        Given "testuser1" as the persona
        When I visit "/"
        Then the version of "testpanel" should be "1.0.0"
        Then the version of "testpanel" should not be "1.0.1-draft"

        ## Make another change, check that gene is removed on both Edit and Select pages
        Given "testuser2" as the persona
        When I remove "COPA"
        And I press "Continue (Edit)"
        Then I should see "BRCA1" within 15 seconds
        But I should not see "COPA"

        When I press "Previous (Select)"
        And I wait for 1 seconds
        Then I should see "Breast cancer pertinent cancer susceptibility" within 15 seconds
        But I should not see "COPA"

        ## Make some changes with multiple saves
        When I add gene "CDK1"
        And I press "Save"
        And I add gene "CDK2"
        And I press "Save"
        And I press "Continue (Edit)"

        # Check that one and only one draft is created for first user
        Given "testuser1" as the persona
        When I visit "/"
        Then I should see "Test gene panel" within 5 seconds
        And the version of "testpanel" should be "1.0.0"
        And the version of "testpanel" should be "1.0.1-draft"
        But the version of "testpanel" should not be "1.0.2-draft"

        # Continue with second user and submit using default minor update
        Given "testuser2" as the persona
        When I submit the gene panel
        Then I should see "Test gene panel" within 5 seconds
        And the version of "testpanel" should be "1.1.0"
        # Hide all previous versions of a gene panel on submit
        But the version of "testpanel" should not be "1.0.1-draft"

        # Check that the old version is still available when toggling inactive
        When I press the element with xpath "//button[@role='switch']"
        Then I should see "Test gene panel" within 1 seconds
        And the version of "testpanel" should be "1.0.0"
        # New version should be displayed together with inactive
        And the version of "testpanel" should be "1.1.0"
        # Draft should be deleted
        But the version of "testpanel" should not be "1.0.1-draft"

    Scenario: Create a new draft from a large, submitted panel

        Given "testuser1" as the persona
        When I visit "/"
        And I press "Metabolsk"

        ## Test that Create New Draft button is disabled while page is loading
        Then I should see an element with xpath "//button[text()='Create New Draft'][@disabled]"
        And I should see "Metabolsk [metabolsk]" within 5 seconds

        When I wait for 1 seconds
        And I press "Create New Draft"

        ## Test that Continue button is disabled while page is loading
        Then I should see an element with xpath "//button[text()='Continue (Select)'][@disabled]"
        And I should see "Create or modify a gene panel" within 5 seconds
        And I should see "Metabolsk_3.1.0" within 15 seconds

        When I wait for 1 seconds
        And I press "Continue (Select)"

        ## Test that Previous, Save and Continue buttons are disabled while page is loading
        Then I should see an element with xpath "//button[text()='Previous (Name)'][@disabled]"
        Then I should see an element with xpath "//button[text()='Save'][@disabled]"
        Then I should see an element with xpath "//button[text()='Continue (Edit)'][@disabled]"

        ## Test that Save button is disabled when no changes have been made
        Then I should see "Select genes and regions for: metabolsk" within 15 seconds
        And I should see an element with xpath "//button[text()='Save'][@disabled]"

        ## Test that Previous, Save and Continue buttons are disabled while page is loading
        When I press "Continue (Edit)"
        Then I should see an element with xpath "//button[text()='Previous (Select)'][@disabled]"
        Then I should see an element with xpath "//button[text()='Save'][@disabled]"
        Then I should see an element with xpath "//button[text()='Submit Gene Panel'][@disabled]"

    Scenario: Delete a non-submitted draft panel

        When I visit "/"

        # Create a new draft
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I wait for 1 seconds
        And I press "Continue (Select)"
        And I visit "/"
        Then I should see "Test gene panel" within 15 seconds
        And I should see "testpanel"
        And the version of "testpanel" should be "0.0.1-draft"

        # Delete the draft
        When I press "Delete"
        And I press "Confirm"
        Then I should not see "Test gene panel"
        And I should not see "testpanel"
        And I should not see "0.0.1-draft"
        But I should see "Lysosomal"

    Scenario: Delete a new draft from submitted panel

        # Submit a new panel
        When I visit "/"
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I wait for 1 seconds
        And I press "Continue (Select)"
        And I add gene "COPA"
        And I press "Continue (Edit)"
        And I wait for 1 seconds
        And I submit the gene panel with version "1.0.0"
        Then I should see "Test gene panel" within 15 seconds
        And the version of "testpanel" should be "1.0.0"

        # Create a new draft from the panel
        When I press "Test gene panel"
        Then I should see "Test gene panel [testpanel]" within 5 seconds

        When I wait for 1 seconds
        And I press "Create New Draft"
        Then I should see "Create or modify a gene panel" within 5 seconds

        When I wait for 1 seconds
        ## Add a change to have something to save
        And I fill in "description" with "Test description"
        And I press "Continue (Select)"
        And I wait for 1 seconds

        # Check that the draft is created and delete it; check that original is still there
        When I visit "/"
        Then I should see "Test gene panel"
        And the version of "testpanel" should be "1.0.1-draft"

        When I press "Delete"
        Then I should see "Test gene panel"
        And the version of "testpanel" should be "1.0.0"
