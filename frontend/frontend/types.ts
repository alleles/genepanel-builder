import { Components } from 'api-types'

export enum GeneExclusionReasons {
  InsufficientEvidence = 'Insufficient evidence',
  TechnicalIssues = 'Technical issues',
  Watchlist = 'Watchlist',
  Other = 'Other',
}

export type Chromosome =
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | '11'
  | '12'
  | '13'
  | '14'
  | '15'
  | '16'
  | '17'
  | '18'
  | '19'
  | '20'
  | '21'
  | '22'
  | 'X'
  | 'Y'
  | 'XY'
  | 'MT'

export interface Gene {
  hgnc_id: number
  symbol: string
  aliases: string[]
  previous_symbols: string[]
  exclusion_reasons?: GeneExclusionReasons
}

export enum ConfidenceLevel {
  red = 1,
  amber = 2,
  green = 3,
}

export type ConfidenceLevels = Record<ConfidenceLevel, boolean>

export interface PanelAppPanel extends Components.Schemas.PanelAppPanel {
  confidence_levels: ConfidenceLevels
}

export interface Region {
  chromosome: Chromosome
  start: number
  end: number
  name?: string
  inheritance: Inheritance
  comment: string
  ref_genome: 'GRCh37' | 'GRCh38' | 'GRCh39'
  meta?: RegionMeta
}

interface RegionMeta {
  coverage: {
    coverageWES: number
    coverageWGS: number
  }
  segdup: number
}

export interface GenePanel {
  id: number | null
  name: string
  shortname: string
  description: string
  version: string
  panel_app_panels: PanelAppPanel[]
  genes: Gene[]
  regions: Region[]
}

export type GenePanelConfig = Components.Schemas.GenePanelConfig

export enum Inheritance {
  AR = 'AR',
  AD = 'AD',
  XR = 'XR',
  XD = 'XD',
  ADAR = 'AD/AR',
  XDXR = 'XD/XR',
  SMU = 'SMu',
  NA = 'N/A',
}

export enum TranscriptOrigin {
  MANE = 'Mane',
  MANEPLUS = 'MANE+',
  DEFAULT = 'Default',
}
