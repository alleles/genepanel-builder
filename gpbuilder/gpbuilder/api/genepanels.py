import logging
import os
import shutil
import tempfile
from pathlib import Path
from typing import Dict, List, Literal, Optional

from fastapi import APIRouter, HTTPException, Response
from fastapi.responses import FileResponse
from pydantic import NonNegativeFloat, PositiveInt
from sqlalchemy.exc import NoResultFound
from sqlmodel import Field, Session, SQLModel, col, select
from starlette.background import BackgroundTask

from gpbuilder.api.coverage import get_region_coverage, get_transcript_coverage
from gpbuilder.api.queries import get_genepanels_containing_gene
from gpbuilder.cli.export_gp import export_panel
from gpbuilder.datamodel.crud.genepanel import (
    CreatePanelException,
    DeletePanelException,
    GenePanelConfig,
    ShortnameExistsException,
    UpdatePanelException,
    create_from_panel,
    create_panel,
    delete_panel,
    release_panel,
    update_panel,
)
from gpbuilder.datamodel.crud.models import GenePanelSelectRequestModel
from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genedefaults import GeneDefaults
from gpbuilder.datamodel.genepanel import GenePanel
from gpbuilder.datamodel.panelapp_panel import ConfidenceLevel, PanelAppPanel
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript

logger = logging.getLogger(__name__)


# Response


class OriginItem(SQLModel):
    panelapp_id: Optional[PositiveInt]
    conf_level: Optional[ConfidenceLevel]


class GeneResponseItem(SQLModel):
    hgnc_id: int
    symbol: str
    default_transcripts: List[str] = Field(default_factory=list)
    inheritance: str = Field(default="N/A")
    available_transcripts: List[str] = Field(default_factory=list)
    transcript_origin: Dict[str, List[str]] = {}
    coverage: Dict[str, NonNegativeFloat] = {}
    segdup: NonNegativeFloat = 0.0
    origin: List[OriginItem] = []


class RegionResponseItem(SQLModel):
    region: Region
    coverage: Dict[str, NonNegativeFloat] = {}
    segdup: NonNegativeFloat = 0.0


class GenePanelSelectResponse(SQLModel):
    genes: List[GeneResponseItem]
    regions: List[RegionResponseItem]


# Endpoints


router = APIRouter()


class GenePanelItem(GenePanel):
    gene_count: int


@router.get("/genepanels/", response_model=list[GenePanelItem], tags=["Gene panels"])
async def get_genepanels():
    with get_session() as session:
        panels = session.exec(select(GenePanel)).all()
        panel_ids = [panel.id for panel in panels]
        hgnc_ids_for_panels = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)  # type: ignore
        return [
            GenePanelItem(**panel.dict(), gene_count=len(hgnc_ids_for_panels[panel.id]))
            for panel in panels
        ]


# @router.get("/genepanels/{panel_id}", response_model=GenePanel, tags=["Gene panels"])
# def get_genepanel(panel_id: int):
#     with get_session() as session:
#         statement = get_genepanel_by_id(panel_id)
#         return session.exec(statement).first()


@router.get(
    "/genepanels/genes/{hgnc_id}", response_model=list[GenePanel], tags=["Gene panels"]
)
def get_genepanels_by_gene(hgnc_id: int):
    with get_session() as session:
        statement = get_genepanels_containing_gene(hgnc_id)
        return session.exec(statement).all()


@router.get(
    "/genepanels/search/{pattern}", response_model=list[GenePanel], tags=["Gene panels"]
)
async def search_panels(pattern: str):
    with get_session() as session:
        statement = select(GenePanel).where(
            col(GenePanel.name).ilike("%" + pattern + "%")
        )
        return session.exec(statement).all()


@router.post(
    "/genepanels/select", response_model=GenePanelSelectResponse, tags=["Gene panels"]
)
async def select_panel(input: GenePanelSelectRequestModel):
    response = GenePanelSelectResponse(genes=[], regions=[])

    if not any(len(item) for item in dict(input).values()):
        return response

    with get_session() as session:
        logger.info("Fetching Custom genes..")
        manually_added_genes = Gene.from_hgnc_ids(session, input.hgnc_ids)
        response_genes = {
            g.hgnc_id: GeneResponseItem(**g.dict()) for g in manually_added_genes
        }

        logger.info("Fetching PanelApp genes..")
        panelapp_panels_conf_levels = [
            (
                PanelAppPanel.from_panelapp_ids(session, [pa_input.panelapp_id])[0],
                pa_input.confidence_levels,
            )
            for pa_input in input.panelapp_panels
        ]

        for panel, conf_levels in panelapp_panels_conf_levels:
            pa_genes = panel.get_genes_from_confidence_levels(session, conf_levels)
            for panelapp_gene in pa_genes:
                # Check if gene is already in the response, if so, add the origin
                # otherwise, add the gene to the response
                origin_item = OriginItem(
                    panelapp_id=panel.id,
                    conf_level=ConfidenceLevel(panelapp_gene.confidence_level),
                )
                existing_response_gene = response_genes.get(panelapp_gene.hgnc_id)
                if existing_response_gene:
                    existing_response_gene.origin.append(origin_item)
                else:
                    response_genes[panelapp_gene.gene.hgnc_id] = GeneResponseItem(
                        **panelapp_gene.gene.dict(), origin=[origin_item]
                    )
        response.genes = list(response_genes.values())

        # Set default inheritance and transcripts + available transcripts for each gene
        hgnc_ids = list(gene.hgnc_id for gene in response.genes)
        default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(
            session, hgnc_ids
        )
        default_inheritance_modes = Gene.get_default_inheritance_by_hgnc_ids(
            session, hgnc_ids
        )

        available_transcripts = Transcript.get_by_hgnc_ids(session, hgnc_ids)

        # Get source of transcripts for each gene
        gene_defaults_transcripts = GeneDefaults.get_transcripts_by_hgnc_ids(
            session, hgnc_ids
        )
        mane_select_transcripts = ManeSelectTranscript.get_transcripts_by_hgnc_ids(
            session,
            hgnc_ids,
            include_mane_select=True,
            include_mane_plus_clinical=False,
        )
        mane_plus_clinical_transcripts = (
            ManeSelectTranscript.get_transcripts_by_hgnc_ids(
                session,
                hgnc_ids,
                include_mane_select=False,
                include_mane_plus_clinical=True,
            )
        )

        for gene in response.genes:
            # Get available transcripts
            gene.available_transcripts = [
                tx.name for tx in available_transcripts[gene.hgnc_id]
            ]
            # Get default transcripts
            gene.default_transcripts = [
                tx.name for tx in default_transcripts[gene.hgnc_id]
            ]
            # Get default inheritance modes
            gene.inheritance = default_inheritance_modes[gene.hgnc_id]
            # Re-populate available transcripts, with defaults first
            gene.available_transcripts = gene.default_transcripts + [
                tx_name
                for tx_name in gene.available_transcripts
                if tx_name not in gene.default_transcripts
            ]

            gene.transcript_origin = {}
            for transcript_name in gene.available_transcripts:
                gene.transcript_origin[transcript_name] = []
                if transcript_name in [
                    tx.name for tx in gene_defaults_transcripts.get(gene.hgnc_id, [])
                ]:
                    gene.transcript_origin[transcript_name].append("Default")
                if transcript_name in [
                    tx.name for tx in mane_select_transcripts.get(gene.hgnc_id, [])
                ]:
                    gene.transcript_origin[transcript_name].append("MANE")
                if transcript_name in [
                    tx.name
                    for tx in mane_plus_clinical_transcripts.get(gene.hgnc_id, [])
                ]:
                    gene.transcript_origin[transcript_name].append("MANE+")

        # Add coverage and segdups
        logger.info("Fetching coverage and segdups..")
        default_tx_names = [tx for g in response.genes for tx in g.default_transcripts]
        default_transcripts = session.exec(
            select(Transcript).where(
                col(Transcript.name).in_(default_tx_names),
            )
        ).all()

        logger.info("Fetching coverage and segdups..")
        wgs_coverage = get_transcript_coverage(default_transcripts, "wgs")
        wes_coverage = get_transcript_coverage(default_transcripts, "wes")
        segdup = get_transcript_coverage(default_transcripts, "segdup")
        for gene in response.genes:
            if not gene.default_transcripts:
                gene.coverage = {
                    "coverageWGS": 0,
                    "coverageWES": 0,
                    "segdup": 0,
                }
                continue

            tx_wgs_coverages = [wgs_coverage[k] for k in gene.default_transcripts]
            tx_wes_coverages = [wes_coverage[k] for k in gene.default_transcripts]
            tx_segdup = [segdup[k] for k in gene.default_transcripts]

            gene.coverage = {
                "coverageWGS": sum(tx_wgs_coverages) / len(tx_wgs_coverages),
                "coverageWES": sum(tx_wes_coverages) / len(tx_wes_coverages),
            }
            gene.segdup = sum(tx_segdup) / len(tx_segdup)

        # Regions
        logger.info("Fetching coverage and segdup for regions..")
        if len(input.regions) > 0:
            wgs_coverage = get_region_coverage(input.regions, "wgs")
            wes_coverage = get_region_coverage(input.regions, "wes")
            segdup = get_region_coverage(input.regions, "segdup")

            for i, r in enumerate(input.regions):
                response.regions.append(
                    RegionResponseItem(
                        region=Region(**r.dict()),
                        coverage={
                            "coverageWGS": wgs_coverage[i],
                            "coverageWES": wes_coverage[i],
                        },
                        segdup=segdup[i],
                    )
                )

        return response


def get_genepanel_by_id(id: int, session: Session) -> GenePanel:
    return session.exec(
        select(GenePanel).where(
            GenePanel.id == id,
        )
    ).one()


# PUT Rationale
# Since for both creating and updating we receive the same payload,
# we might as well use a idempotent endpoint to handle both actions
@router.put(
    "/genepanels/configure",
    response_model=Dict[Literal["id"], int],
    status_code=201,
    tags=["Gene panels"],
)
async def configure(input: GenePanelConfig, response: Response):
    """Create or update a gene panel"""

    if input.version is not None:
        raise HTTPException(
            status_code=400,
            detail="Version cannot be set manually. It will be set automatically.",
        )

    with get_session() as session:
        if input.id:
            # Update existing panel
            genepanel = session.get(GenePanel, input.id)
            if genepanel is None:
                raise HTTPException(status_code=404, detail="Panel not found")

            # Create new version of existing panel if existing panel is not a draft
            if not genepanel.version.endswith("-draft"):
                genepanel = create_from_panel(session, input.id)

        else:
            # Create new panel
            try:
                genepanel = create_panel(input.name, input.shortname, input.description)
            except ShortnameExistsException as e:
                raise HTTPException(status_code=409, detail=str(e))
            except CreatePanelException as e:
                raise HTTPException(status_code=400, detail=str(e))
            session.add(genepanel)

        session.flush()

        response.status_code = 200 if input.id is not None else 201
        try:
            update_panel(session, genepanel, input)
        except UpdatePanelException as e:
            raise HTTPException(status_code=400, detail=str(e))
        session.commit()
        return {"id": genepanel.id}


@router.post(
    "/genepanels/{id}/submit",
    response_model=Dict[Literal["id"], int],
    status_code=200,
    tags=["Gene panels"],
)
async def submit(id: int, version: str):
    with get_session() as session:
        genepanel = session.get(GenePanel, id)
        if genepanel is None:
            raise HTTPException(status_code=404, detail="Panel not found")
        release_panel(session, genepanel, version)
        session.commit()
        return {"id": genepanel.id}


# @router.patch("/genepanel/update/{id}", tags=["Gene panels"])
# async def update_genepanel(id: int, input: GenePanelConfig):
#     print(f"bumping 1.0.0 (major): {VersionInfo.parse('1.0.0').bump_major()}")
#     print(f"bumping 1.0.0 (minor): {VersionInfo.parse('1.0.0').bump_minor()}")
#     print(f"bumping 1.3.0 (major): {VersionInfo.parse('1.3.0').bump_major()}")
# TODO


@router.get(
    "/genepanel/{id}",
    response_model=GenePanelConfig,
    status_code=200,
    tags=["Gene panels"],
)
async def get_genepanel(id: int):
    with get_session() as session:
        try:
            gp = session.exec(select(GenePanel).where(GenePanel.id == id)).one()
        except NoResultFound:
            raise HTTPException(status_code=404, detail="Gene panel not found")

        return gp.as_config()


@router.delete("/genepanel/{id}", tags=["Gene panels"])
async def delete_genepanel(id: int):
    with get_session() as session:
        try:
            delete_panel(session, id)
            session.commit()
        except DeletePanelException as e:
            raise HTTPException(status_code=400, detail=str(e))


@router.get(
    "/genepanel/{id}/export",
    status_code=200,
    tags=["Gene panels"],
)
async def export_genepanel(id: int):
    with get_session() as session:
        try:
            panel = session.exec(select(GenePanel).where(GenePanel.id == id)).one()
        except NoResultFound:
            raise HTTPException(status_code=404, detail="Gene panel not found")

        logger.info(f"Preparing panel {id} for export..")
        with tempfile.TemporaryDirectory() as tempdir:
            targetdir = export_panel(panel, session, tempdir)
            panel_archive = shutil.make_archive(
                base_name=Path(targetdir).name,
                format="zip",
                root_dir=Path(targetdir).parent,
                base_dir=Path(targetdir).name,
            )
            return FileResponse(
                panel_archive,
                media_type="application/zip",
                filename=Path(panel_archive).name,
                background=BackgroundTask(
                    os.unlink, panel_archive
                ),  # Remove file once sent
            )
