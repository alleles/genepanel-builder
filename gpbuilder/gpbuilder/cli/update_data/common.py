from datetime import datetime
from enum import Enum, auto
from pathlib import Path

from sqlmodel import Session

from gpbuilder.datamodel.engine import execute_sql_script
from gpbuilder.datamodel.update import Update, UpdateType


class UpdateStatus(Enum):
    CREATED = auto()
    DELETED = auto()
    IDENTICAL = auto()
    IGNORED = auto()
    REFUSED = auto()
    UPDATED = auto()

    @staticmethod
    def get_counter() -> dict["UpdateStatus", int]:
        class Counter(dict):
            def __str__(self):
                return str({k.name: v for k, v in self.items()})

        return Counter({k: 0 for k in UpdateStatus.__members__.values()})


def massage_hgnc_id(gene_data):
    hgnc_id = gene_data["hgnc_id"]
    if hgnc_id is None:
        return None
    else:
        return int(hgnc_id.split(":")[-1])


def massage_transcr(t):
    if len(t) > 0:
        _t = t[0].strip()
        if _t != "[meta]":
            return _t
    else:
        return None


def trigger_update_hgnc_ids(
    sql_script: Path = "resources/sql/trigger_update_hgnc_ids.sql",
):
    execute_sql_script(sql_script)


def set_next_update_id(update_type: UpdateType, session: Session) -> int:
    update = Update(update_type=update_type, date=datetime.now())
    session.add(update)
    session.flush()  # Push to database to get automatic 'id'
    session.refresh(update)  # Get 'id' back from database
    assert update.id is not None
    return update.id
