from behave import then


@then('the origin for "{gene_name}" should list "{panel_id}"')
@then('the origin for "{gene_name}" should list "{panel_id}" with confidence level "{conf_level}"')
def check_origin(context, gene_name, panel_id, conf_level=None):
    if conf_level:
        context.execute_steps(
            f"""
            Then I should see an element with xpath "//td[text()='{gene_name}']/..//div[@title='PanelApp panel {panel_id}; confidence level {conf_level}']" within 15 seconds
            """
        )
    context.execute_steps(
        f"""
        Then I should see an element with xpath "//td[text()='{gene_name}']/..//div[text()='{panel_id}']" within 15 seconds
        """
    )

