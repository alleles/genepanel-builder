from operator import itemgetter
from typing import Dict, List

from cachetools import TTLCache, cached
from fastapi import APIRouter, HTTPException
from pydantic import BaseModel, NonNegativeFloat, NonNegativeInt, PositiveInt
from sqlmodel.sql.expression import col, select

from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.settings import get_d4_reader, get_settings

router = APIRouter()


class GenomicRegion(BaseModel):
    chromosome: str
    start: NonNegativeInt
    end: PositiveInt


# Region coverage/segdup
class RegionCoverageRequest(BaseModel):
    regions: List[GenomicRegion]
    threshold: NonNegativeInt
    d4_target: str


# Transcript coverage/segdup
class TranscriptCoverageRequest(BaseModel):
    names: List[Transcript]
    threshold: NonNegativeInt
    d4_target: str


def get_region_coverage(regions: List[Region], d4_target: str):
    return (
        get_d4_reader(d4_target)
        .fraction_ge_threshold(
            [(r.chromosome, r.start, r.end) for r in regions],
        )
        .fraction_per_region
    )


def get_slopped_regions(tx: Transcript):
    settings = get_settings()
    upstream, downstream = itemgetter("upstream", "downstream")(settings.coverage.slop)
    use_cds_if_available = settings.coverage.use_cds_if_available

    if use_cds_if_available and tx.is_coding:
        slopped_regions = tx.slopped(upstream, downstream).cds
    else:
        slopped_regions = tx.slopped(upstream, downstream).exons
    return slopped_regions


@cached(
    TTLCache(maxsize=500_000, ttl=7 * 24 * 60 * 60),
    key=lambda tx, d4_target: (tx.id, d4_target, get_settings().coverage),
)
def _tx_coverage(tx: Transcript, d4_target: str):
    return (
        get_d4_reader(d4_target)
        .fraction_ge_threshold(
            [(r.chromosome, r.start, r.end) for r in get_slopped_regions(tx)],
        )
        .fraction_across_regions
    )


def get_transcript_coverage(
    transcripts: List[Transcript],
    d4_target: str,  # wes/wgs
) -> Dict[str, NonNegativeFloat]:
    return {tx.name: _tx_coverage(tx, d4_target) for tx in transcripts}


# Get coverage for a list of transcripts
@router.post(
    "/coverage/transcript",
    response_model=Dict[str, NonNegativeFloat],
    tags=["Genomic stats"],
)
async def transcript_coverage(
    tx_names: List[str], d4_target: str
):  # TODO: threshold goes into config
    with get_session() as session:
        transcripts = session.exec(
            select(Transcript).where(col(Transcript.name).in_(tx_names))
        ).all()
    if not len(transcripts) == len(tx_names):
        raise HTTPException(
            status_code=400,
            detail="Inconsistent length of transcript names.",
        )
    return get_transcript_coverage(transcripts, d4_target)


@router.get("/coverage/init", tags=["Genomic stats"])
async def init_coverage():
    for d4_target in get_settings().d4:
        get_d4_reader(d4_target).populate_chromosome_d4np()
    return {"message": "Coverage initialized"}
