import os

import requests
from selenium.webdriver.chrome.options import Options
from steps.state import restore_state, save_state

from behaving.web import environment as benv

chrome_options = Options()
chrome_options.add_argument("--start-maximized")


def before_all(context):
    save_state(context, name="before_all")

    context.remote_webdriver_url = "http://selenoid:4444/wd/hub"
    context.base_url = os.environ.get("SITE_URL", "http://nginx:8080")

    # Make backend initialize coverage, so we don't have to wait for the
    # loading of d4 data into memory
    requests.get("http://gpbuilder:8000/api/v1/coverage/init")

    context.default_browser = "chrome"
    context.screenshots_dir = "/app/screenshots"
    if not os.path.isdir(context.screenshots_dir):
        os.mkdir(context.screenshots_dir)
    context.browser_args = {
        "desired_capabilities": {
            "enableVNC": True,
            "selenoid:options": {
                "enableVNC": True,
            },
        },
        "options": chrome_options,
    }

    for filename in os.listdir(context.screenshots_dir):
        file_path = os.path.join(context.screenshots_dir, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
        except Exception as e:
            print("Failed to delete %s. Reason: %s" % (file_path, e))


def after_all(context):
    restore_state(context, name="before_all")
    benv.after_all(context)


def before_feature(context, feature):
    benv.before_feature(context, feature)


def after_feature(context, feature):
    benv.after_feature(context, feature)


def before_scenario(context, scenario):
    benv.before_scenario(context, scenario)
    context.personas = {}
    context.persona = None


def after_scenario(context, scenario):
    benv.after_scenario(context, scenario)
