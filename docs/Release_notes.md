# Release notes

| Major versions         | Minor versions         |
| :--------------------- | :--------------------- |
| [v1.3.0](#version-130) | [v1.3.1](#version-131), [v1.3.2](#version-132), [v1.3.3](#version-133), [v1.3.4](#version-134), [v1.3.5](#version-135) |
| [v1.2.0](#version-120) | [v1.2.1](#version-121) |
| [v1.1.0](#version-110) | [v1.1.1](#version-111) |
| [v1.0.0](#version-100) |

## Version 1.3.5

Release date: 03.03.2025

### Highlights

This patch release fixes a bug related to custom regions.

### All changes

<!-- MR !316 -->
- Fix export of custom region names.

## Version 1.3.4

Release date: 25.02.2025

### Highlights

This is a patch release including fixes for custom regions and setup.

### All changes

<!-- MR !312, !313 -->
- Fixes for diff and export of panels with custom regions.
<!-- No furter release notes necesary but adding here for reference:
MR !298 Fix empty lines in README
MR !299 Add support for creating dummy d4 files
MR !300 Pin rust version, fix issue with d4 build 
MR !303 Provide all environment variables in .env-file
MR !304 Example production deployment
MR !305 Fix warnings thrown when building or running compose
MR !306 Default to empty NEXT_PUBLIC_API_URL 
MR !307 Improved documentation
MR !308 Fix unused return tuple element
MR !309 Cosmetics
MR !310 Fix "Poetry fails #8997872070"
MR !311 Upgrade to d4tools post v0.3.11 in genomic-data container
-->
- Various fixes and code improvements.

## Version 1.3.3

Release date: 29.05.2024

### Highlights

This release brings minor improvements to exports.

### All changes

<!-- MR !292 -->
- Changed tsv export to using csv format.
<!-- MR !293 -->
- Fixed gene panel name instead of description in export.
<!-- MR !294, !295 (pyd4 build, pin Rust) -->
- Various fixes and code improvements.

## Version 1.3.2

Release date: 15.02.2024

### Highlights

This release brings improvements to editing large gene panels, including bug fixes and faster navigation.

### All changes

<!-- MR !289 -->
- Disabled navigation buttons on all pages and changes on the Edit page until data is fully loaded/refreshed. This prevents unexpected loss of edits.
- Removed unnecessary saves and disabled Save button if there are no changes to save. This improves navigation speed.
- Added spinner to the Name page while loading data.
<!-- MR !287, !288 -->
- Various fixes and code improvements.
- Sub-command `against-folder` is now implicit when invoking `gpb diff`. Sub-command `create-drafts` is now an option `--create-drafts`.

## Version 1.3.1

Release date: 06.02.2024

### All changes

<!-- MR !284 -->
- Added option to set inheritance mode `SMu` (somatic mutation) manually.

## Version 1.3.0

Release date: 01.02.2024

### Highlights

This release brings a number of improvements to the process and documentation for updating reference data in GPB (gene names, transcripts, inheritance modes and PanelApp panels); see [User manual](./docs/User_manual.md#changes-introduced-by-updates-to-reference-data) and [CLI](./docs/Command_line_interface.md) Docstrings. 

### All changes

<!-- MR !278, !279, !280, !281, !282 -->
- Added CLI commands to produce a diff with an overview of all changes introduced by reference updates using the CLI tool (`gpb diff against-folder <folder>`). The diff can also be run together with a reference update (`gpb update`), either between existing gene panels in GPB before and after a reference update (`--diff`), or with gene panels in GPB against a folder with previous exports (`--diff-against-folder <folder>`). Two files are created: 
  - A table with all changes, with possibilities for sorting by severity of change. 
  - A summary file with counts of changes in each gene panel. 
- Added CLI commands to assign new, bumped draft versions to any changed panels (including ongoing drafts) (`gpb diff create-drafts <folder>`).
- Added a global flag to the CLI tool to run commands without modifying the database (`gpb --dry-run`). This gives the ability to check for potential issues with e.g. a reference data update before applying changes.
<!-- MR !277 -->
- mtDNA genes are now excluded from exports by default, but can be included using the flag `--include-mtdna` with `gpbuilder export genes`.
<!-- No furter release notes necesary but adding here for reference:
MR !108 Untidy Docker
MR !275 Fix bug in diff for MANE Plus Clinical transcripts
MR !276 Fix GFF parser
-->
- Various fixes and code improvements.

## Version 1.2.1

Release date: 21.11.2023

### All changes

<!-- MR !272 -->

- Improved UI performance on the View and Select pages.

## Version 1.2.0

Release date: 14.11.2023

### All changes

<!-- MR !268 -->

- Added more translations of inheritance from OMIM. _Note:_ All X-linked inheritance modes from OMIM, including "pure" `XD` and `XR`, are now translated to `XD/XR` in GPB. See also [User manual](./docs/User_manual.md#inheritance-mode) and [MR !268](https://gitlab.com/alleles/genepanel-builder/-/merge_requests/268).

<!-- MR !269, !270 -->

- Fixed time-out issues when loading or exporting large gene panels.

## Version 1.1.1

Release date: 23.08.2023

### All changes

<!-- MR !265 -->

- Fixed a bug causing inability to export gene panel from UI

## Version 1.1.0

Release date: 23.06.2023

### All changes

<!-- MR !259 -->

- Changed fallback inheritance mode to `N/A`. This is to better separate out genes that have not been curated for inheritance (no configuration and no inheritance information in OMIM); see also the [User manual](./docs/User_manual.md#inheritance-mode).

<!-- MR !256 -->

- Added CLI option `--no-draft` to export diff file without creating new drafts.

<!-- MR !261 -->

- Added option `-mq` to set minimum mapping quality when calculating coverage; see [Create coverage file](./docs/Coverage_segdup.md#create-coverage-file).

<!-- MR !257, !259 -->

- Improvements to UI.

## Version 1.0.0

Release date: 29.03.2023

### Changes

Intial release.
