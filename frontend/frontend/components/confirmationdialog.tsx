import React, { useRef, Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { ExclamationTriangleIcon } from '@heroicons/react/24/outline'
import Button from 'components/button'

export default function ConfirmationDialog({
  onConfirm,
  onClose = () => {},
  children,
}) {
  const cancelButtonRef = useRef(null)

  return (
    <Transition.Root show={true} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={onClose}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500/75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative w-full max-w-md overflow-hidden rounded-lg bg-white p-2 text-left shadow-xl transition-all">
                <div className="bg-white px-3 py-4">
                  <div className="flex items-start">
                    <div className="mx-1 flex h-10 w-10 shrink-0 items-center justify-center rounded-full bg-ellared-light">
                      <ExclamationTriangleIcon
                        className="h-6 w-6 text-ellared-dark"
                        aria-hidden="true"
                      />
                    </div>
                    <div className="w-full pl-4 pr-14">
                      <Dialog.Title
                        as="h3"
                        className="text-lg font-medium leading-9 text-gray-900"
                      >
                        {children}
                      </Dialog.Title>
                    </div>
                  </div>
                </div>
                <div className="flex gap-x-4 px-4 py-3">
                  <Button
                    id="cancel-button"
                    color="cancel"
                    onClick={onClose}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </Button>
                  <Button
                    id="confirm-button"
                    color="confirm"
                    onClick={onConfirm}
                  >
                    Confirm
                  </Button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
