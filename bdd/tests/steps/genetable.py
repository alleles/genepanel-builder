from behave import then, when


@when('I change the inheritance of gene "{gene}" to "{inheritance}"')
def change_gene_inheritance(context, gene, inheritance):
    context.execute_steps(
        f"""
        When I press the element with xpath "//input[@name='inheritance-{gene}']/../div/button"
        Then I should see an element with xpath "//input[@name='inheritance-{gene}']/../div//span[text() = '{inheritance}']" within 1 seconds
        When I press the element with xpath "//input[@name='inheritance-{gene}']/../div//span[text() = '{inheritance}']"
        """
    )


@when('I select the transcript "{transcript}" for gene "{gene}"')
def change_gene_transcript(context, gene, transcript):
    context.execute_steps(
        f"""
        When I press the element with xpath "//input[@name='transcript-{gene}[0]']/../div/button"
        Then I should see an element with xpath "//span[text()='{transcript}']" within 1 seconds
        When I press the element with xpath "//span[text()='{transcript}']"
        """
    )


@then('I should see the transcript "{transcript}" for gene "{gene}"')
def list_gene_transcript(context, gene, transcript):
    context.execute_steps(
        f"""
        When I press the element with xpath "//input[@name='transcript-{gene}[0]']/../div/button"
        Then I should see an element with xpath "//span[contains(text(), '{transcript}')]" within 1 seconds
        """
    )

@then('the selected transcripts for gene "{gene}" should be "{transcripts}"')
def list_gene_transcripts(context, gene, transcripts):
    transcripts = transcripts.strip().split(',').sort()
    el = context.browser.find_by_id(f'transcript-{gene}')
    assert el, f"Gene with id {gene} not found"
    val = el.first.value.strip().split(',').sort()
    assert val == transcripts, f"Expected transcripts {transcripts} but got {val}"

    

@when('I exclude gene "{gene}" with reason "{reason}"')
def exclude_reason(context, gene, reason):
    context.execute_steps(
        f"""
        When I press the element with xpath "//div[@id='exclusion_reasons-{gene}']/div/button"
        Then I should see an element with xpath "//div[@id='exclusion_reasons-{gene}']//span[text() = '{reason}']" within 1 seconds
        When I press the element with xpath "//div[@id='exclusion_reasons-{gene}']//span[text() = '{reason}']"
        And I wait for 1 seconds
        """
    )


@when('I press the header "{header}"')
def sort_header(context, header):
    context.execute_steps(
        f"""
        When I press the element with xpath "//div[text()='{header}']"
        """
    )
