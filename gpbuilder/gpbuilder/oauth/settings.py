import secrets

from starlette.config import Config

# Config will be read from environment variables and/or ".env" files.
config = Config(".env")

TESTING = config.get("TESTING", default=False, cast=bool)
DEVELOPMENT = config.get("DEVELOPMENT", default=False, cast=bool)
THIS_HOSTNAME = config.get("API_HOSTNAME", default="localhost")

SECRET_KEY = secrets.token_urlsafe(64)
SALT = secrets.token_hex(32)

USE_OAUTH = config.get("USE_OAUTH", default=True, cast=bool)

# Error Messages
SESSION_EXPIRED = "Your session has expired"
LOGIN_INCOMPLETE = "Login Incomplete"

# Session Variables
ID_TOKEN = "id_token"
PERSON_INFO = "person_info"
REDIRECT_URL = "redirect_url"
SESSION_ID_COOKIE = "gpb_session"

# Config Variables
SESSION_FILE = config.get("SESSION_FILE", default="/app/session.db")
CALLBACK_ROUTE = CALLBACK_ROUTE = config.get(
    "CALLBACK_ROUTE", default=f"{THIS_HOSTNAME}/auth/callback/tsd"
)
OIDC_CLIENT_ID = config.get("OIDC_CLIENT_ID", default="CHANGE ME!")
OIDC_CLIENT_SECRET = config.get("OIDC_CLIENT_SECRET", default="CHANGE ME!")
CONF_URL = config.get("CONF_URL", default="CHANGE ME!")
SCOPE = config.get("SCOPE", default="openid")

# For setting secure cookie in production, not so in dev
SECURE_COOKIE = not DEVELOPMENT
