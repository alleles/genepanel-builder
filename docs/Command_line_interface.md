# Command line interface

Gene Panel Builder (GPB) comes with a command line interface that can be used to update external data source, update gene defaults, and import/export panels.

The CLI is accessible with `gpb`, and should be self-documented through `gpb --help`, `gpb <subgroup> --help`, or `gpb <subgroup> <command> --help`.

Any CLI command executed will log to `gpb-cli.log`.
