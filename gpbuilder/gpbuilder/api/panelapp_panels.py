from typing import Literal

import sqlmodel
from fastapi import APIRouter

from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel

router = APIRouter()


@router.get("/panelapp/{site}/{id}", response_model=PanelAppPanel, tags=["PanelApp"])
async def get_panel(site: Literal["ENG"], id: int):
    with get_session() as session:
        statement = sqlmodel.select(PanelAppPanel).where(PanelAppPanel.id == id)
        return session.exec(statement).one_or_none()


@router.post("/panelapp/{site}", response_model=list[PanelAppPanel], tags=["PanelApp"])
async def get_panels(site: Literal["ENG"], ids: list[int]):
    with get_session() as session:
        return PanelAppPanel.from_panelapp_ids(session, ids, ensure_all=True)


@router.get(
    "/panelapp/{site}/search/{pattern}",
    response_model=list[PanelAppPanel],
    tags=["PanelApp"],
)
async def search_panels(site: Literal["ENG"], pattern: str):
    with get_session() as session:
        statement = sqlmodel.select(PanelAppPanel).where(
            sqlmodel.or_(
                sqlmodel.col(PanelAppPanel.name).ilike("%" + pattern + "%"),
                sqlmodel.col(PanelAppPanel.disease_group).ilike("%" + pattern + "%"),
                sqlmodel.col(PanelAppPanel.disease_sub_group).ilike(
                    "%" + pattern + "%"
                ),
                PanelAppPanel.id == pattern if pattern.isdecimal() else False,
            )
        )
        return session.exec(statement).unique().all()
