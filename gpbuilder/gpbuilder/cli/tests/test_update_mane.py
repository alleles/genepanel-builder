import pytest
import sqlalchemy.exc
from sqlmodel import Session, select

from gpbuilder.api.tests.mock_data import create_genes, create_transcripts
from gpbuilder.cli.update_data.common import set_next_update_id
from gpbuilder.cli.update_data.mane import insert_mane_transcript
from gpbuilder.datamodel.transcript import ManeSelectTranscript


def test_insert_mane_transcript(session: Session):
    (gene,) = create_genes(1, session)
    (tx,) = create_transcripts(1, gene.hgnc_id, session)
    session.flush()
    assert tx.id is not None
    mane_tx = ManeSelectTranscript(
        id=None,
        transcript_id=tx.id,
        status="MANE Select",
        update_id=1,
    )
    insert_mane_transcript(session, mane_tx)
    session.flush()
    db_mane_tx = session.exec(select(ManeSelectTranscript)).one()
    assert db_mane_tx.dict(exclude={"id"}) == mane_tx.dict(exclude={"id"})


def test_insert_mane_transcript_fails(session: Session):
    (gene,) = create_genes(1, session)
    (tx,) = create_transcripts(1, gene.hgnc_id, session)
    update_id = set_next_update_id("mane_transcript", session)

    session.flush()
    assert tx.id is not None

    mane_tx = ManeSelectTranscript(
        id=None,
        transcript_id=tx.id,
        status="MANE Select",
        update_id=update_id,
    )
    insert_mane_transcript(session, mane_tx)
    session.commit()

    mane_tx2 = ManeSelectTranscript(
        id=None,
        transcript_id=tx.id,
        status="MANE Select",
        update_id=update_id,
    )
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        insert_mane_transcript(session, mane_tx2)
        session.flush()
    session.rollback()

    # Same transcript id, but different status
    mane_tx2 = ManeSelectTranscript(
        id=None,
        transcript_id=tx.id,
        status="MANE Plus Clinical",
        update_id=update_id,
    )
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        insert_mane_transcript(session, mane_tx2)
        session.flush()
    session.rollback()

    db_mane_tx = session.exec(select(ManeSelectTranscript)).one()
    assert db_mane_tx.dict(exclude={"id"}) == mane_tx.dict(exclude={"id"})
