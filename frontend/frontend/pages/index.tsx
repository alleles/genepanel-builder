import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react'

import API from 'api'
import Button from 'components/button'
import GenePanelTable from 'components/genepaneltable'
import DefaultLayout from 'components/layouts/default'

export default function Home() {
  const router = useRouter()

  const newPanelButtonRef = useRef<HTMLButtonElement>(null)

  useEffect(() => {
    if (newPanelButtonRef.current) {
      newPanelButtonRef.current.focus()
    }
  }, [])

  return (
    <main className="m-auto h-screen w-full">
      <div className="sticky top-0 z-30 flex w-full flex-row items-center justify-between bg-ellablue-lighter py-2 px-8">
        <h2 className="text-lg font-medium text-gray-900">All gene panels</h2>
        <div className="text-sm text-gray-500">
          Create a new gene panel or select an existing to modify.
        </div>
        <div>
          <Button
            onClick={() => router.push('/configure/name')}
            ref={newPanelButtonRef}
          >
            <span>New Gene Panel</span>
          </Button>
        </div>
      </div>

      <div className="h-[calc(100vh-89px)] overflow-x-hidden overflow-y-scroll">
        <div className="mt-12 w-full pl-8 pr-7 pb-8">
          <GenePanelTable />
        </div>
      </div>
    </main>
  )
}

Home.getLayout = (page) => <DefaultLayout>{page}</DefaultLayout>
