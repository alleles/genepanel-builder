"""
Functions that return select statements. Functions here should only be queries, that can be
utilized elsewhere, either as direct input to session.exec, or to build subqueries.
"""


import logging

from sqlmodel import col, select
from sqlmodel.sql.expression import SelectOfScalar

from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genepanel import (
    GenePanel,
    GenePanelExcludedGene,
    GenePanelGene,
    GenePanelPanelAppPanel,
)
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel, PanelAppPanelGene

logger = logging.getLogger(__name__)


def get_panelapp_genes(
    panelapp_ids: list[int] | SelectOfScalar[int],
) -> SelectOfScalar[Gene]:
    """Return a list of genes from a list of PanelApp panel IDs"""

    panelapp_genes = (
        select(Gene)
        .join(PanelAppPanelGene)
        .where(
            PanelAppPanelGene.hgnc_id == Gene.hgnc_id,
            col(PanelAppPanelGene.panelapp_panel_id).in_(panelapp_ids),
        )
    )
    return panelapp_genes


def get_panel_app_panels_including_gene(hgnc_id: int) -> SelectOfScalar[PanelAppPanel]:
    """Return a list of PanelApp panels that contain a gene"""

    panel_app_panels = (
        select(PanelAppPanel)
        .join(PanelAppPanelGene)
        .where(
            col(PanelAppPanelGene.hgnc_id) == hgnc_id,
            PanelAppPanelGene.panelapp_panel_id == PanelAppPanel.id,
        )
    )
    return panel_app_panels


def get_genepanels() -> SelectOfScalar[GenePanel]:
    """Return a list of gene panels"""

    genepanels = select(GenePanel)
    return genepanels


def get_genepanel_by_id(panel_id: int) -> SelectOfScalar[GenePanel]:
    """Return gene panel with the id"""

    genepanels = select(GenePanel).where(col(GenePanel.id) == panel_id)
    return genepanels


def get_genepanels_including_panelapp(
    panelapp_ids: list[int] | SelectOfScalar[int],
) -> SelectOfScalar[GenePanel]:
    """Return a list of gene panels that contain a gene from a PanelApp panel"""

    logger.warning("get_genepanels_including_panelapp() is not tested!")
    genepanels = (
        select(GenePanel)
        .join(GenePanelPanelAppPanel)
        .where(col(GenePanelPanelAppPanel.pa_panel_id).in_(panelapp_ids))
    )
    return genepanels


def get_genepanels_explicitely_containing_gene(
    hgnc_id: int,
) -> SelectOfScalar[GenePanel]:
    """Return a list of gene panels that contain explicitely a gene"""

    genepanels = (
        select(GenePanel)
        .join(GenePanelGene, GenePanel.id == GenePanelGene.genepanel_id)
        .where(
            col(GenePanelGene.hgnc_id) == hgnc_id,
        )
    )
    return genepanels


def get_genepanels_implicitely_containing_gene(
    hgnc_id: int,
) -> SelectOfScalar[GenePanel]:
    """Return list of gene panels containing panelapp panels containing the gene"""

    panel_app_panels_including_gene = get_panel_app_panels_including_gene(
        hgnc_id
    ).subquery()
    panel_app_ids: SelectOfScalar[int] = select(
        [panel_app_panels_including_gene.c.id]
    ).distinct()  # type: ignore

    genepanels_including_panelapp = get_genepanels_including_panelapp(panel_app_ids)
    return genepanels_including_panelapp


def get_genepanel_ids_exluding_gene(hgnc_id: int) -> SelectOfScalar[int]:
    """Return a list of gene panels that exclude a gene"""

    genepanel_ids = (
        select([GenePanel.id])
        .join(
            GenePanelExcludedGene,
            GenePanelExcludedGene.genepanel_id == GenePanel.id,
        )
        .where(
            col(GenePanelExcludedGene.hgnc_id) == hgnc_id,
        )
    )
    return genepanel_ids


def get_genepanels_containing_gene(hgnc_id: int) -> SelectOfScalar[GenePanel]:
    """Return a list of gene panels that contain a gene"""

    genepanels_explicitely_containing_gene = get_genepanels_explicitely_containing_gene(
        hgnc_id
    )

    genepanels_implicitely_containing_gene = get_genepanels_implicitely_containing_gene(
        hgnc_id
    )

    genepanels_candidates = genepanels_explicitely_containing_gene.union(
        genepanels_implicitely_containing_gene
    ).subquery()

    genepanel_ids_excluding_gene = get_genepanel_ids_exluding_gene(hgnc_id)

    # Exclude candidate panels that are excluding the gene
    return (
        select(GenePanel)
        .join(
            genepanels_candidates,
            genepanels_candidates.c.id == GenePanel.id,
        )
        .where(genepanels_candidates.c.id.notin_(genepanel_ids_excluding_gene))
    )


if __name__ == "__main__":
    from gpbuilder.datamodel.engine import get_session

    # Use cases
    with get_session() as session:
        print("### get_genepanel_containing_gene ###")
        for gp in session.exec(get_genepanels_containing_gene(12559)).all():
            print(f"Gene panel {gp.id}: {gp.name} contains gene 12559")

        print("### get_panel_app_panels_including_gene ###")
        for panel_app in session.exec(get_panel_app_panels_including_gene(18803)).all():
            print(
                f"PanelApp panel {panel_app.id}: {panel_app.name} contains gene 18803"
            )

        print("### get_panelapp_genes ###")
        print(
            f"Gene count in PanelApp panel 1: {len(session.exec(get_panelapp_genes([1])).all())}"
        )
