# Create mock data for testing
from datetime import datetime
from typing import Optional

import sqlmodel

from gpbuilder.cli.update_data.common import set_next_update_id
from gpbuilder.conftest import unique_index
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genepanel import (
    GenePanel,
    GenePanelExcludedGene,
    GenePanelGene,
    GenePanelPanelAppPanel,
    GenePanelRegion,
)
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel, PanelAppPanelGene
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.datamodel.utils.validators import chromosome_conversion_table

ASCII_aZ = [*range(65, 90 + 1), *range(97, 122 + 1)]


# TODO: account for indices greater than ascii chars length,
# e.g. accumulate chars: if n > len => 'AA...n'
def gen_shortname(n: int) -> str:
    return chr(ASCII_aZ[n % len(ASCII_aZ)])


def create_panels(num: int, session: sqlmodel.Session) -> list[GenePanel]:
    result = [
        GenePanel(
            id=None,
            name=f"Test panel {idx}",
            shortname=f"T{gen_shortname(idx)}",
            description="Description text",
            version="1.0.0-draft",
            updated=None,
        )
        for idx in unique_index(num)
    ]

    for genepanel in result:
        session.add(genepanel)

    return result


def create_panelapp_panels(num: int, session: sqlmodel.Session) -> list[PanelAppPanel]:
    update_id = set_next_update_id("panelapp", session)
    result = [
        PanelAppPanel(
            id=idx,
            name=f"Test panel {idx}",
            version="1.0.0",
            disease_group="Test disease group",
            disease_sub_group="Test disease sub group",
            version_created=datetime.now(),
            update_id=update_id,
        )
        for idx in unique_index(num)
    ]

    for panelapp_panel in result:
        session.add(panelapp_panel)

    return result


def create_genes(num: int, session: sqlmodel.Session) -> list[Gene]:
    update_id = set_next_update_id("gene", session)
    result = [
        Gene(
            hgnc_id=idx,
            symbol=f"GENE{idx}",
            update_id=update_id,
        )
        for idx in unique_index(num)
    ]

    for gene in result:
        session.add(gene)

    return result


def create_regions(num: int, session: sqlmodel.Session) -> list[Region]:
    result = [
        Region(
            id=None,
            ref_genome="GRCh37",
            chromosome=list(chromosome_conversion_table.keys())[
                idx % len(chromosome_conversion_table)
            ],  # type: ignore
            start=(10 * idx),
            end=(100 * idx),
        )
        for idx in unique_index(num)
    ]

    for region in result:
        session.add(region)

    return result


def create_transcripts(num: int, hgnc_id: int, session) -> list[Transcript]:
    result = []
    for idx in unique_index(num):
        # create transcripts with 1, 2 or 3 exons
        n_exons = idx % 3 + 1

        # Use index to create unique start and end positions
        start = 1000 * idx
        end = start + 1000
        exon_starts = [start + i * 100 for i in range(1, n_exons + 1)]
        exon_ends = [exon_start + 100 for exon_start in exon_starts]
        assert len(exon_starts) == len(exon_ends) == n_exons

        result.append(
            Transcript(
                id=None,
                ref_genome="GRCh37",
                chromosome="1",
                start=start + 100,
                end=end + 100,
                name=f"NM_{idx:05}.1",
                strand=["+", "-"][hgnc_id % 2],  # type: ignore
                source="test",
                hgnc_id=hgnc_id,
                coding_start=exon_starts[0] + 25,
                coding_end=exon_ends[-1] - 25,
                exon_starts=exon_starts,
                exon_ends=exon_ends,
            )
        )
    for tx in result:
        session.add(tx)
    return result


def link_panelapp_panel_gene(
    genes: list[Gene],
    panelapp_panel: PanelAppPanel,
    confidence_level: int,
    session: sqlmodel.Session,
) -> list[PanelAppPanelGene]:
    result = [
        PanelAppPanelGene(
            hgnc_id=gene.hgnc_id,
            panelapp_panel_id=panelapp_panel.id,
            confidence_level=confidence_level,
        )
        for gene in genes
    ]
    for panelapp_panel_gene in result:
        session.add(panelapp_panel_gene)

    return result


def link_genepanel_gene(
    gene: Gene,
    genepanel: GenePanel,
    session: sqlmodel.Session,
    manually_added=False,
    transcripts: Optional[list[Transcript]] = None,
    inheritance_mode: Optional[str] = "N/A",
) -> GenePanelGene:
    assert (
        genepanel.id
    ), "genepanel.id must be set (did you forget to run session.flush()?)"
    gene_panel_gene = GenePanelGene(
        id=None,
        hgnc_id=gene.hgnc_id,
        genepanel_id=genepanel.id,
        inheritance_mode=inheritance_mode,
        transcripts=transcripts if transcripts else [],
        manually_added=manually_added,
        comment=None,
    )
    session.add(gene_panel_gene)

    return gene_panel_gene


def link_genepanel_excluded_gene(
    genes: list[Gene], genepanel: GenePanel, session: sqlmodel.Session
) -> list[GenePanelExcludedGene]:
    assert (
        genepanel.id
    ), "genepanel.id must be set (did you forget to run session.flush()?)"
    result = [
        GenePanelExcludedGene(
            hgnc_id=gene.hgnc_id,
            genepanel_id=genepanel.id,
            exclusion_reasons=["Watchlist"],
            comment="FOOBAR",
        )
        for gene in genes
    ]
    for genepanel_excluded_gene in result:
        session.add(genepanel_excluded_gene)

    return result


def link_genepanel_panelapp_panel(
    panelapp_panels: list[PanelAppPanel],
    genepanel: GenePanel,
    session: sqlmodel.Session,
) -> list[GenePanelPanelAppPanel]:
    assert (
        genepanel.id
    ), "genepanel.id must be set (did you forget to run session.flush()?)"
    result = [
        GenePanelPanelAppPanel(
            genepanel_id=genepanel.id,
            pa_panel_id=panelapp_panel.id,
            confidence_levels=[1, 2, 3],
        )
        for panelapp_panel in panelapp_panels
    ]

    for genepanel_panelapp_panel in result:
        session.add(genepanel_panelapp_panel)

    return result


def link_genepanel_region(
    region: Region,
    genepanel: GenePanel,
    session: sqlmodel.Session,
    inheritance_mode: Optional[str] = "N/A",
) -> GenePanelRegion:
    assert (
        genepanel.id and region.id
    ), "genepanel.id and region.id must be set (did you forget to run session.flush()?)"
    genepanel_region = GenePanelRegion(
        genepanel_id=genepanel.id,
        region_id=region.id,
        comment=f"Region {region.id} comment",
        inheritance_mode=inheritance_mode,
    )
    session.add(genepanel_region)
    return genepanel_region
