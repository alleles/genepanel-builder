import secrets
from datetime import datetime, timedelta

from gpbuilder.oauth.store import SessionStoreSingleton

# TODO: override session ttl for testing

userdata = {
    "id_token": {
        "person_info": {
            "name": "test",
            "user": "test",
            "email": "test@example.com",
        }
    }
}


def test_unique_sessionstore():
    x = SessionStoreSingleton("tmp_1.db")
    y = SessionStoreSingleton("tmp_2.db")

    assert x is y
    assert x.filename is y.filename

    x._store.close()
    y._store.close()


def test_sign_session():
    x = SessionStoreSingleton("tmp_1.db")
    sid, expiry_string = x._sign(secrets.token_urlsafe(32))
    expiry = datetime.strptime(expiry_string, "%a, %d %b %Y %H:%M:%S GMT")

    before = expiry - timedelta(1)
    after = expiry + timedelta(minutes=1)

    assert expiry > before
    assert expiry < after

    x._store.close()


def test_validate_session():
    x = SessionStoreSingleton("tmp_1.db")
    session_id, expiry = x.create(userdata)

    assert x.is_valid(session_id) is True
    assert x.is_valid("") is False

    x._store.close()


def test_expire_session():
    x = SessionStoreSingleton("tmp_1.db")
    session_id, expiry = x.create(userdata)

    session_existing = x.fetch(session_id)

    x.expire(session_id)

    session_nonexisting = x.fetch(session_id)

    assert session_existing == userdata
    assert session_nonexisting is None
    assert x.is_valid(session_id) is False

    x._store.close()
