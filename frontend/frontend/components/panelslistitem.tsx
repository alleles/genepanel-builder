import Toggle from 'components/toggle'
import { useState } from 'react'
import { ConfidenceLevel, ConfidenceLevels, PanelAppPanel } from 'types'

export default function PanelsListItem({
  panel,
  onChange,
  children,
}: {
  panel: PanelAppPanel
  onChange: (levels: ConfidenceLevels) => void
  children: any
}) {
  const [includeGreen, setIncludeGreen] = useState(
    panel.confidence_levels[ConfidenceLevel.green]
  )
  const [includeAmber, setIncludeAmber] = useState(
    panel.confidence_levels[ConfidenceLevel.amber]
  )
  const [includeRed, setIncludeRed] = useState(
    panel.confidence_levels[ConfidenceLevel.red]
  )

  return (
    <li
      key={panel.id}
      className="grid grid-cols-10 border-t border-gray-300 py-3"
    >
      <span className="col-span-5 pl-2 pt-2 font-bold text-gray-500">
        {`${panel.name} - v${panel.version}`}
      </span>
      <span className="col-span-3 flex space-x-3 justify-self-end pt-1 text-xs">
        <span id={`${panel.id}-toggleGreen`}>
          <Toggle
            name={`${panel.id}-includeGreen`}
            label="Green"
            color="green"
            toggled={includeGreen}
            onChange={() => {
              onChange({
                ...panel.confidence_levels,
                [ConfidenceLevel.green]: !includeGreen,
              })
              setIncludeGreen(!includeGreen)
            }}
          />
        </span>
        <span id={`${panel.id}-toggleAmber`}>
          <Toggle
            name={`${panel.id}-includeAmber`}
            label="Amber"
            color="amber"
            toggled={includeAmber}
            onChange={() => {
              onChange({
                ...panel.confidence_levels,
                [ConfidenceLevel.amber]: !includeAmber,
              })
              setIncludeAmber(!includeAmber)
            }}
          />
        </span>
        <span id={`${panel.id}-toggleRed`}>
          <Toggle
            name={`${panel.id}-includeRed`}
            label="Red"
            color="red"
            toggled={includeRed}
            onChange={() => {
              onChange({
                ...panel.confidence_levels,
                [ConfidenceLevel.red]: !includeRed,
              })
              setIncludeRed(!includeRed)
            }}
          />
        </span>
      </span>
      <span className="col-span-2 w-32 justify-self-end">{children}</span>
    </li>
  )
}
