# User manual

This document explains how to use Gene panel builder (GPB) for end users. See also [About gene panels](./About_gene_panels.md) for more in-depth explanation of the underlying concepts.

## Get started

GPB consists of four main pages given in successive order:

- All gene panels (home page): Lists all existing gene panels, including drafts.
- [Create or modify a gene panel](#add-or-modify-name-and-description): Name and description of a gene panel.
- [Select genes and regions](#select-genes-and-regions): Add genes and/or regions to your gene panel.
- [Edit gene panel](#edit-configuration-of-genes-and-regions): Change configuration of individual genes and regions and submit the panel.

Log in using your provided credentials (unless GPB is [configured otherwise](/docs/Authentication_and_Authorization.md#running-in-production)). On the first page "All gene panels" you have two main options:

1. To view or modify an existing panel/draft, click the corresponding name in the list.
2. To create a new gene panel from scratch, click `Create New Panel`.

### Saving changes

GPB saves all changes when you navigate to another page or click the `Save` button in the top right corner (on relevant pages). There is no "undo" function other than the standard browser controls (`Ctrl/Command+Z`), but simply refreshing the page you are on will reset to the state at last save.

### Drafts and versions

If the process of creating or modifying a gene panel has been started, but not finished (submitted), the ongoing work will be saved as a draft, with the `-draft` tag appended to the version patch. By default, all new gene panels are given initial version `0.0.1-draft`, whereas new drafts from existing panels are versioned as a patch (e.g. `1.2.0 --> 1.2.1-draft`).

When a draft has been manually created from an existing panel, both the original and the new draft version will be listed on the front page (e.g. `1.2.0` and `1.2.1-draft`). After a draft has been [submitted](#submit-a-gene-panel), any previously _submitted_ versions of the panel are hidden, but can still be accessed by toggling the `Show inactive (old) gene panels` button:

![Show inactive toggle](img/show_inactive.png)

!!! Note

    After an [update to reference data](#changes-introduced-by-updates-to-reference-data) (gene names, transcripts, inheritance and PanelApp panels), most gene panel versions in existance prior to the update will no longer look exactly the same in GPB after it. This is because GPB always uses the most recent reference data when viewing a panel, and a reference update will most likely introduce changes.

    If needed, the exact, historical versions of all gene panels can be retrieved in their _exported_ form from the [Gene panel store](https://gitlab.com/alleles/genepanel-store) repository. See also [Changes introduced by updates to reference data](#changes-introduced-by-updates-to-reference-data).

To continue with a draft, simply click on it in the list, or choose `Delete` to remove it. Note that the latter will only remove the draft and not any submitted panel the draft was based upon.

Note also that it is possible to create multiple drafts from the same panel. These will be versioned with patch increments based on when they were created (e.g. `1.2.1-draft` and `1.2.2-draft`), and changes are _not_ reflected across the drafts, only relative to the original panel.

!!! Warning

    [Submitting](#submit-a-gene-panel) a panel will delete all other drafts, so take care to submit the correct version, and delete unnecessary/temporary drafts whenever possible.

## View an existing gene panel

When you select an existing gene panel from the list on the "All gene panels" page, you are first presented with a read-only view of that panel. If you want to make changes, click `Create New / Modify Draft` (depending on if you view a submitted panel or an ongoing draft).

## Add or modify name and description

Choosing either `Create New Panel` or `Create New / Modify Draft` you will be taken to the "Create or modify a gene panel" page. Here, you must provide a name and short name for the panel (if this does not already exist), and optionally a brief description.

!!! Note

    The short name is used for tracking versions and must be unique. After creation, it is not possible to change the short name in the GPB user interface. Should this be desirable, a request in this regard must be addressed to the system administrators.

Click `Continue ...` to proceed to the next page, or `Cancel ...` to leave without saving any changes.

## Select genes and regions

On the "Select genes and regions for ..." page, you can add genes through existing [Genomics England PanelApp panels](https://panelapp.genomicsengland.co.uk/) or directly as individual genes, as well as special regions not defined as a gene. Once you are done, click `Continue ...` to proceed to the next page.

### Add PanelApp panels

Using PanelApp panels has the advantage of subscribing your panel to subsequent updates from [Genomics England](https://panelapp.genomicsengland.co.uk/). This means that, when updating the gene panel, any genes added to or removed from the PanelApp panel will also be added/removed from your gene panel.

To add a PanelApp panel, search using the ID or name, select it from the results and click `Add`. The panel will then be added to a list below the search box.

By default, only genes marked with review status "Green" will be added, but in the list you have the option to also add genes with status "Amber" and "Red":

![PanelApp toggle](img/panelapp_toggle.png)

This can be changed at any time by going back to the "Select genes and regions for ..." page.

To remove a PanelApp panel, click `Remove` to the right in the list.

### Add individual genes

Instead of using existing PanelApp panels, you also have the option of adding individual genes, either by search or as a list:

- To add an individual gene, search using a gene symbol (existing or alias) or HGNC ID. Select a result in the dropdown and click `Add` to add it to your gene panel.
- To add multiple genes, paste a list of genes into the corresponding box and clicking `Add List`. Note that the list must contain HGNC IDs only, not gene symbols.

To remove a manually added gene, click `Remove` to the right in the list.

!!! Note

    PanelApp panels always take precedence. If you try to add an individual gene that is already added through a PanelApp panel, the gene will not be listed under "added genes" on this page when you save. Note that the gene will still be included through the PanelApp association.

### Add regions

If you have a region of interest that isn't contained within the definition and transcript of a gene, you can add any genomic region to the gene panel here. Provide a descriptive name and genomic coordinates and click `Add`.

## Edit configuration of genes and regions

On the "Edit gene panel: ..." page, you can change several parameters for each added gene or region:

- [Inheritance mode](#inheritance-mode)
- [Default transcript (genes only)](#default-transcript-genes-only)
- [Exclusion (genes only)](#exclusion-genes-only)
- [Comment](#comment)

### Configuration and order of precedence

For both inheritance mode and default transcripts, GPB applies configuration in the following order of precedence:

1. Manually set in the present gene panel, using GPB.
2. Gene default configuration: Applies across all gene panels. Note this can currently only be changed by a system administrator.
3. Automatic using external sources (if available)
4. Fallback values

!!! Note

    Configuration set in either the gene panel or the gene defaults will be retained through subsequent gene panel updates, with two exceptions: 
    
    1. If a configured inheritance mode is equal to that retrieved from OMIM for a gene, subsequent updates will use OMIM values. 
    2. If a configured transcript is equal to that retrieved from MANE Select, subsequent updates will use values from MANE Select.

### Inheritance mode

GPB automatically populates the inheritance modes for each gene, setting one of:

- `AD` (autosomal dominant)
- `AR` (autosomal recessive)
- `AD/AR` (autosomal; indeterminate)
- `XD` (X-linked dominant)
- `XR` (X-linked recessive)
- `XD/XR` (X-linked; indeterminate)
- `SMu` (somatic mutation)
- `N/A` (not available)

Unless a default or gene panel-specific configuration has been specified, these values are based on a sanitized sum of inheritance modes reported in [OMIM](https://www.omim.org/). If no valid inheritance modes are available from OMIM, the fallback value `N/A` will be used. Note also that `SMu` is never populated automatically, but can be set manually.

To override a given value for the current gene panel, select a new value from the dropdown. It is recommended to add a [comment](#comment) when doing this.

!!! Note

    All X-linked inheritance modes from OMIM, including "pure" `XD` and `XR`, are translated to `XD/XR` in GPB. This is due to the many uncertainties with treating genes on X as dominant or recessive. Note that it is still possible to overrule GPB's choice by setting `XD` or `XR` in the gene panel or the default gene config.

### Default transcript (genes only)

Transcripts are marked with with tags when available:

- `Default`: Specified in the gene default configuration.
- `MANE Select`: Transcript selected by the [MANE project](https://www.ncbi.nlm.nih.gov/refseq/MANE/).
- `MANE Plus Clinical`: Additional transcript when MANE Select transcript does not cover all clinically relevant variants.

GPB automatically chooses the transcript with the `Default` tag, otherwise the `MANE Select` and `MANE Plus Clinical` transcripts. If no MANE entry is available, GPB chooses the RefSeq transcript with the longest coding frame (if coding), otherwise longest transcription frame.

To override these settings for the current gene panel, select a different transcript in the dropdown. Note that you can select more than one default transcript per gene. It is recommended to add a [comment](#comment) when making changes here.

If a change has been made for the current gene panel (relative to the automatic choice), the cell will be shaded with light green. To filter the column to only show rows where the transcript has changed, click the `Modified only` checkbox:

![Modified transcripts only](img/modified_transcript.png)

### Exclusion (genes only)

Individual genes can be excluded from use in the gene panel without removing it completely. This is the only way of excluding genes added through PanelApp panels (without also removing the PanelApp panel).

You can select multiple reasons for exclusion; note the "Watchlist" option is useful if you want mark a gene for special attention in later revisions; use the search box to quickly locate genes with this label.

### Comment

Add any notes useful for later revisions here.

### Informational columns

The other columns listed in the tables on this page are:

- HGNC ID (genes only)
- Symbol (genes only): Latest symbol available.
- Coverage: The average percentage of base pairs in the currently selected transcript(s) that are covered with reads above a given threshold (e.g. >10x reads) by a given sequencing method (e.g. WGS or WES).
  - Exact values will depend on the [configuration](./Coverage_segdup.md#configure-display-of-coverage-and-segdup-in-the-frontend), which decides if only coding regions are included and whether to include a given numer of bases extending into the introns.
  - Low numbers (e.g. <0.90) is indicative that variant calls for (parts of) this transcript will be absent or of low quality for the given method.
- SegDup: The average percentage of exonic base pairs in the currently selected transcript(s) that are covered by a known segmental duplication (as defined by [UCSC](http://genome.ucsc.edu/cgi-bin/hgTables?hgta_doSchemaDb=hg19&hgta_doSchemaTable=genomicSuperDups)), with homology exceeding a given threshold (e.g. >=90 % homology).
  - Exact values will depend on the [configuration](./Coverage_segdup.md#configure-display-of-coverage-and-segdup-in-the-frontend), as for coverage.
  - Any number >0 here indicates that the transcript is likely to have, at least in certain portions, poor variant calling quality.
- Origin: If the gene was added through one or more PanelApp panels, the PanelApp ID(s) and review status for the gene in each panel is shown here. The panel can be found on Genomics England PanelApp's webpages using the digits in the ID at the end of a panel URL, e.g. https://panelapp.genomicsengland.co.uk/panels/1207/.

!!! Tip

    Detailed information about SegDups (using the same data as GPB) can be found in the [UCSC Genome Browser](https://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19); open the link and turn on the Segmental Dups track under Repeats. Searching by the transcript reference or gene symbol will give you an overview of any SegDups covering the area, including degree of homology.

## Submit a gene panel

When done with editing a gene panel, click `Submit Gene Panel`. By default, the gene panel will get a minor version update (e.g. `1.0.0 --> 1.1.0`), but you may also choose to make it a major version update (e.g. `1.3.0 --> 2.0.0`). Click `Confirm` to finish the process. See also [Drafts and versions](#drafts-and-versions).

!!! Note

    Deciding between major or minor version update depends on the respective internal procedure, but as a rule-of-thumb, any update that changes the gene content of the panel should be considered a major update.

    It is recommended that new, production ready gene panels are submitted as a major version update with version `1.0.0`.

## Changes introduced by updates to reference data

Reference data in GPB for [gene names](https://www.genenames.org/), [MANE transcripts](https://www.ncbi.nlm.nih.gov/refseq/MANE/), [OMIM inheritance](https://www.omim.org/) and [Genomics England PanelApp panels](https://panelapp.genomicsengland.co.uk/) are downloaded in a version controlled cache and not fetched directly from each source. This avoids unexpected changes to gene panels when viewing them at different points in time, as GPB always loads the most recent reference data when viewing a gene panel. However, this also means that these references are only as up-to-date as the last time the cache was updated.

!!! Note

    To keep gene panels reasonably up-to-date with the latest reference data it is recommended that your system administrator performs reference updates relatively frequently, e.g. quarterly. Frequent updates also reduce the amount of changes that will have to be reviewed by GPB users after each update.

### Reviewing changes after a reference update

Updating reference data is likely to introduce changes to many of the existing gene panels; some [update scenario examples](#update-scenarios) are given at the end of this document.

To aid review of these changes, any gene panel with a change caused by the update will be changed to a draft with a version bump (e.g. `1.0.0 --> 1.0.1-draft`). Changes for all panels are summarized in a table _diff_ file with the following columns:

| Column              | Explanation                                                                |
| :------------------ | :------------------------------------------------------------------------- |
| Panel name          | Gene panel short name                                                      |
| Panel version       | New (draft) version of panel                                               |
| Panel version (old) | Old version of gene panel (current version in production or ongoing draft) |
| HGNC ID             | HGNC ID                                                                    |
| Gene symbol (new)   | Gene symbol (updated)                                                      |
| Gene symbol (old)   | Previous gene symbol (if different or removed)                             |
| Transcript (new)    | New transcript (if different or new gene)                                  |
| Transcript (old)    | Previous transcript (if different or removed gene)                         |
| Inheritance (new)   | New inheritance (if different or new gene)                                 |
| Inheritance (old)   | Previous inheritance (if different or removed gene)                        |
| Change type         | `Gene: NEW` (if gene was added)                                            |
|                     | `Gene: REMOVED` (if gene was removed)                                      |
|                     | `New inheritance` (if inheritance changed)                                 |
|                     | `New transcript` (if transcript changed)                                   |
|                     | `New gene symbol` (if gene symbol changed)                                 |

Use the file to review all changes and make any necessary manual overrides in each corresponding draft in GPB before submitting. The last column in the diff file, "Change type", is especially useful for sorting or filtering by the severity of changes. For example, changes to inheritance, transcripts and gene names can be summarily acknowledged, while changes to gene content (`Gene: NEW` or `Gene: REMOVED`) should be reviewed more thoroughly. See also [Auto-submission of minor updates](#auto-submission-of-minor-updates) and [Avoid major update](#avoid-major-update).

!!! Warning

    After performing a reference update, _all_ gene panels with a newly created draft should be submitted to keep configurations consistent across panels (created with command line tool). Otherwise, a gene that appears in multiple gene panels may end up with unwanted configuration differences across different panels.

### Ongoing drafts and reference updates

All gene panels in GPB can be checked for changes when performing a reference update, including any manually created, ongoing drafts. It is recommended to run the CLI command to create new drafts based on changes. If both the original and the ongoing draft have changes, _two_ new drafts will be created. Note that "drafts of drafts" (i.e., based on the ongoing draft and not the original version) will always have the highest version number. Examples:

| Scenario                                          | Before       | After        |
| :------------------------------------------------ | :----------- | :----------- |
| Submitted only, with changes                      | v1.0.0       | v1.0.1-draft |
| Submitted + manual draft (both have changes)      | v1.0.0       | v1.0.2-draft |
|                                                   | v1.0.1-draft | v1.0.3-draft |
| Submitted + manual draft (only draft has changes) | v1.0.0       | v1.0.0       |
|                                                   | v1.0.1-draft | v1.0.2-draft |

!!! Warning

    If there are multiple drafts of a panel, to prevent confusion it is recommended to manually delete the draft(s) you no longer need or plan to work on in GPB (see [Drafts and versions](#drafts-and-versions)). However, to ensure consistency across gene panels, _make sure that at least one draft is actually submitted_.

    Note that submission of any draft will automatically delete all other drafts of that panel (see [Drafts and versions](#drafts-and-versions)).

### Auto-submission of minor updates

When performing reference updates, your system administrator has the option to automatically submit panels that only include "minor" changes, defined as updates to gene names, transcripts or inheritance. This corresponds to `New inheritance`, `New transcript` and `New gene symbol` in the "Change type" column of the [diff file](#reviewing-changes-after-a-reference-update), and reduces the amount of manual review needed after each update.

When an update introduces changes to gene content (usually defined as a "major" update), the new version still has to be reviewed and submitted manually in GPB.

### Avoid major update

Updates to PanelApp panels may introduce changes to the gene content (genes added or removed). This usually requires a major version update of the gene panel. If you want to avoid this (and keep only "minor" changes), you can make the following changes to the gene panel in GPB before submitting the draft:

- Exclude genes that have been added through the PanelApp panel, e.g. using the "Watchlist" option (see [Exclusion](#exclusion-genes-only)).
- Manually add genes that have been removed from the PanelApp panel and make a note that will allow you to easily identify these genes if you plan to exclude them at a later timepoint.

### Update scenarios

Some examples of how to handle different update scenarios are given below. Note that these are only examples and that the exact procedure will depend on internal procedures and preferences.

Definitions for all scenarios:

- Last production deployment: The last time a gene panel was exported and deployed to production.
- Update reference data: A system administrator updates reference data (gene names, transcripts, inheritance and PanelApp panels) in GPB.
- Review: A gene panel curator reviews changes introduced by a reference update and makes any necessary changes to the gene panel in GPB.
- Submit: A gene panel curator submits a gene panel for production.
- Production deployment: A system administrator exports and deploys a gene panel to production.

#### Scenario A: No changes from reference update

In this scenario, typical for small gene panels, references have not changed for any of the included genes since the last production deployment, and the last exported panel will be kept as is. No action is required.

![Scenario A](img/update_scenarioA.png)

#### Scenario B: Reference update changes panel

In this scenario, the reference update introduces changes to a gene panel. A new draft is created and a gene panel curator reviews the changes and submits the draft for production.

![Scenario B](img/update_scenarioB.png)

#### Scenario C: Reference update changes both draft and original panel

In this scenario, the reference update introduces changes to both the original panel and an ongoing draft of the same panel. Two new drafts are created and a gene panel curator reviews the changes and submits _one of the drafts_ for production; in this case, the draft based on the ongoing draft.

![Scenario C](img/update_scenarioC.png)

#### Scenario D: Submit a changed panel in-between reference data updates

In this scenario, a gene panel curator submits a new version of a gene panel in-between reference data updates. No review is necessary, but the exported files will be accompanied by a diff file for documentation purposes.

![Scenario D](img/update_scenarioD.png)
