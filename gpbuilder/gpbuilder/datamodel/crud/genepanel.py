import logging
from datetime import datetime

import semver
from sqlalchemy.exc import NoResultFound
from sqlmodel import Session, Text, cast, col, delete, func, select, update

from gpbuilder.datamodel.crud.models import GenePanelConfig
from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genepanel import (
    GenePanel,
    GenePanelExcludedGene,
    GenePanelGene,
    GenePanelGeneTranscript,
    GenePanelPanelAppPanel,
    GenePanelRegion,
)
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import Transcript

logger = logging.getLogger(__name__)


class ShortnameExistsException(Exception):
    pass


class CreatePanelException(Exception):
    pass


class DeletePanelException(Exception):
    pass


class UpdatePanelException(Exception):
    pass


class VersionException(Exception):
    pass


def _short_name_exists(shortname: str) -> bool:
    with get_session() as session:
        genepanel_count = session.exec(
            select(func.count(GenePanel.id)).where(  # type: ignore
                GenePanel.shortname == shortname,
            )
        ).one()
    return genepanel_count != 0


def next_version(session: Session, shortname) -> str:
    # Get max version for this genepanel
    max_version = session.exec(
        select(GenePanel.version)  # type: ignore
        .where(
            GenePanel.shortname == shortname,
        )
        .order_by(col(GenePanel.version).desc())
    ).first()

    if max_version:
        next_version = semver.bump_patch(max_version) + "-draft"
    else:
        next_version = "0.0.1-draft"
    return next_version


def create_panel(name, shortname, description) -> GenePanel:
    # Check that shortname is not already used
    if _short_name_exists(shortname):
        raise ShortnameExistsException("shortname-already-exists")

    with get_session() as session:
        new_version = next_version(session, shortname)

    # Create new panel
    genepanel = GenePanel(
        name=name,
        shortname=shortname,
        description=description,
        version=new_version,
    )
    return genepanel


def create_from_panel(session: Session, id: int) -> GenePanel:
    existing_panel = session.get(GenePanel, id)
    if existing_panel is None:
        raise CreatePanelException("Panel not found")

    new_version = next_version(session, existing_panel.shortname)

    # Create new panel
    genepanel = GenePanel(
        name=existing_panel.name,
        shortname=existing_panel.shortname,
        description=existing_panel.description,
        version=new_version,
    )
    session.add(genepanel)
    session.flush()

    # Copy all genes, regions, excluded genes, panelapp panels
    # TODO: Rewrite as a utility function to deepcopy all relationships + nested relationsships
    for tbl in [
        GenePanelRegion,
        GenePanelExcludedGene,
        GenePanelPanelAppPanel,
    ]:
        rows = session.exec(
            select(tbl).where(tbl.genepanel_id == existing_panel.id)
        ).all()

        for row in rows:
            copy = tbl(**row.dict())
            if hasattr(copy, "id"):
                copy.id = None
            copy.genepanel_id = genepanel.id
            session.add(copy)

    # Special treatment for genes
    rows = session.exec(
        select(GenePanelGene).where(GenePanelGene.genepanel_id == existing_panel.id)
    ).all()

    for row in rows:
        copy = GenePanelGene(**row.dict())
        if hasattr(copy, "id"):
            copy.id = None
        copy.genepanel_id = genepanel.id
        session.add(copy)
        session.flush()
        gpg_tx = session.exec(
            select(GenePanelGeneTranscript).where(
                GenePanelGeneTranscript.genepanel_gene_id == row.id
            )
        ).all()
        for tx in gpg_tx:
            copy_tx = GenePanelGeneTranscript(**tx.dict())
            copy_tx.genepanel_gene_id = copy.id
            session.add(copy_tx)
    session.flush()
    return genepanel


def update_panel(
    session: Session, genepanel: GenePanel, genepanel_config: GenePanelConfig
):
    if genepanel_config.shortname != genepanel.shortname:
        raise UpdatePanelException("Can't change shortname")

    logger.info(f"Updating panel {genepanel}")

    genepanel.name = genepanel_config.name
    genepanel.description = genepanel_config.description
    genepanel.updated = datetime.utcnow()

    # Clear genepanel before adding everything from the config
    _clear_genepanel(session, genepanel.id)  # type: ignore

    logger.debug(f"Adding PanelApp panels for {genepanel}")
    db_pa_panels_by_id: dict[int, PanelAppPanel] = {
        pa_panel.id: pa_panel
        for pa_panel in session.exec(
            select(PanelAppPanel).where(
                col(PanelAppPanel.id).in_(
                    [panel.panelapp_id for panel in genepanel_config.panelapp_panels]
                )
            )
        ).all()
    }
    panelapp_hgnc_ids: set[int] = set()
    for panelapp_panel in genepanel_config.panelapp_panels:
        # Track which genes are valid in the diff
        genes = db_pa_panels_by_id[
            panelapp_panel.panelapp_id
        ].get_genes_from_confidence_levels(session, panelapp_panel.confidence_levels)
        panelapp_hgnc_ids |= set(gene.hgnc_id for gene in genes)

        # Add PanelApp panel to genepanel
        session.add(
            GenePanelPanelAppPanel(
                genepanel_id=genepanel.id,  # type: ignore
                pa_panel_id=panelapp_panel.panelapp_id,
                confidence_levels=[
                    level
                    for level, included in panelapp_panel.confidence_levels.items()
                    if included
                ],
            )
        )

    # Genes that are manually added, but are also part of a specified PanelApp panel, should be
    # tied to the PanelApp panel and not treated as manually added
    manually_added_hgnc_ids = set(genepanel_config.hgnc_ids)
    for hgnc_id in set(manually_added_hgnc_ids):
        # Check if they are already added by a PanelApp panel, if so, skip
        contained_in_panelapp_panel = hgnc_id in panelapp_hgnc_ids
        if contained_in_panelapp_panel > 0:
            manually_added_hgnc_ids.discard(hgnc_id)

    logger.debug(f"Adding genes with changes for {genepanel}")
    # Track which genes are valid in the diff, and which transcripts and inheritance modes are
    # actually differing from the default values
    valid_hgnc_ids = panelapp_hgnc_ids | manually_added_hgnc_ids
    default_inheritance = Gene.get_default_inheritance_by_hgnc_ids(
        session, list(valid_hgnc_ids)
    )
    default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(
        session, list(valid_hgnc_ids)
    )

    for hgnc_id, gene in genepanel_config.diff.items():
        if hgnc_id not in valid_hgnc_ids:
            logger.warning(
                f"Gene with HGNC ID {hgnc_id} will be removed from the gene panel because it isn't"
                " part of any PanelApp panel and is not manually included."
            )
            continue
        if gene.exclusion_reasons:
            session.add(
                GenePanelExcludedGene(
                    hgnc_id=hgnc_id,
                    genepanel_id=genepanel.id,  # type: ignore
                    exclusion_reasons=gene.exclusion_reasons,
                    manually_added=hgnc_id in manually_added_hgnc_ids,
                    comment=gene.comment,
                )
            )
            # Manually added gene added to the db
            manually_added_hgnc_ids.discard(hgnc_id)
        else:
            if set(gene.transcripts) == set(
                [tx.name for tx in default_transcripts[hgnc_id]]
            ):
                gene.transcripts = []
            if gene.inheritance == default_inheritance[hgnc_id]:
                gene.inheritance = None

            if (
                gene.inheritance is None
                and not gene.transcripts
                and not gene.comment
                and hgnc_id not in manually_added_hgnc_ids
            ):
                logger.warning(
                    f"Gene with hgnc_id {hgnc_id} will not be added as a gene panel gene, since "
                    f"it has no changes."
                )
                continue

            if gene.transcripts:
                tx = session.exec(
                    select(Transcript).where(col(Transcript.name).in_(gene.transcripts))
                ).all()
            else:
                tx = []
            session.add(
                GenePanelGene(
                    hgnc_id=hgnc_id,
                    genepanel_id=genepanel.id,  # type: ignore
                    inheritance_mode=gene.inheritance,
                    transcripts=tx,
                    manually_added=hgnc_id in manually_added_hgnc_ids,
                    comment=gene.comment,
                )
            )
            # Manually added gene added to the db
            manually_added_hgnc_ids.discard(hgnc_id)

    logger.debug(f"Adding manually added genes for {genepanel}")
    for hgnc_id in manually_added_hgnc_ids:
        # Check if they are already added by a PanelApp panel, if so, skip
        session.add(
            GenePanelGene(
                hgnc_id=hgnc_id,
                genepanel_id=genepanel.id,  # type: ignore
                manually_added=True,
            )
        )

    logger.debug(f"Adding regions for {genepanel}")
    # Add regions + gene panel regions
    for region in genepanel_config.regions:
        db_region = session.exec(
            select(Region).where(
                Region.ref_genome == region.ref_genome,
                Region.chromosome == region.chromosome,
                Region.start == region.start,
                Region.end == region.end,
            )
        ).one_or_none()
        if not db_region:
            db_region = Region(
                **region.dict(exclude={"comment", "inheritance", "name"})
            )
            session.add(db_region)
            session.flush()

        session.add(
            GenePanelRegion(
                genepanel_id=genepanel.id,  # type: ignore
                region_id=db_region.id,  # type: ignore
                inheritance_mode=region.inheritance,
                name=region.name,
                comment=region.comment,
            )
        )

    return genepanel


def delete_panel(session: Session, id: int):
    # Same as current delete endpoint
    try:
        panel = session.exec(select(GenePanel).where(GenePanel.id == id)).one()
    except NoResultFound:
        raise DeletePanelException("Gene panel not found")

    if "draft" not in panel.version:
        raise DeletePanelException("Cannot delete non-draft gene panel.")

    session.exec(delete(GenePanel).where(GenePanel.id == id))  # type: ignore


def _clear_genepanel(session: Session, id: int) -> None:
    # Delete all existing genes, regions and PanelApp panels
    for tbl in [
        GenePanelGene,
        GenePanelExcludedGene,
        GenePanelRegion,
        GenePanelPanelAppPanel,
    ]:
        session.exec(delete(tbl).where(tbl.genepanel_id == id))  # type: ignore


def release_panel(
    session: Session,
    genepanel: GenePanel,
    to_version: str,
):
    # Check version
    if to_version.endswith("draft"):
        raise VersionException(f"Version {to_version} is not a release version")

    if not genepanel.version.endswith("draft"):
        raise VersionException(
            f"Gene panel {genepanel.shortname} is already released (version {genepanel.version})."
        )

    # Check that version specified makes sense (e.g. not skipping versions)
    valid_versions = [
        str(semver.VersionInfo.parse(genepanel.version).next_version(x))
        for x in ["major", "minor"]
    ]
    if to_version not in valid_versions:
        raise VersionException(
            f"Invalid version specified {to_version} for {genepanel.version}. "
            f"Valid versions are {valid_versions}"
        )
    # Delete older drafts
    ids_to_delete = session.exec(
        select(GenePanel.id).where(  # type: ignore
            GenePanel.id != genepanel.id,
            GenePanel.shortname == genepanel.shortname,
            cast(col(GenePanel.version), Text).endswith("draft"),
        )
    ).all()
    session.exec(delete(GenePanel).where(col(GenePanel.id).in_(ids_to_delete)))  # type: ignore

    # Make the rest inactive
    session.exec(
        update(GenePanel)
        .where(GenePanel.id != genepanel.id, GenePanel.shortname == genepanel.shortname)
        .values(inactive=True)
    )

    # Update version
    genepanel.version = to_version

    return genepanel
