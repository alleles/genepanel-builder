from functools import cached_property

import pydantic
from sqlalchemy.dialects import postgresql
from sqlmodel import Column, Field, SQLModel

from gpbuilder.datamodel.utils.types import Chromosome, ReferenceGenome
from gpbuilder.datamodel.utils.validators import chromosome_validator


class SQLModelBase(SQLModel):
    "SQLModel with enforced validation"

    def __init__(self, **data) -> None:
        "Explicitly run validation if data is provided and table=True"

        # SQLModel turns off validation when subclassed with table=True
        # If table=False, then validation happens as expected
        is_table = getattr(self.__config__, "table", False)
        if not is_table:
            return super().__init__(**data)

        # TLDR; Skip validation if no data provided - assume this is intended
        # Details: If called from SQLModel.from_orm, this creates an intermediate empty object,
        # which will cause validation to fail. This will not break validation in from_orm,
        # as this is handled separately (sqlmodel/main.py:L551)
        if data:
            _, _, validation_error = pydantic.validate_model(self.__class__, data)
            if validation_error:
                raise validation_error
        return super().__init__(**data)

    def __repr__(self):
        return (
            self.__class__.__name__
            + "("
            + ", ".join(f"{k}={getattr(self, k)}" for k in sorted(self.__fields__))
            + ")"
        )

    def __str__(self):
        return repr(self)

    class Config:
        extra = "forbid"
        keep_untouched = (cached_property,)


class GenomicFeature(SQLModelBase):
    """Feature coordinates are 0-based half-open: [start, end)"""

    ref_genome: ReferenceGenome = Field(sa_column=Column(postgresql.TEXT()))
    chromosome: Chromosome = Field(sa_column=Column(postgresql.TEXT()))
    start: int
    end: int

    @pydantic.validator("chromosome", pre=True)
    def validate_chromosome(cls, v: str):
        return chromosome_validator(v)

    @pydantic.root_validator
    def validate_start_end(cls, values):
        start = values.get("start")
        end = values.get("end")
        assert 0 < start < end, "Start and end must be positive and start < end"
        return values
