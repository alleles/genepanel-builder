Feature: Submit a new or modified gene panel

    As a user I should be able to submit a newly created or modified gene panel.

    Background:
        Given initial state
        Given a browser

        When I visit "/"
        And I press "New Gene Panel"
        # Add name and short name
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I press "Continue (Select)"
        # Populate table
        And I add panel "Familial breast cancer"
        And I add gene "COPA"
        And I add region "chr7:1000-10000"
        And I press "Continue (Edit)"
        Then I should see "BRCA1" within 15 seconds

    Scenario: Submit a new panel as minor version update

        # Submit the panel with default (minor update)
        When I submit the gene panel
        Then I should see "All gene panels" within 5 seconds
        And I should see "Test gene panel" within 5 seconds
        And the number of genes in "testpanel" should be 11
        And the version of "testpanel" should be "0.1.0"

    Scenario: Submit a new panel as major version update

        # Submit the panel with optional major update
        When I submit the gene panel with version "1.0.0"
        Then I should see "All gene panels" within 5 seconds
        And I should see "Test gene panel" within 5 seconds
        And the number of genes in "testpanel" should be 11
        And the version of "testpanel" should be "1.0.0"


