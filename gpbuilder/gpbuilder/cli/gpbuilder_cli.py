#!/usr/bin/env python3
import logging
import os
import sys
from pathlib import Path
from types import SimpleNamespace
from typing import Optional

import click
import sqlmodel
import typer

from gpbuilder.cli import db, export_gp, import_gp, update
from gpbuilder.cli.diff import diff
from gpbuilder.datamodel.engine import engine

DEVELOPMENT: Optional[str] = os.getenv("DEVELOPMENT")


app = typer.Typer(pretty_exceptions_enable=False)


if DEVELOPMENT == "true":
    app.add_typer(db.app)
app.add_typer(diff.app, name="diff")
app.add_typer(export_gp.app, name="export")
app.add_typer(import_gp.app, name="import")
app.add_typer(update.app, name="update")


class CLILogFormatter(logging.Formatter):
    def format(self, record):
        ctx = click.get_current_context()
        clicommand = ctx.info_name
        while ctx.parent is not None:
            clicommand = f"{ctx.parent.info_name} {clicommand}"
            ctx = ctx.parent
        setattr(record, "clicommand", clicommand)
        return super().format(record)


@click.pass_context
def postcmd(ctx: typer.Context, *args, dry_run, **kwargs):
    "Result callback for all commands - will be called after any command is executed"
    # Rollback session if dry_run is True, otherwise commit
    logger = logging.getLogger()
    if dry_run:
        logger.info("Dry run - rolling back changes")
        ctx.obj.session.rollback()
    else:
        logger.debug("Commiting changes (if any) to database")
        ctx.obj.session.commit()
    ctx.obj.session.close()


@app.callback(result_callback=postcmd)
def gpb(
    ctx: typer.Context,
    skip_confirmation: bool = typer.Option(
        False, "--yes", "-y", help="Skip confirmation"
    ),
    verbose: bool = typer.Option(
        False,
        "--verbose",
        "-v",
        help="Write additional debug messages to log",
    ),
    dry_run: bool = typer.Option(
        False,
        "--dry-run",
        "-d",
        help="Do not write to database",
    ),
):
    """
    Command line interface to the genepanel builder database
    """
    # Create namespace for context, available in any subcommand
    # Provide a session here, that can be rolled back in postcmd if dry_run is True
    #
    # NOTE: This session should be used in all subcommands, and no command should commit anything to the database
    # This is done in postcmd (flush is OK)
    ctx.obj = SimpleNamespace(
        skip_confirmation=skip_confirmation,
        verbose=verbose,
        session=sqlmodel.Session(engine, expire_on_commit=False),
        dry_run=dry_run,
    )

    # Initialize logging to file
    log_dir = Path(os.environ["LOGS_DIR"])
    log_dir.mkdir(parents=True, exist_ok=True)
    log_file = log_dir / "gpb-cli.log"

    logger = logging.getLogger()
    if verbose:
        logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(log_file, "a")
    formatter = CLILogFormatter(
        "%(asctime)s [%(clicommand)s:%(levelname)s]: %(message)s",
        "%Y-%m-%d %H:%M:%S",
    )
    handler.setFormatter(formatter)
    logger.info(f"Adding log output to {log_file}")
    logger.addHandler(handler)

    logger.info(f"Invoking '{' '.join(sys.argv)}'")


if __name__ == "__main__":
    app()
