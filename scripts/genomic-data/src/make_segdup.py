#!/usr/bin/env python3

"""
Creates a file with non-overlapping segmental duplication 
"""

import argparse
import subprocess
import sys
from pathlib import Path

import mysql.connector
from lib.commands import run_command


def download_table(
    host: str, database: str, user: str, password: str, output=Path(".")
) -> Path:
    """Download repeated regions for homo sapiens from UCSC"""

    print("Downloading segmental duplications")

    # Connecting from the server
    conn = mysql.connector.connect(
        host=host, user=user, database=database, password=password
    )

    query = "SELECT chrom, chromStart, chromEnd, fracMatch FROM genomicSuperDups"
    cursorObject = conn.cursor()
    cursorObject.execute(query)
    table = cursorObject.fetchall()

    segdup_file_path = output / "segdup.bed"
    with open(segdup_file_path, "w") as segdup_file:
        segdup_file.writelines(map(lambda row: "\t".join(map(str, row)) + "\n", table))

    conn.close()

    print(f"Saved table at {segdup_file_path}")
    return segdup_file_path


def round_fracmatch(line: str) -> str:
    """Correct format of lines in the bed file to match our convention and D4.

    - Rename chromosomes from chr# to #: e.g. chr12 --> 12
    - Round frac match values to integers. Drops percentage decimals and become integers
      in the range 1 and 100.

    """

    if not line:
        return line
    fields: list[str] = line.split()
    assert len(fields) == 4
    fields[0] = fields[0][3:]
    fields[3] = str(int(float(fields[3]) * 100))
    return "\t".join(fields)


def set_max_fracMatch_per_base(
    segdup_file_path: Path, valid_chromosomes: list[str]
) -> Path:
    cmd: list[str] = [
        "bedtools",
        "merge",
        "-c",
        "4",
        "-o",
        "max",
        "-i",
        str(segdup_file_path),
    ]
    print(f"Running \"{' '.join(cmd)}\"")
    try:
        s = subprocess.run(cmd, check=True, encoding="utf-8", capture_output=True)
    except subprocess.CalledProcessError as e:
        print(
            f"""Failed to run bedtools to merge overlapping
                regions in segdup file.\n{e.stderr}""",
            file=sys.stderr,
        )
        sys.exit(-1)

    merged_bed_file_path = Path(
        segdup_file_path.resolve().parent / "segdup_max_per_base.bedgraph"
    )
    with open(merged_bed_file_path, "w") as merged_bed_file:
        for line in map(round_fracmatch, s.stdout.split("\n")):
            if line.split("\t")[0] not in valid_chromosomes:
                continue
            merged_bed_file.write(line + "\n")
    print(f"Saved max fracmatch per base at {merged_bed_file_path}")
    return merged_bed_file_path


def make_segdup_file(
    host: str,
    database: str,
    user: str,
    password: str,
    genome: Path,
    output: Path,
):
    """Creates non-overlapping segment duplication file in D4 format"""

    if not output.exists():
        output.mkdir()

    # Download data from UCSC: 1 table bed file

    segdup_file_path: Path = output / "segdup.bed"
    if not segdup_file_path.exists():
        download_table(host, database, user, password, output)

    # Merge overlapping regions: creates a new bed file
    with open(genome, "r") as g:
        valid_chroms = [line.split("\t")[0] for line in g.readlines() if line]
    per_base_segdup_file_path = set_max_fracMatch_per_base(
        segdup_file_path, valid_chroms
    )

    # Convert .bed to .d4
    d4_file_path: Path = output / "segdup.d4"
    cmd: list[str] = [
        "d4tools",
        "create",
        "-z",
        "-g",
        str(genome),
        str(per_base_segdup_file_path),
        str(d4_file_path),
    ]
    run_command(cmd, "Failed to run d4tools to convert bed file to d4 format")
    print("Saved as d4 file.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Creates non-overlapping segment duplication file in D4 format"
    )
    parser.add_argument(
        "--host",
        required=False,
        default="genome-euro-mysql.soe.ucsc.edu",
        help="UCSC MySQL database URL",
    )
    parser.add_argument(
        "--database",
        required=False,
        default="hg19",
        help="Database to use (e.g. hg19, hg38)",
    )
    parser.add_argument(
        "--user", required=False, default="genomep", help="User for UCSC MySQL access"
    )
    parser.add_argument(
        "--password",
        required=False,
        default="password",
        help="Password for UCSC MySQL access",
    )
    parser.add_argument(
        "--genome",
        required=True,
        type=Path,
        help="""Genome file with the length of each chromosome, one per line. Needed for
                d4tools create.""",
        metavar="<file_name.genome>",
    )
    parser.add_argument(
        "--output",
        required=True,
        default=".",
        type=Path,
        help="Output directory for new dump",
    )
    args = parser.parse_args()

    sys.exit(
        make_segdup_file(
            args.host,
            args.database,
            args.user,
            args.password,
            args.genome,
            args.output,
        )
    )
