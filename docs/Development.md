# Setup for local development

## Prerequisites

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Create hosts entries

Add the following to your hosts file:

```bash
127.0.0.1 genepanelbuilder.local api.genepanelbuilder.local
```

On linux/macs this is typically located at `/etc/hosts`. On Windows it is located at `C:\Windows\System32\drivers\etc\hosts`. You will need sudo or administrator privileges to edit this file.

## Install git hooks

We use [pre-commit](https://pre-commit.com/) to run some checks before committing code.
To install `pre-commit` run the following command:

```bash
pip install pre-commit
```

which will install it globally on your python installation. On MacOS you might want to use `brew`

```bash
brew install pre-commit
```

Then run the following command to install the git hooks:

```bash
pre-commit install
```

The hooks will check your code for inconsistencies/errors before committing. If you want to run the checks manually, you can run:

```bash
pre-commit run
```

## Running the entire stack

To run the entire stack, run the following command:

```bash
docker-compose up
```

You should now be able to access the application at [http://genepanelbuilder.local:8080](http://genepanelbuilder.local:8080). The API is available at [http://api.genepanelbuilder.local:8080](http://api.genepanelbuilder.local:8080).

## Running individual services within VSCode

If you want to run individual services within VSCode, you can use the [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension. This will allow you to run the services within a Docker container, and will automatically mount the source code from your local machine.

Simply open VSCode in the directory of the service you want to work on. You will be prompted to reopen the folder in a container. Click "Reopen in Container" and the service will be started within a Docker container.

## Testing locally

To run the tests locally, run the following command:

``` sh
docker compose up -d
```

Backend unit tests:

``` sh
docker exec -it gpbuilder pytest
```

BDD tests:

``` sh
docker exec -it bdd behave tests
```

Make sure api types are up to date:

``` sh
docker exec -it frontend yarn update-api-types
git diff frontend/frontend/api-types.d.ts
```

You should see no changes.

## Development tips and tricks

### Errors on running docker compose

A "catch-all" solution to errors when spinning up the app is:

- `docker compose down --remove-orphans`
- `sudo rm -rf .gpbuilder-docker-data`
- `docker compose build`
- `docker compose up`

### Database

The following steps will give you a clean database:

- `docker compose stop postgres gpbuilder`
- `sudo rm -rf .gpbuilder-docker-data/psql`
- `docker compose up postgres gpbuilder` (or start VSCode devcontainer)

By default, the folder `.gpbuilder-docker-data/psql` will be populated with data from the latest dump on dev. If you are making changes to the datamodel, the dump will be out of sync and the following needs to be done:

- Repopulate database: `gpb db populate`
- For bdd tests to run in CI, add "bdd-no-dump" to MR title
  
### Set up TAB-TAB autocompletion in CLI

To se up autocompletion of CLI commands by pushing the `TAB`-key twice, enter an interactive shell in the `gpbuilder` container and run

```bash
gpb --install-completion
source /home/gpbuilder-user/.bashrc
```

See also [CLI documentation](./Command_line_interface.md).
