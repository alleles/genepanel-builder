from enum import IntEnum

import semver
from pydantic import PositiveInt, validator
from sqlmodel import Field, SQLModel

from gpbuilder.datamodel.region import Region


class ConfidenceLevel(IntEnum):
    red = 1
    amber = 2
    green = 3


class PanelAppInput(SQLModel):
    panelapp_id: PositiveInt
    confidence_levels: dict[ConfidenceLevel, bool]

    @property
    def origin(self):
        return

    class Config:
        use_enum_values = True


class GenePanelSelectRequestModel(SQLModel):
    hgnc_ids: list[PositiveInt]
    panelapp_panels: list[PanelAppInput]
    regions: list[Region]


class GenePanelConfigRegion(Region):
    comment: str = Field(default="")
    inheritance: str = Field(default="N/A")


class GenePanelConfigGene(SQLModel):
    inheritance: str | None = Field(default=None)
    transcripts: list[str] = Field(default_factory=list)
    exclusion_reasons: list[str] = Field(default_factory=list)
    comment: str | None = Field(default=None)

    class Config:
        extra = "forbid"


class GenePanelConfig(GenePanelSelectRequestModel):
    id: int | None
    name: str
    shortname: str
    description: str | None
    version: str | None
    diff: dict[int, GenePanelConfigGene]
    regions: list[GenePanelConfigRegion]

    @validator("version")
    def valid_semver(cls, v):
        if not v:
            return None
        if not semver.VersionInfo.isvalid(v):
            raise ValueError("Invalid semantic versioning")
        return v
