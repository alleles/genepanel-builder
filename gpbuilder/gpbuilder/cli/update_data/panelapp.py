from operator import itemgetter
from typing import Iterator

from sqlmodel import Session, select

from gpbuilder.cli.update_data.common import (
    massage_hgnc_id,
    set_next_update_id,
)
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel, PanelAppPanelGene
from gpbuilder.panelapp_api.panelapp_api import PanelAppApi


def insert_panelapp_panel(
    session: Session,
    panelapp_panel: PanelAppPanel,
    panelapp_panel_genes: list[PanelAppPanelGene],
):
    db_pa_panel = session.get(PanelAppPanel, panelapp_panel.id)

    if db_pa_panel is None:
        session.add(panelapp_panel)
    else:
        [setattr(db_pa_panel, k, v) for k, v in panelapp_panel.dict().items()]
        session.add(db_pa_panel)
    session.flush()

    # Delete all PanelAppPanelGene entries for this panel
    db_pa_panel_genes = session.exec(
        select(PanelAppPanelGene).where(
            PanelAppPanelGene.panelapp_panel_id == panelapp_panel.id
        )
    ).all()
    [session.delete(db_pa_panel_gene) for db_pa_panel_gene in db_pa_panel_genes]
    session.flush()

    for panelapp_panel_gene in panelapp_panel_genes:
        # If a gene occurs twice in the PanelApp panel, use the highest confidence level
        existing = session.exec(
            select(PanelAppPanelGene).where(
                PanelAppPanelGene.hgnc_id == panelapp_panel_gene.hgnc_id,
                PanelAppPanelGene.panelapp_panel_id
                == panelapp_panel_gene.panelapp_panel_id,
            )
        ).one_or_none()
        if existing:
            existing.confidence_level = max(
                existing.confidence_level, panelapp_panel_gene.confidence_level
            )
            session.add(existing)
        else:
            session.add(panelapp_panel_gene)

        session.flush()

    session.flush()


def fetch_panelapp_panels() -> Iterator[tuple[PanelAppPanel, list[PanelAppPanelGene]]]:
    panelapp_panels = PanelAppApi.get_all_panels(site="ENG")

    for panel_meta in panelapp_panels.values():
        panel = PanelAppApi.get_panel(panel_meta["id"])
        # Set all PanelAppPanel attributes explicitly
        pa_panel = PanelAppPanel(
            id=panel["id"],
            name=panel["name"],
            version=panel["version"],
            version_created=panel["version_created"],
            disease_group=panel["disease_group"],
            disease_sub_group=panel["disease_sub_group"],
            relevant_disorders=panel["relevant_disorders"],
        )

        pa_panel_genes = []
        for gene in panel["genes"]:
            gene_data, confidence_level = itemgetter("gene_data", "confidence_level")(
                gene
            )
            hgnc_id = massage_hgnc_id(gene_data)
            # ignore genes with confidence level 0, as they are phenotype only
            if hgnc_id is not None and confidence_level != "0":
                pa_panel_genes.append(
                    PanelAppPanelGene(
                        hgnc_id=hgnc_id,
                        panelapp_panel_id=panel["id"],
                        confidence_level=int(confidence_level),
                    )
                )
        yield pa_panel, pa_panel_genes


def populate_panelapp_panels(session: Session):
    update_id = set_next_update_id("panelapp", session)
    for panel, genes in fetch_panelapp_panels():
        panel.update_id = update_id
        insert_panelapp_panel(session, panel, genes)
