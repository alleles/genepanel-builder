import logging.config
import os
from importlib import metadata

from . import genenames_api, ncbi_api, omim_api, panelapp_api

__version__ = metadata.version(__package__)

_log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {"format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"}
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "standard",
            "stream": "ext://sys.stdout",
        }
    },
    "loggers": {
        "": {"handlers": ["console"], "level": os.environ.get("LOGGING_LEVEL", "INFO")}
    },
}
logging.config.dictConfig(_log_config)
