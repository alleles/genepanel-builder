import csv
from pathlib import Path

import typer
from sqlmodel import Session

from gpbuilder.datamodel.crud.genepanel import create_panel, update_panel
from gpbuilder.datamodel.crud.models import GenePanelConfig

app = typer.Typer()


@app.command()
def panel(
    ctx: typer.Context,
    file: Path,
    name: str,
    short_name: str,
    description: str,
    version: str = "1.0.0",
):
    """
    Import a gene panel in tsv format. The file should contain the following columns:

    - HGNC ID
    - Gene symbol
    - Comma separated list of transcripts
    - Inheritance mode

    The transcripts and inheritance mode are optional.

    Gene symbols are optional, but can be included for documentation purposes.
    """
    session: Session = ctx.obj.session
    with open(file, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        gene_overrides = {}
        hgnc_ids = []
        for hgnc_id, symbol, transcripts, inheritance in reader:
            if transcripts:
                transcripts = transcripts.strip(" ").split(",")
            hgnc_ids.append(hgnc_id)
            if inheritance or transcripts:
                gene_overrides[hgnc_id] = {
                    "transcripts": transcripts or [],
                    "inheritance": inheritance or None,
                }

        panel = create_panel(name, short_name, description)
        panel.version = version
        session.add(panel)
        session.flush()

        config = GenePanelConfig(
            id=panel.id,
            hgnc_ids=hgnc_ids,
            name=name,
            shortname=short_name,
            description=description,
            version=None,
            panelapp_panels=[],
            regions=[],
            diff=gene_overrides,
        )

        update_panel(session, panel, config)
