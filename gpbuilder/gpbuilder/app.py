import logging
import os
import secrets
import subprocess
import sys

import sqlmodel
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.routing import APIRoute
from pyd4 import D4File
from starlette.middleware.sessions import SessionMiddleware

from gpbuilder.api import api_router
from gpbuilder.coverage.mock_d4_file import create_mock_d4_file
from gpbuilder.datamodel.engine import create_db_and_tables, get_session
from gpbuilder.oauth import auth_router
from gpbuilder.settings import get_d4_reader, get_settings

logger = logging.getLogger(__name__)


app = FastAPI()

origins = [
    "http://frontend",
    "http://frontend:3000",
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:8080",
    "http://gpb.allel.es",
    "https://gpb.allel.es",
    os.environ.get("SITE_URL", "http://frontend:3000"),
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "OPTIONS", "DELETE"],
    allow_headers=["Content-Type"],
)

app.add_middleware(SessionMiddleware, secret_key=secrets.token_urlsafe(64))

app.include_router(api_router, prefix="/api/v1")
app.include_router(auth_router, prefix="/api/auth")


def use_route_names_as_operation_ids(app: FastAPI) -> None:
    """
    Simplify operation IDs so that generated API clients have simpler function
    names.

    Should be called only after all routes have been added.
    """
    api_routes = [route for route in app.routes if isinstance(route, APIRoute)]
    for route in api_routes:
        route.operation_id = f"{'_'.join(route.tags).replace(' ', '_')}_{route.name}"

    assert (
        len(set(route.operation_id for route in api_routes)) == len(api_routes)
    ), "Unique identifiers for api endpoints is needed. Make sure function names and router tags are descriptive."


use_route_names_as_operation_ids(app)


def generate_mock_d4_files():
    """Generate mock d4 files if not already generated"""
    for key, d4_config in get_settings().d4.items():
        generate = False
        if not d4_config.path.exists():
            generate = True
        else:
            # Check if d4 file is complete
            available_chromosomes = []
            try:
                available_chromosomes = D4File(str(d4_config.path)).chrom_names()
            except Exception:
                pass
            if len(available_chromosomes) != 25:
                generate = True
                os.unlink(d4_config.path)

        if generate:
            logger.info(f"Creating mock D4 file for {d4_config.long_name}")
            if key == "wes":
                create_mock_d4_file(
                    d4_config.path, seed=1, low_fraction=0.05, clustering=0.995
                )
            elif key == "wgs":
                create_mock_d4_file(
                    d4_config.path, seed=2, low_fraction=0.03, clustering=0.99
                )
            elif key == "segdup":
                create_mock_d4_file(
                    d4_config.path,
                    low=0,
                    high=100,
                    seed=3,
                    low_fraction=0.99,
                    clustering=0.9995,
                )
            else:
                create_mock_d4_file(d4_config.path)


@app.on_event("startup")
async def on_startup():
    from gpbuilder.datamodel.gene import Gene

    create_db_and_tables()

    def load_d4_files():
        """Loads all chromosomes on all d4 files into memory"""
        for d4_target in get_settings().d4:
            get_d4_reader(d4_target).populate_chromosome_d4np()

    if os.environ.get("DEVELOPMENT") != "true":
        # Production mode
        load_d4_files()

    else:
        # Development mode

        # Load database from dump if not already loaded
        with get_session() as session:
            ids = session.query(Gene.hgnc_id).limit(1).all()
        if not ids:
            sqlmodel.Session.close_all()
            logger.info("Loading database from dump")
            subprocess.run(
                ["sh", "-c", "scripts/postgres/pg-dumper.sh -r"], stdout=sys.stdout
            )

        # Generate mock d4 files
        generate_mock_d4_files()
