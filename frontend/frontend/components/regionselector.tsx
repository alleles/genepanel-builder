import React, { useState } from 'react'

import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import Input from 'components/input'
import { GenePanelConfig, Inheritance, Region } from 'types'

const regionRegExp =
  /^\s*(chr)?\s*(?<chr>[1-9]|1\d|2[0-2]|[X,Y]|M(T)?)\s*:\s*(?<start>[1-9]\d*)\s*-\s*(?<end>[1-9]\d*)?$/i

export const regionKey = (region: Region) =>
  `${region.chromosome}:${region.start}-${region.end}`

export default function RegionSelector() {
  const [regionString, setRegionString] = useState('')
  const [regionName, setRegionName] = useState('')
  const { regions, addRegion, removeRegion } = useGenePanel()

  const isRegionNameValid = (regionName: string) => {
    const match = /__/.exec(regionName)
    if (match) {
      return false
    }
    return true
  }

  const isRegionValid = (regionString: string) => {
    const match = regionRegExp.exec(regionString)
    if (match && parseInt(match.groups.end) > parseInt(match.groups.start)) {
      return true
    }
    return false
  }

  const parseRegion = (regionString: string): Region => {
    const match = regionRegExp.exec(regionString)
    return {
      chromosome:
        match.groups.chr.toUpperCase() === 'M'
          ? 'MT'
          : (match.groups.chr.toUpperCase() as GenePanelConfig['regions'][number]['chromosome']),
      start: parseInt(match.groups.start),
      end: parseInt(match.groups.end),
      name: regionName,
      comment: '',
      inheritance: Inheritance.NA,
      ref_genome: 'GRCh37' as 'GRCh37',
    }
  }

  return (
    <React.Fragment>
      <div className="grid grid-cols-5 items-start space-x-2">
        <div
          className="col-span-2"
          style={{
            color: isRegionNameValid(regionName) ? '#221ab8' : '#d90909',
          }}
          title="Name must not contain consecutive underscores ['__']"
        >
          <Input
            placeholder="Name of region ..."
            name="region-name"
            value={regionName}
            onChange={setRegionName}
          />
        </div>
        <div
          className="col-span-2"
          style={{ color: isRegionValid(regionString) ? '#1ab822' : '#d90909' }}
        >
          <Input
            placeholder="Chromosome region ... (e.g. chr7:100000-101000)"
            name="region-input"
            value={regionString}
            onChange={(r) => setRegionString(r)}
          />
        </div>
        <div className="w-32 justify-self-end" id="region-add">
          <Button
            color="confirm"
            disabled={
              !isRegionValid(regionString) ||
              !isRegionNameValid(regionName) ||
              regions
                .map(regionKey)
                .includes(regionKey(parseRegion(regionString)))
            }
            onClick={() => {
              addRegion(parseRegion(regionString))
              setRegionString('')
            }}
          >
            Add
          </Button>
        </div>
      </div>
      <div className="mt-3">
        <ul>
          {regions.map((region) => (
            <li
              key={regionKey(region)}
              className="grid grid-cols-5 items-start gap-2 border-t border-gray-200 py-3"
            >
              <span className="col-span-2 pt-2 pl-2 font-bold text-gray-500">
                {region.name}
              </span>
              <span className="col-span-2 pt-2 pl-4 pr-2 font-bold text-gray-500">
                {`chr${region.chromosome}:${region.start}-${region.end} `}
              </span>
              <span className="col-span-1 w-32 justify-self-end">
                <Button color="danger" onClick={() => removeRegion(region)}>
                  Remove
                </Button>
              </span>
            </li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  )
}
