Feature: Edit and filter a new gene panel

    As a user I should be able to view all added genes and regions.
    I should then be able to change inheritance mode and default transcript for individual genes and regions, or remove them altogether.

    Background:
        Given initial state
        Given a browser
        When I visit "/"
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I fill in "description" with "Test description"
        And I press "Continue (Select)"
        And I wait for 1 seconds

    Scenario: Sort and filter HGNC ID, Symbol and Origin columns

        # Populate table, allow page to draw
        When I add panel "Familial breast cancer"
        And I add panel "Ovarian cancer pertinent cancer susceptibility"
        And I press "Continue (Edit)"

        # Test that Origin is listed for both panels
        Then the origin for "BRCA1" should list "117" with confidence level "green"
        And the origin for "BRCA1" should list "158" with confidence level "green"

        # Test sorting
        When I press the header "HGNC ID"
        And I wait for 1 seconds
        Then the value of the cell in row 0, column 0 in the table with id "gene-table" should be "795"

        When I press the header "HGNC ID"
        Then the value of the cell in row 0, column 0 in the table with id "gene-table" should be "26144"

        When I press the header "Symbol"
        Then the value of the cell in row 0, column 1 in the table with id "gene-table" should be "ATM"

        When I press the header "Symbol"
        Then the value of the cell in row 0, column 1 in the table with id "gene-table" should be "TP53"

        ## Note: "value" doesn't work here, cannot test sorting
        When I press the header "Origin"

        # Test filtering
        ## Note: no filtering available for Origin column
        When I fill in "hgnc_id-filter" with "1"
        And I wait for 1 seconds
        Then I should see "BRCA1"
        But I should not see "PTEN"

        When I fill in "symbol-filter" with "BRC"
        And I wait for 1 seconds
        Then I should see "BRCA1"
        And I should see "BRCA2"
        But I should not see "CHEK2"

    Scenario: Check that inheritance mode is populated correctly and is changeable

        # Add a gene with no config and mixed OMIM inheritance (BRCA2 = 1101 with OMIM inh AR; AD, SMu; AD; AD, AR, SMu; AD, SMu. Expect: "AD/AR")
        When I add gene "BRCA2"
        # Add a gene with no config and mixed OMIM inheritance (MPL = 7217 with OMIM inh AR; AD, Smu. Expect: "AD/AR"). Note: Use HGNC ID to bypass bdd quirk
        And I add gene "7217"
        # Add a gene without an OMIM entry (AMD1P2 = 460 in X/Y PAR2 region. Expect: "N/A")
        And I add gene "AMD1P2"
        # Add a gene with no valid inheritance from OMIM (RGS5 = 10001, "Mu". Expect: "N/A")
        And I add gene "RGS5"
        # Add a gene with OMIM entry "XL" (ARR3 = 710. Expect: "XD/XR")
        And I add gene "ARR3"
        # Add a gene with OMIM entry "XLR" (CLCN5 = 2023. Expect: "XD/XR")
        And I add gene "CLCN5"
        # Add a gene with OMIM entry "XLD" (PDK3 = 8811. Expect: "XD/XR")
        And I add gene "PDK3"

        And I press "Continue (Edit)"

        Then I should see "1101" within 15 seconds
        # Check that inheritance modes are as expected
        And field "inheritance-1101" should have the value "AD/AR"
        And field "inheritance-7217" should have the value "AD/AR"
        And field "inheritance-460" should have the value "N/A"
        And field "inheritance-10001" should have the value "N/A"
        And field "inheritance-710" should have the value "XD/XR"
        And field "inheritance-2023" should have the value "XD/XR"
        And field "inheritance-8811" should have the value "XD/XR"

        # Check that all options exist in dropdown for BRCA2
        When I change the inheritance of gene "1101" to "AD"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "XR"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "XD"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "AD/AR"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "XD/XR"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "SMu"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "N/A"
        And I wait for 1 seconds
        And I change the inheritance of gene "1101" to "AR"
        And I wait for 1 seconds
        Then field "inheritance-1101" should have the value "AR"

    Scenario: Change transcript

        When I add panel "Familial breast cancer"
        And I press "Continue (Edit)"

        Then I should see "1100" within 15 seconds
        # Check default transcript for BRCA1
        And field "transcript-1100[0]" should have the value "NM_007294.4"

        # Check that relevant transcripts exist in dropdown for BRCA1
        When I wait for 1 seconds
        Then I should see the transcript "NM_007297" for gene "1100"
        When I wait for 1 seconds
        Then I should see the transcript "NM_007298" for gene "1100"
        When I wait for 1 seconds
        Then I should see the transcript "NM_007299" for gene "1100"
        When I wait for 1 seconds
        Then I should see the transcript "NM_007300" for gene "1100"
        When I wait for 1 seconds
        Then I should see the transcript "NR_027676" for gene "1100"

        # Select a second default transcript
        When I select the transcript "NM_007297.4" for gene "1100"
        And I wait for 1 seconds
        Then field "transcript-1100[0]" should have the value "NM_007294.4" within 5 seconds
        And field "transcript-1100[1]" should have the value "NM_007297.4"

        # Test that filter shows only genes with changed transcripts
        When I press the element with xpath "//div[text()='Transcript']/following-sibling::span/input[@type='checkbox']"
        And I wait for 1 seconds
        Then I should see "1100"
        But I should not see "795"
        And I should not see "1101"
        And I should not see "16627"
        And I should not see "26144"
        And I should not see "9588"
        And I should not see "9820"
        And I should not see "9823"
        And I should not see "11389"
        And I should not see "11998"

    Scenario: Sort and filter Coverage and SegDup columns

        When I add panel "Familial breast cancer"
        And I press "Continue (Edit)"
        Then I should see "1100" within 15 seconds

        # Test sort
        When I press the header "Coverage WGS"
        Then the value of the cell in row 0, column 4 in the table with id "gene-table" should be "0.993"

        When I press the header "Coverage WGS"
        Then the value of the cell in row 0, column 4 in the table with id "gene-table" should be "0.811"

        When I press the header "Coverage WES"
        Then the value of the cell in row 0, column 5 in the table with id "gene-table" should be "1.00"

        When I press the header "Coverage WES"
        Then the value of the cell in row 0, column 5 in the table with id "gene-table" should be "0.929"

        When I press the header "SegDup"
        Then the value of the cell in row 0, column 6 in the table with id "gene-table" should be "0.522"

        When I press the header "SegDup"
        Then the value of the cell in row 0, column 6 in the table with id "gene-table" should be "0.00"

        # Test filter
        When I fill in "coverageWGS-filtermax" with "0.95"
        And I wait for 1 seconds
        Then I should see "CHEK2"
        And I should not see "ATM"

        When I fill in "coverageWGS-filtermax" with "1"
        And I fill in "coverageWES-filtermax" with "0.95"
        And I wait for 1 seconds
        Then I should see "BRCA2"
        And I should not see "ATM"

        When I fill in "coverageWES-filtermax" with "1"
        And I fill in "segDup-filtermin" with "0.5"
        And I wait for 1 seconds
        Then I should see "TP53"
        And I should not see "ATM"

    Scenario: Exclude genes

        When I add panel "Familial breast cancer"
        And I press "Continue (Edit)"
        Then I should see "1100" within 15 seconds

        # Exclude genes with different reasons
        When I exclude gene "795" with reason "Insufficient evidence"
        And I exclude gene "1100" with reason "Technical issues"
        And I exclude gene "1101" with reason "Watchlist"
        And I exclude gene "16627" with reason "Other"
        ## Set two reasons for one gene
        And I exclude gene "26144" with reason "Technical issues"
        And I exclude gene "26144" with reason "Watchlist"

        # Test sort
        When I press the header "Exclusion"
        And I wait for 1 seconds
        Then the value of the cell in row 0, column 8 in the table with id "gene-table" should be "Watchlist"
        And the value of the cell in row 1, column 8 in the table with id "gene-table" should be "2 selected"

        When I press the header "Exclusion"
        And I wait for 1 seconds
        Then the value of the cell in row 5, column 8 in the table with id "gene-table" should be "Insufficient evidence"
        And the value of the cell in row 6, column 8 in the table with id "gene-table" should be "Other"

        # Test filter
        When I fill in "exclusion_reasons-filter" with "watch"
        And I wait for 1 seconds
        Then I should see "BRCA2"
        And I should see "PALB2"
        But I should not see "ATM"

    Scenario: Add comment

        When I add panel "Familial breast cancer"
        And I press "Continue (Edit)"
        Then I should see "1100" within 15 seconds

        # Add a comment to three genes
        When I fill in "comment-1100" with "A comment"
        ## Workaround to refocus
        And I press the header "Comment"
        ## Now sorted A-Z
        And I wait for 1 seconds
        And I fill in "comment-9588" with "B comment"
        ## Workaround to refocus
        And I press the header "Comment"
        ## Now sorted Z-A
        And I wait for 1 seconds
        And I fill in "comment-16627" with "Some text"

        # Test sort
        When I press the header "Comment"
        ## Now sorted default
        And I wait for 1 seconds
        And I press the header "Comment"
        ## Now sorted A-Z
        And I wait for 1 seconds
        ## Using HGNC ID as a workaround, as searching for value in column 9 doesn't work
        Then the value of the cell in row 7, column 0 in the table with id "gene-table" should be "1100"

        When I press the header "Comment"
        ## Now sorted Z-A
        And I wait for 1 seconds
        Then the value of the cell in row 0, column 0 in the table with id "gene-table" should be "16627"

        # Test filter
        When I fill in "comment-filter" with "comment"
        And I wait for 1 seconds
        Then I should see "PTEN"
        And I should see "BRCA1"
        But I should not see "CHEK2"

    Scenario: Edit regions

        When I add region "chr1:1000000-2000000" with name "Region 1"
        And I add region "chr2:3000000-4000000" with name "Region 2"
        And I press "Continue (Edit)"

        # Check that regions are listed with given names
        Then field "region-name-0" should have the value "Region 1" within 5 seconds
        And field "region-name-1" should have the value "Region 2"
        # Check that default inheritance is "N/A"
        And field "inheritance-region-0" should have the value "N/A"
        And field "inheritance-region-1" should have the value "N/A"

        # Edit region name TODO: Fix this test
        # When I fill in "region-name-0" with "Region 1 edited"
        # And I press "Save"
        # And I reload
        # Then field "region-name-0" should have the value "Region 1 edited" within 5 seconds

        # Edit region comment
        When I fill in "region-comment-1" with "Region Comment 2"
        And I press "Save"
        And I reload
        Then field "region-comment-1" should have the value "Region Comment 2"

        # Edit region inheritance
        When I change the inheritance of region 0 to "AD"
        And I press "Save"
        And I reload
        Then field "inheritance-region-0" should have the value "AD"
