from fastapi import APIRouter, Depends

from gpbuilder.api import coverage, genepanels, genes, panelapp_panels, settings
from gpbuilder.oauth.api import authenticated

api_router = APIRouter(dependencies=[Depends(authenticated)])
api_router.include_router(genes.router)
api_router.include_router(genepanels.router)
api_router.include_router(panelapp_panels.router)
api_router.include_router(coverage.router)
api_router.include_router(settings.router)
