import logging
import shutil
import tempfile
import time
from pathlib import Path
from typing import Optional

import click
import typer
from sqlmodel import Session, select

from gpbuilder.cli.diff.diff import diff_against_exported
from gpbuilder.cli.export_gp import export_panel
from gpbuilder.cli.shared import progress_spinner
from gpbuilder.cli.update_data.gene import populate_gene_table
from gpbuilder.cli.update_data.mane import update_mane_transcripts
from gpbuilder.cli.update_data.panelapp import populate_panelapp_panels
from gpbuilder.cli.update_data.phenotype import populate_omim_phenotypes
from gpbuilder.cli.update_data.transcript import update_transcripts_table
from gpbuilder.datamodel.crud.genepanel import update_panel
from gpbuilder.datamodel.genepanel import GenePanel
from gpbuilder.datamodel.scripts.update_gene_defaults import (
    clean_gene_defaults_file,
    update_gene_defaults,
)

logger = logging.getLogger(__name__)

app = typer.Typer()


@click.pass_context
def summarize_update(
    ctx: typer.Context,
    *args,
    diff: bool,
    diff_against_folder: Path | None,
    keep_tempdir: bool = False,
):
    # Rebuild panels
    session: Session = ctx.obj.session
    session.flush()
    for panel in session.exec(select(GenePanel)).all():
        update_panel(session, panel, panel.as_config())

    timestamp = time.strftime("%Y-%m-%dT%H%M")

    output_prefixes = []

    # Create diff if --diff-against-folder is passed
    if diff_against_folder is not None:
        cmpfolder = Path(diff_against_folder)
        output_prefix = Path(f"gpb-update-folder-diff-{timestamp}")
        diff_against_exported(session, cmpfolder, output_prefix)
        output_prefixes.append(output_prefix)

    # Create diff if --diff is passed
    if diff:
        cmpfolder = Path(ctx.obj.exported_panels_folder)
        output_prefix = Path(f"gpb-update-diff-{timestamp}")
        diff_against_exported(session, cmpfolder, output_prefix)
        output_prefixes.append(output_prefix)
        if not keep_tempdir:
            logger.info("Cleaning up")
            shutil.rmtree(ctx.obj.exported_panels_folder)

    for prefix in output_prefixes:
        logger.info(f"Diffs written to {prefix.absolute()}*.tsv")


# The `result_callback`` function is passed the return values of the run commands as well as any
# `Option`s passed to the parent `callback` function.
@app.callback(result_callback=summarize_update)
def update(
    ctx: typer.Context,
    diff: bool = typer.Option(
        False,
        "--diff",
        help=(
            "Compute changes caused by the update by exporting panels to a temporary directory "
            "prior to the update itself"
        ),
    ),
    diff_against_folder: Optional[Path] = typer.Option(
        None,
        "--diff-against-folder",
        help=(
            "Compute changes caused by the update comparing the updated panels to those found in "
            "the specified folder"
        ),
    ),
    keep_tempdir: bool = typer.Option(
        False,
        "-k",
        "--keep-tempdir",
        help="Do not delete temporary panel exports",
    ),
):
    """
    Update the database with content from external data sources.
    The order of the update commands is important (See individual update command).
    """
    if diff:
        # Before the update, export all panels to a folder to diff against
        outputdir = Path(tempfile.mkdtemp())
        logger.info(
            "Exporting all panels before the update to compute any diffs due to it"
        )
        session = ctx.obj.session
        for panel in session.exec(select(GenePanel)).all():
            export_panel(panel, session, outputdir)
        logger.debug(f"Exported panels to {outputdir}")
        ctx.obj.exported_panels_folder = outputdir
    if diff_against_folder is not None:
        assert diff_against_folder.parent.is_dir()
    if keep_tempdir:
        logger.info("Keeping pre-update exports for manual inspection")


@app.command()
def all_external(ctx: typer.Context):
    """
    Update the database with content from external data sources.
    The order of the update commands is important, see individual update commands.

    This command is equivalent to running the following commands sequentially:

    1. gpbuilder update gene

    2. gpbuilder update phenotype

    3. gpbuilder update transcript

    4. gpbuilder update mane

    5. gpbuilder update panelapp
    """
    for func in [gene, phenotype, transcript, mane, panelapp]:
        # Replace the context with a new one to avoid the 'all' command being
        # used as context for the subcommands - this is to get correct logging.
        # In the logs it would appear like the subcommands were called sequentially.
        sub_ctx = typer.Context(
            click.command(func), parent=ctx.parent, info_name=func.__name__
        )
        sub_ctx.invoke(func, sub_ctx)


@app.command()
def gene(ctx: typer.Context):
    """
    Update 'gene' table with data from genenames.org.
    """
    logger.info("Updating gene table")
    progress_spinner(populate_gene_table, ctx.obj.session)


@app.command()
def phenotype(ctx: typer.Context):
    """
    Update 'phenotype' table with data from omim.org [requires updated 'gene'].

    NOTE: the environment variable OMIM_API_KEY must be set (to a valid key).
    """
    logger.info("Updating phenotype table")
    progress_spinner(populate_omim_phenotypes, ctx.obj.session)


@app.command()
def transcript(ctx: typer.Context):
    """
    Update 'transcript' table with data from ftp.ncbi.nlm.nih.gov and ftp.ebi.ac.uk
    [requires updated 'gene'].
    """
    logger.info("Updating transcript table - this may take a while")
    ref_genome = "GRCh37"  # TODO: Support GRCh38 - untested, but _should_ work
    progress_spinner(update_transcripts_table, ctx.obj.session, ref_genome)


@app.command()
def mane(ctx: typer.Context):
    """
    Update 'maneselecttranscript' table with data from ftp.ncbi.nlm.nih.gov
    [requires updated 'transcript', 'gene'].
    """
    logger.info("Updating mane select transcript table")
    progress_spinner(update_mane_transcripts, ctx.obj.session)


@app.command()
def panelapp(ctx: typer.Context):
    """
    Update 'panelapppanel' table with data from panelapp.genomicsengland.co.uk/api/v1
    [requires updated 'gene'].

    This will update PanelApp panels "in place", i.e. the new versions will overwrite any existing
    old version of a PanelApp panel.
    """
    logger.info("Updating panelapp panels")
    progress_spinner(populate_panelapp_panels, ctx.obj.session)


@app.command()
def gene_defaults(ctx: typer.Context, filename: Path):
    """
    Update the GeneDefaults table from file.

    This will override any existing defaults, so make sure to run this command with a file
    containing all defaults.

    The expected file format is a tsv-file with the following columns:

    - HGNC ID

    - Gene symbol (ignored)

    - Inheritance

    - Comma separated list of transcript IDs

    See the 'clean-gene-defaults' command for more information on how to prepare the file.
    """
    logger.info(f"Updating gene defaults from file {filename}")

    update_gene_defaults(ctx.obj.session, filename)


@app.command()
def clean_gene_defaults(filename: Path):
    """
    Clean the gene defaults file and output a cleaned version [e.g. input.tsv -> input_clean.tsv].

    This command takes a tsv-file with the following columns:

    - HGNC ID

    - Gene symbol (ignored)

    - Inheritance

    - Comma separated list of transcript IDs

    It will perform the following operations:

    - update gene symbols to those approved by HGNC

    - remove genes with withdrawn HGNC IDs

    - remove specified transcripts if they match MANE transcripts

    - remove specified inheritance if it matches OMIM inheritance

    - remove entries with neither transcript nor inheritance
    """
    logger.info(f"Cleaning gene defaults file {filename}")
    cleaned_filename = filename.with_suffix(f"_clean{filename.suffix}")
    clean_gene_defaults_file(filename, cleaned_filename)
    logger.info(f"Cleaned gene defaults written to {cleaned_filename}")
