Feature: View a draft or submitted gene panel

    As a user I should be able to view a submitted or draft gene panel as read-only.
    I should then be able to create a new draft for a submitted panel or edit the draft itself.

    Background:
        Given a browser

    @setup
    Scenario: Create a submitted and a draft panel and save state

        Given initial state

        # Create a new submitted panel
        When I visit "/"
        And I press "New Gene Panel"
        ## Add name and short name
        And I fill in "name" with "Test submitted gene panel"
        And I fill in "short-name" with "testsubmit"
        And I fill in "description" with "Test description for a submitted panel"
        And I press "Continue (Select)"
        Then I should see "Select genes" within 5 seconds

        ## Populate tables
        When I add panel "Breast cancer pertinent cancer susceptibility"
        And I add gene "COPA"
        ### Manually add a gene that already exists in the added panel
        And I add gene "BRCA1"
        And I add region "chr7:1000-10000" with name "Test region"
        And I press "Continue (Edit)"
        Then I should see "BRCA2" within 15 seconds

        ## Make some changes
        ### Change inheritance mode for COPA (default AD)
        When I change the inheritance of gene "2230" to "AR"
        ### Change transcript for COPA (default NM_004371.4)
        And I select the transcript "NM_001098398.2" for gene "2230"
        And I wait for 1 seconds
        And I select the transcript "NM_004371.4" for gene "2230"
        ### Select a second transcript for BRCA1 (in addition to NM_007294.4). Mote: wait needed for step to work.
        And I wait for 1 seconds
        And I select the transcript "NM_007300.4" for gene "1100"
        ### Exclude gene BRCA2 with two reasons
        And I wait for 1 seconds
        And I exclude gene "1101" with reason "Insufficient evidence"
        And I wait for 1 seconds
        And I exclude gene "1101" with reason "Watchlist"
        ### Add a comment to COPA
        And I fill in "comment-2230" with "Comment for COPA"

        ## Submit the panel and check that it's stored with correct name, short name, number of genes and version
        When I submit the gene panel
        Then I should see "All gene panels" within 5 seconds
        And I should see "Test submitted gene panel" within 5 seconds
        And the number of genes in "testsubmit" should be 4
        And the version of "testsubmit" should be "0.1.0"


        # Create a new draft panel with the same setup but without submitting
        When I press "New Gene Panel"
        ## Add name and short name
        And I fill in "name" with "Test draft gene panel"
        And I fill in "short-name" with "testdraft"
        And I fill in "description" with "Test description for a draft panel"
        And I press "Continue (Select)"
        Then I should see "Select genes" within 5 seconds

        ## Populate tables
        When I add panel "Breast cancer pertinent cancer susceptibility"
        And I add gene "COPA"
        ### Manually add a gene that already exists in the added panel
        And I add gene "BRCA1"
        And I add region "chr7:1000-10000" with name "Test region"
        And I press "Continue (Edit)"
        Then I should see "BRCA2" within 15 seconds

        ## Make some changes
        ### Change inheritance mode for COPA (default AD)
        When I change the inheritance of gene "2230" to "AR"
        ### Change transcript for COPA (default NM_004371.4)
        And I select the transcript "NM_001098398.2" for gene "2230"
        And I wait for 1 seconds
        And I select the transcript "NM_004371.4" for gene "2230"
        ### Select a second transcript for BRCA1 (in addition to NM_007294.4). Mote: wait needed for step to work.
        And I wait for 1 seconds
        And I select the transcript "NM_007300.4" for gene "1100"
        ### Exclude gene BRCA2 with two reasons
        And I wait for 1 seconds
        And I exclude gene "1101" with reason "Insufficient evidence"
        And I wait for 1 seconds
        And I exclude gene "1101" with reason "Watchlist"
        ### Add a comment to COPA
        And I fill in "comment-2230" with "Comment for COPA"
        And I press "Save"

        ## Check that the panel is stored with correct name, short name, number of genes and version
        When I visit "/"
        Then I should see "All gene panels" within 5 seconds
        And I should see "Test draft gene panel" within 5 seconds
        And the number of genes in "testdraft" should be 4
        And the version of "testdraft" should be "0.0.1-draft"

        # Save state for reuse
        When I save the state as "draft_submitted_pamel"

    Scenario: View a submitted gene panel

        Given state "draft_submitted_pamel"
        When I visit "/"
        And I press "Test submitted gene panel"

        # Check that name, short name and comment are shown
        Then I should see "Test submitted gene panel [testsubmit]" within 5 seconds
        And I should see "Test description for a submitted panel"

        # Check that the PanelApp panel is listed
        And I should see "Breast cancer pertinent cancer susceptibility"

        # Check that the manually added gene COPA is listed
        And I should see "HGNC:2230" within 30 seconds
        ## Check that the PanelApp panel genes are not listed as manually added
        But I should not see "HGNC:1100"
        And I should not see "HGNC:1101"
        And I should not see "HGNC:26144"
        And I should not see "HGNC:11998"

        # Check that the manually added region is listed with name and coordinates
        And I should see an element with xpath "//li/span[text()='Test region']"
        And I should see an element with xpath "//li/span[text()='Test region']/following-sibling::span[contains(text(),'chr7:1000-10000')]"

        # Check that all genes are listed in the table

        # Check that custom inheritance is set for COPA gene
        And field "inheritance-2230" should have the value "AR"

        # Check that custom transcripts are set for COPA and BRCA1 genes
        And the selected transcripts for gene "2230" should be "NM_001098398.2"
        And the selected transcripts for gene "1100" should be "NM_007294.4, NM_007300.4"

        # Check that BRCA2 is excluded with correct reasons
        And field "exclusion_reasons-1101" should have the value "Insufficient evidence, Watchlist"

        # Check that comment for COPA is visible
        And field "comment-2230" should have the value "Comment for COPA"

        # Correct button is visible
        And I should see "Create New Draft"

    Scenario: View a draft gene panel

        Given state "draft_submitted_pamel"
        When I visit "/"
        And I press "Test draft gene panel"

        # Check that name, short name and comment are shown
        Then I should see "Test draft gene panel [testdraft]" within 5 seconds
        And I should see "Test description for a draft panel"

        # Check that the PanelApp panel is listed
        And I should see "Breast cancer pertinent cancer susceptibility"

        # Check that the manually added gene COPA is listed
        And I should see "HGNC:2230" within 30 seconds
        ## Check that the PanelApp panel genes are not listed as manually added
        But I should not see "HGNC:1100"
        And I should not see "HGNC:1101"
        And I should not see "HGNC:26144"
        And I should not see "HGNC:11998"

        # Check that the manually added region is listed with name and coordinates
        And I should see an element with xpath "//li/span[text()='Test region']"
        And I should see an element with xpath "//li/span[text()='Test region']/following-sibling::span[contains(text(),'chr7:1000-10000')]"

        # Check that all genes are listed in the table

        # Check that custom inheritance is set for COPA gene
        And field "inheritance-2230" should have the value "AR"

        # Check that custom transcripts are set for COPA and BRCA1 genes
        And the selected transcripts for gene "2230" should be "NM_001098398.2"
        And the selected transcripts for gene "1100" should be "NM_007294.4, NM_007300.4"

        # Check that BRCA2 is excluded with correct reasons
        And field "exclusion_reasons-1101" should have the value "Insufficient evidence, Watchlist"

        # Check that comment for COPA is visible
        And field "comment-2230" should have the value "Comment for COPA"

        # Correct button is visible
        And I should see "Modify Draft"
