import { forwardRef, MouseEvent, ReactNode } from 'react'

interface Props {
  id?: string
  children: ReactNode
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void
  type?: 'button' | 'submit' | 'reset'
  color?: 'primary' | 'danger' | 'cancel' | 'confirm'
  padding?: 'full' | 'narrow'
  disabled?: boolean
}

const Button = forwardRef<HTMLButtonElement, Props>(
  (
    {
      id,
      children,
      onClick,
      type = 'button',
      color = 'primary',
      padding = 'full',
      disabled = false,
    }: Props,
    ref?
  ) => {
    let colorClass
    switch (color) {
      case 'danger':
        colorClass =
          'text-white bg-ellared-dark shadow-sm hover:shadow-md hover:bg-ellared focus:ring-ellared-light'
        break

      case 'cancel':
        colorClass =
          'text-gray-700 bg-gray-300 shadow-sm hover:shadow-md hover:bg-gray-400 hover:text-gray-900 focus:ring-gray-300'
        break

      case 'confirm':
        colorClass =
          'text-white bg-ellagreen-dark shadow-sm hover:shadow-md hover:bg-ellagreen hover:text-gray-700 focus:ring-ellagreen'
        break

      default:
        colorClass =
          'text-white bg-ellablue-darkest shadow-sm hover:shadow-md hover:bg-ellablue-darker focus:ring-ellablue-lighter'
        break
    }

    let paddingClass
    switch (padding) {
      case 'narrow':
        paddingClass = 'py-0'
        break

      default:
        paddingClass = 'py-2'
        break
    }

    return (
      <button
        id={id}
        className={`flex w-full justify-center whitespace-nowrap rounded-md ${paddingClass} px-4 text-sm font-medium tracking-wide focus:outline-none ${
          disabled
            ? 'cursor-default bg-gray-200 text-gray-400'
            : `${colorClass} focus:ring-2`
        }`}
        onClick={(event) => {
          if (!disabled && onClick) {
            onClick(event)
          }
        }}
        type={type}
        disabled={disabled}
        ref={ref}
      >
        {children}
      </button>
    )
  }
)

Button.displayName = 'Button'

export default Button
