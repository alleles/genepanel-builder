# Test NCBI API functionality

import logging
import os

from gpbuilder import ncbi_api


class TestGFFManager(object):
    gff_filename = "refseq.gff.gz"
    gff_filepath = os.path.join(os.path.dirname(__file__), gff_filename)
    gffmanager = ncbi_api.GFFManager(file_path=gff_filepath)

    def test_gff_db(self, caplog):
        caplog.set_level(logging.INFO)
        refseq = self.gffmanager.db["gene-BDNF-AS"]
        assert refseq.featuretype == "gene"
        assert refseq.start == 27528399 and refseq.end == 27719718

    def test_gff_scaffolds(self, caplog):
        caplog.set_level(logging.INFO)
        assert "NW_003315972.1" not in self.gffmanager.scaffold_lookup_table
        assert "NW_003571031.1" not in self.gffmanager.scaffold_lookup_table
        assert self.gffmanager.scaffold_lookup_table["NC_000020.10"] == "20"

    def test_gff_attributes(self, caplog):
        caplog.set_level(logging.INFO)
        f = self.gffmanager.db["exon-NM_000535.7-7"]
        query = self.gffmanager.find_attributes(f, "gene_synonym")
        assert query["gene_synonym"] == "HNPCC4,MLH4,MMRCS4,PMS-2,PMS2CL,PMSL2"

    def test_gff_xref(self, caplog):
        caplog.set_level(logging.INFO)
        f = self.gffmanager.db["gene-BDNF-AS"]
        assert self.gffmanager.find_xref_id(f) == "20608"

    def test_gff_founder_types(self, caplog):
        caplog.set_level(logging.INFO)
        assert self.gffmanager.founder_types() == {"gene", "pseudogene", "region"}

    def test_gff_match_types(self, caplog):
        caplog.set_level(logging.INFO)
        assert self.gffmanager.match_types("transcr") == {
            "transcript",
            "primary_transcript",
        }

    def test_query_children_attributes(self, caplog):
        caplog.set_level(logging.INFO)
        flist = list(
            self.gffmanager.query_children_attributes(types="transcript", gene="clta")
        )
        assert len(flist) == 1 and flist.pop().id == "rna-NR_132349.2"
        elist = list(
            map(
                lambda e: e.id,
                sorted(
                    self.gffmanager.query_children_attributes(
                        parents="transcript", product="clathrin"
                    ),
                    key=lambda e: (e.start, e.end),
                ),
            )
        )
        assert elist == [
            "exon-NR_132349.2-1",
            "exon-NR_132349.2-2",
            "exon-NR_132349.2-3",
            "exon-NR_132349.2-4",
        ]
