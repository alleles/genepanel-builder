import pytest

from gpbuilder.api.tests.mock_data import create_genes, set_next_update_id
from gpbuilder.datamodel.phenotype import Phenotype, PhenotypeGene


@pytest.mark.parametrize(
    "inheritance_descriptions, expected",
    [
        (["x-linked recessive"], "XD/XR"),
        (["x-linked dominant"], "XD/XR"),
        (["x-linked"], "XD/XR"),
        (["autosomal dominant"], "AD"),
        (["autosomal recessive"], "AR"),
        (["x-linked recessive", "x-linked dominant"], "XD/XR"),
        (["x-linked recessive", "x-linked dominant", "autosomal dominant"], "AD/AR"),
        (["foobar", "autosomal dominant"], "AD"),
        (["foobar", "x-linked dominant"], "XD/XR"),
        (["foobar", "x-linked"], "XD/XR"),
    ],
)
def test_get_inheritance_modes(session, inheritance_descriptions, expected):
    genes = create_genes(4, session)
    update_id = set_next_update_id("phenotype", session)
    ph = Phenotype(mim_number=123, titles={"foo", "bar"}, update_id=update_id)
    session.add_all(genes + [ph])
    session.flush()

    # Use input inheritance descriptions to create phenotype genes for first gene
    phenotype_genes = [
        PhenotypeGene(
            gene_id=genes[0].hgnc_id,
            phenotype_id=ph.mim_number,
            inheritance_descr=inheritance_descr,
        )
        for inheritance_descr in inheritance_descriptions
    ]

    # Add some phenotype genes for other genes

    phenotype_gene_invalid_inheritance = PhenotypeGene(
        gene_id=genes[1].hgnc_id,
        phenotype_id=ph.mim_number,
        inheritance_descr="foobar",
    )

    # No phenotype genes for genes[2]
    # Phenotypes that we will not request
    phenotype_gene_not_requested = PhenotypeGene(
        gene_id=genes[3].hgnc_id,
        phenotype_id=ph.mim_number,
        inheritance_descr="x-linked recessive",
    )

    session.add_all(
        phenotype_genes
        + [phenotype_gene_invalid_inheritance]
        + [phenotype_gene_not_requested]
    )
    session.flush()

    inheritance_modes = PhenotypeGene.get_inheritance_by_hgnc_ids(
        session, [gene.hgnc_id for gene in genes[:-1]]
    )
    assert inheritance_modes == {genes[0].hgnc_id: expected}

    inheritance_modes_all = PhenotypeGene.get_inheritance_by_hgnc_ids(
        session, [gene.hgnc_id for gene in genes]
    )
    assert inheritance_modes_all == {
        genes[0].hgnc_id: expected,
        genes[3].hgnc_id: "XD/XR",
    }
