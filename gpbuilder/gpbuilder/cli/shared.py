import logging
from typing import Any, Callable, Optional

import typer
from rich.console import Console
from rich.progress import Progress, SpinnerColumn, TextColumn
from rich.prompt import Confirm

err_console = Console(stderr=True)
console = Console(stderr=False)


def confirmation(ctx: typer.Context) -> Optional[bool]:
    if ctx.obj.skip_confirmation:
        return True
    msg_prompt = f"Do you really want to: [bold green]{ctx.command_path}[/bold green]?"

    if not Confirm.ask(msg_prompt):
        raise typer.Abort()
    else:
        return True


def init_log(name: str, file_name: str = "", verbose: bool = False):
    log = logging.getLogger()  # root logger
    if verbose:
        log.setLevel(logging.DEBUG)
    if file_name:
        fh = logging.FileHandler(file_name, "w")
        formatter = logging.Formatter(
            "%(asctime)s [%(name)s:%(levelname)s]: %(message)s", "%Y-%m-%d %H:%M:%S"
        )
        fh.setFormatter(formatter)
        log.addHandler(fh)
    return log


def progress_spinner(
    cmd: Callable[..., Any], *args, description: str = "Processing ..."
):
    with Progress(
        SpinnerColumn(),
        TextColumn("[progress.description]{task.description}"),
        transient=True,
    ) as progress:
        progress.add_task(description=description, total=None)
        cmd(*args)
    console.print(f"Done processing {getattr(cmd, '__name__', '')}")


def safe_str(s):
    """Prevent outputting None"""
    if s is None:
        return ""
    return str(s)
