import { HTMLProps, ReactElement } from 'react'

type Props = HTMLProps<HTMLDivElement> & {
  buttons: { key: string; label: string | ReactElement }[]
  selectedButton: string | null
  onChange: (buttonKey: string) => void
  direction?: 'row' | 'column'
  nullable?: boolean // If set to true, you can click a selected button again to remove the selected option and set thew value to an empty string.
  disabled: boolean
}

export function RadioButtons({
  buttons,
  selectedButton,
  onChange,
  nullable = false,
  disabled,
}: Props) {
  const onOptionButtonClick = (optionKey: string) => {
    if (nullable && optionKey === selectedButton) {
      onChange('')
    }

    if (optionKey !== selectedButton) {
      onChange(optionKey)
    }
  }

  return (
    <div>
      {buttons.map((button) => (
        <button
          className={`mx-auto flex w-full justify-center rounded-sm p-2 ${getSpecificButtonClasses(
            selectedButton === button.key,
            nullable,
            disabled
          )}`}
          key={button.key}
          onClick={() => !disabled && onOptionButtonClick(button.key)}
        >
          {button.label}
        </button>
      ))}
    </div>
  )
}

const getSpecificButtonClasses = (selected, nullable, disabled) => {
  if (selected) {
    return `bg-gray-700 text-white ${
      !nullable || disabled ? 'cursor-default' : 'cursor-pointer'
    }`
  }

  return `bg-gray-100 text-gray-500 ${
    disabled ? 'cursor-default' : 'cursor-pointer hover:bg-gray-200'
  }`
}
