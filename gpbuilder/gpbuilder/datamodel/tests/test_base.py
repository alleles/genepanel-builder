from types import SimpleNamespace

import pytest
from pydantic import ValidationError
from sqlmodel import Field

from gpbuilder.datamodel.base import SQLModelBase


class Foo(SQLModelBase):
    x: int


class FooTable(SQLModelBase, table=True):
    x: int = Field(primary_key=True)


def test_validation():
    Foo(x=1)  # Passes validation
    with pytest.raises(ValidationError):
        Foo(x="not an integer")  # type: ignore
    with pytest.raises(ValidationError):
        Foo()  # type: ignore


def test_from_orm():
    Foo.from_orm(SimpleNamespace(x=1))  # Passes validation
    with pytest.raises(ValidationError):
        Foo.from_orm(SimpleNamespace(x="not an integer"))


def test_table_validation():
    FooTable()  # type: ignore # Skips validation
    FooTable(x=1)  # Passes validation
    with pytest.raises(ValidationError):
        FooTable(x="not an integer")  # type: ignore


def test_table_from_orm():
    FooTable.from_orm(SimpleNamespace(x=1))  # Passes validation
    with pytest.raises(ValidationError):
        FooTable.from_orm(SimpleNamespace(x="not an integer"))
