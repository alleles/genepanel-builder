import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid'
import { classNames } from 'components/utils'
import { Fragment, useState } from 'react'

interface Option {
  name: string
  label: string
  value: string
  selected?: boolean
}

export default function Select({
  name,
  label,
  options,
  multiple,
  onChange,
  placeholder,
  disabled,
}: {
  name?: string
  label?: string
  options: Option[] | any
  multiple?: boolean
  onChange?: (v) => void
  placeholder?: string | React.ReactNode
  disabled?: boolean
}) {
  const [selected, setSelected] = useState<string | string[]>(
    multiple
      ? options.filter((o) => o.selected).map((o) => o.value) || []
      : options.find((o) => o.selected)?.value
  )

  const displayValue = () => {
    if (multiple) {
      if (selected.length == 1) {
        return options.find((option) => option.value === selected[0])?.label
      } else if (selected.length == 0) {
        return placeholder || ''
      } else {
        return `${selected.length} selected`
      }
    } else {
      if (!selected) {
        return placeholder || ''
      } else {
        return options.find((option) => option.value === selected)?.label
      }
    }
  }

  return (
    <Listbox
      name={name}
      value={selected}
      onChange={(v) => {
        setSelected(v)
        onChange(v)
      }}
      disabled={disabled}
      multiple={multiple}
    >
      {({ open }) => (
        <>
          <Listbox.Label className="block text-sm font-medium text-gray-700">
            {label}
          </Listbox.Label>
          <div className="relative">
            <Listbox.Button
              className={`relative w-full cursor-default rounded border border-reactgray py-1.5 pl-3 pr-8 text-left ${
                disabled
                  ? 'text-gray-400'
                  : 'text-gray-700 hover:border-reactgray-dark'
              }`}
            >
              <span className="block truncate">{displayValue()}</span>
              <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                <ChevronUpDownIcon
                  className={`h-5 w-5 ${
                    disabled ? 'text-gray-200' : 'text-gray-400'
                  }`}
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="absolute z-40 mt-0.5 max-h-64 w-full overflow-auto rounded bg-white text-base shadow-lg ring-1 ring-black/5 focus:outline-none sm:text-sm">
                {options.map((option) => (
                  <Listbox.Option
                    key={option.value}
                    className={({ active }) =>
                      classNames(
                        active
                          ? 'text-white bg-ellablue-darkest'
                          : 'text-gray-900',
                        'relative cursor-default select-none py-2 pl-3 pr-8'
                      )
                    }
                    value={option.value}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={classNames(
                            selected ? 'font-semibold' : 'font-normal',
                            'block truncate'
                          )}
                        >
                          {option.label}
                        </span>
                        {selected ? (
                          <span
                            className={classNames(
                              active ? 'text-white' : 'text-ellablue-darkest',
                              'absolute inset-y-0 right-0 flex items-center pr-2'
                            )}
                          >
                            <CheckIcon className="h-5 w-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  )
}
