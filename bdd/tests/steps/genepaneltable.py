from behave import then


@then('the version of "{short_name}" should be "{version}"')
def panel_version(context, short_name, version):
    context.execute_steps(
        f"""
        Then I should see an element with xpath "//td[text()='{short_name}']//following-sibling::td[text()='{version}']"
        """
    )


@then('the version of "{short_name}" should not be "{version}"')
def panel_version(context, short_name, version):
    context.execute_steps(
        f"""
        Then I should not see an element with xpath "//td[text()='{short_name}']//following-sibling::td[text()='{version}']"
        """
    )


@then('the number of genes in "{short_name}" should be {number}')
def panel_genes(context, short_name, number):
    context.execute_steps(
        f"""
        Then I should see an element with xpath "//td[text()='{short_name}']//following-sibling::td[text()='{number}']"
        """
    )