class ShortnameExistsException(Exception):
    pass


class CreatePanelException(Exception):
    pass


class DeletePanelException(Exception):
    pass


class UpdatePanelException(Exception):
    pass


class VersionException(Exception):
    pass
