"""
Here follows an incomplete list of endpoints that are not implemented, but
should be implemented in the future.

See queries.py for utility queries that can be utilized to implement these
"""

from fastapi import APIRouter

from gpbuilder.datamodel.genepanel import GenePanel

router = APIRouter()


@router.post("/genepanels/", response_model=GenePanel, tags=["Gene panels"])
def configure(panel: GenePanel):
    "Configure a gene panel"
    raise NotImplementedError()
