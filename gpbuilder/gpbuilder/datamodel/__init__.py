from enum import IntFlag
from typing import Type

import sqlalchemy.types as types
from sqlalchemy import String
from sqlalchemy.ext.compiler import compiles


class SemVer(types.UserDefinedType):
    cache_ok = True

    def get_col_spec(self):
        return "SEMVER"

    def bind_processor(self, dialect):
        def process(value):
            return value

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            return value

        return process


@compiles(String, "postgresql")
def compile_text_postgres(*args, **kwargs):
    return "TEXT"
