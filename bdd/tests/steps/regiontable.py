from behave import then, when


@when('I change the inheritance of region {region} to "{inheritance}"')
def change_region_inheritance(context, region, inheritance):
    context.execute_steps(
        f"""
        When I press the element with xpath "//input[@name='inheritance-region-{region}']/../div/button"
        Then I should see an element with xpath "//input[@name='inheritance-region-{region}']/../div//span[text() = '{inheritance}']" within 1 seconds
        When I press the element with xpath "//input[@name='inheritance-region-{region}']/../div//span[text() = '{inheritance}']"
        """
    )