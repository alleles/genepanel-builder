#!/usr/bin/env python3

import os
from pathlib import Path

import typer

from gpbuilder.cli import import_gp, update
from gpbuilder.cli.shared import confirmation, progress_spinner
from gpbuilder.datamodel.engine import (
    copy_to_initial_db,
    create_db_and_tables,
    drop_all,
)
from gpbuilder.datamodel.scripts.update_gene_defaults import update_gene_defaults

app = typer.Typer()


@app.callback()
def db():
    """
    Apply actions to the entire database (applicable when environment variable DEVELOPMENT=true)
    """


@app.command("drop")
def drop(ctx: typer.Context):
    """
    Remove all content and tables from database
    """
    assert confirmation(ctx)
    progress_spinner(drop_all)


@app.command("create")
def create(ctx: typer.Context):
    """
    Create all database tables
    """
    assert confirmation(ctx)
    progress_spinner(create_db_and_tables)


@app.command("populate")
def populate(ctx: typer.Context, dump: bool = False):
    """
    Populate the database with default content
    """
    assert confirmation(ctx)
    progress_spinner(drop_all, description="Dropping ...")
    progress_spinner(create_db_and_tables, description="Creating ...")
    ctx.invoke(update.all_external, ctx)
    for f in Path("resources/panel-configs").glob("*.tsv"):
        name, version = f.stem.split("_")
        import_gp.panel(ctx, f, name, name.lower(), f.stem, version=version)
    filename = Path("resources/genedefaults.tsv").absolute()
    progress_spinner(update_gene_defaults, ctx.obj.session, filename)
    # Need to commit here before copying to initial db
    ctx.obj.session.commit()
    copy_to_initial_db()

    if dump:
        os.system("/app/gpbuilder/scripts/postgres/pg-dumper.sh -d")


@app.command("restore")
def restore(ctx: typer.Context):
    """
    Restore the database from default dump
    """
    assert confirmation(ctx)
    os.system("/app/gpbuilder/scripts/postgres/pg-dumper.sh -r")
