#!/usr/bin/env python3

import sys
from argparse import ArgumentParser
from pathlib import Path

from make_d4 import make_d4
from make_per_base import make_per_base


def has_crams(files: list[Path]) -> bool:
    """Check if input list of files contains any CRAM files."""
    print("Checking presence of CRAM files...")
    for file in files:
        if file.suffix == ".cram":
            return True
    return False


def calculate_coverage(
    input_path: Path,
    genome_reference: Path | None,
    genome_sizes: Path,
    output_location: Path,
    mapq: int,
) -> None:
    try:
        with open(input_path) as input_file:
            alignment_files: list[Path] = [
                Path(alignment_file.strip())
                for alignment_file in map(str, input_file.readlines())
                if not alignment_file.strip().startswith("#")
            ]
            if has_crams(alignment_files):
                if not genome_reference:
                    raise ValueError(
                        """
                        A genome reference is required for CRAM files. Please provide one using the
                        `--genome-reference` command line option.
                        """
                    )

            output_location.mkdir(parents=True, exist_ok=True)

            per_base_files: list[Path] = make_per_base(
                alignment_files, genome_reference, mapq, output_location
            )

            make_d4(per_base_files, genome_sizes, output_location)

    except FileNotFoundError:
        print(f"File {args.input_file} does not exist.")
        sys.exit(-1)


if __name__ == "__main__":
    parser = ArgumentParser(description="Calculate coverage for a set of sample files.")
    parser.add_argument(
        "-i",
        "--input",
        type=Path,
        required=True,
        help="Text file with a list of alignment files: one (.bam | .cram) file path per line.",
    )
    parser.add_argument(
        "--genome-reference",
        dest="genome_reference",
        metavar="<genome_reference_file.fasta>",
        type=Path,
        required=False,
        help=(
            "Genome reference FASTA file for use with CRAM files "
            "[Needed by mosdepth to convert CRAM files to per base BED.]",
        ),
    )
    parser.add_argument(
        "--genome-sizes",
        dest="genome_sizes",
        metavar="<genome_file.genome>",
        type=Path,
        required=True,
        help=(
            "Genome description file containing the each chromosome's length, one per line "
            "[Needed by d4tools's `create` command].",
        ),
    )
    parser.add_argument(
        "--mapq",
        metavar="<mapq>",
        type=int,
        required=False,
        default=60,
        help=(
            "Minimum mapping quality for inclusion of alignment files in coverage calculations "
            "[optional; default=60]."
        ),
    )
    parser.add_argument(
        "-o",
        "--output",
        metavar="<output_path>",
        type=Path,
        required=True,
        help="Path to the directory to save the output files in.",
    )

    args = parser.parse_args()
    calculate_coverage(
        args.input, args.genome_reference, args.genome_sizes, args.output, args.mapq
    )
