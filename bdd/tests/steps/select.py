from behave import when


@when('I fill in panelApp name with "{panel}"')
def fill_in_panelapp_name(context, panel):
    context.execute_steps(
        f"""
        When I fill in "search-panelapp-input" with "{panel}"
        """
    )


@when('I add panel "{panel}"')
def add_panel(context, panel):
    context.execute_steps(
        f"""
        When I fill in panelApp name with "{panel}"
        Then I should see an element with xpath "//div[contains(text(), '{panel}')]" within 15 seconds
        When I press the element with xpath "//div[contains(text(), '{panel}')]"
        And I press "panelapp-add"
        Then I should see "{panel}" within 15 seconds
        """
    )


@when('I add gene "{gene}"')
def add_gene(context, gene):
    context.execute_steps(
        f"""
        When I fill in "search-genes-input" with "{gene}"
        Then I should see an element with xpath "//div[contains(text(), '{gene}')]" within 15 seconds
        When I press the element with xpath "//div[contains(text(), '{gene}')]"
        And I press "gene-add"
        Then I should see "{gene}" within 15 seconds
        """
    )


@when('I add multiple genes "{genes}"')
def add_multiple_genes(context, genes):
    genes = [g.strip() for g in genes.split(",")]
    inp = context.browser.find_by_id("search-genes-listinput")
    assert inp, "Could not find search-genes-listinput"
    inp.fill("\n".join(genes))

    context.execute_steps(
        """
        When I wait for 1 seconds
        When I press "Add List"
    """
    )


@when('I add region "{region}"')
@when('I add region "{region}" with name "{name}"')
def add_region(context, region, name=None):
    context.execute_steps(
        f"""
        When I fill in "region-input" with "{region}"
        And I fill in "region-name" with "{name}"
        And I press "region-add"
        Then I should see "{region}" within 15 seconds
        And I should see "{name}"
        """
    )


@when('I remove "{input}"')
def remove_input(context, input):
    context.execute_steps(
        f"""
        When I press the element with xpath "//*[contains(text(), '{input}')]//following::button[contains(text(), 'Remove')][1]"
        Then I should not see "{input}"
        """
    )
