"""
Script to update the transcript names in a config file to match the transcripts in the database.

This script looks for transcripts in the database, and selects an updated transcript name 
based on the following precedent:

1. Same transcript
2. MANE Select transcript with the same base (e.g. NM_000123.4 -> NM_000123.5)
3. Highest available transcript with the same base (e.g. NM_000123.4 -> NM_000123.6)
4. MANE Select transcript with the same HGNC ID (e.g. NM_000123.4 -> NM_000456.4)

If none of the above are found, the script will fail.

"""

import logging
import re
from pathlib import Path

from sqlmodel import Integer, func, select

from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript

logger = logging.getLogger(__name__)


def find_closes_transcript_match(
    tx_name: str, hgnc_id: int
) -> tuple[Transcript, str | None]:
    # Some transcripts are given as e.g. NM_000123Exon4.3, which is not a valid transcript name.
    # We consider e.g. NM_000123Exon4.3 as NM_000123.3
    m = re.match(r"(NM_\d+).*?\.(\d+)", tx_name)
    assert m, f"Could not parse transcript name {tx_name}"
    tx_base, tx_version = m.groups()
    source = None
    with get_session() as session:
        transcript = session.exec(
            select(Transcript).where(Transcript.name == f"{tx_base}.{tx_version}")
        ).one_or_none()

        if transcript is None:
            # Select MANE transcript if this has the same base
            transcript = session.exec(
                select(Transcript)
                .join(ManeSelectTranscript)
                .where(
                    Transcript.name.startswith(tx_base + "."),
                    ManeSelectTranscript.status == "MANE Select",
                )
            ).one_or_none()
            if transcript:
                source = "MANE Select (same base)"

        if transcript is None:
            # Select highest available version
            transcript = session.exec(
                select(Transcript)
                .where(Transcript.name.startswith(tx_base + "."))
                .order_by(func.split_part(Transcript.name, ".", 2).cast(Integer).desc())
            ).first()
            if transcript:
                source = "Highest available version (same base)"

        if transcript is None:
            # Select MANE transcript if this has the same base
            transcript = session.exec(
                select(Transcript)
                .join(ManeSelectTranscript)
                .where(
                    Transcript.hgnc_id == hgnc_id,
                    ManeSelectTranscript.status == "MANE Select",
                )
            ).one_or_none()
            if transcript:
                source = "MANE Select (different base)"
        assert transcript is not None, f"Could not find transcript {tx_name}"
    return (transcript, source)


def update_panel_config(cfg_file: Path) -> str:
    pttrn = r".*?(NM_[^\.]+?\.\d+).*"
    updated_cfg = []
    with open(cfg_file, "r") as f:
        for line in f:
            m = re.match(pttrn, line)
            if not m:
                updated_cfg.append(line)
                continue

            tx_name = m.group(1)
            hgnc_id = int(line.split("\t")[1])

            match_tx, source = find_closes_transcript_match(tx_name, hgnc_id)

            if source:
                logger.warning(f"Rewriting {tx_name} to {match_tx.name} ({source})")
            line = re.sub(tx_name, match_tx.name, line)
            if line in updated_cfg:
                logger.warning(f"Dropping duplicate: {line.strip()}")
                continue
            updated_cfg.append(line)
    return "".join(updated_cfg)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--dry-run", action="store_true")
    parser.add_argument("cfg_files", type=Path, nargs="+")
    args = parser.parse_args()

    for cfg_file in args.cfg_files:
        logger.info(f"Rewriting {cfg_file.absolute()}")
        updated_panel_config = update_panel_config(cfg_file.absolute())
        if not args.dry_run:
            with open(cfg_file, "w") as f:
                f.write(updated_panel_config)
        else:
            logger.info("DRY RUN: Not writing to file")
