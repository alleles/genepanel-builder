import API from 'api'
import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import Layout from 'components/layouts/configure'
import DefaultLayout from 'components/layouts/default'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { ConfidenceLevel } from 'types'
import { regionKey } from 'components/regionselector'
import GeneTable, { GeneTableItem } from 'components/genetable'
import RegionTable, { RegionTableItem } from 'components/regiontable'
import TrafficLight from 'components/traffic-light'

export default function ViewPanel() {
  const router = useRouter()
  const { id } = router.query

  const [geneData, setGeneData] = useState<GeneTableItem[]>([])
  const [regionData, setRegionData] = useState<RegionTableItem[]>([])

  const {
    genePanelConfig,
    panelAppPanels,
    genes,
    regions,
    fetchGeneRegionDetails,
  } = useGenePanel()
  const loading = id && !genePanelConfig.id

  useEffect(() => {
    fetchGeneRegionDetails().then((data) => {
      const [genes, regions] = data
      setGeneData(genes)
      setRegionData(regions)
    })
  }, [fetchGeneRegionDetails])
  if (!genePanelConfig) {
    return null
  }

  return (
    <div className="m-auto h-screen w-full">
      <div className="sticky top-0 z-30 flex w-full flex-row items-center justify-between bg-ellapurple-lighter py-2 px-8">
        <h2 className="text-lg font-medium text-gray-900">
          View gene panel:
          <span className="font-normal italic">
            {` ${genePanelConfig.shortname}`}
          </span>
        </h2>
        <div className="text-sm text-gray-500">
          This is a read-only view. To modify or create a new draft, click the
          button to the right.
        </div>
        <div>
          <div className="w-40">
            <Button
              disabled={loading}
              onClick={() =>
                router.push(`/configure/name?id=${genePanelConfig.id}`)
              }
            >
              {genePanelConfig.version.indexOf('draft') === -1
                ? 'Create New Draft'
                : 'Modify Draft'}
            </Button>
          </div>
        </div>
      </div>

      <div className="h-[calc(100vh-89px)] overflow-x-hidden overflow-y-scroll">
        <div className="mt-6 w-full space-y-6 pl-8 pr-7 pb-8">
          <h2 className="inline-flex w-full items-center">
            <span className="whitespace-nowrap bg-white pr-5 text-2xl text-gray-900">
              Name and description
            </span>
            <hr className="my-6 h-px w-full border-0 bg-gray-200" />
          </h2>
          <div>
            <div className="text-gray-700">
              {genePanelConfig.name}{' '}
              <span className="font-mono">[{genePanelConfig.shortname}]</span>
            </div>
            <div className="pt-2 text-sm text-gray-500">
              {genePanelConfig.description}
            </div>
          </div>

          <h2 className="inline-flex w-full items-center">
            <span className="whitespace-nowrap bg-white pr-5 text-2xl text-gray-900">
              Sources for selection
            </span>
            <hr className="my-6 h-px w-full border-0 bg-gray-200" />
          </h2>
          <div>
            <h3 className="text-lg text-gray-900">PanelApp panels</h3>
            <ul>
              {panelAppPanels.map((panel) => (
                <li
                  className="grid grid-cols-3 py-2 text-sm text-gray-700"
                  key={panel.id}
                >
                  <div className="col-span-2 inline-flex items-center">
                    <span>{`${panel.name} - v${panel.version}`}</span>
                    {panel.confidence_levels[ConfidenceLevel.green] ? (
                      <span className="ml-2">
                        <TrafficLight
                          color="green"
                          size={14}
                          clickable={false}
                        />
                      </span>
                    ) : null}
                    {panel.confidence_levels[ConfidenceLevel.amber] ? (
                      <span className="ml-2">
                        <TrafficLight
                          color="amber"
                          size={14}
                          clickable={false}
                        />
                      </span>
                    ) : null}
                    {panel.confidence_levels[ConfidenceLevel.red] ? (
                      <span className="ml-2">
                        <TrafficLight color="red" size={14} clickable={false} />
                      </span>
                    ) : null}
                  </div>
                </li>
              ))}
            </ul>
          </div>
          <div>
            <h3 className="text-lg text-gray-900">Manually added genes</h3>
            <ul className="mr-2 max-h-64 overflow-y-auto rounded border-r border-gray-300 pb-2 hover:shadow-sm 2xl:w-3/5">
              {genes.map((gene) => (
                <li
                  key={gene.hgnc_id}
                  className="grid grid-cols-5 items-start text-sm"
                >
                  <span className="col-span-1 pt-2 text-gray-700">
                    {gene.symbol}
                  </span>
                  <span className="col-span-1 pt-2 text-gray-700">
                    HGNC:{gene.hgnc_id}
                  </span>
                  <span className="col-span-3 pt-2 text-gray-700">
                    <span className="italic">Alias: </span>
                    {gene.aliases.length > 0 ? (
                      <span>{gene.aliases.join(', ')}</span>
                    ) : (
                      <span>-</span>
                    )}
                    <span className="italic"> | Previous: </span>
                    {gene.previous_symbols.length > 0 ? (
                      <span>{gene.previous_symbols.join(', ')}</span>
                    ) : (
                      <span>-</span>
                    )}
                  </span>
                </li>
              ))}
            </ul>
          </div>
          <div>
            <h3 className="text-lg text-gray-900">Manually added regions</h3>
            <ul className="mr-2 max-h-64 overflow-y-auto rounded border-r border-gray-300 pb-2 hover:shadow-sm 2xl:w-3/5">
              {regions.map((region) => (
                <li
                  key={regionKey(region)}
                  className="grid grid-cols-3 items-start gap-2 text-sm"
                >
                  <span className="col-span-1 pt-2 text-gray-700">
                    {region.name}
                  </span>
                  <span className="col-span-1 pt-2 text-gray-700">
                    {`chr${region.chromosome}:${region.start}-${region.end} `}
                  </span>
                </li>
              ))}
            </ul>
          </div>

          <h2 className="inline-flex w-full items-center">
            <span className="whitespace-nowrap bg-white pr-5 text-2xl text-gray-900">
              Selected genes and regions
            </span>
            <hr className="my-6 h-px w-full border-0 bg-gray-200" />
          </h2>
          <div>
            <h3 className="mb-4 text-lg text-gray-900">Genes</h3>
            <GeneTable
              genes={geneData}
              onGenesUpdated={() => {}}
              readOnly={true}
            />
          </div>
          <div>
            <h3 className="mb-4 text-lg text-gray-900">Regions</h3>
            <RegionTable
              regions={regionData}
              onRegionsUpdated={() => {}}
              readOnly={true}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

ViewPanel.getLayout = (page) => (
  <DefaultLayout>
    <Layout>{page}</Layout>
  </DefaultLayout>
)
