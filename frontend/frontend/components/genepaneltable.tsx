import {
  CellContext,
  ColumnDef,
  createColumnHelper,
} from '@tanstack/react-table'
import API from 'api'
import ConfirmationDialog from 'components/confirmationdialog'
import GPLink from 'components/link'
import Table from 'components/table'
import { format } from 'date-fns'
import React, { useEffect, useState } from 'react'

import { ArrowDownTrayIcon } from '@heroicons/react/24/outline'
import Toggle from 'components/toggle'
import Link from 'next/link'
import Button from './button'
export type GenePanelRow = {
  id: number
  name: string
  shortname: string
  description: string | null
  version: string
  created: Date
  updated: Date | null
  inactive: boolean
  gene_count: number
  masterpanel_id: number | null
}

export default function GenePanelTable() {
  const [genePanels, setGenePanels] = useState([])
  const [data, setData] = useState<GenePanelRow[]>([])
  const [showInactive, setShowInactive] = React.useState(false)
  const [toDelete, setToDelete] = useState(0)

  const fetchPanels = () => {
    API.GenePanels.get().then((res) => {
      const panels = res.data.map((panel) => ({
        ...panel,
        updated: panel.updated || panel.created,
      }))
      panels.sort((a, b) => {
        if (a.shortname > b.shortname) return 1
        if (a.shortname < b.shortname) return -1
        if (a.version > b.version) return 1
        if (a.version < b.version) return -1

        return 0
      })
      setGenePanels(panels)
    })
  }

  useEffect(() => {
    fetchPanels()
  }, [])

  useEffect(() => {
    if (!showInactive) {
      const panels = genePanels.filter((panel) => !panel.inactive)
      setData(panels)
    } else setData(genePanels)
  }, [genePanels, showInactive])

  const DateRenderer = function (info: CellContext<GenePanelRow, Date>) {
    const value = info.getValue()
    return (
      <span>{value ? format(new Date(value), 'yyyy-MM-dd HH:mm') : ''}</span>
    )
  }

  const PanelNameRender = (info) => {
    return (
      <div className="text-ellablue-darkest underline">
        <GPLink href={`/view?id=${info.row.original.id}`}>
          {info.row.original.name}
        </GPLink>
      </div>
    )
  }

  const columnHelper = createColumnHelper<GenePanelRow>()
  const columns = React.useMemo<ColumnDef<GenePanelRow>[]>(
    () => {
      let cols = [
        columnHelper.accessor('id', {
          header: 'ID',
          enableColumnFilter: false,
          size: 60,
        }),
        columnHelper.accessor('name', {
          header: 'Name',
          cell: (info) => PanelNameRender(info),
          filterFn: 'fuzzy',
          size: 240,
        }),
        columnHelper.accessor('shortname', {
          header: 'Short Name',
          filterFn: 'fuzzy',
          size: 120,
        }),
        columnHelper.accessor('description', {
          header: 'Description',
          filterFn: 'fuzzy',
          enableSorting: false,
          size: 300,
        }),
        columnHelper.accessor('gene_count', {
          header: 'Gene count',
          enableColumnFilter: false,
          size: 120,
        }),

        columnHelper.accessor('version', {
          header: 'Version',
          filterFn: 'fuzzy',
          size: 120,
        }),
        columnHelper.accessor('created', {
          header: 'Created',
          cell: (info) => DateRenderer(info),
          enableColumnFilter: true,
          size: 120,
        }),
        columnHelper.accessor('updated', {
          header: 'Updated',
          cell: (info) => DateRenderer(info),
          enableColumnFilter: true,
          size: 120,
        }),
        {
          header: 'Delete draft',
          cell: (info) => {
            const row = info.row
            if (row.getValue('version').indexOf('draft') > 0)
              return (
                <Button
                  color="danger"
                  padding="narrow"
                  onClick={() => {
                    setToDelete(row.getValue('id'))
                  }}
                >
                  Delete
                </Button>
              )
          },
          size: 60,
        },
        {
          header: 'Download',
          cell: (info) => {
            const row = info.row
            return (
              <div className="pl-4">
                <Link href={`/api/v1/genepanel/${row.getValue('id')}/export`}>
                  <ArrowDownTrayIcon className="h-5 w-5 text-gray-500" />
                </Link>
              </div>
            )
          },
          size: 60,
        },
      ]
      if (showInactive)
        cols = [
          ...cols,
          {
            header: 'Inactive',
            enableColumnFilter: false,
            accessorKey: 'inactive',
            size: 60,
          },
        ]

      return cols
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data, showInactive] // data is sufficient, because all renderers only depend on this
  )

  return (
    <>
      <div className="mb-2">
        <Toggle
          name="showInactive"
          label="Show inactive (old) gene panels"
          onChange={setShowInactive}
        />
      </div>
      <Table columns={columns} rows={data}></Table>
      {toDelete > 0 && (
        <ConfirmationDialog
          onConfirm={() => {
            API.GenePanel.del(toDelete).then(() => {
              fetchPanels()
              setToDelete(0)
            })
          }}
          onClose={() => {
            setToDelete(0)
          }}
        >
          Are you sure you want to delete this draft?
        </ConfirmationDialog>
      )}
    </>
  )
}
