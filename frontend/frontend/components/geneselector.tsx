import React, { useEffect, useState } from 'react'
import AsyncSelect from 'react-select/async'

import API from 'api'
import { AxiosError } from 'axios'
import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import { asyncDebounce } from 'utils'

const searchCallback = asyncDebounce(async (searchTerm, ...rest) => {
  const response = await API.GeneSearch.search(searchTerm)

  return response.data.map((doc) => ({
    value: doc,
    label:
      `${doc.symbol} - HGNC:${doc.hgnc_id} ` +
      `${
        doc.aliases.length ? ' (Alias: ' + doc.aliases.join(', ') + ' |' : '('
      } ` +
      `${
        doc.previous_symbols.length > 0
          ? 'Previous: ' + doc.previous_symbols.join(', ') + ')'
          : ')'
      }`,
  }))
}, 300)

const fetchGenes = async (list) => {
  const hgnc_ids = list
    .replace(/^HGNC:/gm, '')
    .replace(/,\s+/g, ',')
    .split(/[\n,\s+]/)
    .map(Number)
    .filter(Boolean)

  const response = await API.Genes.post(hgnc_ids)
  return response.data.map((doc) => ({
    value: doc,
    label:
      `${doc.symbol} - ${doc.hgnc_id} ` +
      `${
        doc.aliases.length ? ' (aliases: ' + doc.aliases.join(', ') + ')' : ''
      } ` +
      `${
        doc.previous_symbols.length > 0
          ? '[previous symbols: ' + doc.previous_symbols.join(', ') + ']'
          : ''
      }`,
  }))
}

export default function GeneSelector() {
  const [selected, setSelected] = useState(null)
  const [batchImport, setBatchImport] = useState('')
  const [error, setError] = useState<string | null>(null)

  useEffect(() => {
    if (
      batchImport === '' ||
      batchImport.match(/^(HGNC:\d{1,}[\n\r]?|[\d+\n\r])+$/)
    ) {
      setError(null)
    } else {
      setError(
        'Invalid input. Only HGNC IDs (e.g. 1101 or HGNC:1101) separated with line breaks accepted.'
      )
    }
  }, [batchImport])

  const { genes, addGenes, removeGene } = useGenePanel()

  return (
    <React.Fragment>
      <div className="space-y-4">
        <div className="grid grid-cols-5 items-start space-x-2">
          <AsyncSelect
            className="col-span-4"
            placeholder="Search gene by symbol, id or aliases …"
            inputId="search-genes-input"
            // @ts-ignore
            loadOptions={searchCallback}
            value={selected}
            onChange={(selected) => setSelected(selected)}
          />
          <div className="w-32 justify-self-end" id="gene-add">
            <Button
              color="confirm"
              disabled={!selected}
              onClick={() => {
                addGenes([selected.value])
                setSelected(null)
              }}
            >
              Add
            </Button>
          </div>
        </div>
        <div
          id="search-genes-list"
          className="grid grid-cols-5 items-start space-x-2"
        >
          <div className="col-span-4">
            <textarea
              id="search-genes-listinput"
              color="confirm"
              value={batchImport}
              onChange={(e) => {
                setBatchImport(e.target.value)
              }}
              rows={3}
              className={`block w-full rounded border border-reactgray py-1 pt-2 pl-2 text-gray-700 placeholder:text-reactgray-darkest hover:border-reactgray-dark ${
                error === null
                  ? 'border-reactgray'
                  : 'border-ellared-dark hover:border-ellared-dark focus:ring-2 focus:ring-ellared-dark'
              }  `}
              placeholder="Paste a list of HGNC IDs here ..."
            ></textarea>
            {error && <span className="text-ellared-dark">{error}</span>}
          </div>
          <div className="w-32 justify-self-end" id="gene-list-add">
            <Button
              disabled={Boolean(error) || batchImport === ''}
              onClick={() => {
                fetchGenes(batchImport)
                  .then((data) => {
                    addGenes(
                      data.map((g) => ({
                        ...g.value,
                        aliases: g.value.aliases ?? [],
                        previous_symbols: g.value.previous_symbols ?? [],
                      }))
                    )
                    setBatchImport('')
                  })
                  .catch((error) => {
                    if (
                      error instanceof AxiosError &&
                      error.response.status === 400
                    ) {
                      setError(error.response.data.detail)
                    } else {
                      throw error
                    }
                  })
              }}
            >
              Add List
            </Button>
          </div>
        </div>
      </div>
      <div className="mt-3">
        <ul>
          {genes.map((gene) => (
            <li
              key={gene.hgnc_id}
              className="grid grid-cols-7 items-start gap-2 border-t border-gray-200 py-3"
            >
              <span className="col-span-1 pt-2 pl-2 font-bold text-gray-500">
                {gene.symbol}
              </span>
              <span className="col-span-1 pt-2 pl-4 text-gray-500">
                HGNC:{gene.hgnc_id}
              </span>
              <span className="col-span-3 pt-2 pr-2 text-gray-500">
                <span className="italic">Alias: </span>
                {gene.aliases.length > 0 ? (
                  <span>{gene.aliases.join(', ')}</span>
                ) : (
                  <span>-</span>
                )}
                <span className="italic"> | Previous: </span>
                {gene.previous_symbols.length > 0 ? (
                  <span>{gene.previous_symbols.join(', ')}</span>
                ) : (
                  <span>-</span>
                )}
              </span>
              <span className="col-span-2 w-32 justify-self-end">
                <Button color="danger" onClick={() => removeGene(gene.hgnc_id)}>
                  Remove
                </Button>
              </span>
            </li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  )
}
