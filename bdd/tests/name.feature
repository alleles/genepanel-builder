Feature: Name a new gene panel

    As a user I should be able to provide a name, short name and description to a new gene panel,
    within constraints, including not allowed to use an already existing short name.

    Background:
        Given initial state
        Given a browser

    Scenario: Draft and submit a new panel with a name and short name

        Given "testuser1" as the persona
        When I visit "/"
        And I press "New Gene Panel"

        # Add name and description
        ## Test cannot procede without name
        And I press "Continue (Select)"
        Then field "name" should be invalid
        And field "short-name" should be invalid

        ## Test cannot procede with invalid short name
        When I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "test panel"
        And I press "Continue (Select)"
        Then field "short-name" should be invalid

        ## Test cannot procede with already existing short name
        When I fill in "short-name" with "lysosomal"
        And I press "Continue (Select)"
        And I wait for 1 seconds
        Then field "short-name" should be invalid

        ## Start over with valid entries
        When I fill in "name" with "Change this name"
        And I fill in "short-name" with "testpanel"
        And I fill in "description" with "Test description"
        And I press "Continue (Select)"
        Then I should see "Select genes and regions for: testpanel" within 5 seconds

        # Test that can change saved name
        When I wait for 1 seconds
        And I press "Previous (Name)"
        And I wait for 1 seconds
        And I fill in "name" with "Test gene panel"
        And I press "Continue (Select)"
        Then I should see "Select genes and regions for: testpanel" within 5 seconds

        # Populate with a gene panel
        When I add panel "Familial breast cancer"
        And I press "Continue (Edit)"
        # Check that name is stored
        Then I should see "Edit gene panel: testpanel" within 5 seconds

        ## Check that new gene panel is stored with name, short name, description and draft version
        Given "testuser2" as the persona
        When I visit "/"
        Then I should see "Test gene panel" within 5 seconds
        And I should see "testpanel"
        And I should see "Test description"
        And the version of "testpanel" should be "0.0.1-draft"

        # Submit the panel and check that it's stored with name, short name, description and final version
        Given "testuser1" as the persona
        When I submit the gene panel
        Then I should see "All gene panels" within 5 seconds
        And I should see "Test gene panel" within 5 seconds
        And I should see "Test description"
        And the version of "testpanel" should be "0.1.0"
