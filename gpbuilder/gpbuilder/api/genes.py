from typing import List, Optional

import sqlmodel
from fastapi import APIRouter, HTTPException, status

from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.gene import Gene

router = APIRouter()


@router.get(
    "/genenames/hgnc/{hgnc_id}", response_model=Optional[Gene], tags=["Gene names"]
)
async def get_hgnc_id(hgnc_id: int):
    with get_session() as session:
        statement = sqlmodel.select(Gene).where(Gene.hgnc_id == hgnc_id)
        return session.exec(statement).one_or_none()


@router.post(
    "/genenames/hgnc_ids",
    response_model=List[Gene],
    tags=["Gene names"],
)
async def get_hgnc_ids(hgnc_ids: List[int], ensure_all: bool = False):
    with get_session() as session:
        try:
            return Gene.from_hgnc_ids(session, hgnc_ids, ensure_all=ensure_all)
        except ValueError as e:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@router.get(
    "/genenames/search/{pattern}", response_model=List[Gene], tags=["Gene names"]
)
async def search_genes(pattern: str):
    with get_session() as session:
        statement = sqlmodel.select(Gene).where(
            sqlmodel.or_(
                sqlmodel.col(Gene.symbol).ilike("%" + pattern + "%"),
                sqlmodel.func.array_to_string(Gene.aliases, " ").ilike(
                    "%" + pattern + "%"
                ),
                sqlmodel.func.array_to_string(Gene.previous_symbols, " ").ilike(
                    "%" + pattern + "%"
                ),
                Gene.hgnc_id == pattern if pattern.isdecimal() else False,
            )
        )
        return session.exec(statement).all()
