import {
  compareItems,
  RankingInfo,
  rankItem,
} from '@tanstack/match-sorter-utils'
import {
  Column,
  ColumnDef,
  ColumnFiltersState,
  FilterFn,
  flexRender,
  getCoreRowModel,
  getFacetedMinMaxValues,
  getFacetedRowModel,
  getFacetedUniqueValues,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  Row,
  SortingFn,
  sortingFns,
  SortingState,
  Table,
  useReactTable,
} from '@tanstack/react-table'
import React from 'react'

import {
  ArrowDownIcon,
  ArrowUpIcon,
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/24/solid'
import { classNames } from 'components/utils'
import Spinner from 'components/spinner'
declare module '@tanstack/table-core' {
  interface FilterFns {
    fuzzy: FilterFn<unknown>
  }
  interface FilterMeta {
    itemRank: RankingInfo
  }
}

const fuzzyFilter: FilterFn<any> = (row, columnId, value, addMeta) => {
  // Rank the item
  const itemRank = rankItem(row.getValue(columnId), value)

  // Store the itemRank info
  addMeta({
    itemRank,
  })
  // Return if the item should be filtered in/out
  return itemRank.passed
}

export const fuzzySort: SortingFn<any> = (rowA, rowB, columnId) => {
  let dir = 0
  // Only sort by rank if the column has ranking information
  if (rowA.columnFiltersMeta[columnId]) {
    dir = compareItems(
      rowA.columnFiltersMeta[columnId]?.itemRank!,
      rowB.columnFiltersMeta[columnId]?.itemRank!
    )
  }

  // Provide an alphanumeric fallback for when the item ranks are equal
  return dir === 0 ? sortingFns.alphanumeric(rowA, rowB, columnId) : dir
}

function PaginationButton({
  onClick,
  disabled,
  children,
}: {
  onClick: () => void
  disabled?: boolean
  children: React.ReactNode
}) {
  return (
    <button
      className="rounded border p-1 hover:border-reactgray-dark"
      onClick={onClick}
      disabled={disabled}
      type="button"
    >
      {children}
    </button>
  )
}

interface Filter {
  filterFn: FilterFn<any>
  view: ({
    column,
    table,
  }: {
    column: Column<any, unknown>
    table: Table<any>
  }) => React.ReactNode
}

interface TableProps {
  id?: string
  columns: ColumnDef<any>[]
  rows: any[]
  rowRenderer?: (row: Row<any>, rowIndex: number) => React.ReactNode
  additionalFilters?: Record<string, Filter>
  loading?: boolean
}

export default function GenericTable({
  id,
  columns,
  rows,
  rowRenderer,
  additionalFilters = {},
  loading = false,
}: TableProps) {
  const [sorting, setSorting] = React.useState<SortingState>([])
  const [columnFilters, setColumnFilters] = React.useState<ColumnFiltersState>(
    []
  )

  const filterFns = Object.entries(additionalFilters).reduce(
    (acc, [name, filter]) => {
      acc[name] = filter.filterFn
      return acc
    },
    {}
  )

  const table = useReactTable({
    data: rows,
    columns,
    // @ts-ignore
    filterFns: {
      fuzzy: fuzzyFilter,
      ...filterFns,
    },
    state: {
      columnFilters,
      sorting,
    },
    onColumnFiltersChange: setColumnFilters,
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFacetedRowModel: getFacetedRowModel(),
    getFacetedUniqueValues: getFacetedUniqueValues(),
    getFacetedMinMaxValues: getFacetedMinMaxValues(),
    autoResetPageIndex: false,
  })

  const defaultRowRenderer = (row: Row<any>, rowIndex: number) => (
    <tr key={row.id}>
      {row.getVisibleCells().map((cell) => (
        <td className="px-2 pb-3 align-top text-sm text-gray-700" key={cell.id}>
          {flexRender(cell.column.columnDef.cell, cell.getContext())}
        </td>
      ))}
    </tr>
  )

  const filterFactory = (column: Column<any>, table: Table<any>) => {
    if (!column.getCanFilter()) return null
    const filterFn = column.columnDef.filterFn.toString()

    if (additionalFilters[filterFn]) {
      return additionalFilters[filterFn].view({ column, table })
    }
    return <Filter column={column} table={table} />
  }

  return loading ? (
    <div className="flex h-96 items-center justify-center">
      <Spinner />
    </div>
  ) : (
    <div className="rounded pb-1 shadow ring-1 ring-black/5">
      <table id={id} className="w-full table-auto">
        <thead className="bg-gray-100">
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th
                  className="whitespace-nowrap px-2 py-3 text-left align-top text-sm font-semibold text-gray-900"
                  key={header.id}
                  style={{ width: header.getSize() }}
                >
                  {header.isPlaceholder ? null : (
                    <div>
                      <div
                        className={classNames(
                          header.column.getCanSort()
                            ? 'cursor-pointer select-none'
                            : '',
                          'flex'
                        )}
                        onClick={header.column.getToggleSortingHandler()}
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                        {{
                          asc: (
                            <ArrowUpIcon className="h-5 w-5 text-ellablue-darkest" />
                          ),
                          desc: (
                            <ArrowDownIcon className="h-5 w-5 text-ellablue-darkest" />
                          ),
                        }[header.column.getIsSorted() as string] ?? null}
                      </div>
                      {filterFactory(header.column, table)}
                    </div>
                  )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="bg-white">
          <tr className="h-2"></tr>
          {table
            .getRowModel()
            .rows.map((row, rowIndex) =>
              rowRenderer
                ? rowRenderer(row, rowIndex)
                : defaultRowRenderer(row, rowIndex)
            )}
        </tbody>
      </table>

      <div className="mt-4 mb-2 ml-2 flex gap-2 whitespace-nowrap">
        <PaginationButton
          onClick={() => {
            table.setPageIndex(0)
          }}
          disabled={!table.getCanPreviousPage()}
        >
          <ChevronDoubleLeftIcon className="h-5 w-5 text-ellablue-darkest" />
        </PaginationButton>
        <PaginationButton
          onClick={() => {
            table.previousPage()
          }}
          disabled={!table.getCanPreviousPage()}
        >
          <ChevronLeftIcon className="h-5 w-5 text-ellablue-darkest" />
        </PaginationButton>
        <PaginationButton
          onClick={() => {
            table.nextPage()
          }}
          disabled={!table.getCanNextPage()}
        >
          <ChevronRightIcon className="h-5 w-5 text-ellablue-darkest" />
        </PaginationButton>
        <PaginationButton
          onClick={() => {
            table.setPageIndex(table.getPageCount() - 1)
          }}
          disabled={!table.getCanNextPage()}
        >
          <ChevronDoubleRightIcon className="h-5 w-5 text-ellablue-darkest" />
        </PaginationButton>
        <span className="flex items-center gap-1">
          <div>Page</div>
          <strong>
            {table.getState().pagination.pageIndex + 1} of{' '}
            {table.getPageCount()}
          </strong>
        </span>
        <span className="flex items-center gap-1">
          | Go to page:
          <input
            type="number"
            defaultValue={table.getState().pagination.pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              table.setPageIndex(page)
            }}
            className="w-1/4 rounded-md border p-1 hover:border-reactgray-dark"
          />
        </span>
      </div>
    </div>
  )
}

function Filter({
  column,
  table,
}: {
  column: Column<any, unknown>
  table: Table<any>
}) {
  const firstValue = table
    .getPreFilteredRowModel()
    .flatRows[0]?.getValue(column.id)

  const columnFilterValue = column.getFilterValue()

  return typeof firstValue === 'number' ? (
    <div>
      <div className="mt-1 flex flex-col">
        <DebouncedInput
          name={column.id + '-filtermin'}
          type="number"
          min={Number(column.getFacetedMinMaxValues()?.[0] ?? '')}
          max={Number(column.getFacetedMinMaxValues()?.[1] ?? '')}
          value={(columnFilterValue as [number, number])?.[0] ?? ''}
          onChange={(value) => {
            column.setFilterValue((old: [number, number]) => [value, old?.[1]])
            table.resetPagination()
          }}
          placeholder="min"
          className="w-full rounded-md border p-1 font-normal hover:border-reactgray-dark"
        />
        <DebouncedInput
          name={column.id + '-filtermax'}
          type="number"
          min={Number(column.getFacetedMinMaxValues()?.[0] ?? '')}
          max={Number(column.getFacetedMinMaxValues()?.[1] ?? '')}
          value={(columnFilterValue as [number, number])?.[1] ?? ''}
          onChange={(value) => {
            column.setFilterValue((old: [number, number]) => [old?.[0], value])
            table.resetPagination()
          }}
          placeholder="max"
          className="w-full rounded-md border p-1 font-normal hover:border-reactgray-dark"
        />
      </div>
      <div className="h-1" />
    </div>
  ) : (
    <DebouncedInput
      name={column.id + '-filter'}
      type="text"
      value={(columnFilterValue ?? '') as string}
      onChange={(value) => {
        column.setFilterValue(value)
        table.resetPagination()
      }}
      placeholder="Search ..."
      className="mt-1 w-full rounded-md border p-1 font-normal hover:border-reactgray-dark"
      list={column.id + 'list'}
    />
  )
}

// A debounced input react component
function DebouncedInput({
  value: initialValue,
  onChange,
  debounce = 500,
  ...props
}: {
  value: string | number
  onChange: (value: string | number) => void
  debounce?: number
} & Omit<React.InputHTMLAttributes<HTMLInputElement>, 'onChange'>) {
  const [value, setValue] = React.useState(initialValue)

  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      onChange(value)
    }, debounce)

    return () => clearTimeout(timeout)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  return (
    <input
      {...props}
      value={value}
      onChange={(e) => setValue(e.target.value)}
    />
  )
}
