/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  safelist: [
    'bg-panelgreen-500',
    'bg-panelgreen-200',
    'bg-panelamber-500',
    'bg-panelamber-200',
    'bg-panelred-500',
    'bg-panelred-200',
  ],

  theme: {
    extend: {
      colors: {
        ellablue: {
          lightest: '#eaeff2',
          lighter: '#cbd7df',
          DEFAULT: '#9caebf',
          darker: '#859baf',
          darkest: '#567490',
        },
        ellapurple: {
          lightest: '#f5f5f9',
          lighter: '#d6d8e6',
          DEFAULT: '#c3c6db',
          dark: '#9597bc',
        },
        ellagreen: {
          light: '#f6f9f8',
          DEFAULT: '#c9dbd9',
          dark: '#72979d',
        },
        ellared: {
          light: '#f8f5f5',
          DEFAULT: '#d8c8c5',
          dark: '#9c6a62',
        },
        ellayellow: {
          light: '#fcf8f2',
          DEFAULT: '#f1ddc0',
          dark: '#d59844',
        },
        reactgray: {
          DEFAULT: '#cccccc',
          dark: '#b3b3b3',
          darkest: '#808080',
        },
        panelgreen: {
          200: '#deeeda',
          500: '#5aab4a',
        },
        panelamber: {
          200: '#faeedc',
          500: '#e6ad54',
        },
        panelred: {
          200: '#f4dddc',
          500: '#cb5751',
        },
      },
    },
  },
  plugins: [],
}
