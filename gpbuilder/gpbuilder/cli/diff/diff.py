import logging
import time
from pathlib import Path

import semver
import sqlmodel
import typer
from pydantic.error_wrappers import ValidationError
from sqlmodel import select

from gpbuilder.cli.diff.utils import (
    ChangeType,
    DiffRowRecord,
    compute_diff,
    diff_to_rows,
    extract_transcript_records,
)
from gpbuilder.datamodel.crud.genepanel import next_version
from gpbuilder.datamodel.genepanel import GenePanel

app = typer.Typer()

logger = logging.getLogger()


def _panel_folder_to_name_version(folder: Path):
    """Split a folder name into panel name and version"""
    try:
        panel_shortname, panel_version = folder.name.split("_v")
        return panel_shortname, semver.VersionInfo.parse(panel_version)
    except ValueError:
        logger.error(f"Invalid panel folder name: {folder}")
        return ()


def bump_panel_versions(
    session: sqlmodel.Session,
    panel_diffs: list[tuple[GenePanel, dict]],
    skip_draft_if_no_gene_changes: bool = False,
):
    """
    Rename versions of (non-draft) panels with any detected differences and any draft panels based
    on them.
    """
    no_submitted_versions = []
    for panel, diff in panel_diffs:
        if not diff or panel.draft:
            continue

        gene_changes = any(
            change[0] in [ChangeType.added, ChangeType.removed]
            for changes in diff.values()
            for change in changes
        )

        panels_to_bump = session.exec(
            select(GenePanel)
            .where(
                GenePanel.shortname == panel.shortname,
                GenePanel.version >= panel.version,
            )
            .order_by(GenePanel.version)
        ).all()

        has_submitted_version = False

        # Rename panel, and all drafts based on it
        for panel_to_bump in panels_to_bump:
            if not gene_changes and skip_draft_if_no_gene_changes:
                # Bump minor version
                existing_version = semver.VersionInfo.parse(panel_to_bump.version)
                new_version = str(
                    existing_version.replace(minor=existing_version.minor + 1)
                )
            else:
                new_version = next_version(session, panel.shortname)

            if not new_version.endswith("-draft"):
                has_submitted_version = True

            logger.info(
                f"Bumping {panel_to_bump.shortname}: {panel_to_bump.version} -> {new_version}"
            )
            panel_to_bump.version = new_version
            session.flush()

        if not has_submitted_version:
            no_submitted_versions.append(panel)

    if no_submitted_versions:
        logger.warning(
            "These panels remain at draft stage after version bumping and require expert review:"
        )
        for p in no_submitted_versions:
            logger.warning(f"- {p.shortname}")


def diff_against_exported(
    session: sqlmodel.Session,
    folder: Path,
    output_prefix: Path,
    matching_versions_only: bool = False,
):
    """
    Summarize the differences between the active panels in the database, including any draft
    versions derived from the latest submitted ones, and their latest exported counterparts in the
    given folder.
    """
    with (
        output_prefix.with_suffix(".tsv").open("w") as full_table_handle,
        output_prefix.with_suffix(".summary.tsv").open("w") as summary_table_handle,
    ):
        header = (
            "Panel name\tPanel version\tPanel version (old)\t"
            + DiffRowRecord.header_to_tsv()
        )
        summary_header = "\t".join(
            [
                "Panel name",
                "Panel version",
                "Panel version (old)",
                "Total differences",
                "Genes added",
                "Genes removed",
                "Symbol changes",
                "Transcript changes",
                "Inheritance changes",
            ]
        )
        full_table_handle.write(header + "\n")
        summary_table_handle.write(summary_header + "\n")

        panel_diffs = list()
        panel_folder_diffs = list(
            diff_all_against_folder(session, folder, matching_versions_only, True)
        )

        # Iterate over all diffs
        for panel, compare_panel_folder, diff in panel_folder_diffs:
            compare_panel_name, compare_panel_version = _panel_folder_to_name_version(
                compare_panel_folder
            )
            assert compare_panel_name == panel.shortname

            # Prefix each row with the panel names and versions
            row_prefix = (
                "\t".join(
                    [
                        panel.shortname,
                        f"v{panel.version}",
                        f"v{compare_panel_version}",
                    ]
                )
                + "\t"
            )

            # Write each diff row to the output file
            diff_counts = {
                ChangeType.added: 0,
                ChangeType.removed: 0,
                ChangeType.symbol: 0,
                ChangeType.transcript: 0,
                ChangeType.inheritance: 0,
            }

            for changes in diff.values():
                for change in changes:
                    diff_counts[change[0]] += 1

            for diff_row in diff_to_rows(diff):
                row = row_prefix + diff_row.to_tsv()
                full_table_handle.write(row + "\n")

            # Write just number of diffs in the summary file
            summary_table_handle.write(
                row_prefix
                + "\t".join(
                    [
                        str(sum(diff_counts.values())),
                        str(diff_counts[ChangeType.added]),
                        str(diff_counts[ChangeType.removed]),
                        str(diff_counts[ChangeType.symbol]),
                        str(diff_counts[ChangeType.transcript]),
                        str(diff_counts[ChangeType.inheritance]),
                    ]
                )
                + "\n"
            )

            panel_diffs.append((panel, diff))

        logger.info(
            f"diff report written to '{Path(full_table_handle.name).absolute()}'."
            f"diff summary written to '{Path(summary_table_handle.name).absolute()}'."
        )

        return panel_diffs


def diff_all_against_folder(
    session: sqlmodel.Session,
    folder: Path,
    matching_versions_only=False,
    include_drafts=True,
):
    """
    Compare all active database panels to the exported panels in the given folder
    """
    logger.info(f"Comparing database panels to exported panels in {folder}")

    # Iterate over all pairs of panel + folder to compare
    for panel, compare_panel_folder in find_diff_pairs(
        session,
        folder,
        matching_versions_only=matching_versions_only,
        include_drafts=include_drafts,
    ):
        yield (
            panel,
            compare_panel_folder,
            diff_panel_to_folder(panel, compare_panel_folder),
        )


def diff_panel_to_folder(panel: GenePanel, folder: Path):
    """
    Compare a database panel's content to that of a folder holding an exported panel.
    """
    logger.info(
        f"Comparing {panel.shortname}_v{panel.version} (id: {panel.id}) to {folder}"
    )
    _, panel_tx_records = panel.prepare_for_export()
    exported_panel_tx_records = extract_transcript_records(folder)
    try:
        diff, _ = compute_diff(exported_panel_tx_records, panel_tx_records)
    except ValidationError as e:
        logger.error(f"Could not compute diff: {e}")
        raise

    return diff


def find_diff_pairs(
    session: sqlmodel.Session,
    folder: Path,
    matching_versions_only=False,
    include_drafts=True,
):
    """
    Find pairs of panels and folders to compare.

    All active panels in the database are paired to any exported versions of them in the given
    folder.

    If matching_versions_only is True, only panels with exact matching versions will be paired.
    If matching_versions_only is False, any panels with version greater than or equal to the
    exported one will be paired.

    If no matching folder is found, the panel will be skipped.
    """
    panel_ids = GenePanel.active_panel_ids(session, include_drafts=include_drafts)

    panels = session.exec(
        select(GenePanel)
        .where(GenePanel.id.in_(panel_ids))
        .order_by(GenePanel.shortname, GenePanel.version)
    ).all()

    panel_folders = [
        f
        for f in sorted(
            folder.iterdir(), key=_panel_folder_to_name_version, reverse=True
        )
        if f.is_dir()
    ]

    # assert panel_ids
    for panel in panels:
        if matching_versions_only:
            compare_panel_folder = next(
                (
                    f
                    for f in panel_folders
                    if f.name == f"{panel.shortname}_v{panel.version}"
                ),
                None,
            )
        else:
            # The panel_folders list is sorted, so the first match will be the latest version
            compare_panel_folder = next(
                (f for f in panel_folders if f.name.startswith(panel.shortname + "_v")),
                None,
            )
            if compare_panel_folder:
                _, compare_panel_version = _panel_folder_to_name_version(
                    compare_panel_folder
                )

                panel_version = semver.VersionInfo.parse(panel.version)
                if compare_panel_version > panel_version:
                    logger.warning(
                        f"Version {panel_version} is older than {compare_panel_version} found in "
                        f"folder '{compare_panel_folder}'."
                    )
                    compare_panel_folder = None
                    continue

        if not compare_panel_folder:
            logger.info(
                f"No panels in '{folder}' can be compared to '{panel.shortname}_v{panel.version}'."
                " Skipped."
            )
            continue

        yield panel, compare_panel_folder


@app.callback(invoke_without_command=True)
def diff(
    ctx: typer.Context,
    create_drafts: bool = typer.Option(
        False, "--create-drafts", help="Bump panel versions when different"
    ),
    matching_versions_only: bool = typer.Option(
        False,
        "--matching-versions-only",
        help="Only compare panels with exact matching versions",
    ),
    output_prefix: Path = typer.Option(
        f"gpb-diff-{time.strftime('%Y-%m-%dT%H%M')}",
        "-o",
        "--output-prefix",
        help="Prefix used for diff output files",
    ),
    skip_draft_if_no_gene_changes: bool = typer.Option(
        False,
        "--skip-draft-if-no-gene-changes",
        help="Skip draft creation if there are no gene changes",
    ),
    folder: Path = typer.Argument(
        None, help="Directory containing the exported panels.", show_default=False
    ),
):
    """
    Compare all active panels in the database to their exported counterparts in the given folder.

    Each active panel in the database will be compared to its latest exported counterpart in the
    given folder, provided the latter's version is lower or equal to that of the panel in the
    database.

    If the `--matching-versions-only` flag is used, panels with no exact match among the exported
    ones will be disregarded.

    If the `--create-drafts` flag is used, version bumps will be triggered for all panels with
    detected differences from their exported counterparts, as well as any existing draft panels
    based on them. The _order_ of the panels will remain the same.

    NOTE: No additional panels are _actually_ created, database panels are simply renamed.

    Examples:

    Exported panel: panel_v1.1.0

    Database panels:

    - panel_v1.1.0

    - panel_v1.1.1-draft

    - panel_v1.1.2-draft

    If the database panel panel_v1.1.0 differs from the exported panel panel_v1.1.0, the
    following panel versions will be bumped with a new draft-version:

    - panel_v1.1.0 -> panel1_v1.1.3-draft

    - panel_v1.1.1-draft -> panel1_v1.1.4-draft

    - panel_v1.1.2-draft -> panel1_v1.1.5-draft

    If the flag --skip-draft-if-no-gene-changes is set, and the difference does not entail
    any gene additions or removals, the panel's minor version will be bumped:

    - panel_v1.1.0 -> panel1_v1.2.0

    - panel_v1.1.1-draft -> panel1_v1.2.1-draft

    - panel_v1.1.2-draft -> panel1_v1.2.2-draft

    The detected differences will be written to two tab-separated files with the given prefix:

    - <output_prefix>.tsv

    - <output_prefix>.summary.tsv
    """
    session: sqlmodel.Session = ctx.obj.session

    panel_diffs = diff_against_exported(
        session, folder, output_prefix, matching_versions_only
    )

    if create_drafts:
        bump_panel_versions(session, panel_diffs, skip_draft_if_no_gene_changes)
