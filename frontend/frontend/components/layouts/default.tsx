import TopBanner from 'components/topbanner'
import { ReactNode, useEffect, useState } from 'react'
import { useSession } from 'components/sessioncontext'

export default function Layout({ children }: { children: ReactNode }) {
  const [authenticated, setAuthenticated] = useState(false)
  const { validateSession, getUserInfo } = useSession()

  useEffect(() => {
    validateSession().then((result) => {
      setAuthenticated(result)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (!authenticated) return null

  getUserInfo()

  return (
    <div className="flex h-screen flex-col">
      <TopBanner />
      <main className="overflow-hidden">{children}</main>
    </div>
  )
}
