import pytest

from gpbuilder.api.tests.mock_data import (
    create_genes,
    create_panelapp_panels,
    create_panels,
    create_regions,
    create_transcripts,
    link_genepanel_excluded_gene,
    link_genepanel_gene,
    link_genepanel_panelapp_panel,
    link_genepanel_region,
    link_panelapp_panel_gene,
)
from gpbuilder.datamodel.genepanel import GenePanel
from gpbuilder.datamodel.utils.records import RegionRecord, TranscriptRecord


# TODO: split into separate tests for phenotypes, regions, transcripts
@pytest.mark.parametrize("num_genes,num_regions", [(3, 2), (0, 0), (1, 0), (0, 1)])
def test_assemble_genepanels(session, mocker, num_genes, num_regions):
    genes = create_genes(num_genes, session)

    # First gene has 1 transcript, second has 2, third has 3 etc
    all_transcripts = [
        create_transcripts(i + 1, gene.hgnc_id, session) for i, gene in enumerate(genes)
    ]

    regions = create_regions(num_regions, session)

    (panel,) = create_panels(1, session)

    session.commit()

    transcripts = []
    regions = []

    # Include all genes and transcripts
    for gene, gene_transcripts in zip(genes, all_transcripts):
        gpg = link_genepanel_gene(gene, panel, session, transcripts=gene_transcripts)
        transcripts.extend(
            list(zip(gpg.transcripts, (gpg.inheritance_mode,) * len(gpg.transcripts)))
        )
    session.commit()

    # Add regions
    for region in regions:
        gpr = link_genepanel_region(region, panel, session)
        regions.append((region, gpr.inheritance_mode))

    session.commit()

    panel_regions, panel_transcripts = panel.prepare_for_export()

    assert len(panel_regions) == sum(len(tx.exons) for tx, _ in transcripts) + len(
        regions
    )

    # Check that we have all the expected transcripts
    expected_transcripts = {
        TranscriptRecord(
            **tx.dict(),
            hgnc_symbol=tx.gene.symbol,  # type: ignore
            inheritance=inheritance,
            metadata="{}",
        )
        for tx, inheritance in transcripts
    }

    expected_transcripts.update(
        TranscriptRecord(
            **rx.dict(),
            inheritance=inheritance,
            metadata="{}",
            score=0.0,
        )
        for rx, inheritance in regions
    )

    assert len(panel_transcripts) == len(expected_transcripts)

    for tx in expected_transcripts:
        expected_equal = next(x for x in panel_transcripts if x.name == tx.name)
        assert tx == expected_equal

    # Check that we have all the expected regions
    expected_regions = set()
    for tx, _ in transcripts:
        expected_regions.update(
            RegionRecord(**dict({**region.dict(), "name": name}))
            for (region, name) in zip(tx.cds, tx.cds_names)
        )

    for rx, _ in regions:
        expected_regions.add(RegionRecord(**rx.dict()))

    assert len(panel_regions) == len(expected_regions)

    for rx in expected_regions:
        expected_equal = next(x for x in panel_regions if x.name == rx.name)
        assert rx == expected_equal


def test_hgnc_ids_in_genepanel(session):
    genes = create_genes(5, session)
    panels = create_panels(2, session)
    panelapp_panels = create_panelapp_panels(3, session)

    session.add_all(genes + panels + panelapp_panels)
    session.flush()

    panel_ids: list[int] = [p.id for p in panels]  # type: ignore

    link_panelapp_panel_gene([genes[0]], panelapp_panels[0], 1, session)
    link_panelapp_panel_gene([genes[1]], panelapp_panels[1], 1, session)
    link_panelapp_panel_gene([genes[2]], panelapp_panels[2], 1, session)
    session.flush()

    # No genes added
    res = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)
    assert all(len(v) == 0 for v in res.values())

    # panelapp_panel[0] added to panel[0]
    link_genepanel_panelapp_panel([panelapp_panels[0]], panels[0], session)
    session.flush()
    res = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)
    assert res[panel_ids[0]] == set([genes[0].hgnc_id])
    assert res[panel_ids[1]] == set()

    # panelapp_panel[1:] added to panel[1]
    link_genepanel_panelapp_panel(panelapp_panels[1:], panels[1], session)
    session.flush()
    res = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)
    assert res[panel_ids[0]] == set([genes[0].hgnc_id])
    assert res[panel_ids[1]] == set([genes[1].hgnc_id, genes[2].hgnc_id])

    # genes[3:] added to panel[0]
    for gene in genes[3:]:
        link_genepanel_gene(gene, panels[0], session)
    session.flush()
    res = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)
    assert res[panel_ids[0]] == set(genes[k].hgnc_id for k in (0, 3, 4))
    assert res[panel_ids[1]] == set([genes[1].hgnc_id, genes[2].hgnc_id])

    # genes[0] and genes[4] excluded from panels[0], genes[1] excluded from panels[1]
    link_genepanel_excluded_gene([genes[0]], panels[0], session)
    link_genepanel_excluded_gene([genes[4]], panels[0], session)
    link_genepanel_excluded_gene([genes[1]], panels[1], session)
    session.flush()
    res = GenePanel.hgnc_ids_by_panel_id(session, panel_ids)
    assert res[panel_ids[0]] == set([genes[3].hgnc_id])
    assert res[panel_ids[1]] == set([genes[2].hgnc_id])
