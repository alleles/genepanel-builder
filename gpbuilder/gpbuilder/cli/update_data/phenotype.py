import logging
from collections import defaultdict
from typing import Iterator

from sqlmodel import Session, select

from gpbuilder.cli.update_data.common import (
    UpdateStatus,
    set_next_update_id,
)
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.phenotype import Phenotype, PhenotypeGene
from gpbuilder.omim_api.omim_api import OmimApi

logger = logging.getLogger(__name__)


def insert_phenotype(
    session: Session, phenotype: Phenotype, phenotype_gene: PhenotypeGene
):
    db_phenotype = session.get(Phenotype, phenotype.mim_number)
    if db_phenotype is None:
        session.add(phenotype)
    else:
        db_phenotype.titles = set(db_phenotype.titles).union(phenotype.titles)
        session.add(db_phenotype)
    session.flush()
    db_phenotype_gene = session.exec(
        select(PhenotypeGene).where(
            PhenotypeGene.phenotype_id == phenotype_gene.phenotype_id,
            PhenotypeGene.gene_id == phenotype_gene.gene_id,
            PhenotypeGene.inheritance_descr == phenotype_gene.inheritance_descr,
        )
    ).one_or_none()
    if not db_phenotype_gene:
        session.add(phenotype_gene)

    session.flush()


def fetch_omim_phenotypes(
    session: Session,
) -> Iterator[tuple[Phenotype, PhenotypeGene]]:
    symbols_dict = Gene.symbol_by_hgnc_ids(session)

    assert len(set(symbols_dict.values())) == len(
        symbols_dict
    ), "Non-unique symbols in database. This shouldn't happen."
    # Invert the dictionary
    hgnc_id_dict = {symbol: hgnc_id for hgnc_id, symbol in symbols_dict.items()}

    for omim_gene_map in OmimApi.get_data():
        if not omim_gene_map.phenotypeMapList:
            logger.debug(
                f"Empty phenotype map: discarded OMIM gene '{omim_gene_map.mimNumber}'"
            )
            continue
        if omim_gene_map.approvedGeneSymbols is None:
            logger.warning(
                f"Empty gene symbol: discarded OMIM gene '{omim_gene_map.mimNumber}'"
            )
            continue
        hgnc_id = hgnc_id_dict.get(omim_gene_map.approvedGeneSymbols)
        if hgnc_id is None:
            # Lookup the HGNC ID in the database recursively
            gene = Gene.get_approved_gene_by_symbol(
                session, omim_gene_map.approvedGeneSymbols
            )
            if gene is None:
                logger.warning(
                    f"Unable to find gene matching symbol '{omim_gene_map.approvedGeneSymbols}'"
                )
                continue
            logger.info(
                f"Did not find HGNC ID for {gene.symbol}, but found {gene.hgnc_id} ({gene.symbol})"
            )
            hgnc_id = gene.hgnc_id

        for phenotype_map in omim_gene_map.phenotypeMapList:
            if not phenotype_map.phenotypeMimNumber:
                logger.debug(
                    f"No MIM number found: discarded phenotype '{phenotype_map.phenotype}'"
                )
                continue

            phenotype = Phenotype(
                mim_number=phenotype_map.phenotypeMimNumber,
                titles={phenotype_map.phenotype},
                update_id=-1,
            )

            phenotype_gene = PhenotypeGene(
                gene_id=hgnc_id,
                gene_mim_number=omim_gene_map.mimNumber,
                phenotype_id=phenotype_map.phenotypeMimNumber,
                inheritance_descr=phenotype_map.phenotypeInheritance,
            )

            yield phenotype, phenotype_gene


def count_changes(cmp_before: dict, cmp_after: dict):
    added = set(cmp_after.keys() - set(cmp_before.keys()))
    removed = set(cmp_before.keys() - set(cmp_after.keys()))
    changed = set(
        key
        for key in set(cmp_before.keys()) & set(cmp_after.keys())
        if cmp_before[key] != cmp_after[key]
    )
    equal = set(
        key
        for key in set(cmp_before.keys()) & set(cmp_after.keys())
        if cmp_before[key] == cmp_after[key]
    )

    counter = UpdateStatus.get_counter()
    counter[UpdateStatus.CREATED] = len(added)
    counter[UpdateStatus.DELETED] = len(removed)
    counter[UpdateStatus.UPDATED] = len(changed)
    counter[UpdateStatus.IDENTICAL] = len(equal)
    return counter


def count_phenotype_changes(before: list[Phenotype], after: list[Phenotype]):
    cmp_before = {p.mim_number: set(p.titles) for p in before}
    cmp_after = {p.mim_number: set(p.titles) for p in after}
    return count_changes(cmp_before, cmp_after)


def count_phenotypegene_changes(
    before: list[PhenotypeGene], after: list[PhenotypeGene]
):
    cmp_before = defaultdict(set)
    for pg in before:
        cmp_before[pg.gene_id].add((pg.phenotype_id, pg.inheritance_descr))
    cmp_after = defaultdict(set)
    for pg in after:
        cmp_after[pg.gene_id].add((pg.phenotype_id, pg.inheritance_descr))

    return count_changes(cmp_before, cmp_after)


def populate_omim_phenotypes(session: Session):
    all_hgnc_ids = session.exec(select(Gene.hgnc_id)).all()
    default_inheritance_before = Gene.get_default_inheritance_by_hgnc_ids(
        session, all_hgnc_ids
    )

    phenotypes_before = session.exec(select(Phenotype)).all()
    phenotype_genes_before = session.exec(select(PhenotypeGene)).all()
    for phenotype_gene in phenotype_genes_before:
        session.delete(phenotype_gene)
    for phenotype in phenotypes_before:
        session.delete(phenotype)
    session.flush()

    logger.info("Updating OMIM phenotypes")
    update_id = set_next_update_id("phenotype", session)
    for phenotype, phenotype_gene in fetch_omim_phenotypes(session):
        phenotype.update_id = update_id
        insert_phenotype(session, phenotype, phenotype_gene)

    session.flush()
    phenotypes_after = session.exec(select(Phenotype)).all()
    phenotype_genes_after = session.exec(select(PhenotypeGene)).all()

    logger.info(
        f"Phenotype changes: {count_phenotype_changes(phenotypes_before, phenotypes_after)}"
    )
    logger.info(
        f"PhenoypeGene changes: {count_phenotypegene_changes(phenotype_genes_before, phenotype_genes_after)}"
    )

    default_inheritance_after = Gene.get_default_inheritance_by_hgnc_ids(
        session, all_hgnc_ids
    )

    logger.info(
        f"Inheritance changes: {count_changes(default_inheritance_before, default_inheritance_after)}"
    )

    logger.info("OMIM phenotypes updated")
