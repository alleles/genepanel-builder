all
exclude_rule 'MD013'
exclude_rule 'MD033'
exclude_rule 'MD041'
exclude_rule 'MD046'
exclude_rule 'MD057'
rule 'MD007', :indent => 4, :start_indented => false
rule 'MD029', :style => 'one_or_ordered'
