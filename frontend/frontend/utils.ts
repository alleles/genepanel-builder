import debounce from 'lodash/debounce'
import mergeWith from 'lodash/mergeWith'
import isEqual from 'lodash/isEqual'
import clone from 'lodash/clone'

function asyncDebounce(func, wait) {
  const debounced = debounce((resolve, reject, args) => {
    func(...args)
      .then(resolve)
      .catch(reject)
  }, wait)
  return (...args) =>
    new Promise((resolve, reject) => {
      debounced(resolve, reject, args)
    })
}

export { asyncDebounce }

const customizer = (objValue, srcValue) => {
  if (Array.isArray(srcValue)) {
    return srcValue
  }
  return undefined
}

export const deepMerge = (objValue, srcValue) =>
  mergeWith(objValue, srcValue, customizer)

export const setEquals = (a: any[], b: any[]) =>
  a.length === b.length && [...a].every((x) => b.includes(x))
