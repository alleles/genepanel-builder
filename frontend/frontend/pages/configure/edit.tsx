import cloneDeep from 'lodash/cloneDeep'
import isEqual from 'lodash/isEqual'
import { useEffect, useState } from 'react'

import API from 'api'
import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import GeneTable, { GeneTableItem } from 'components/genetable'
import Layout from 'components/layouts/configure'
import ConfirmSubmit from 'components/submitdialog'

import DefaultLayout from 'components/layouts/default'
import RegionTable, { RegionTableItem } from 'components/regiontable'
import { useRouter } from 'next/router'
import { GenePanelConfig } from 'types'
import { deepMerge } from 'utils'

export default function Panel() {
  const router = useRouter()
  const { id } = router.query

  const {
    genePanelConfig,
    fetchGeneRegionDetails,
    savePanel,
    dirty,
    setDirty,
  } = useGenePanel()
  const loading = id && !genePanelConfig.id
  const [geneTableItems, setGeneTableItems] = useState<GeneTableItem[]>([])
  const { diff } = genePanelConfig
  const [updatedRegions, setUpdatedRegions] = useState<
    GenePanelConfig['regions']
  >([])
  const [regionTableItems, setRegionTableItems] = useState<RegionTableItem[]>(
    []
  )
  const [open, setOpen] = useState(false)
  const [disabled, setDisabled] = useState(false)
  const [unsavedDiff, setUnsavedDiff] = useState<GenePanelConfig['diff']>({})

  useEffect(() => {
    setUpdatedRegions(genePanelConfig.regions)
  }, [genePanelConfig.regions])

  useEffect(() => {
    fetchGeneRegionDetails().then((data) => {
      const [geneData, regionData] = data
      setGeneTableItems(geneData)
      setRegionTableItems(regionData)
      setDisabled(false)
    })
  }, [fetchGeneRegionDetails])

  const updateDiff = (change: any) => {
    setUnsavedDiff(deepMerge(cloneDeep(unsavedDiff), change))
  }

  const saveGenePanel = async () => {
    const newDiff = deepMerge(cloneDeep(diff), unsavedDiff)
    return savePanel({
      diff: newDiff,
      regions: updatedRegions.map((region) => ({
        chromosome: region.chromosome as any,
        start: region.start,
        end: region.end,
        ref_genome: 'GRCh37',
        name: region.name,
        inheritance: region.inheritance,
        comment: region.comment,
      })),
    }).then((id) => {
      setUnsavedDiff({})
      return id
    })
  }

  return (
    <>
      <form className="m-auto h-screen w-full">
        <div className="sticky top-0 z-30 flex w-full flex-row items-center justify-between bg-ellablue-lighter py-2 px-8">
          <h2 className="text-lg font-medium text-gray-700">
            Edit gene panel:
            <span className="font-normal italic">
              {` ${genePanelConfig.shortname}`}
            </span>
          </h2>
          <div className="text-sm text-gray-500">
            Change default transcript(s) or inheritance mode, or remove
            individual genes/regions.
          </div>
          <div className="flex gap-x-2">
            <div className="w-40">
              <Button
                disabled={loading || disabled}
                onClick={() => {
                  saveGenePanel().then((id) => {
                    router.push(`/configure/select?id=${id}`)
                  })
                }}
              >
                Previous (Select)
              </Button>
            </div>
            <div className="w-20">
              <Button
                disabled={disabled || loading || !dirty}
                color="primary"
                onClick={() => {
                  setDisabled(
                    geneTableItems.length + regionTableItems.length > 250
                  )
                  saveGenePanel().then((id) => {
                    router.push(`/configure/edit?id=${id}`)
                  })
                }}
              >
                Save
              </Button>
            </div>
            <div className="w-40">
              <Button
                disabled={
                  disabled ||
                  loading ||
                  (geneTableItems.length === 0 && regionTableItems.length === 0)
                }
                color="confirm"
                onClick={() => setOpen(true)}
              >
                Submit Gene Panel
              </Button>
            </div>
          </div>
        </div>

        <div className="h-[calc(100vh-89px)] overflow-x-hidden overflow-y-scroll pl-8 pr-7 pb-8">
          <div className="mt-6 space-y-6">
            <h2 className="pl-2 text-lg font-medium text-gray-700">Genes</h2>
            <div id="gene-table">
              <GeneTable
                genes={geneTableItems}
                onGenesUpdated={updateDiff}
                disabled={disabled}
                loading={loading}
                setDirty={setDirty}
              />
            </div>

            <h2 className="pl-2 pt-4 text-lg font-medium text-gray-700">
              Regions
            </h2>
            <div id="region-table">
              <RegionTable
                regions={regionTableItems}
                onRegionsUpdated={(row, property_name, value) => {
                  let newRegions = [...regionTableItems]
                  newRegions[row][property_name] = value
                  setRegionTableItems(newRegions)

                  setUpdatedRegions(
                    newRegions.map((tableRegion) => ({
                      name: tableRegion.name,
                      chromosome: tableRegion.chromosome,
                      start: tableRegion.start,
                      end: tableRegion.end,
                      inheritance: tableRegion.inheritance,
                      comment: tableRegion.comment,
                      ref_genome: 'GRCh37',
                    }))
                  )
                }}
                disabled={disabled}
                loading={loading}
                setDirty={setDirty}
              />
            </div>
          </div>
        </div>
      </form>
      {open && (
        <ConfirmSubmit
          onConfirm={(version) => {
            saveGenePanel().then(() => {
              API.SubmitPanel.post(genePanelConfig.id, version).then(() => {
                setOpen(false)
                router.push('/')
              })
            })
          }}
          onClose={() => setOpen(false)}
        />
      )}
    </>
  )
}

Panel.getLayout = (page) => (
  <DefaultLayout>
    <Layout>{page}</Layout>
  </DefaultLayout>
)
