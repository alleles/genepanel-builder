import {
  CellContext,
  ColumnDef,
  createColumnHelper,
  FilterFn,
  flexRender,
  Row,
} from '@tanstack/react-table'
import React, { useCallback, useEffect } from 'react'

import API from 'api'
import Select from 'components/select'
import Table, { fuzzySort } from 'components/table'
import TextArea from 'components/textarea'
import TrafficLight from 'components/traffic-light'
import {
  ConfidenceLevel,
  GeneExclusionReasons,
  Inheritance,
  TranscriptOrigin,
} from 'types'
import { setEquals } from 'utils'

export type GeneTableItem = {
  hgnc_id: string
  symbol: string
  inheritance: Inheritance
  transcripts: {
    current: string[]
    available: string[]
    origin: TranscriptOrigin[]
    defaults: string[]
  }
  coverageWGS: number
  coverageWES: number
  segDup: number
  panelAppPanels: { id: number; confidence: ConfidenceLevel }[]
  exclusion_reasons: GeneExclusionReasons[]
  comment: string
}

export type GeneTableItemDiff = {
  [hgnc_id: string]: {
    inheritance?: Inheritance
    transcripts?: string[]
    exclusion_reasons?: GeneExclusionReasons[]
    comment?: string
  }
}

const defaultTranscriptFilter: FilterFn<any> = (row, columnId, filterOn) => {
  const { current, defaults } = row.getValue(
    'transcripts'
  ) as GeneTableItem['transcripts']
  if (!filterOn) {
    return true
  }
  return !setEquals(current, defaults)
}

declare module '@tanstack/table-core' {
  interface FilterFns {
    defaultTranscriptFilter
  }
}

const defaultTranscriptFilterView = ({ column, table }) => {
  return (
    <span className="leading-10">
      <input
        type="checkbox"
        className="mr-1 accent-ellablue-darkest"
        onChange={(ev) => {
          column.setFilterValue(ev.target.checked)
          table.resetPagination()
        }}
      />
      <span className="font-normal italic text-gray-500">Modified only</span>
    </span>
  )
}

export default function GeneTable({
  genes,
  onGenesUpdated,
  readOnly = false,
  loading,
  disabled = false,
  setDirty = () => {},
}: {
  genes: GeneTableItem[]
  onGenesUpdated: (diff: GeneTableItemDiff) => void
  readOnly?: boolean
  loading?: boolean
  disabled?: boolean
  setDirty?: () => void
}) {
  const [data, setData] = React.useState(genes)
  const setDataDirty = (genes: GeneTableItem[]) => {
    setData(genes)
    setDirty()
  }
  useEffect(() => {
    setData(genes)
  }, [genes])

  const columnHelper = createColumnHelper<GeneTableItem>()

  const OriginRenderer = function (
    panelAppPanels: { id: number; confidence: ConfidenceLevel }[]
  ) {
    const origin = panelAppPanels.map((panel) => {
      return (
        <div
          className="flex items-center justify-end  pr-2"
          key={panel.id}
          title={`PanelApp panel ${panel.id}; confidence level ${
            ConfidenceLevel[panel.confidence]
          }`}
        >
          <div className="mr-1 h-fit">{panel.id}</div>
          <div className="h-fit py-1">
            {panel.confidence == 1 && (
              <TrafficLight color="red" size={14} clickable={false} />
            )}
            {panel.confidence == 2 && (
              <TrafficLight color="amber" size={14} clickable={false} />
            )}
            {panel.confidence == 3 && (
              <TrafficLight color="green" size={14} clickable={false} />
            )}
          </div>
        </div>
      )
    })

    return <div>{origin}</div>
  }

  const TranscriptTagRenderer = function ({ tag }: { tag: string }) {
    let color = ''
    switch (tag) {
      case 'MANE':
        color = 'bg-ellablue-lighter'
        break
      case 'MANE+':
        color = 'bg-ellablue-lightest'
        break
      case 'Default':
        color = 'bg-ellayellow'
        break
      default:
        color = 'bg-gray-200'
    }

    return (
      <dd
        className={`rounded-full border border-gray-400 ${color} w-[calc(4.5_*_0.875rem)] text-center text-xs tracking-wide text-gray-600`}
      >
        {tag}
      </dd>
    )
  }

  const TranscriptValueRenderer = function ({
    name,
    tags,
  }: {
    name: string
    tags?: string[]
  }) {
    return (
      <div className="flex flex-row items-center justify-between">
        <span>{name}</span>
        <dl className="flex flex-row space-x-2">
          {tags.map((tag) => (
            <TranscriptTagRenderer tag={tag} key={tag} />
          ))}
        </dl>
      </div>
    )
  }

  const TranscriptSelectRenderer = function (
    info: CellContext<
      GeneTableItem,
      {
        current: string[]
        available: string[]
        origin: TranscriptOrigin[]
        defaults: string[]
      }
    >
  ) {
    const current = info.getValue().current || []
    const available = info.getValue().available || []
    const defaults = info.getValue().defaults || []
    const origin = info.getValue().origin || []
    const onChange = async (value: string[]) => {
      const newData = [...data]
      const getCoverage = async (target) =>
        await API.TranscriptCoverage.get(target, value).then(
          (response) =>
            Object.values(response.data).reduce(
              (a: number, b: number) => a + b,
              0
            ) / Object.values(response.data).length
        )

      newData[info.row.index] = {
        ...newData[info.row.index],
        coverageWES: await getCoverage('wes'),
        coverageWGS: await getCoverage('wgs'),
        segDup: await getCoverage('segdup'),
      }
      newData[info.row.index][info.column.id].current = value
      setDataDirty(newData)
      onGenesUpdated({
        [newData[info.row.index].hgnc_id]: { transcripts: value },
      })
    }
    if (readOnly) {
      return (
        <div className={isExcluded(info.row) ? 'opacity-50' : ''}>
          {/*
            The hidden input is used to store the current value of the select for bdd tests.
          */}
          <input
            type="hidden"
            id={`transcript-${data[info.row.index].hgnc_id}`}
            value={current}
          />
          {current.map((transcript) => (
            <TranscriptValueRenderer
              key={transcript}
              name={transcript}
              tags={origin[transcript]}
            />
          ))}
        </div>
      )
    }

    return (
      <div
        className={`
        ${!setEquals(defaults, current) ? 'bg-ellagreen-light' : ''}
        ${isExcluded(info.row) ? 'opacity-50' : ''}`}
      >
        <Select
          name={`transcript-${data[info.row.index].hgnc_id}`}
          multiple={true}
          disabled={disabled || isExcluded(info.row)}
          options={available?.map((transcript) => {
            return {
              name: transcript,
              value: transcript,
              label: (
                <TranscriptValueRenderer
                  name={transcript}
                  tags={origin[transcript]}
                />
              ),
              selected: current.includes(transcript),
            }
          })}
          placeholder={<span className="text-red-400">Select transcript</span>}
          onChange={onChange}
        />
      </div>
    )
  }

  const isExcluded = (row: Row<GeneTableItem>) =>
    (row.getValue('exclusion_reasons') as string[]).length > 0

  const CommentRenderer = function (info: CellContext<GeneTableItem, string>) {
    // We do not want to update data onChange, because this will cause the whole table to
    // rerender, and thus lose focus on the input field.
    // Therefore, we use a local state variable to keep track of the input value, and only
    // update the data when the input field loses focus.
    const initialValue = info.getValue()
    // We need to keep and update the state of the cell normally
    const [value, setValue] = React.useState(initialValue)

    const onBlur = (v) => {
      const newData = [...data]
      newData[info.row.index][info.column.id] = v
      setDataDirty(newData)
      onGenesUpdated({
        [newData[info.row.index].hgnc_id]: { comment: v },
      })
    }
    if (readOnly) {
      return <span id={`comment-${data[info.row.index].hgnc_id}`}>{value}</span>
    }

    return (
      <TextArea
        name={`comment-${data[info.row.index].hgnc_id}`}
        value={value}
        disabled={disabled}
        onChange={(value) => {
          setValue(value)
          setDirty()
        }}
        onBlur={onBlur}
        maxRows={10}
      />
    )
  }

  const InheritanceRenderer = function (
    info: CellContext<GeneTableItem, Inheritance>
  ) {
    const current = info.getValue()
    const onChange = (value: string) => {
      const newData = [...data]
      newData[info.row.index][info.column.id] = value
      setDataDirty(newData)
      onGenesUpdated({
        [newData[info.row.index].hgnc_id]: {
          inheritance: value as Inheritance,
        },
      })
    }

    if (readOnly) {
      return (
        <span id={`inheritance-${data[info.row.index].hgnc_id}`}>
          {current}
        </span>
      )
    }
    return (
      <div className={isExcluded(info.row) ? 'opacity-50' : ''}>
        <Select
          name={`inheritance-${data[info.row.index].hgnc_id}`}
          multiple={false}
          disabled={disabled || isExcluded(info.row)}
          options={Object.values(Inheritance).map((inheritance_mode) => {
            return {
              name: inheritance_mode,
              value: inheritance_mode,
              label: inheritance_mode,
              selected: current === inheritance_mode,
            }
          })}
          onChange={onChange}
        />
      </div>
    )
  }

  const ExclusionRenderer = function (
    info: CellContext<GeneTableItem, GeneExclusionReasons[]>
  ) {
    const current = info.getValue() || []
    const onChange = (value) => {
      const newData = [...data]
      newData[info.row.index][info.column.id] = value
      setDataDirty(newData)
      onGenesUpdated({
        [newData[info.row.index].hgnc_id]: {
          exclusion_reasons: value as GeneExclusionReasons[],
        },
      })
    }

    if (readOnly) {
      return (
        <span id={`exclusion_reasons-${data[info.row.index].hgnc_id}`}>
          {current.join(', ')}
        </span>
      )
    }
    return (
      <div id={`exclusion_reasons-${data[info.row.index].hgnc_id}`}>
        <Select
          disabled={disabled}
          name={`exclusion_reasons-${data[info.row.index].hgnc_id}`}
          multiple={true}
          options={Object.values(GeneExclusionReasons).map((option) => {
            return {
              name: option,
              value: option,
              label: option,
              selected: current.includes(option),
            }
          })}
          placeholder={<span className="text-ellagreen-dark">Included</span>}
          onChange={onChange}
        ></Select>
      </div>
    )
  }

  const CoverageRenderer = (info: CellContext<GeneTableItem, number>) => (
    <div className="text-left">
      {isNaN(info.getValue()) ? '-' : info.getValue().toPrecision(3)}
    </div>
  )

  const columns = React.useMemo<ColumnDef<GeneTableItem>[]>(
    () => [
      columnHelper.accessor('hgnc_id', {
        header: 'HGNC ID',
        filterFn: 'fuzzy',
        sortingFn: fuzzySort,
        size: 120,
      }),
      columnHelper.accessor('symbol', {
        header: 'Symbol',
        filterFn: 'fuzzy',
        size: 120,
      }),
      columnHelper.accessor('inheritance', {
        header: 'Inheritance',
        cell: (info) => InheritanceRenderer(info),
        enableSorting: true,
        enableColumnFilter: true,
        size: 120,
      }),
      columnHelper.accessor('transcripts', {
        header: 'Transcript',
        cell: (info) => TranscriptSelectRenderer(info),
        filterFn: 'defaultTranscriptFilter',
        enableSorting: false,
        enableColumnFilter: true,
        size: 240,
      }),
      columnHelper.accessor('coverageWGS', {
        header: 'Coverage WGS',
        cell: (info) => CoverageRenderer(info),
        size: 120,
      }),
      columnHelper.accessor('coverageWES', {
        header: 'Coverage WES',
        cell: (info) => CoverageRenderer(info),
        size: 120,
      }),
      columnHelper.accessor('segDup', {
        header: 'SegDup',
        cell: (info) => CoverageRenderer(info),
        size: 120,
      }),
      columnHelper.accessor('panelAppPanels', {
        header: 'Origin',
        cell: (info) => OriginRenderer(info.getValue()),
        enableColumnFilter: true,
        size: 120,
        filterFn: (rows, id, filterValue) => {
          return rows.getValue('panelAppPanels')[0]?.id == filterValue
        },
        enableGlobalFilter: false,
      }),
      columnHelper.accessor('exclusion_reasons', {
        header: 'Exclusion',
        cell: (info) => ExclusionRenderer(info),
        enableColumnFilter: true,
        filterFn: 'fuzzy',
        size: 180,
      }),
      columnHelper.accessor('comment', {
        header: 'Comment',
        cell: (info) => CommentRenderer(info),
        size: 480,
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data, readOnly, disabled] // data is sufficient, because all renderers only depend on this
  )

  const rowRenderer = (row: Row<GeneTableItem>, rowIndex: number) => {
    let textColor = 'text-gray-900'
    let bgColor = 'bg-white'
    if (isExcluded(row)) {
      textColor = 'text-gray-400'
      bgColor = 'bg-ellared/25'
    }
    let classNames = `${textColor} ${bgColor}`

    return (
      <tr className={classNames} key={row.id}>
        {row.getVisibleCells().map((cell) => (
          <td
            className={`px-2 ${readOnly ? 'pb-3 align-top' : ''} text-sm`}
            key={cell.id}
          >
            {flexRender(cell.column.columnDef.cell, cell.getContext())}
          </td>
        ))}
      </tr>
    )
  }

  return (
    <Table
      id="gene-table"
      columns={columns}
      rows={data}
      rowRenderer={rowRenderer}
      loading={loading}
      additionalFilters={{
        defaultTranscriptFilter: {
          filterFn: defaultTranscriptFilter,
          view: defaultTranscriptFilterView,
        },
      }}
    ></Table>
  )
}
