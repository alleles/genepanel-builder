# Test Omim.org API

import json
import logging
import os

import pytest

from gpbuilder import omim_api

# pytest-mock module install a 'mocker' fixture (see https://github.com/pytest-dev/pytest-mock/)


@pytest.fixture
def mock_data(mocker, caplog):
    caplog.set_level(logging.DEBUG)

    def _dump(filename, status_code):
        abs_filename = os.path.join(os.path.dirname(__file__), filename)
        file_content = open(abs_filename, mode="r").read()
        mock = mocker.Mock("omim_api.omim_api.requests.Response")
        mock.url = "dummy.url"
        mock.text = file_content
        mock.status_code = status_code
        return mock

    return _dump


@pytest.fixture
def api():
    return omim_api.OmimApi("dummyApiKey")


@pytest.fixture
def search_brca1_response(mock_data):
    return mock_data("sample_api_response_brca1_symbol.json", 200)


@pytest.fixture
def search_response_with_missing_phenotype(mock_data):
    mock = mock_data("sample_api_response_missing_phenotype.json", 200)
    genemap = json.loads(mock.text)["omim"]["searchResponse"]["geneMapList"]
    assert any(
        map(lambda x: "phenotypeMapList" not in x["geneMap"], genemap)
    ), "one of the geneMap dict should have an empty phenoTypeList"
    return mock


@pytest.fixture
def search_response_with_multiple_matches_for_gene_symbol(mock_data):
    mock = mock_data("multiple-symbol-match.json", 200)
    genemap = json.loads(mock.text)["omim"]["searchResponse"]["geneMapList"]
    assert any(
        map(lambda x: "phenotypeMapList" not in x["geneMap"], genemap)
    ), "one of the geneMap dict should be without phenotypeMapList"
    assert any(
        map(lambda x: "phenotypeMapList" in x["geneMap"], genemap)
    ), "one of the geneMap dict should have a phenotypeMapList"
    return mock


@pytest.fixture
def search_two_symbol_response(mock_data):
    return mock_data("sample_api_response_two_symbols.json", 200)


@pytest.fixture
def search_response_multiple_phenotypes(mock_data):
    mock = mock_data("sample_api_response_symbol_with_multiple_phenotypes.json", 200)
    phenomap = json.loads(mock.text)["omim"]["searchResponse"]["geneMapList"][0][
        "geneMap"
    ]["phenotypeMapList"]
    assert (
        len(phenomap) > 1
    ), "this test fixture should have several phenotypes for the gene symbol"
    return mock


def test_empty_message_when_all_symbols_are_found(api, mocker, search_brca1_response):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_brca1_response
    gene_list = api.search("BRCA1")
    assert gene_list is not None
    assert len(gene_list["meta_info"][0]["messages"]) == 0


def test_message_contains_symbols_that_were_not_found(
    api, mocker, search_brca1_response
):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_brca1_response
    a_symbol = "a-symbol-no-in-the-search-response"
    gene_list = api.search(a_symbol)
    assert gene_list is not None
    assert any([m.find(a_symbol) > 0 for m in gene_list["meta_info"][0]["messages"]])


def test_search_dummy(api, mocker, search_brca1_response):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_brca1_response
    gene_list = api.search("dummy-gene-symbol")["gene_info"]
    assert gene_list is not None
    assert len(gene_list) == 0


def test_meta_in_response(api, mocker, search_brca1_response):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_brca1_response
    meta = api.search("dummy-gene-symbol")["meta_info"]
    assert meta is not None
    assert len(meta[0]["search"]) > 10
    assert len(str(meta[0]["searchTime"])) > 0
    assert len(str(meta[0]["searchDate"])) == len("dd-mm-yyyy")


def test_search(api, mocker, search_two_symbol_response):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_two_symbol_response
    gene_list = api.search(["timm8a", "brca1"])["phenotype_info"]
    assert gene_list is not None
    assert len(gene_list) == 2


def test_search_too_many_symbols(api):
    long_symbol_list = [str(i) for i in range(1, omim_api.MAX_SYMBOLS_ALLOWED + 2)]
    with pytest.raises(omim_api.OmimApi.RequestException):
        _ = api.call_api(*long_symbol_list)


def test_search_with_many_symbols_makes_several_api_calls(
    api, mocker, search_brca1_response
):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_brca1_response
    long_symbol_list = [str(i) for i in range(1, omim_api.MAX_SYMBOLS_IN_SEARCH + 30)]
    long_symbol_list[0] = "BRCA1"  # response content is checked against input
    response = api.search(long_symbol_list)
    number_of_api_calls = 2
    assert len(response["meta_info"]) == number_of_api_calls
    assert len(response["phenotype_info"]) == 1


def test_search_without_phenotype(api, mocker, search_response_with_missing_phenotype):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_response_with_missing_phenotype
    response = api.search("tap2")  # response content is checked against input
    assert "phenotype_info" in response
    assert len(response["phenotype_info"]) == 1


def test_search_where_symbols_has_multiple_phenotypes(
    api, mocker, search_response_multiple_phenotypes
):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_response_multiple_phenotypes
    the_symbol = "ctsc"
    response = api.search(the_symbol)
    assert "phenotype_info" in response
    phenotypes = response["phenotype_info"][the_symbol.upper()]
    assert phenotypes[0] is not None
    assert phenotypes[1] is not None


def test_merge(api):
    # noinspection PyProtectedMember
    merged = api._merge(
        {"phenotype_info": {"sym": {}}, "meta": []},
        {"phenotype_info": {"sym2": {}}, "meta": []},
    )
    assert len(merged["phenotype_info"]) == 2


def test_search_where_result_has_mixed_phenotype(
    api, mocker, search_response_with_multiple_matches_for_gene_symbol
):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_response_with_multiple_matches_for_gene_symbol
    response = api.search("alg2")
    the_symbol = "alg2"
    assert "phenotype_info" in response
    phenotypes = response["phenotype_info"][the_symbol.upper()]
    assert phenotypes[0] is not None
    assert (
        len(response["meta_info"][0]["messages"]) == 1
    ), "should have one message about ignored gene map record"


def test_omimtool_find_omim_number(api, mocker, search_response_multiple_phenotypes):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_response_multiple_phenotypes
    the_symbol = "ctsc"
    response = api.search(the_symbol)
    assert "gene_info" in response
    g = response["gene_info"][the_symbol.upper()]
    print("%s/%s" % (g["gene_symbol"], g["omim_number"]))
    omim_n = omim_api.OmimTools.find_omim_number(the_symbol, response)
    assert omim_n == 602365


def test_omimtool_find_inheritance(api, mocker, search_response_multiple_phenotypes):
    mocker.patch(
        "gpbuilder.omim_api.omim_api.requests"
    ).get.return_value = search_response_multiple_phenotypes
    the_symbol = "ctsc"
    response = api.search(the_symbol)
    assert "phenotype_info" in response
    phenotypes = response["phenotype_info"][the_symbol.upper()]
    print("\n".join(["%s/%s" % (p["phenotype"], p["inheritance"]) for p in phenotypes]))
    inheritance = omim_api.OmimTools.find_inheritance(the_symbol, response)
    assert inheritance[0] == "Autosomal recessive"
    assert len(set(inheritance)) == 1
