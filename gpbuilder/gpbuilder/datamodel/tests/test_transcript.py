from gpbuilder.datamodel.transcript import Transcript


def generateTranscript(
    name,
    chromosome,
    strand,
    start,
    end,
    coding_start,
    coding_end,
    exon_starts,
    exon_ends,
    hgnc_id,
):
    tx = Transcript(
        name=name,
        source="Custom",
        ref_genome="GRCh37",
        chromosome=chromosome,
        strand=strand,
        start=int(start),
        end=int(end),
        coding_start=coding_start,
        coding_end=coding_end,
        exon_starts=list(int(n) for n in exon_starts.split(",") if n.strip()),
        exon_ends=list(int(n) for n in exon_ends.split(",") if n.strip()),
        hgnc_id=int(hgnc_id),
    )
    return tx


def generateTranscriptConstruct(
    name,
    chromosome,
    strand,
    start,
    end,
    coding_start,
    coding_end,
    exon_starts,
    exon_ends,
    hgnc_id,
):
    tx = Transcript.construct(
        name=name,
        source="Custom",
        ref_genome="GRCh37",
        chromosome=chromosome,
        strand=strand,
        start=int(start),
        end=int(end),
        coding_start=coding_start,
        coding_end=coding_end,
        exon_starts=list(int(n) for n in exon_starts.split(",") if n.strip()),
        exon_ends=list(int(n) for n in exon_ends.split(",") if n.strip()),
        hgnc_id=int(hgnc_id),
    )
    return tx


class TestTranscript:
    def test_transcript(self):
        tx = generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        )
        assert type(tx) == Transcript
        assert ("1", 100, 200) == (tx.chromosome, tx.read_start, tx.read_end)
        tx = generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        )
        assert ("1", 100, 200) == (tx.chromosome, tx.read_start, tx.read_end)

    def test_exons(self):
        # single exon, positive strand
        assert [("1", 100, 200)] == list(
            (exon.chromosome, exon.start, exon.end)
            for exon in generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).exons
        )
        # single exon, negative strand
        assert [("1", 100, 200)] == list(
            (exon.chromosome, exon.start, exon.end)
            for exon in generateTranscript(
                "A",
                "1",
                "-",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).exons
        )
        # multiple exons, positive strand, no coding frame
        assert [("1", 24474, 25037), ("1", 25139, 25344), ("1", 25583, 25944)] == list(
            (exon.chromosome, exon.start, exon.end)
            for exon in generateTranscript(
                "NR_026820",
                "1",
                "+",
                "24474",
                "25944",
                None,
                None,
                "24474,25139,25583,",
                "25037,25344,25944,",
                "1010",
            ).exons
        )
        # multiple exons, positive strand, coding frame
        tx = generateTranscript(
            "NM_05101",
            "1",
            "+",
            "938709",
            "939896",
            "938816",
            "939789",
            "938709,939226,939824",
            "938819,939782,939896",
            "1010",
        )
        assert [
            ("1", 938709, 938819),
            ("1", 939226, 939782),
            ("1", 939824, 939896),
        ] == list((exon.chromosome, exon.start, exon.end) for exon in tx.exons)
        root_name = f"1010{Transcript.name_sep}NM_05101{Transcript.name_sep}exon"
        assert [f"{root_name}1", f"{root_name}2", f"{root_name}3"] == tx.exon_names
        # multiple exons, negative strand, coding frame
        tx = generateTranscript(
            "NM_05101",
            "1",
            "-",
            "938709",
            "939896",
            "938816",
            "939789",
            "938709,939226,939824",
            "938819,939782,939896",
            "1010",
        )
        assert [
            ("1", 938709, 938819),
            ("1", 939226, 939782),
            ("1", 939824, 939896),
        ] == list((exon.chromosome, exon.start, exon.end) for exon in tx.exons)
        root_name = f"1010{Transcript.name_sep}NM_05101{Transcript.name_sep}exon"
        assert [f"{root_name}3", f"{root_name}2", f"{root_name}1"] == tx.exon_names

    def test_introns(self):
        # single exon (no introns), positive strand, coding frame
        assert [] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).introns
        )
        # single exon (no introns), negative strand, coding frame
        assert [] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "-",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).introns
        )
        # multiple exons, single intron, positive strand, coding frame
        assert [("1", 120, 180)] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,180,",
                "120,200,",
                "1010",
            ).introns
        )
        # multiple exons, single intron, negative strand, coding frame
        assert [("1", 120, 180)] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "-",
                "100",
                "200",
                "110",
                "190",
                "100,180,",
                "120,200,",
                "1010",
            ).introns
        )
        # multiple exons, multiple introns, positive strand, coding frame
        assert [("1", 110, 140), ("1", 150, 190)] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,140,190,",
                "110,150,200,",
                "1010",
            ).introns
        )
        # multiple exons, multiple introns, negative strand, coding frame
        assert [("1", 110, 140), ("1", 150, 190)] == list(
            (intron.chromosome, intron.start, intron.end)
            for intron in generateTranscript(
                "A",
                "1",
                "-",
                "100",
                "200",
                "110",
                "190",
                "100,140,190,",
                "110,150,200,",
                "1010",
            ).introns
        )

    def test_cds(self):
        # single exon, positive strand
        assert [("1", 110, 190)] == list(
            (cx.chromosome, cx.start, cx.end)
            for cx in generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).cds
        )
        # single exon, negative strand
        assert [("1", 110, 190)] == list(
            (cx.chromosome, cx.start, cx.end)
            for cx in generateTranscript(
                "A",
                "1",
                "-",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1010",
            ).cds
        )
        # multiple exons, first excluded, positive strand
        tx = generateTranscript(
            "NM_02682",
            "1",
            "+",
            "24474",
            "25944",
            "25144",
            "25844",
            "24474,25139,25583,",
            "25037,25344,25944,",
            "1010",
        )
        assert [("1", 25144, 25344), ("1", 25583, 25844)] == list(
            (cx.chromosome, cx.start, cx.end) for cx in tx.cds
        )
        root_name = f"1010{Transcript.name_sep}NM_02682{Transcript.name_sep}exon"
        assert [f"{root_name}2", f"{root_name}3"] == tx.cds_names
        # multiple exons, last excluded, positive strand
        tx = generateTranscript(
            "NM_02682",
            "1",
            "+",
            "24474",
            "25944",
            "24644",
            "25244",
            "24474,25139,25583,",
            "25037,25344,25944,",
            "1010",
        )
        assert [("1", 24644, 25037), ("1", 25139, 25244)] == list(
            (cx.chromosome, cx.start, cx.end) for cx in tx.cds
        )
        root_name = f"1010{Transcript.name_sep}NM_02682{Transcript.name_sep}exon"
        assert [f"{root_name}1", f"{root_name}2"] == tx.cds_names
        # multiple exons, first excluded, negative strand
        tx = generateTranscript(
            "NM_02682",
            "1",
            "-",
            "24474",
            "25944",
            "25144",
            "25844",
            "24474,25139,25583,",
            "25037,25344,25944,",
            "1010",
        )
        assert [("1", 25144, 25344), ("1", 25583, 25844)] == list(
            (cx.chromosome, cx.start, cx.end) for cx in tx.cds
        )
        root_name = f"1010{Transcript.name_sep}NM_02682{Transcript.name_sep}exon"
        assert [f"{root_name}2", f"{root_name}1"] == tx.cds_names
        # multiple exons, last excluded, negative strand
        tx = generateTranscript(
            "NM_02682",
            "1",
            "-",
            "24474",
            "25944",
            "24644",
            "25244",
            "24474,25139,25583,",
            "25037,25344,25944,",
            "1010",
        )
        assert [("1", 24644, 25037), ("1", 25139, 25244)] == list(
            (cx.chromosome, cx.start, cx.end) for cx in tx.cds
        )
        root_name = f"1010{Transcript.name_sep}NM_02682{Transcript.name_sep}exon"
        assert [f"{root_name}3", f"{root_name}2"] == tx.cds_names

    def test_utr5(self):
        assert ("1", 100, 110) == generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).utr5
        assert ("1", 190, 200) == generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).utr5

    def test_utr3(self):
        assert ("1", 190, 200) == generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).utr3
        assert ("1", 100, 110) == generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).utr3

    def test_no_exons(self):
        assert (
            []
            == generateTranscript(
                "A", "1", "+", "100", "360", "110", "320", "", "", "1010"
            ).exons
        )

    def test_no_coding(self):
        tx = generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "360",
            None,
            None,
            "100",
            "220",
            "1010",
        )
        assert tx.cds == []
        assert ~tx.is_coding

    def test_slop(self):
        assert generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "90",
            "196",
            "80,",
            "206,",
            "1010",
        ) == generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).slopped(20, 6)
        assert generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "104",
            "210",
            "94,",
            "220,",
            "1010",
        ) == generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1010",
        ).slopped(20, 6)
        assert generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "104",
            "210",
            "94,144,184",
            "150,190,220",
            "1010",
        ) == generateTranscript(
            "A",
            "1",
            "-",
            "100",
            "200",
            "110",
            "190",
            "100,150,190",
            "130,170,200",
            "1010",
        ).slopped(20, 6)

    def test_find_longest_frames(self):
        tx_sample = [
            generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1111",
            ),
            generateTranscript(
                "B",
                "1",
                "+",
                "100",
                "360",
                "110",
                "350",
                "100,300,",
                "200,350,",
                "1111",
            ),
            generateTranscript(
                "C",
                "1",
                "+",
                "100",
                "360",
                "110",
                "350",
                "100,320,",
                "200,320,",
                "1111",
            ),
            generateTranscript(
                "D",
                "1",
                "+",
                "100",
                "360",
                "110",
                "320",
                "100,320,",
                "200,350,",
                "1111",
            ),
        ]
        longest_refseq = Transcript._find_longest_frames(tx_sample).pop()
        assert longest_refseq.name == "B"
        tx_sample = [
            generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                None,
                None,
                "100,",
                "200,",
                "1111",
            ),
            generateTranscript(
                "B",
                "1",
                "+",
                "100",
                "360",
                "110",
                "350",
                "",
                "",
                "1111",
            ),
            generateTranscript("C", "1", "+", "100", "360", None, None, "", "", "1111"),
        ]
        longest_refseq = Transcript._find_longest_frames(tx_sample).pop()
        assert longest_refseq.name == "B"
        tx_sample = [
            generateTranscript(
                "A",
                "1",
                "+",
                "100",
                "200",
                "110",
                "190",
                "100,",
                "200,",
                "1111",
            ),
            generateTranscript(
                "B",
                "1",
                "+",
                "100",
                "360",
                None,
                None,
                "100,300,",
                "200,350,",
                "1111",
            ),
        ]
        longest_refseq = Transcript._find_longest_frames(tx_sample).pop()
        assert longest_refseq.name == "A"

    def test_unvalidated_transcript(self, session):
        tx_a = generateTranscript(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1111",
        )
        tx_b = generateTranscriptConstruct(
            "A",
            "1",
            "+",
            "100",
            "200",
            "110",
            "190",
            "100,",
            "200,",
            "1111",
        )
        assert type(tx_a) == type(tx_b) == Transcript
        assert set(dir(tx_a)).difference(dir(tx_b)) == {"_sa_instance_state"}
        assert tx_a.chromosome == tx_b.chromosome == "1"
        assert tx_a.name_tuple == tx_b.name_tuple == ("1111", "A")
