import API from 'api'
import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import GeneSelector from 'components/geneselector'
import Layout from 'components/layouts/configure'
import DefaultLayout from 'components/layouts/default'
import PanelAppSelector from 'components/panelappselector'
import RegionSelector from 'components/regionselector'
import { useRouter } from 'next/router'

export default function SelectPanel() {
  const { query, push } = useRouter()
  const { id } = query
  const { genePanelConfig, savePanel, dirty } = useGenePanel()
  const loading = id && !genePanelConfig.id

  const onSubmit = (ev) => {
    ev.preventDefault()
    push(query?.id ? `/configure/edit?id=${query.id}` : '/configure/edit')
  }

  return (
    <form className="m-auto h-screen w-full" onSubmit={onSubmit}>
      <div className="sticky top-0 z-30 flex w-full flex-row items-center justify-between bg-ellablue-lighter py-2 px-8">
        <h2 className="text-lg font-medium text-gray-900">
          Select genes and regions for:
          <span className="font-normal italic">
            {` ${genePanelConfig.shortname}`}
          </span>
        </h2>
        <div className="text-sm text-gray-500">
          Add PanelApp panels (subscribe), genes and/or regions.
        </div>
        <div className="flex gap-x-2">
          <div className="w-40">
            <Button
              disabled={loading}
              onClick={() => {
                savePanel().then((id) => {
                  push(id ? `/configure/name?id=${id}` : '/configure/name')
                })
              }}
            >
              Previous (Name)
            </Button>
          </div>
          <div className="w-20">
            <Button
              disabled={!dirty}
              onClick={() =>
                savePanel().then((id) => {
                  push(id ? `/configure/select?id=${id}` : '/configure/select')
                })
              }
            >
              Save
            </Button>
          </div>

          <div className="ml-auto w-40 justify-end">
            <Button
              disabled={loading}
              color="confirm"
              onClick={() => {
                savePanel().then((id) => {
                  push(id ? `/configure/edit?id=${id}` : '/configure/edit')
                })
              }}
            >
              Continue (Edit)
            </Button>
          </div>
        </div>
      </div>

      <div className="h-[calc(100vh-89px)] overflow-x-hidden overflow-y-scroll">
        <div className="mx-auto divide-y divide-gray-200 pl-8 pr-7 pb-8 2xl:w-4/5">
          <div className="mt-12">
            <div className="mb-6 grid grid-cols-6 items-start gap-4">
              <label
                htmlFor="genepanels"
                className="block pt-2 font-medium text-gray-700"
              >
                PanelApp panels
              </label>
              <div className="col-span-5 w-full">
                <PanelAppSelector />
              </div>
            </div>
          </div>

          <div className="space-y-6">
            <div className="my-6 grid grid-cols-6 items-start gap-4">
              <label
                htmlFor="genes"
                className="block pt-2 font-medium text-gray-700"
              >
                Genes
              </label>
              <div className="col-span-5 w-full">
                <GeneSelector />
              </div>
            </div>
          </div>

          <div className="space-y-6">
            <div className="mt-6 grid grid-cols-6 items-start gap-4">
              <label
                htmlFor="regions"
                className="block pt-2 font-medium text-gray-700"
              >
                Regions
              </label>
              <div className="col-span-5 w-full">
                <RegionSelector />
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

SelectPanel.getLayout = (page) => (
  <DefaultLayout>
    <Layout>{page}</Layout>
  </DefaultLayout>
)
