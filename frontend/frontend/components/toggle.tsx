import React, { useState } from 'react'
import { Switch } from '@headlessui/react'

import { classNames } from 'components/utils'

export default function Toggle({
  id,
  name,
  label,
  toggled = false,
  color = 'green',
  onChange,
}: {
  id?: string
  name?: string
  label?: string
  toggled?: boolean
  color?: 'green' | 'amber' | 'red'
  onChange: (toggled: boolean) => void
}) {
  const [checked, setChecked] = useState(toggled)

  return (
    <div>
      <span className="mr-2">
        <Switch
          id={id}
          name={name}
          checked={checked}
          onChange={(status) => {
            setChecked(status)
            if (onChange) onChange(status)
          }}
          className={classNames(
            checked ? `bg-panel${color}-500` : `bg-panel${color}-200`,
            'relative inline-flex h-6 w-11 items-center rounded-full'
          )}
        >
          <span
            className={`${
              checked ? 'translate-x-6' : 'translate-x-1'
            } inline-block h-4 w-4 rounded-full bg-white transition`}
          />
        </Switch>
      </span>
      <label className="align-top text-gray-500" htmlFor={id}>
        {label}
      </label>
    </div>
  )
}
