import json

from fastapi.testclient import TestClient
from sqlmodel import select

from gpbuilder.api.genepanels import ConfidenceLevel
from gpbuilder.api.tests.mock_data import (
    create_genes,
    create_panelapp_panels,
    create_panels,
    create_regions,
    create_transcripts,
    link_genepanel_excluded_gene,
    link_genepanel_gene,
    link_genepanel_panelapp_panel,
    link_genepanel_region,
    link_panelapp_panel_gene,
)
from gpbuilder.app import app
from gpbuilder.datamodel.crud.genepanel import GenePanelConfig
from gpbuilder.datamodel.crud.models import (
    GenePanelConfigGene,
    GenePanelConfigRegion,
    GenePanelSelectRequestModel,
    PanelAppInput,
)
from gpbuilder.datamodel.genepanel import (
    GenePanel,
    GenePanelExcludedGene,
    GenePanelGene,
    GenePanelPanelAppPanel,
    GenePanelRegion,
)
from gpbuilder.datamodel.region import Region

client = TestClient(app)


def test_search_genepanel_by_pattern_of_letters():
    response = client.get("/api/v1/genepanels/search/mypanel")
    assert response.status_code == 200


def test_search_genepanel_by_pattern_with_ints():
    response = client.get("/api/v1/genepanels/search/112")
    assert response.status_code == 200


# Notice the call of post(..., json={...}) it can handle strings for the enum confidence levels
def test_post_select(session):
    (gene,) = create_genes(1, session)
    (panelapp_panel,) = create_panelapp_panels(1, session)
    session.commit()

    response = client.post(
        "/api/v1/genepanels/select",
        json={
            "hgnc_ids": [gene.hgnc_id],
            "panelapp_panels": [
                {
                    "panelapp_id": panelapp_panel.id,
                    "confidence_levels": {"1": "true", "2": "true", "3": "false"},
                }
            ],
            "regions": [
                Region(chromosome="12", start=123, end=1100, ref_genome="GRCh37").dict()
            ],
        },
    )
    assert response.status_code == 200


def test_post_select_invalid_hgnc_id():
    response = client.post(
        "/api/v1/genepanels/select",
        json={
            "hgnc_ids": ["ABC"],
            "panelapp_panels": [],
            "regions": [
                Region(chromosome="12", start=123, end=1100, ref_genome="GRCh37").dict()
            ],
        },
    )
    assert response.status_code == 422


def test_post_select_list_with_positive_and_neg_ints(session):
    (gene,) = create_genes(1, session)
    session.commit()
    response = client.post(
        "/api/v1/genepanels/select",
        json={
            "hgnc_ids": [gene.hgnc_id],
            "panelapp_panels": [],
            "regions": [dict(chromosome="1", start=123, end=-1, ref_genome="GRCh37")],
        },
    )
    assert response.status_code == 422


# Notice the call of post(..., json=data.dict()) the instance of GenePanelSelectRequestModel must be
# transformed to dict for the json parameter. This way still fails in some cases when model has
# nested models containing, e.g., enums
def test_post_select_with_empty_lists():
    data = GenePanelSelectRequestModel(hgnc_ids=[], panelapp_panels=[], regions=[])
    response = client.post("/api/v1/genepanels/select", json=data.dict())
    assert response.status_code == 200


# Test with a correct PannelAppInput
# Notice the call of post(..., data=data.json()) the instance of GenePanelSelectRequestModel must be
# transformed to json for the data parameter
def test_post_select_with_panelapp_panels(session):
    genes = create_genes(3, session)
    (panelapp_panel,) = create_panelapp_panels(1, session)
    session.commit()
    pannelAppInput1 = PanelAppInput(
        panelapp_id=panelapp_panel.id,
        confidence_levels={
            ConfidenceLevel.red: True,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.green: False,
        },
    )

    data = GenePanelSelectRequestModel(
        hgnc_ids=[gene.hgnc_id for gene in genes],
        panelapp_panels=[pannelAppInput1],
        regions=[
            Region(chromosome="1", start=2, end=3, ref_genome="GRCh37"),
            Region(chromosome="2", start=2, end=4, ref_genome="GRCh37"),
        ],
    )

    response = client.post(
        "/api/v1/genepanels/select",
        data=data.json(),
    )
    assert response.status_code == 200


def test_post_select_with_panelapp_panels_wrong_id():
    response = client.post(
        "/api/v1/genepanels/select",
        json={
            "hgnc_ids": [],
            "panelapp_panels": [
                {
                    "panelapp_panel_id": "ABC",  # Here is the error
                    "confidence_levels": {"1": "true", "2": "true", "3": "false"},
                }
            ],
            "regions": [
                Region(chromosome="12", start=123, end=1100, ref_genome="GRCh37").dict()
            ],
        },
    )
    assert response.status_code == 422


def test_post_select_with_panelapp_panels_missing_confidence_values(session):
    panelapp_panels = create_panelapp_panels(2, session)
    genes = create_genes(3, session)
    session.commit()

    pannelAppInput1 = PanelAppInput(
        panelapp_id=panelapp_panels[0].id, confidence_levels={ConfidenceLevel.red: True}
    )
    pannelAppInput2 = PanelAppInput(
        panelapp_id=panelapp_panels[1].id,
        confidence_levels={ConfidenceLevel.red: False, ConfidenceLevel.green: True},
    )
    data = GenePanelSelectRequestModel(
        hgnc_ids=[gene.hgnc_id for gene in genes],
        panelapp_panels=[pannelAppInput1, pannelAppInput2],
        regions=[
            Region(chromosome="1", start=2, end=3, ref_genome="GRCh37"),
            Region(chromosome="2", start=2, end=4, ref_genome="GRCh37"),
        ],
    )

    response = client.post(
        "/api/v1/genepanels/select",
        data=data.json(),
    )
    assert response.status_code == 200


def test_create_genepanel(session):
    genes = create_genes(3, session)
    hgnc_ids = [gene.hgnc_id for gene in genes]
    session.commit()

    data = GenePanelConfig(
        id=None,
        hgnc_ids=hgnc_ids,
        panelapp_panels=[],
        regions=[],
        name="test",
        shortname="testA",
        version=None,
        description="foo",
        diff={genes[0].hgnc_id: GenePanelConfigGene(comment="foo")},
    )

    response = client.put("/api/v1/genepanels/configure", json=data.dict())
    assert response.status_code == 201
    # gp_id = response.json()["id"]

    db_genepanel = session.exec(
        select(GenePanel).where(
            GenePanel.name == "test", GenePanel.version == "0.0.1-draft"
        )
    ).one()

    #    assert db_genepanel.name == "test"
    #    assert db_genepanel.version == "1.0.0-draft"

    # Check that the genes are added to the gene panel
    db_genes = session.exec(
        select(GenePanelGene).where(GenePanelGene.genepanel_id == db_genepanel.id)
    ).all()

    assert set([gene.hgnc_id for gene in db_genes]) == set(hgnc_ids)

    # Check that the diff is added to the gene panel
    session.exec(
        select(GenePanelGene).where(
            GenePanelGene.genepanel_id == db_genepanel.id,
            GenePanelGene.hgnc_id == genes[0].hgnc_id,
            GenePanelGene.comment == "foo",
        ),
    ).one()


def test_create_panel_with_existing_shortname(session):
    (panel,) = create_panels(1, session)
    session.flush()
    session.commit()

    data = GenePanelConfig(
        id=None,
        hgnc_ids=[],
        panelapp_panels=[],
        regions=[],
        name="test NEW",
        shortname=panel.shortname,
        version=None,
        description="foo",
        diff={},
    )

    response = client.put("/api/v1/genepanels/configure", json=data.dict())
    assert response.status_code == 409


def test_genes_origin_enforced_creating_genepanel(session):
    # Add test gene
    genes = create_genes(2, session)
    session.add_all(genes)
    session.flush()

    # Add PanelApp panel with that gene
    pa_panel = create_panelapp_panels(1, session)[0]
    session.flush()

    link_panelapp_panel_gene(genes, pa_panel, 3, session)

    session.commit()

    # Add both genes both manually and as part of the PanelApp panel
    data = GenePanelConfig(
        id=None,
        hgnc_ids=[g.hgnc_id for g in genes],
        panelapp_panels=[
            PanelAppInput(
                panelapp_id=pa_panel.id,
                confidence_levels={
                    ConfidenceLevel.red: False,
                    ConfidenceLevel.amber: False,
                    ConfidenceLevel.green: True,
                },
            )
        ],
        regions=[],
        name="test",
        shortname="testA",
        version=None,
        description="foobar",
        diff={genes[0].hgnc_id: GenePanelConfigGene(comment="foo")},
    )

    response = client.put("/api/v1/genepanels/configure", json=data.dict())

    assert response.status_code == 201

    id = response.json()["id"]

    # Check that genes were not added manually, because they are contained in the PanelApp panel
    gpgs = session.exec(
        select(GenePanelGene).where(GenePanelGene.genepanel_id == id)
    ).all()

    # One gene has a diff, so it should appear
    assert len(gpgs) == 1
    assert gpgs[0].hgnc_id == genes[0].hgnc_id
    # .. but not as manually added
    assert gpgs[0].manually_added is False


def test_update_genepanel_non_draft(session):
    (panel,) = create_panels(1, session)
    panel.version = "1.0.0"
    session.commit()

    data = GenePanelConfig(
        id=panel.id,
        hgnc_ids=[],
        panelapp_panels=[],
        regions=[],
        name=panel.name,
        shortname=panel.shortname,
        version=None,
        description=panel.description,
        diff={},
    )

    response = client.put("/api/v1/genepanels/configure", json=data.dict())

    assert response.status_code == 200
    assert response.json()["id"] != panel.id
    new_genepanel = session.get(GenePanel, response.json()["id"])
    assert new_genepanel.version == "1.0.1-draft"

    response = client.put("/api/v1/genepanels/configure", json=data.dict())
    new_genepanel = session.get(GenePanel, response.json()["id"])
    assert new_genepanel.version == "1.0.2-draft"


def test_update_genepanel_draft(session):
    # Create a panel
    genepanels = create_panels(1, session)
    session.flush()

    genes = create_genes(3, session)
    session.flush()

    hgnc_ids = [genes[0].hgnc_id, genes[1].hgnc_id]

    pa_panels = create_panelapp_panels(1, session)
    session.flush()
    link_genepanel_gene(genes[0], genepanels[0], session)
    link_genepanel_excluded_gene([genes[1]], genepanels[0], session)
    link_panelapp_panel_gene([genes[2]], pa_panels[0], 1, session)
    link_genepanel_panelapp_panel(pa_panels, genepanels[0], session)

    session.commit()

    payload = GenePanelConfig(
        id=genepanels[0].id,
        hgnc_ids=hgnc_ids,
        panelapp_panels=[
            PanelAppInput(
                panelapp_id=pa_panels[0].id,
                confidence_levels={
                    ConfidenceLevel.red: True,
                    ConfidenceLevel.amber: False,
                    ConfidenceLevel.green: False,
                },
            )
        ],
        regions=[
            GenePanelConfigRegion(
                id=None,
                chromosome="1",
                start=10,
                end=100,
                ref_genome="GRCh37",
                name="Region1",
            ),
            GenePanelConfigRegion(
                id=None,
                chromosome="2",
                start=20,
                end=200,
                ref_genome="GRCh37",
                name="Region2",
            ),
        ],
        name="Test panel 1",
        shortname=genepanels[0].shortname,
        version=None,
        description="foo",
        diff={
            genes[0].hgnc_id: GenePanelConfigGene(inheritance="ADAR"),
            genes[1].hgnc_id: GenePanelConfigGene(
                exclusion_reasons=["Technical issues"], comment="BAZBAR"
            ),
        },
    )

    response = client.put("/api/v1/genepanels/configure", json=payload.dict())
    assert response.status_code == 200

    # Need to expire session to get updated data
    session.expire_all()

    db_genepanel = session.get(GenePanel, genepanels[0].id)
    assert db_genepanel.description == "foo"
    assert db_genepanel.version == "1.0.0-draft"

    db_genepanel_gene = session.exec(
        select(GenePanelGene, GenePanelGene.genepanel_id == db_genepanel.id)
    ).scalar()
    assert db_genepanel_gene.inheritance_mode == "ADAR"

    db_genepanel_excluded_gene = session.exec(
        select(
            GenePanelExcludedGene,
            GenePanelExcludedGene.genepanel_id == db_genepanel.id,
        )
    ).scalar()
    assert db_genepanel_excluded_gene.exclusion_reasons == ["Technical issues"]

    db_genepanel_panelapp_panel = session.exec(
        select(
            GenePanelPanelAppPanel,
            GenePanelPanelAppPanel.genepanel_id == db_genepanel.id,
        )
    ).scalar()
    assert db_genepanel_panelapp_panel.confidence_levels == [1]

    db_regions = (
        session.exec(
            select(
                GenePanelRegion.region_id,
                GenePanelRegion.name,
                GenePanelRegion.comment,
                GenePanelRegion.inheritance_mode,
                Region.chromosome,
                Region.start,
                Region.end,
                Region.ref_genome,
            )  # type: ignore
            .outerjoin(Region, Region.id == GenePanelRegion.region_id)
            .where(GenePanelRegion.genepanel_id == db_genepanel.id)
        )
        .mappings()
        .all()
    )
    assert len(db_regions) == 2
    assert db_regions[0].region_id == 1
    assert db_regions[0].chromosome == "1"
    assert db_regions[0].start == 10
    assert db_regions[0].end == 100
    assert db_regions[1].region_id == 2
    assert db_regions[1].chromosome == "2"
    assert db_regions[1].start == 20
    assert db_regions[1].end == 200


def test_get_genepanel(session):
    # Create a panel
    genepanels = create_panels(1, session)
    session.flush()

    genes = create_genes(4, session)
    [session.add(gene) for gene in genes]
    manually_added_gene = genes[3]
    session.flush()

    pa_panels = create_panelapp_panels(1, session)
    session.flush()

    (region,) = create_regions(1, session)
    session.flush()

    (transcript,) = create_transcripts(1, manually_added_gene.hgnc_id, session)
    session.flush()

    genepanel_gene = link_genepanel_gene(genes[0], genepanels[0], session)
    (excluded_gene,) = link_genepanel_excluded_gene([genes[1]], genepanels[0], session)
    link_panelapp_panel_gene([genes[2]], pa_panels[0], 1, session)
    manually_added_genepanel_gene = link_genepanel_gene(
        manually_added_gene,
        genepanels[0],
        session,
        manually_added=True,
        transcripts=[transcript],
    )
    (genepanel_papanel,) = link_genepanel_panelapp_panel(
        pa_panels, genepanels[0], session
    )
    genepanel_region = link_genepanel_region(region, genepanels[0], session)
    session.flush()
    session.commit()

    # test get created panel
    response = client.get(f"/api/v1/genepanel/{genepanels[0].id}")
    assert response.status_code == 200

    response_body = response.json()
    assert response_body["diff"][str(genepanel_gene.hgnc_id)] == {
        "comment": genepanel_gene.comment,
        "exclusion_reasons": [],
        "inheritance": genepanel_gene.inheritance_mode,
        "transcripts": [tx.name for tx in genepanel_gene.transcripts],
    }
    assert response_body["diff"][str(excluded_gene.hgnc_id)] == {
        "comment": excluded_gene.comment,
        "exclusion_reasons": excluded_gene.exclusion_reasons,
        "inheritance": None,
        "transcripts": [],
    }

    assert response_body == {
        "id": genepanels[0].id,
        "name": genepanels[0].name,
        "shortname": genepanels[0].shortname,
        "description": genepanels[0].description,
        "version": genepanels[0].version,
        "hgnc_ids": [manually_added_genepanel_gene.hgnc_id],
        "panelapp_panels": [
            {
                "panelapp_id": genepanel_papanel.pa_panel_id,
                "confidence_levels": {
                    "1": 1 in genepanel_papanel.confidence_levels,
                    "2": 2 in genepanel_papanel.confidence_levels,
                    "3": 3 in genepanel_papanel.confidence_levels,
                },
            }
        ],
        "diff": {
            str(genepanel_gene.hgnc_id): {
                "comment": genepanel_gene.comment,
                "exclusion_reasons": [],
                "inheritance": genepanel_gene.inheritance_mode,
                "transcripts": [tx.name for tx in genepanel_gene.transcripts],
            },
            str(excluded_gene.hgnc_id): {
                "comment": excluded_gene.comment,
                "exclusion_reasons": excluded_gene.exclusion_reasons,
                "inheritance": None,
                "transcripts": [],
            },
            str(manually_added_genepanel_gene.hgnc_id): {
                "comment": manually_added_genepanel_gene.comment,
                "exclusion_reasons": [],
                "inheritance": manually_added_genepanel_gene.inheritance_mode,
                "transcripts": [manually_added_genepanel_gene.transcripts[0].name],
            },
        },
        "regions": [
            {
                "id": genepanel_region.region_id,
                "name": genepanel_region.name,
                "comment": genepanel_region.comment,
                "inheritance": genepanel_region.inheritance_mode,
                "chromosome": genepanel_region.region.chromosome,
                "start": genepanel_region.region.start,
                "end": genepanel_region.region.end,
                "ref_genome": genepanel_region.region.ref_genome,
                "strand": genepanel_region.region.strand,
            }
        ],
    }


def test_get_payload_equals_response(session):
    genes = create_genes(3, session)
    [session.add(gene) for gene in genes]
    (pa_panel,) = create_panelapp_panels(1, session)
    session.commit()

    # test get updated panel
    payload = GenePanelConfig(
        id=None,
        hgnc_ids=[genes[0].hgnc_id, genes[1].hgnc_id],
        panelapp_panels=[
            PanelAppInput(
                panelapp_id=pa_panel.id,
                confidence_levels={
                    ConfidenceLevel.red: True,
                    ConfidenceLevel.amber: False,
                    ConfidenceLevel.green: False,
                },
            )
        ],
        regions=[
            GenePanelConfigRegion(
                id=1,
                chromosome="1",
                start=10,
                end=100,
                inheritance="AD/AR",
                ref_genome="GRCh37",
                name="Region 1 name",
                comment="Region 1 comment",
            ),
            GenePanelConfigRegion(
                id=2,
                chromosome="2",
                start=20,
                end=200,
                ref_genome="GRCh37",
                inheritance="XD",
                name="Region 2 name",
                comment="Region 2 comment",
            ),
        ],
        name="Test panel 1",
        shortname="TA",
        version=None,
        description="NEW DESCRIPTION",
        diff={
            genes[0].hgnc_id: GenePanelConfigGene(inheritance="XR"),
            genes[1].hgnc_id: GenePanelConfigGene(
                exclusion_reasons=["Other"], comment="BAZBAR"
            ),
        },
    )

    update_response = client.put("/api/v1/genepanels/configure", json=payload.dict())
    assert update_response.status_code == 201
    gp_id = update_response.json()["id"]

    response = client.get(f"/api/v1/genepanel/{gp_id}")
    assert response.status_code == 200

    response_body = response.json()
    payload_body = json.loads(payload.json())
    payload_body["id"] = gp_id
    del payload_body["version"]
    del response_body["version"]
    assert payload_body == response_body


def test_delete_genepanel(session):
    genepanels = create_panels(1, session)
    session.commit()

    assert "draft" in genepanels[0].version

    # Delete panel
    response = client.delete(f"/api/v1/genepanel/{genepanels[0].id}")
    assert response.status_code == 200

    # Getting the deleted panel now fails
    response = client.get(f"/api/v1/genepanel/{genepanels[0].id}")
    assert response.status_code == 404

    # A submitted panel cannot be deleted
    genepanels = create_panels(1, session)
    genepanels[0].version = "1.0.0"
    session.commit()

    response = client.delete(f"/api/v1/genepanel/{genepanels[0].id}")
    assert response.status_code == 400
