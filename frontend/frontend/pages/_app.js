import 'styles/globals.css'
import DefaultLayout from 'components/layouts/default'
import { SessionProvider } from 'components/sessioncontext'

function MyApp({ Component, pageProps }) {
  // Use the layout defined at the page level, if available
  const getLayout =
    Component.getLayout || ((page) => <DefaultLayout>{page}</DefaultLayout>)
  return (
    <SessionProvider>{getLayout(<Component {...pageProps} />)}</SessionProvider>
  )
}

export default MyApp
