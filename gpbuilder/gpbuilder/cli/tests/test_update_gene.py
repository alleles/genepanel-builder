import pytest
import sqlalchemy.exc
from sqlmodel import Session

from gpbuilder.cli.update_data.gene import insert_gene
from gpbuilder.datamodel.gene import Gene


def test_insert_gene(session):
    gene = Gene(
        hgnc_id=1,
        symbol="symbol",
        aliases=["alias1", "alias2"],
        previous_symbols=["prev1", "prev2"],
        withdrawn=False,
        new_hgnc_id=None,
    )

    insert_gene(session, gene)
    db_gene = session.get(Gene, gene.hgnc_id)
    assert db_gene == gene


def test_insert_gene_deferred(session: Session):
    withdrawn_gene = Gene(
        hgnc_id=1,
        symbol="symbol",
        aliases=["alias1", "alias2"],
        previous_symbols=["prev1", "prev2"],
        withdrawn=True,
        new_hgnc_id=2,
    )

    new_gene = Gene(
        hgnc_id=2,
        symbol="symbol",
        aliases=["alias1", "alias2"],
        previous_symbols=["prev1", "prev2"],
        withdrawn=False,
        new_hgnc_id=None,
    )

    with pytest.raises(sqlalchemy.exc.IntegrityError):
        # Should fail since hgnc_id 2 is not available in the database
        insert_gene(session, withdrawn_gene, defer=False)
    session.rollback()  # Rollback to avoid breaking other tests

    # Insert with defer = True
    insert_gene(session, withdrawn_gene, defer=True)
    insert_gene(
        session, new_gene, defer=True
    )  # Since new_hgnc_id is None, defer=False is the same as defer=True
    db_withdrawn_gene = session.get(Gene, withdrawn_gene.hgnc_id)
    db_new_gene = session.get(Gene, new_gene.hgnc_id)
    assert db_withdrawn_gene is not None
    assert db_withdrawn_gene.new_hgnc_id is None
    assert db_new_gene == new_gene

    # Insert with defer = False
    insert_gene(session, withdrawn_gene, defer=False)
    db_withdrawn_gene = session.get(Gene, withdrawn_gene.hgnc_id)
    assert db_withdrawn_gene is not None
    assert (
        db_withdrawn_gene.new_hgnc_id == new_gene.hgnc_id == withdrawn_gene.new_hgnc_id
    )
