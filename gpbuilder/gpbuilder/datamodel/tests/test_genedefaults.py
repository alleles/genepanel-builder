import pytest
from pydantic import ValidationError
from sqlmodel import Session

from gpbuilder.api.tests.mock_data import create_genes, create_transcripts
from gpbuilder.datamodel.genedefaults import GeneDefaults


def test_get_transcripts_by_hgnc_id(session: Session):
    genes = create_genes(4, session)
    hgnc_ids = [gene.hgnc_id for gene in genes]
    session.flush()

    # Create default transcripts. Gene 0 has no default transcripts
    default_transcripts = {
        gene.hgnc_id: create_transcripts(i, gene.hgnc_id, session)
        for i, gene in enumerate(genes)
    }

    # Create some additional transcripts, that won't be part of the default
    for i, gene in enumerate(genes):
        create_transcripts(i, gene.hgnc_id, session)

    session.flush()

    gene_defaults = [
        GeneDefaults(
            hgnc_id=gene.hgnc_id,
            transcripts=default_transcripts[gene.hgnc_id],
        )
        for gene in genes
        if default_transcripts[gene.hgnc_id]
    ]
    session.add_all(gene_defaults)
    session.flush()
    transcripts_by_hgnc_id = GeneDefaults.get_transcripts_by_hgnc_ids(session, hgnc_ids)
    assert (
        genes[0].hgnc_id not in transcripts_by_hgnc_id
    ), "genes[0] should have no default transcripts"
    assert transcripts_by_hgnc_id == {
        gene.hgnc_id: default_transcripts[gene.hgnc_id] for gene in genes[1:]
    }

    transcripts_by_hgnc_id = GeneDefaults.get_transcripts_by_hgnc_ids(
        session, [genes[1].hgnc_id]
    )
    assert transcripts_by_hgnc_id == {
        genes[1].hgnc_id: default_transcripts[genes[1].hgnc_id]
    }


def test_get_inheritance_by_hgnc_id(session: Session):
    num_genes = len(GeneDefaults._allowed_inheritance_values) + 2
    genes = create_genes(num_genes, session)
    session.flush()

    # Genes 0 and 1 have no entry in GeneDefaults
    tx = create_transcripts(1, genes[1].hgnc_id, session)
    gene_defaults = [GeneDefaults(hgnc_id=genes[1].hgnc_id, transcripts=tx)]
    gene_defaults = [
        GeneDefaults(
            hgnc_id=gene.hgnc_id,
            inheritance=GeneDefaults._allowed_inheritance_values[i],
        )
        for i, gene in enumerate(genes[2:])
    ]
    session.add_all(gene_defaults)
    session.flush()
    inheritance_by_hgnc_id = GeneDefaults.get_inheritance_by_hgnc_ids(
        session, [gene.hgnc_id for gene in genes]
    )
    assert genes[0].hgnc_id not in inheritance_by_hgnc_id
    assert genes[1].hgnc_id not in inheritance_by_hgnc_id

    assert inheritance_by_hgnc_id == {
        gene.hgnc_id: GeneDefaults._allowed_inheritance_values[i]
        for i, gene in enumerate(genes[2:])
    }


def test_validation(session: Session):
    genes = create_genes(3, session)
    tx = create_transcripts(1, genes[0].hgnc_id, session)
    session.flush()

    # Works if either inheritance or transcripts (or both) is set
    GeneDefaults(hgnc_id=genes[0].hgnc_id, inheritance="AD")
    GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=[], inheritance="AD")
    GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=tx)
    GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=tx, inheritance=None)
    GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=tx, inheritance="AD")
    gd = GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=tx, inheritance="invalid")
    assert gd.inheritance is None, "Invalid inheritance should result in default value"

    # No transcript or inheritance
    with pytest.raises(ValidationError):
        GeneDefaults(hgnc_id=genes[0].hgnc_id)

    with pytest.raises(ValidationError):
        GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=[])

    with pytest.raises(ValidationError):
        GeneDefaults(hgnc_id=genes[0].hgnc_id, inheritance="")

    # Invalid transcript (transcript not tied to gene)
    with pytest.raises(ValidationError):
        GeneDefaults(hgnc_id=genes[1].hgnc_id, transcripts=tx)

    # Invalid transcript type
    with pytest.raises(ValidationError):
        GeneDefaults(hgnc_id=genes[0].hgnc_id, transcripts=[1, 2, 3])
