from gpbuilder.api.tests.mock_data import create_genes, create_transcripts
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genedefaults import GeneDefaults
from gpbuilder.datamodel.phenotype import Phenotype, PhenotypeGene
from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript


def test_default_transcripts(session):
    # Test the correct order of precedence for default transcripts
    # 1. Gene defaults
    # 2. MANE Select
    # 3. Longest transcript
    # 4. Fallback (empty list)

    genes = create_genes(4, session)

    # Fallback: empty lists
    hgnc_ids = [gene.hgnc_id for gene in genes]
    default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(session, hgnc_ids)
    assert default_transcripts == {hgnc_id: [] for hgnc_id in hgnc_ids}

    # Create three transcripts for each gene
    transcripts = {}
    longest_transcripts = {}
    mane_select_transcripts = {}
    gene_default_transcripts = {}
    for gene in genes:
        transcripts = create_transcripts(3, gene.hgnc_id, session)
        # Find the longest transcript for each gene
        longest_transcripts[gene.hgnc_id] = Transcript._find_longest_frames(transcripts)
        assert (
            len(longest_transcripts[gene.hgnc_id]) == 1
        ), "Expected only one longest transcript"
        transcripts.remove(longest_transcripts[gene.hgnc_id][0])

        # Set the other two transcripts as MANE Select and gene defaults
        mane_select_transcripts[gene.hgnc_id] = [transcripts.pop()]
        gene_default_transcripts[gene.hgnc_id] = [transcripts.pop()]
        assert len(transcripts) == 0, "Expected no more transcripts"

    # Test that the longest transcript is returned when no MANE Select or gene defaults are available
    default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(session, hgnc_ids)
    assert default_transcripts == longest_transcripts

    # Test that the MANE Select transcript is returned when no gene defaults are available
    session.add_all(
        ManeSelectTranscript(
            id=None,
            status="MANE Select",
            transcript_id=mane_select_transcripts[gene.hgnc_id][0].id,
        )
        for gene in genes
    )

    default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(session, hgnc_ids)
    assert default_transcripts == mane_select_transcripts

    # Test that the gene defaults are returned when available
    session.add_all(
        GeneDefaults(
            id=None,
            hgnc_id=gene.hgnc_id,
            transcripts=gene_default_transcripts[gene.hgnc_id],
        )
        for gene in genes
    )

    default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(session, hgnc_ids)
    assert default_transcripts == gene_default_transcripts


def test_default_inheritance(session):
    # Test the correct order of precedence for default inheritance
    # 1. Gene defaults
    # 2. Phenotype genes
    # 3. Fallback ("N/A")

    # Fallback: "N/A"
    genes = create_genes(3, session)
    hgnc_ids = [gene.hgnc_id for gene in genes]
    default_inheritance = Gene.get_default_inheritance_by_hgnc_ids(session, hgnc_ids)
    assert default_inheritance == {hgnc_id: "N/A" for hgnc_id in hgnc_ids}

    # Add phenotype genes
    ph = Phenotype(mim_number=12345, titles={"Test phenotype"}, update_id=1)
    session.add(ph)
    session.add_all(
        PhenotypeGene(
            id=None,
            gene_id=gene.hgnc_id,
            inheritance_descr="autosomal dominant",
            phenotype_id=ph.mim_number,
        )
        for gene in genes
    )

    # Test that the phenotype genes are returned when no gene defaults are available
    default_inheritance = Gene.get_default_inheritance_by_hgnc_ids(session, hgnc_ids)
    assert default_inheritance == {hgnc_id: "AD" for hgnc_id in hgnc_ids}

    # Add gene defaults
    session.add_all(
        GeneDefaults(
            id=None,
            hgnc_id=gene.hgnc_id,
            inheritance="AR",
        )
        for gene in genes
    )

    # Test that the gene defaults are returned when available
    default_inheritance = Gene.get_default_inheritance_by_hgnc_ids(session, hgnc_ids)
    assert default_inheritance == {hgnc_id: "AR" for hgnc_id in hgnc_ids}
