import { forwardRef } from 'react'
import ReactTextareaAutosize, {
  TextareaAutosizeProps,
} from 'react-textarea-autosize'

interface Props extends Omit<TextareaAutosizeProps, 'onChange'> {
  onChange: (value: string) => void
  resize?: boolean
}

const TextArea = forwardRef<HTMLTextAreaElement, Props>(
  (
    {
      id,
      name,
      placeholder,
      onChange,
      onBlur,
      value,
      minRows,
      maxRows,
      rows,
      resize = false,
      ...props
    }: Props,
    ref
  ) => {
    const onTextAreaChange = (e) => {
      if (onChange) {
        onChange(e.target.value)
      }
    }

    const onTextAreaBlur = (e) => {
      if (onBlur) {
        onBlur(e.target.value)
      }
    }

    return (
      <ReactTextareaAutosize
        cacheMeasurements
        id={id}
        minRows={minRows ?? rows}
        maxRows={maxRows ?? rows}
        name={name}
        onChange={onTextAreaChange}
        onBlur={onTextAreaBlur}
        className="w-full rounded border border-reactgray py-1.5 px-2 align-middle placeholder:text-reactgray-darkest hover:border-reactgray-dark [&:not(:focus):not(:placeholder-shown)]:invalid:border-red-500"
        value={value}
        placeholder={placeholder}
        ref={ref}
        {...props}
      />
    )
  }
)

TextArea.displayName = 'TextArea'

export default TextArea
