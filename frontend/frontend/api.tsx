import { PathsDictionary } from 'api-types'
import axios from 'axios'

axios.defaults.withCredentials = true
// Disable timeout in production - this should be handled by nginx.
// However, useful to have in development, as it gives a clear error message it times out
axios.defaults.timeout = process.env.NODE_ENV === 'development' ? 60_000 : 0
axios.defaults.baseURL = `${process.env.NEXT_PUBLIC_API_URL}`

namespace API {
  export namespace GenePanels {
    const path = '/api/v1/genepanels/'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (): Response => axios.get(path)
  }

  export namespace GenePanel {
    const path = '/api/v1/genepanel/{id}'
    type Response = ReturnType<PathsDictionary[typeof path]['delete']>
    export const del = (id): Response => axios.delete(path.replace(/{id}/, id))
  }

  export namespace GenePanelConfig {
    const path = '/api/v1/genepanel/{id}'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (id): Response => axios.get(path.replace(/{id}/, id))
  }

  export namespace Select {
    const path = '/api/v1/genepanels/select'
    type RequestBody = Parameters<PathsDictionary[typeof path]['post']>[1]
    type Response = ReturnType<PathsDictionary[typeof path]['post']>
    export const post = (body: RequestBody): Response => axios.post(path, body)
  }

  export namespace Configure {
    const path = '/api/v1/genepanels/configure'
    type RequestBody = Parameters<PathsDictionary[typeof path]['put']>[1]
    type Response = ReturnType<PathsDictionary[typeof path]['put']>
    export const put = (body: RequestBody): Response => axios.put(path, body)
  }

  export namespace SubmitPanel {
    const path = '/api/v1/genepanels/{id}/submit'
    type RequestBody = Parameters<PathsDictionary[typeof path]['post']>[1]
    type Response = ReturnType<PathsDictionary[typeof path]['post']>
    export const post = (id, version: RequestBody): Response =>
      axios.post(path.replace(/{id}/, id), null, { params: { version } })
  }

  export namespace GeneSearch {
    const path = '/api/v1/genenames/search/{pattern}'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const search = (pattern): Response =>
      axios.get(path.replace(/{pattern}/, pattern))

    const batchPath = '/api/v1/genenames/hgnc_ids'
    type BatchResponse = ReturnType<PathsDictionary[typeof batchPath]['post']>
    export const batch = (hgncIds): BatchResponse =>
      axios.post(batchPath, hgncIds, { params: { ensure_all: true } })
  }

  export namespace Genes {
    const path = '/api/v1/genenames/hgnc_ids'
    type Response = ReturnType<PathsDictionary[typeof path]['post']>
    type Request = Parameters<PathsDictionary[typeof path]['post']>[1]
    export const post = (ids: Request): Response =>
      axios.post(path, ids, { params: { ensure_all: true } })
  }

  export namespace PanelAppSearch {
    const path = '/api/v1/panelapp/{site}/search/{pattern}'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const search = (pattern): Response =>
      axios.get(path.replace(/{site}/, 'ENG').replace(/{pattern}/, pattern))
  }

  export namespace PanelAppPanels {
    const path = '/api/v1/panelapp/{site}'
    type Response = ReturnType<PathsDictionary[typeof path]['post']>
    type Request = Parameters<PathsDictionary[typeof path]['post']>[1]
    export const post = (ids: Request): Response =>
      axios.post(path.replace(/{site}/, 'ENG'), ids)
  }

  export namespace TranscriptCoverage {
    const path = '/api/v1/coverage/transcript'
    type Response = ReturnType<PathsDictionary[typeof path]['post']>
    type RequestQueryParams = Parameters<
      PathsDictionary[typeof path]['post']
    >[0]
    type RequestBodyParams = Parameters<PathsDictionary[typeof path]['post']>[1]
    export const get = (
      d4_target: RequestQueryParams,
      body: RequestBodyParams
    ): Response => axios.post(path, body, { params: { d4_target } })
  }

  export namespace AuthVerify {
    const path = '/api/auth/verify'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (): Response => axios.get(path)
  }

  export namespace AuthInfo {
    const path = '/api/auth/userinfo'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (): Response => axios.get(path)
  }

  export namespace AuthLogout {
    const path = '/api/auth/logout'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (): Response => axios.get(path)
  }
  export namespace SettingsGetVersion {
    const path = '/api/v1/version'
    type Response = ReturnType<PathsDictionary[typeof path]['get']>
    export const get = (): Response => axios.get(path)
  }
}
export default API
