from datetime import datetime

from gpbuilder.api.tests.mock_data import create_genes
from gpbuilder.cli.update_data.panelapp import insert_panelapp_panel
from gpbuilder.conftest import unique_index
from gpbuilder.datamodel.crud.models import ConfidenceLevel
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel, PanelAppPanelGene


def test_insert_panelapp(session):
    idx = next(unique_index())
    genes = create_genes(3, session)
    pa_panel = PanelAppPanel(
        id=idx,
        name=f"Test panel {idx}",
        version="1.0.0",
        disease_group="Test disease group",
        disease_sub_group="Test disease sub group",
        version_created=datetime.now(),
        update_id=1,
    )

    pa_panel_genes = [
        PanelAppPanelGene(
            hgnc_id=gene.hgnc_id, panelapp_panel_id=idx, confidence_level=i + 1
        )
        for i, gene in enumerate(genes)
    ]
    # Add first gene twice to test that duplicates are using the highest confidence level
    (duplicated_gene,) = create_genes(1, session)
    pa_panel_genes.extend(
        [
            PanelAppPanelGene(
                hgnc_id=duplicated_gene.hgnc_id,
                panelapp_panel_id=idx,
                confidence_level=conf_level,
            )
            for conf_level in [1, 2, 3]
        ]
    )

    insert_panelapp_panel(session, pa_panel, pa_panel_genes)

    db_pa_panel: PanelAppPanel = session.get(PanelAppPanel, idx)

    assert db_pa_panel is not None

    assert len(db_pa_panel.genes) == 4
    assert len(db_pa_panel.green_genes) == 2
    assert len(db_pa_panel.amber_genes) == 1
    assert len(db_pa_panel.red_genes) == 1

    assert db_pa_panel.red_genes[0].hgnc_id == genes[0].hgnc_id
    assert db_pa_panel.amber_genes[0].hgnc_id == genes[1].hgnc_id
    assert set(g.hgnc_id for g in db_pa_panel.green_genes) == set(
        [genes[2].hgnc_id, duplicated_gene.hgnc_id]
    )


def test_update_panelapp_panel(session):
    idx = next(unique_index())
    (red_gene, amber_gene, green_gene) = create_genes(3, session)
    pa_panel = PanelAppPanel(
        id=idx,
        name=f"Test panel {idx}",
        version="1.0.0",
        disease_group="Test disease group",
        disease_sub_group="Test disease sub group",
        version_created=datetime.now(),
        update_id=1,
    )

    # Add one gene at each confidence level to the panel
    pa_panel_genes = [
        PanelAppPanelGene(
            hgnc_id=red_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.red,
        ),
        PanelAppPanelGene(
            hgnc_id=amber_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.amber,
        ),
        PanelAppPanelGene(
            hgnc_id=green_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.green,
        ),
    ]

    insert_panelapp_panel(session, pa_panel, pa_panel_genes)

    db_pa_panel: PanelAppPanel = session.get(PanelAppPanel, idx)

    assert len(db_pa_panel.genes) == 3
    assert len(db_pa_panel.green_genes) == 1
    assert len(db_pa_panel.amber_genes) == 1
    assert len(db_pa_panel.red_genes) == 1

    assert db_pa_panel.red_genes[0].hgnc_id == red_gene.hgnc_id
    assert db_pa_panel.amber_genes[0].hgnc_id == amber_gene.hgnc_id
    assert db_pa_panel.green_genes[0].hgnc_id == green_gene.hgnc_id

    # Update the panel (note: same id)
    updated_pa_panel = PanelAppPanel(
        id=idx,
        name=f"Test panel {idx} - updated",
        version="2.0.0",
        disease_group="Test disease group - updated",
        disease_sub_group="Test disease sub group - updated",
        version_created=datetime.now(),
        update_id=2,
    )

    # Updated gene list: Remove red gene, move amber gene to green, add new red gene
    (new_gene,) = create_genes(1, session)

    updated_pa_panel_genes = [
        PanelAppPanelGene(
            hgnc_id=amber_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.green,
        ),
        PanelAppPanelGene(
            hgnc_id=green_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.green,
        ),
        PanelAppPanelGene(
            hgnc_id=new_gene.hgnc_id,
            panelapp_panel_id=idx,
            confidence_level=ConfidenceLevel.red,
        ),
    ]

    insert_panelapp_panel(session, updated_pa_panel, updated_pa_panel_genes)
    session.flush()

    db_pa_panel: PanelAppPanel = session.get(PanelAppPanel, idx)

    assert db_pa_panel == updated_pa_panel

    assert len(db_pa_panel.genes) == 3
    assert len(db_pa_panel.green_genes) == 2
    assert len(db_pa_panel.amber_genes) == 0
    assert len(db_pa_panel.red_genes) == 1

    assert db_pa_panel.red_genes[0].hgnc_id == new_gene.hgnc_id
    assert set(g.hgnc_id for g in db_pa_panel.green_genes) == {
        amber_gene.hgnc_id,
        green_gene.hgnc_id,
    }
