import { ArrowRightIcon } from '@heroicons/react/24/outline'
import { useState } from 'react'

import ConfirmationDialog from 'components/confirmationdialog'
import useGenePanel from 'components/genepanelcontext'
import { RadioButtons } from 'components/radiobuttons'
import { SemVer } from 'semver'

export default function ConfirmSubmit({ onConfirm, onClose }) {
  const { genePanelConfig } = useGenePanel()

  const [selectedVersion, setSelectedVersion] = useState('minor')

  const versionIncrements = {
    major: new SemVer(genePanelConfig.version).inc('major').format(),
    minor: new SemVer(genePanelConfig.version).inc('minor').format(),
  }

  const versionSelector = () => {
    const versionChange = (from, to) => {
      return (
        <div className="flex items-center">
          <p className="text-sm">{from}</p>
          <ArrowRightIcon className="mx-1 h-4 w-4"></ArrowRightIcon>
          <p className="text-sm">{to}</p>
        </div>
      )
    }
    if (versionIncrements.minor === versionIncrements.major) {
      return (
        <div className="text-gray-500">
          {versionChange(genePanelConfig.version, versionIncrements.minor)}
        </div>
      )
    } else {
      return (
        <RadioButtons
          buttons={[
            {
              key: 'major',
              label: versionChange(
                genePanelConfig.version,
                versionIncrements.major
              ),
            },
            {
              key: 'minor',
              label: versionChange(
                genePanelConfig.version,
                versionIncrements.minor
              ),
            },
          ]}
          onChange={(key) => setSelectedVersion(key)}
          direction="row"
          selectedButton={selectedVersion}
          disabled={false}
        />
      )
    }
  }

  return (
    <ConfirmationDialog
      onConfirm={() => {
        onConfirm(versionIncrements[selectedVersion])
      }}
      onClose={onClose}
    >
      Submit Gene Panel
      <div className="mt-2 text-sm font-normal text-gray-500">
        <p>
          Select version to submit for:
          <span className="italic">{` ${genePanelConfig.shortname}`}</span>
        </p>
        <p className="pt-3">
          <span className="font-bold">Full panel name:</span>
          {` ${genePanelConfig.name}`}
        </p>
        <div className="mt-4 justify-center">{versionSelector()}</div>
      </div>
    </ConfirmationDialog>
  )
}
