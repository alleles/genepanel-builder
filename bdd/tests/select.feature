Feature: Select genes and regions for a new gene panel

    As a user I should be able to create a new gene panel based on input of PanelApp panels, individual genes and regions.

    Background:
        Given initial state
        Given a browser
        When I visit "/"
        And I press "New Gene Panel"
        And I fill in "name" with "Test gene panel"
        And I fill in "short-name" with "testpanel"
        And I fill in "description" with "Test description"
        And I press "Continue (Select)"
        Then I should see "Select genes and regions"

    Scenario: Add or remove a PanelApp panel
        # Add a PanelApp panels with Green (default); check that search works
        When I fill in "search-panelapp-input" with "kl"
        Then I should see an element with xpath "//div[contains(text(), 'Kleine-Levin syndrome')]" within 15 seconds
        And I should see an element with xpath "//div[contains(text(), 'Stickler syndrome')]"

        When I press the element with xpath "//div[contains(text(), 'Kleine-Levin syndrome')]"
        And I press "panelapp-add"
        Then I should see "Kleine-Levin syndrome"
        ## Check that only Green are selected by default
        And I should see an element with xpath "//*[@name='213-includeGreen']"
        But I should not see an element with xpath "//*[@name='213-includeAmber']"
        And I should not see an element with xpath "//*[@name='213-includeRed']"

        ## Check that only Green are added
        When I press "Continue (Edit)"
        Then I should see "DNMT1" within 15 seconds
        And the origin for "DNMT1" should list "213" with confidence level "green"
        But I should not see "CSNK1D"
        And I should not see "AKR1C2"

        # Add Amber and Red
        When I press "Previous (Select)"
        Then I should see "Kleine-Levin syndrome" within 3 seconds
        When I wait for 1 seconds
        When I press the element with xpath "//*[@id='213-toggleAmber']//button"
        And I press the element with xpath "//*[@id='213-toggleRed']//button"
        And I press "Continue (Edit)"
        ## Check that Green, Amber and Red are added in the gene table
        Then I should see "DNMT1" within 15 seconds
        And the origin for "DNMT1" should list "213" with confidence level "green"
        And I should see "CSNK1D"
        And the origin for "CSNK1D" should list "213" with confidence level "amber"
        And I should see "AKR1C2"
        And the origin for "AKR1C2" should list "213" with confidence level "red"

        # Add another PanelApp panel
        When I press "Previous (Select)"
        Then I should see "Kleine-Levin syndrome" within 3 seconds

        When I add panel "Stickler syndrome"
        ## Remove Amber and Red from previous panel to reduce list
        And I press the element with xpath "//*[@id='213-toggleAmber']//button"
        And I press the element with xpath "//*[@id='213-toggleRed']//button"
        ## Check that both panels are added in the gene table
        And I press "Continue (Edit)"
        Then I should see "DNMT1" within 15 seconds
        And the origin for "DNMT1" should list "213"
        And I should see "COL11A1"
        And the origin for "COL11A1" should list "3"

        # Remove one of the panels
        When I press "Previous (Select)"
        Then I should see "Kleine-Levin syndrome" within 3 seconds
        When I wait for 1 seconds
        And I remove "Stickler syndrome"
        And I wait for 1 seconds
        Then I should not see "Stickler syndrome"

        When I press "Continue (Edit)"
        Then I should see "DNMT1" within 15 seconds
        But I should not see "COL11A1"

    Scenario: Add or remove a Gene
        # Add two genes
        ## Add first by gene symbol
        When I add gene "CDKN2A"
        Then I should see "HGNC:1787"
        ## Check that searching by HGNC ID works
        When I fill in "search-genes-input" with "11998"
        Then I should see an element with xpath "//div[contains(text(), 'TP53')]" within 15 seconds

        When I press the element with xpath "//div[contains(text(), 'TP53')]"
        And I press "gene-add"
        Then I should see "HGNC:11998"

        ## Check that both genes are added
        When I press "Continue (Edit)"
        Then I should see "CDKN2A" within 15 seconds
        And I should see "TP53"

        # Remove one of the genes
        When I wait for 1 seconds
        And I press "Previous (Select)"
        And I wait for 1 seconds
        And I remove "CDKN2A"
        And I press "Continue (Edit)"
        Then I should see "TP53" within 15 seconds
        But I should not see "CDKN2A"

        # Search by old gene symbol but when added check that only current one shows as main name
        When I wait for 1 seconds
        And I press "Previous (Select)"
        And I fill in "search-genes-input" with "CECR1"
        Then I should see an element with xpath "//div[contains(text(), 'ADA2')]" within 15 seconds

        When I press the element with xpath "//div[contains(text(), 'ADA2')]"
        And I press "gene-add"
        Then I should see "ADA2"
        And I should see "Previous: IDGFL, CECR1"

        ## Check that gene is added
        When I press "Continue (Edit)"
        Then I should see "ADA2" within 15 seconds

        # Add a list of genes
        When I wait for 1 seconds
        And I press "Previous (Select)"
        And I wait for 1 seconds
        And I add multiple genes "21497, HGNC:414"
        Then I should see "ACAD9" within 1 seconds
        And I should see "ALDOA" within 1 seconds

        ## Check that genes are added
        When I press "Continue (Edit)"
        Then I should see "ACAD9" within 15 seconds
        And I should see "ALDOA"

        ## Test invalid input
        When I wait for 1 seconds
        And I press "Previous (Select)"
        And I fill in "search-genes-listinput" with "2149734"
        And I press "Add List"
        Then I should see "Not all HGNC IDs found: 2149734" within 1 seconds

        When I fill in "search-genes-listinput" with "BRCA1"
        Then I should see "Invalid input. Only HGNC IDs (e.g. 1101 or HGNC:1101) separated with line breaks accepted." within 1 seconds

    Scenario: Add or remove a Region
        # Add two regions; second with name
        When I wait for 1 seconds
        And I add region "chr7:100000-101000"
        And I add region "chr7:200000-201000" with name "This is a region name"
        ## Check that both regions are added; second with name
        And I press "Continue (Edit)"
        Then I should see "Edit gene panel" within 1 seconds
        And I should see "7:100000-101000" within 15 seconds
        And I should see "7:200000-201000"
        And I should see an element with xpath "//textarea[@name='region-name-1'][contains(text(),'This is a region name')]"

        # Remove one of the regions
        When I press "Previous (Select)"
        When I wait for 1 seconds
        When I remove "chr7:200000-201000"
        ## Check that region is removed
        And I press "Continue (Edit)"
        Then I should see "Edit gene panel" within 1 seconds
        And I should see "7:100000-101000" within 15 seconds
        But I should not see "7:200000-201000"

        # Test invalid region format
        When I press "Previous (Select)"
        When I wait for 1 seconds
        And I fill in "region-input" with "chr23:100000-101000"
        Then I should see an element with xpath "//*[@id='region-add']/button[@disabled]"

    Scenario: Add mixed panel and gene
        # Add two single-gene panels and a separate gene
        ## Panel with COCH
        When I add panel "Familial Meniere Disease"
        ## Panel with SCN5A
        And I add panel "Brugada syndrome"
        ## Gene not in any of the panels
        And I add gene "FOXP2"
        ## Add a gene that is included in one of the gene panels, check that it's removed as manual entry on save
        And I add gene "COCH"
        And I press "Save"
        Then I should not see "COCH"

        # Check that all genes are listed with correct origin in gene table
        When I press "Continue (Edit)"
        Then I should see "SCN5A" within 15 seconds
        And the origin for "SCN5A" should list "13"
        And I should see "COCH"
        And the origin for "COCH" should list "394"
        And I should see "FOXP2"

        # Test that when adding the gene first with a change, then the panel, Origin and change is still present
        When I press "Previous (Select)"
        And I wait for 1 seconds
        ## Remove panel with COCH
        And I remove "Familial Meniere Disease"
        And I wait for 1 seconds
        ## Add COCH as manual entry
        And I add gene "COCH"
        And I press "Continue (Edit)"
        Then I should see "COCH" within 15 seconds

        ## Make a change to COCH
        When I fill in "comment-2180" with "Comment for COCH"
        ## Add panel with COCH
        And I press "Previous (Select)"
        And I wait for 1 seconds
        And I add panel "Familial Meniere Disease"
        ## Check that COCH is not present as manual entry
        And I press "Save"
        Then I should not see "COCH"

        ## Check that COCH is present with correct origin and change
        When I press "Continue (Edit)"
        Then I should see "COCH" within 15 seconds
        And the origin for "COCH" should list "394"
        ## Check that change is still present
        And field "comment-2180" should have the value "Comment for COCH"
