from datetime import datetime
from typing import List, Optional

from semver import VersionInfo
from sqlalchemy import PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import object_session, selectinload
from sqlalchemy.sql.expression import cast
from sqlalchemy.sql.schema import Column, Identity
from sqlmodel import (
    ARRAY,
    DateTime,
    Field,
    ForeignKey,
    Integer,
    Relationship,
    Session,
    Text,
    and_,
    any_,
    col,
    func,
    select,
)

from gpbuilder.datamodel import SemVer
from gpbuilder.datamodel.base import SQLModelBase
from gpbuilder.datamodel.crud.models import (
    ConfidenceLevel,
    GenePanelConfig,
    GenePanelConfigGene,
    GenePanelConfigRegion,
    PanelAppInput,
)
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel, PanelAppPanelGene
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript
from gpbuilder.datamodel.utils.records import RegionRecord, TranscriptRecord


class GenePanelGeneTranscript(SQLModelBase, table=True):
    genepanel_gene_id: int = Field(
        sa_column=Column(Integer, ForeignKey("genepanelgene.id", ondelete="CASCADE"))
    )
    transcript_id: int = Field(foreign_key="transcript.id")
    transcript: Transcript = Relationship(sa_relationship_kwargs={"viewonly": True})

    __table_args__ = (PrimaryKeyConstraint("genepanel_gene_id", "transcript_id"),)


class GenePanelGene(SQLModelBase, table=True):
    id: Optional[int] = Field(
        default=None, sa_column=Column(Integer, Identity(), primary_key=True)
    )
    genepanel_id: int = Field(
        sa_column=Column(Integer, ForeignKey("genepanel.id", ondelete="CASCADE"))
    )
    hgnc_id: int = Field(foreign_key="gene.hgnc_id")
    inheritance_mode: Optional[str] = Field(default=None)
    gene: "Gene" = Relationship(sa_relationship_kwargs={"viewonly": True})
    transcripts: List[Transcript] = Relationship(
        link_model=GenePanelGeneTranscript,
    )
    manually_added: bool = Field(default=False)
    comment: Optional[str] = Field(default=None)

    __table_args__ = (UniqueConstraint("hgnc_id", "genepanel_id"),)

    # To allow setting transcripts
    class Config:
        extra = "ignore"


class GenePanelExcludedGene(SQLModelBase, table=True):
    hgnc_id: int = Field(foreign_key="gene.hgnc_id")
    genepanel_id: int = Field(
        sa_column=Column(Integer, ForeignKey("genepanel.id", ondelete="CASCADE"))
    )
    exclusion_reasons: List[str] = Field(
        sa_column=Column(postgresql.ARRAY(Text())),
        default=[],
    )
    manually_added: bool = Field(default=False)
    comment: Optional[str]

    __table_args__ = (PrimaryKeyConstraint("hgnc_id", "genepanel_id"),)


class GenePanelPanelAppPanel(SQLModelBase, table=True):
    genepanel_id: int = Field(
        sa_column=Column(Integer, ForeignKey("genepanel.id", ondelete="CASCADE"))
    )
    pa_panel_id: int = Field(foreign_key="panelapppanel.id")
    confidence_levels: List[int] = Field(sa_column=Column(ARRAY(Integer)))

    __table_args__ = (PrimaryKeyConstraint("genepanel_id", "pa_panel_id"),)


class GenePanelRegion(SQLModelBase, table=True):
    genepanel_id: int = Field(
        sa_column=Column(Integer, ForeignKey("genepanel.id", ondelete="CASCADE"))
    )
    region_id: int = Field(foreign_key="region.id")
    region: Region = Relationship(sa_relationship_kwargs={"viewonly": True})
    inheritance_mode: Optional[str]
    name: str = Field(default="Unnamed region")
    comment: str = Field(default="")

    __table_args__ = (PrimaryKeyConstraint("region_id", "genepanel_id"),)


class GenePanel(SQLModelBase, table=True):
    id: Optional[int] = Field(
        default=None, sa_column=Column(Integer, Identity(), primary_key=True)
    )
    name: str
    shortname: str
    description: Optional[str]
    version: str = Field(sa_column=Column(SemVer, default="0.0.1-draft"))
    created: datetime = Field(
        default_factory=datetime.utcnow, sa_column=Column(DateTime(timezone=True))
    )
    updated: Optional[datetime] = Field(
        default=None, sa_column=Column(DateTime(timezone=True))
    )
    # TODO: should possibly have a previous id col for diff
    inactive: bool = Field(default=False)

    genes: List["Gene"] = Relationship(link_model=GenePanelGene)
    panelapp_panels: List["PanelAppPanel"] = Relationship(
        link_model=GenePanelPanelAppPanel
    )
    regions: List["Region"] = Relationship(link_model=GenePanelRegion)
    excluded_genes: List["Gene"] = Relationship(link_model=GenePanelExcludedGene)

    # TODO after #87
    masterpanel_id: Optional[int] = Field(foreign_key="genepanel.id", default=None)

    __table_args__ = (UniqueConstraint("shortname", "version"),)

    class Config:
        arbitrary_types_allowed = True

    @property
    def draft(self):
        return VersionInfo.parse(self.version).prerelease == "draft"

    def as_config(self):
        session = object_session(self)
        assert type(session) is Session
        panelapp_panels = session.exec(
            select(GenePanelPanelAppPanel).where(
                GenePanelPanelAppPanel.genepanel_id == self.id
            )
        ).all()
        excluded_genes = session.exec(
            select(GenePanelExcludedGene).where(
                GenePanelExcludedGene.genepanel_id == self.id
            )
        ).all()
        genes = session.exec(
            select(GenePanelGene).where(GenePanelGene.genepanel_id == self.id)
        ).all()

        regions = session.exec(
            select(GenePanelRegion).where(GenePanelRegion.genepanel_id == self.id)
        ).all()

        diff = {gene.hgnc_id: GenePanelConfigGene() for gene in genes + excluded_genes}
        for gene in excluded_genes:
            diff[gene.hgnc_id].exclusion_reasons = gene.exclusion_reasons
            diff[gene.hgnc_id].comment = gene.comment

        for gene in genes:
            diff[gene.hgnc_id].inheritance = gene.inheritance_mode
            diff[gene.hgnc_id].transcripts = [tx.name for tx in gene.transcripts]
            diff[gene.hgnc_id].comment = gene.comment

        return GenePanelConfig(
            id=self.id,
            name=self.name,
            shortname=self.shortname,
            description=self.description,
            version=self.version or "",
            hgnc_ids=[g.hgnc_id for g in genes + excluded_genes if g.manually_added],
            panelapp_panels=[
                PanelAppInput(
                    panelapp_id=panelapp_panel.pa_panel_id,
                    confidence_levels={
                        level: level in panelapp_panel.confidence_levels
                        for level in ConfidenceLevel
                    },
                )
                for panelapp_panel in panelapp_panels
            ],
            regions=[
                GenePanelConfigRegion(
                    id=region.region_id,
                    chromosome=region.region.chromosome,
                    start=region.region.start,
                    end=region.region.end,
                    ref_genome=region.region.ref_genome,
                    inheritance=region.inheritance_mode or "N/A",
                    name=region.name,
                    comment=region.comment,
                )
                for region in regions
            ],
            diff=diff,
        )

    def assemble_gene_info(self):
        # Get database GenePanelGene entry, if there
        session = object_session(self)
        assert type(session) is Session
        assert self.id is not None
        hgnc_ids = list(GenePanel.hgnc_ids_by_panel_id(session, [self.id])[self.id])
        gpgs = self.get_genepanel_genes_by_hgnc_ids(session)
        # Get default transcripts
        default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(
            session, hgnc_ids
        )
        # Get default inheritance modes
        default_inheritance_modes = Gene.get_default_inheritance_by_hgnc_ids(
            session, hgnc_ids
        )
        for hgnc_id in hgnc_ids:
            gpg = gpgs.get(hgnc_id)
            if gpg and gpg.transcripts and gpg.inheritance_mode:
                yield gpg

            if gpg and gpg.transcripts:
                transcripts = gpg.transcripts
            else:
                transcripts = default_transcripts[hgnc_id]

            if gpg and gpg.inheritance_mode:
                inheritance_mode = gpg.inheritance_mode
            else:
                inheritance_mode = default_inheritance_modes[hgnc_id]

            yield GenePanelGene(
                hgnc_id=hgnc_id,
                gene=gpg.gene if gpg else None,
                genepanel_id=self.id,  # type: ignore
                inheritance_mode=inheritance_mode,
                transcripts=transcripts,
            )

    def get_genepanel_genes_by_hgnc_ids(
        self, session: Session
    ) -> dict[int, GenePanelGene]:
        return {
            g.hgnc_id: g
            for g in session.exec(
                select(GenePanelGene)
                .options(
                    # Load gene and transcripts relationships, to avoid extra queries when accessing them
                    # Overhead here is minimal, overhead when accessing them without this is significant
                    # (possibly 2*N+1 queries for N GenePanelGenes)
                    selectinload(GenePanelGene.gene),
                    selectinload(GenePanelGene.transcripts),
                )
                .where(
                    GenePanelGene.genepanel_id == self.id,
                )
            )
            .unique()
            .all()
        }

    def prepare_for_export(
        self,
    ) -> tuple[set[RegionRecord], set[TranscriptRecord]]:
        """Assemble gene panel gene and region records for export"""
        session = object_session(self)
        assert type(session) is Session
        hgnc_ids = GenePanel.hgnc_ids_by_panel_id(session, [self.id]).get(self.id, [])  # type: ignore[arg-type]
        region_records: set[RegionRecord] = set()
        transcript_records: set[TranscriptRecord] = set()
        # Get transcripts statuses
        transcript_statuses = {
            transcript_id: status
            for (transcript_id, status) in session.exec(
                select(ManeSelectTranscript.transcript_id, ManeSelectTranscript.status)
            ).all()
        }

        # Get custom regions
        genepanel_regions = session.exec(
            select(GenePanelRegion)
            # Since we're always interested in the region, we can load it here
            .options(selectinload(GenePanelRegion.region))
            .where(GenePanelRegion.genepanel_id == self.id)
        ).all()
        for gpr in genepanel_regions:
            region_dict = gpr.region.dict()
            region_dict["name"] = gpr.name
            # Add custom region record to regions set
            region_records.add(RegionRecord(**region_dict))
            # Add custom region record to transcripts set [TODO: add metadata]
            transcript_records.add(
                TranscriptRecord(
                    **region_dict,
                    inheritance=gpr.inheritance_mode,
                    metadata="{}",
                    tags="Custom region",
                )
            )

        gpgs = {gpg.hgnc_id: gpg for gpg in self.assemble_gene_info()}
        for hgnc_id in hgnc_ids:
            # Get custom gene
            gpg = gpgs[hgnc_id]
            for gptx in gpg.transcripts:
                tx_regions = gptx.cds or gptx.exons
                tx_regions_names = gptx.cds_names or gptx.exon_names
                assert len(tx_regions) == len(tx_regions_names)
                # Add transcript regions records to regions set
                region_records.update(
                    RegionRecord(**dict({**r.dict(), "name": name}))
                    for (r, name) in zip(tx_regions, tx_regions_names)
                )
                tx_tags = transcript_statuses.get(gptx.id)

                # Add transcript record to transcripts set [TODO: add metadata]
                transcript_records.add(
                    TranscriptRecord(
                        **gptx.dict(),
                        hgnc_symbol=gptx.gene.symbol,  # type: ignore
                        inheritance=gpg.inheritance_mode,
                        metadata="{}",
                        tags=tx_tags,
                    )
                )

        return region_records, transcript_records

    @classmethod
    def hgnc_ids_by_panel_id(cls, session, panel_ids: list[int]):
        panelapp_panel_genes_statement = (
            select(cls.id, func.array_agg(col(PanelAppPanelGene.hgnc_id).distinct()))
            .join(
                GenePanelPanelAppPanel,
                GenePanelPanelAppPanel.genepanel_id == cls.id,
            )
            .join(
                PanelAppPanelGene,
                and_(
                    GenePanelPanelAppPanel.pa_panel_id
                    == PanelAppPanelGene.panelapp_panel_id,
                    PanelAppPanelGene.confidence_level
                    == any_(GenePanelPanelAppPanel.confidence_levels),
                ),
            )
            .where(cls.id.in_(panel_ids))
            .group_by(cls.id)
        )

        panelapp_panel_genes = {
            panel_id: hgnc_ids
            for panel_id, hgnc_ids in session.exec(panelapp_panel_genes_statement)
        }

        excluded_genes_statement = (
            select(
                cls.id, func.array_agg(col(GenePanelExcludedGene.hgnc_id).distinct())
            )
            .select_from(cls)
            .join(GenePanelExcludedGene, GenePanelExcludedGene.genepanel_id == cls.id)
            .where(cls.id.in_(panel_ids))
            .group_by(cls.id)
        )

        excluded_genes = {
            panel_id: hgnc_ids
            for panel_id, hgnc_ids in session.exec(excluded_genes_statement)
        }

        added_genes_statement = (
            select(cls.id, func.array_agg(col(GenePanelGene.hgnc_id).distinct()))
            .select_from(cls)
            .join(GenePanelGene, GenePanelGene.genepanel_id == cls.id)
            .where(cls.id.in_(panel_ids))
            .group_by(cls.id)
        )

        added_genes = {
            panel_id: hgnc_ids
            for panel_id, hgnc_ids in session.exec(added_genes_statement)
        }

        # Combine gene arrays to get all genes
        return {
            panel_id: set(
                added_genes.get(panel_id, []) + panelapp_panel_genes.get(panel_id, [])
            )
            - set(excluded_genes.get(panel_id, []))
            for panel_id in panel_ids
        }

    @classmethod
    def active_panel_ids(cls, session, include_drafts=False):
        query = select(cls.id).where(~cls.inactive)
        if not include_drafts:
            query = query.where(cast(cls.version, Text).not_like("%" + "-draft"))
        panel_ids = session.exec(query).all()
        return panel_ids

    @classmethod
    def all_panel_ids(cls, session):
        ids = session.exec(select(cls.id))
        return ids

    def __str__(self):
        return f"<{self.id}, {self.shortname}, {self.version}>"
