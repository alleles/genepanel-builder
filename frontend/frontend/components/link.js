import Link from 'next/link'

export default function GPLink({ href, children }) {
  return (
    <div className="flex">
      <Link href={href}>{children}</Link>
    </div>
  )
}
