import os
from tempfile import NamedTemporaryFile

import pytest

from gpbuilder.app import generate_mock_d4_files
from gpbuilder.coverage.mock_d4_file import MockD4File
from gpbuilder.datamodel.engine import create_db_and_tables, drop_all, get_session

create_db_and_tables()
generate_mock_d4_files()


class Index:
    def __init__(self):
        self.I = 0

    def __call__(self, n=1):
        I = self.I
        self.I += n
        yield from range(I + 1, I + n + 1)


unique_index = Index()


@pytest.fixture
def mock_data(mocker):
    def _dump(filename, status_code):
        abs_filename = os.path.join(os.path.dirname(__file__), filename)
        file_content = open(abs_filename, mode="r").read()
        mock = mocker.Mock("panelapp_api.panelapp_api.requests.Response")
        mock.url = "dummy.url"
        mock.text = file_content
        mock.status_code = status_code
        return mock

    return _dump


@pytest.fixture
def session():
    drop_all()
    create_db_and_tables()
    with get_session() as session:
        yield session


@pytest.fixture
def mock_d4_file():
    """
    Create a temporary D4 file that can be used to test the D4NumpyReader, and functions utilizing it
    (e.g. coverage for transcripts and regions)
    """
    with NamedTemporaryFile(suffix=".d4") as f:
        yield MockD4File(f.name)
