"""
Functions in this file can be used as validators on Pydantic/SQLModel classes.

Example:
class MyModel(SQLModel):
    chromosome: Chromosome

    @validator("chromosome")
    def validate_chromosome(cls, v):
        return chromosome_validator(v)
"""

from typing import Dict, TypedDict

from gpbuilder.datamodel.utils.types import Chromosome


class _Chromosome_value(TypedDict):
    name: Chromosome
    number: int


chromosome_conversion_table: Dict[int | str, _Chromosome_value] = {}

# add numeric codes
for k in range(1, 23):
    for s in (k, "%d" % k, "%02d" % k, "chr%d" % k, "chr%02d" % k):
        name: Chromosome = str(k)  # type: ignore
        chromosome_conversion_table[s] = {"name": name, "number": k}

# add non-autosome codes
for label in ("chr", "ch", "c", ""):
    chromosome_conversion_table.update(
        {
            label + "x": {"name": "X", "number": 23},
            label + "y": {"name": "Y", "number": 24},
            label + "xy": {"name": "XY", "number": 25},
            label + "m": {"name": "MT", "number": 26},
            label + "mt": {"name": "MT", "number": 26},
            label + "X": {"name": "X", "number": 23},
            label + "Y": {"name": "Y", "number": 24},
            label + "M": {"name": "MT", "number": 26},
            label + "MT": {"name": "MT", "number": 26},
        }
    )

# add NCBI's chromosome codes
chromosome_conversion_table.update(
    {
        "NC_000001.10": {"name": "1", "number": 1},
        "NC_000002.11": {"name": "2", "number": 2},
        "NC_000003.11": {"name": "3", "number": 3},
        "NC_000004.11": {"name": "4", "number": 4},
        "NC_000005.9": {"name": "5", "number": 5},
        "NC_000006.11": {"name": "6", "number": 6},
        "NC_000007.13": {"name": "7", "number": 7},
        "NC_000008.10": {"name": "8", "number": 8},
        "NC_000009.11": {"name": "9", "number": 9},
        "NC_000010.10": {"name": "10", "number": 10},
        "NC_000011.9": {"name": "11", "number": 11},
        "NC_000012.11": {"name": "12", "number": 12},
        "NC_000013.10": {"name": "13", "number": 13},
        "NC_000014.8": {"name": "14", "number": 14},
        "NC_000015.9": {"name": "15", "number": 15},
        "NC_000016.9": {"name": "16", "number": 16},
        "NC_000017.10": {"name": "17", "number": 17},
        "NC_000018.9": {"name": "18", "number": 18},
        "NC_000019.9": {"name": "19", "number": 19},
        "NC_000020.10": {"name": "20", "number": 20},
        "NC_000021.8": {"name": "21", "number": 21},
        "NC_000022.10": {"name": "22", "number": 22},
        "NC_000023.10": {"name": "X", "number": 23},
        "NC_000024.9": {"name": "Y", "number": 24},
        "NC_012920.1": {"name": "MT", "number": 26},
    }
)


def chromosome_validator(val: str) -> Chromosome:
    try:
        return chromosome_conversion_table[val]["name"]
    except KeyError:
        raise ValueError(f"Invalid chromosome name {val}")
