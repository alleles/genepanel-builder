from collections import defaultdict
from typing import ClassVar, Optional, Set

from pydantic import PositiveInt
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.sql.schema import Column
from sqlmodel import Field, Identity, Integer, Relationship, Session, Text, col, select

from gpbuilder.datamodel.base import SQLModelBase
from gpbuilder.datamodel.gene import Gene


class PhenotypeGene(SQLModelBase, table=True):
    """Phenotype genes, inheritance-mediated association"""

    id: Optional[int] = Field(
        default=None, sa_column=Column(Integer, Identity(), primary_key=True)
    )
    gene_id: int = Field(foreign_key="gene.hgnc_id")
    gene_mim_number: Optional[int] = Field(default=None)
    gene: Gene = Relationship()
    inheritance_descr: Optional[str]
    phenotype_id: int = Field(foreign_key="phenotype.mim_number")
    phenotype: "Phenotype" = Relationship()
    pubmed_id: Optional[PositiveInt] = Field(default=None)

    # ClassVar, not part of the table
    default_inheritance: ClassVar[str] = "AD/AR"
    default_x_inheritance: ClassVar[str] = "XD/XR"

    @property
    def gene_symbol(self):
        return self.gene.symbol

    @property
    def inheritance_mode(self):
        return self.__class__.get_inheritance_code(self.inheritance_descr)

    @property
    def phenotype_titles(self):
        return "; ".join(sorted(self.phenotype.titles))

    @staticmethod
    def get_inheritance_code(descr: str | None) -> str | None:
        if not descr:
            return None
        # TODO: add digenic, multifactorial, etc.
        match descr.lower():  # noqa: E999
            case (
                "x-linked"
                | "x-linked dominant"
                | "x-linked recessive"
                | "x-linked dominant; x-linked recessive"
                | "somatic mosaicism; x-linked"
                | "somatic mosaicism; x-linked dominant"
                | "somatic mosaicism; x-linked recessive"
                | "pseudoautosomal dominant"
                | "pseudoautosomal recessive"
                | "isolated cases; multifactorial; x-linked"
            ):
                return "XD/XR"
            case (
                "autosomal dominant"
                | "?autosomal dominant"
                | "autosomal dominant; digenic dominant"
                | "autosomal dominant; isolated cases"
                | "autosomal dominant; multifactorial"
                | "autosomal dominant; somatic mosaicism"
                | "autosomal dominant; somatic mutation"
                | "digenic dominant"
            ):
                return "AD"
            case (
                "autosomal recessive"
                | "autosomal recessive; digenic recessive"
                | "autosomal recessive; multifactorial"
                | "autosomal recessive; somatic mosaicism"
                | "digenic recessive"
            ):
                return "AR"
            case (
                "autosomal dominant; autosomal recessive"
                | "?autosomal dominant; autosomal recessive"
                | "autosomal dominant; autosomal recessive; digenic dominant"
                | "autosomal dominant; autosomal recessive; multifactorial"
                | "autosomal dominant; autosomal recessive; somatic mutation"
            ):
                return "AD/AR"
            case _:
                return None

    @staticmethod
    def get_inheritance_by_hgnc_ids(
        session: Session, hgnc_ids: list[int]
    ) -> dict[int, str]:
        """
        Get inheritance by HGNC id.

        Will return default inheritance if multiple inheritances are found for a HGNC id.
        """

        phenotype_genes = session.exec(
            select(PhenotypeGene).where(col(PhenotypeGene.gene_id).in_(hgnc_ids))
        ).all()

        all_inheritance_modes: dict[int, set[str]] = defaultdict(set)
        for phenotype_gene in phenotype_genes:
            if phenotype_gene.inheritance_mode:
                all_inheritance_modes[phenotype_gene.gene_id].add(
                    phenotype_gene.inheritance_mode
                )

        inheritance_modes: dict[int, str] = {}

        # Collate inheritance modes
        for hgnc_id, inheritance_mode in all_inheritance_modes.items():
            if len(inheritance_mode) > 1 and any(
                mode in ["AD", "AR", "AD/AR"] for mode in inheritance_mode
            ):
                inheritance_modes[hgnc_id] = PhenotypeGene.default_inheritance
            elif len(inheritance_mode) > 1 and all(
                mode in ["XD", "XR", "XD/XR"] for mode in inheritance_mode
            ):
                inheritance_modes[hgnc_id] = PhenotypeGene.default_x_inheritance
            else:
                assert len(inheritance_mode) == 1, str(inheritance_mode)
                inheritance_modes[hgnc_id] = inheritance_mode.pop()

        return inheritance_modes


class Phenotype(SQLModelBase, table=True):
    """Phenotype model (OMIM)"""

    mim_number: int = Field(sa_column=Column(Integer, Identity(), primary_key=True))
    titles: Set[str] = Field(sa_column=Column(ARRAY(Text())))
    update_id: int = Field(foreign_key="update.id")
