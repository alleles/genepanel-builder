from gpbuilder.api.coverage import get_region_coverage, get_transcript_coverage
from gpbuilder.conftest import MockD4File
from gpbuilder.datamodel.region import Region
from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.settings import _D4_READERS, _GENERAL_SETTINGS, D4Config


def test_get_region_coverage(mock_d4_file: MockD4File):
    regions = [
        Region(chromosome="1", start=100, end=200, ref_genome="GRCh37"),
        Region(chromosome="1", start=200, end=300, ref_genome="GRCh37"),
        Region(chromosome="1", start=300, end=400, ref_genome="GRCh37"),
        Region(chromosome="1", start=500, end=600, ref_genome="GRCh37"),
    ]
    _D4_READERS.pop("test", None)
    _GENERAL_SETTINGS.d4["test"] = D4Config(
        long_name="test", path=mock_d4_file.path, threshold=1
    )

    # 0.5 between 100 and 200
    mock_d4_file.add_data([0] * 50, "1", 100)
    mock_d4_file.add_data([1] * 50, "1", 150)

    # 0.0 between 200 and 300
    mock_d4_file.add_data([0] * 100, "1", 200)

    # 0.7 between 300 and 400
    mock_d4_file.add_data([0] * 30, "1", 300)
    mock_d4_file.add_data([1] * 70, "1", 330)

    # 0.3 between 500 and 600
    mock_d4_file.add_data([1] * 30, "1", 500)
    mock_d4_file.add_data([0] * 70, "1", 530)

    expected = [0.5, 0.0, 0.7, 0.3]
    result = get_region_coverage(regions, "test")
    assert result == expected


def test_get_transcript_exon_coverage(mock_d4_file: MockD4File):
    transcript = Transcript(
        ref_genome="GRCh37",
        chromosome="1",
        start=100,
        end=600,
        name="NM_00000.1",
        strand="+",
        source="test",
        hgnc_id=1,
        coding_start=150,
        coding_end=850,
        exon_starts=[100, 500, 800],
        exon_ends=[300, 600, 900],
    )

    exon_coverage = [0.3, 0.5, 0.7]

    global _D4_READERS
    _D4_READERS.pop("test", None)
    _GENERAL_SETTINGS.d4["test"] = D4Config(
        long_name="test", path=mock_d4_file.path, threshold=1
    )

    # Exons: 0.3 of first; 0.5 of second; 0.7 of third
    total_region_size_above_threshold = 0
    for coverage, exon_start, exon_end in zip(
        exon_coverage, transcript.exon_starts, transcript.exon_ends
    ):
        region_size_above_threshold = int(coverage * (exon_end - exon_start))
        total_region_size_above_threshold += region_size_above_threshold
        mock_d4_file.add_data([1] * region_size_above_threshold, "1", exon_start)

    total_exon_length = sum(
        ee - es for ee, es in zip(transcript.exon_ends, transcript.exon_starts)
    )
    expected_transcript_coverage = total_region_size_above_threshold / total_exon_length

    assert get_region_coverage(transcript.exons, "test") == exon_coverage

    _GENERAL_SETTINGS.coverage.use_cds_if_available = False
    _GENERAL_SETTINGS.coverage.slop = {"upstream": 0, "downstream": 0}
    assert (
        get_transcript_coverage([transcript], "test")[transcript.name]
        == expected_transcript_coverage
    )

    # Add slop upstream
    _GENERAL_SETTINGS.coverage.slop = {"upstream": 10, "downstream": 0}
    expected_transcript_coverage = total_region_size_above_threshold / (
        total_exon_length + 10 * len(transcript.exons)
    )
    assert (
        get_transcript_coverage([transcript], "test")[transcript.name]
        == expected_transcript_coverage
    )

    # Add slop downstream
    _GENERAL_SETTINGS.coverage.slop = {"upstream": 0, "downstream": 20}
    expected_transcript_coverage = total_region_size_above_threshold / (
        total_exon_length + 20 * len(transcript.exons)
    )
    assert (
        get_transcript_coverage([transcript], "test")[transcript.name]
        == expected_transcript_coverage
    )
