import logging
import re
import secrets
from operator import itemgetter

from authlib.integrations.starlette_client import OAuth
from fastapi import APIRouter, Depends, HTTPException, Response, status
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse

from gpbuilder.oauth import settings
from gpbuilder.oauth.store import SessionStoreSingleton

log = logging.getLogger(__name__)

router = APIRouter()

session_store = SessionStoreSingleton(filename=settings.SESSION_FILE)

oauth = OAuth()
oauth.register(
    "oidc_client",
    client_id=settings.OIDC_CLIENT_ID,
    client_secret=settings.OIDC_CLIENT_SECRET,
    server_metadata_url=settings.CONF_URL,
    client_kwargs={"scope": settings.SCOPE},
)


# For use as auth dependency injection on genepanel builder endpoints
async def authenticated(request: Request, response: Response):
    if settings.DEVELOPMENT or not settings.USE_OAUTH:
        return True

    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    response.delete_cookie("session")

    if not session_store.is_valid(session_id):
        session_store.expire(session_id)
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            headers={
                "Location": "/api/auth/login",
                "Set-Cookie": f"{settings.SESSION_ID_COOKIE}=''; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT",
            },
        )


@router.get("/login", tags=["Auth"])
async def login(request: Request):
    if settings.DEVELOPMENT or not settings.USE_OAUTH:
        return RedirectResponse("/")

    session_store.clear_expired()

    redirect_url = request.query_params.get(settings.REDIRECT_URL, "/")
    redirect_uri = request.url_for(
        "callback",
    )

    # workaround for unprivileged ports on dev
    if re.match(r"^http://genepanelbuilder.local", redirect_uri):
        redirect_uri = re.sub(r"local", "local:8080", redirect_uri)

    if re.match(r"^http://localhost", redirect_uri):
        redirect_uri = re.sub(r"localhost", "localhost:5000", redirect_uri)

    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    new_session = False
    current = {}

    if session_store.is_valid(session_id):
        current = session_store.fetch(session_id)
    else:
        new_session = True

    if not new_session and not current:
        response = RedirectResponse(redirect_url)
        response.delete_cookie(settings.SESSION_ID_COOKIE)
        return response

    current[settings.REDIRECT_URL] = redirect_url

    # Need to add a nonce to the request
    nonce = secrets.token_urlsafe(32)
    request.state.nonce = nonce

    response = await oauth.oidc_client.authorize_redirect(
        request,
        redirect_uri,
        nonce=nonce,
    )

    if new_session:
        session_id, session_expiry = session_store.create(current)

        response.set_cookie(
            key=settings.SESSION_ID_COOKIE,
            value=session_id,
            expires=session_expiry,
            max_age=session_expiry,
            secure=settings.SECURE_COOKIE,
            httponly=True,
        )
    else:
        session_store.store(session_id=session_id, data=current)

    return response


@router.get("/callback", tags=["Auth"])
async def callback(request: Request):
    if settings.DEVELOPMENT or not settings.USE_OAUTH:
        return {"message": "Not currently active"}

    session_store.clear_expired()

    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    current = session_store.fetch(session_id)
    redirect_url = current.get(settings.REDIRECT_URL, "/")

    try:
        token = await oauth.oidc_client.authorize_access_token(request)
        id_token = token[settings.ID_TOKEN]
        person_info = await oauth.oidc_client.userinfo(token=token)

    except Exception:
        log.error("Authentication failed. Trying to login again.")
        response = RedirectResponse("/")
        response.delete_cookie(key=settings.SESSION_ID_COOKIE)
        response.delete_cookie(key="session")
        return response

    current[settings.ID_TOKEN] = id_token
    current[settings.PERSON_INFO] = person_info
    session_store.store(session_id, current)

    response = RedirectResponse(redirect_url)
    response.delete_cookie(key="session")

    return response


@router.get("/verify", tags=["Auth"])
async def verify(request: Request, response: Response):
    if settings.DEVELOPMENT or not settings.USE_OAUTH:
        response.status_code = 200
        return response

    session_store.clear_expired()
    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    if not session_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    if not session_store.is_valid(session_id):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    else:
        response.status_code = 200
    return response


@router.get("/userinfo", tags=["Auth"])
async def info(request: Request, authenticated=Depends(authenticated)):
    if settings.DEVELOPMENT or not settings.USE_OAUTH:
        return JSONResponse(
            {"user": "xnn-testuser", "name": "Test User", "email": "test@example.org"}
        )

    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    if not session_store.is_valid(session_id):
        return JSONResponse(dict(status=settings.SESSION_EXPIRED), status_code=401)

    current = session_store.fetch(session_id)

    if settings.PERSON_INFO not in current:
        return JSONResponse(dict(status=settings.LOGIN_INCOMPLETE), status_code=401)

    person_info = session_store.fetch(session_id).get(settings.PERSON_INFO)

    user, name, email = itemgetter("user", "name", "email")(person_info)

    return JSONResponse({"user": user, "name": name, "email": email})


@router.get("/logout", tags=["Auth"])
async def logout(request: Request, response: Response):
    session_id = request.cookies.get(settings.SESSION_ID_COOKIE, "")

    if session_id:
        session_store.expire(session_id)

    response.status_code = 200
    response.delete_cookie(key="session")
    response.delete_cookie(key=settings.SESSION_ID_COOKIE)

    return response
