#!/usr/bin/env python
"Module API to retrieve gene names and synonyms from gpbuilder.genenames.org"

import argparse
import json
import logging
import re
from typing import List, Optional

import requests
from pydantic import BaseModel, validator

geneBaseUrl = "https://www.genenames.org/cgi-bin/download/custom"


class GenenamesTools(object):
    """Tools namespace class"""

    class GeneName(BaseModel):
        id: int
        approved_symbol: str
        synonyms: List[str]
        previous: List[str]
        withdrawn: bool
        chromosome: str
        new_id: Optional[int]

        @validator("synonyms", pre=True)
        def symbol_split(cls, v, values, **kwargs):
            return list(filter(lambda x: x, [w.strip() for w in v.split(",")]))

        @validator("previous", pre=True)
        def previous_split(cls, v):
            return list(filter(lambda x: x, [w.strip() for w in v.split(",")]))

        @validator("new_id", pre=True)
        def parse_new_id(cls, v):
            hgnc_search_pattern = re.compile(r"\[HGNC:(?P<new_id>[0-9]+)\]")
            hgnc_search_result = hgnc_search_pattern.search(v)
            new_hgnc_id = getattr(hgnc_search_result, "groupdict", lambda: {})().get(
                "new_id"
            )
            return new_hgnc_id

        @validator("withdrawn", pre=True)
        def check_withdrawn_status(cls, v):
            return v != "Approved"

    _log = logging.getLogger(__name__)

    @staticmethod
    def download_gene_synonyms(raw=False):
        """
        Downloads gene naming information, like HGNC ID and synonyms
        :return: if raw=True, the raw content of the request;
                 else, a dict hgnc_id: d, where d is a dict with keys 'approved_symbol', 'synonyms'
                 and 'previous', and the values of 'synonyms' and 'previous' are lists of symbols
        """

        _log = logging.getLogger(__name__)

        #         headers = {
        #             'Accept': 'application/json'
        #             }

        params = {
            "col": [
                "gd_hgnc_id",
                "gd_app_sym",
                "gd_app_name",
                "gd_status",
                "gd_aliases",
                "gd_prev_sym",
                "gd_pub_chrom_map",
            ],
            "order_by": "gd_app_sym_sort",
            "format": "text",
            "limit": "",
            "hgnc_dbtag": "off",
            "submit": "submit",
            "status": ["Approved", "Entry Withdrawn"],
        }

        response = requests.get(geneBaseUrl, params=params)
        _log.debug(
            "genenames.org response:\n"
            + "\n".join(
                ("{k}={v}".format(k=k, v=v) for (k, v) in response.__dict__.items())
            )
            + "\n=====================================================================\n"
        )

        if raw:
            r = response
            _log.debug(
                "unparsed content:\n"
                + "\n".join(dir(r) + [r.content.decode()])
                + "\n=========="
            )
        else:
            r = GenenamesTools.parse_content(response.content.decode())
            _log.debug(
                "parsed content:\n"
                + "\n".join("{k}={v}".format(k=k, v=r[k]) for k in r)
                + "\n=========="
            )

        return r

    @staticmethod
    def find_hgnc_ids(symbol, symbol_db):
        """Extract HGNC IDs for a gene symbol from a symbol database"""

        # search the approved symbols
        hgnc_ids = [k for k, v in symbol_db.items() if v["approved_symbol"] == symbol]
        if len(hgnc_ids) > 1:
            GenenamesTools._log.warning(
                "{n} HGNC IDs [{l}] found for '{s}'".format(
                    n=len(hgnc_ids), l=",".join(hgnc_ids), s=symbol
                )
            )
        if hgnc_ids:
            GenenamesTools._log.debug("returning HGNC IDs %s" % str(hgnc_ids))
            return sorted(hgnc_ids)

        GenenamesTools._log.warning(
            "no HGNC IDs found for '{s}': searching synonyms".format(s=symbol)
        )

        # otherwise search lists of synonyms..
        hgnc_ids_syn = [k for k, v in symbol_db.items() if symbol in v["synonyms"]]
        if len(hgnc_ids_syn) > 1:
            GenenamesTools._log.warning(
                "{n} genes [{l}] are also known as '{s}'".format(
                    n=len(hgnc_ids_syn), l=",".join(hgnc_ids_syn), s=symbol
                )
            )
        if hgnc_ids_syn:
            GenenamesTools._log.debug(
                "returning synonymoys HGNC IDs %s" % str(hgnc_ids_syn)
            )
            return sorted(hgnc_ids_syn)

        GenenamesTools._log.warning(
            "no genes known as '{s}': searching previous symbols".format(s=symbol)
        )

        # ..or lists of previous approved symbols
        hgnc_ids_prev = [k for k, v in symbol_db.items() if symbol in v["previous"]]
        if len(hgnc_ids_prev) > 1:
            GenenamesTools._log.warning(
                "{n} genes [{l}] were previously known as '{s}'".format(
                    n=len(hgnc_ids_prev), l=",".join(hgnc_ids_prev), s=symbol
                )
            )
        if hgnc_ids_prev:
            GenenamesTools._log.debug(
                "returning previous HGNC IDs %s" % str(hgnc_ids_prev)
            )
            return sorted(hgnc_ids_prev)

        GenenamesTools._log.warning("no genes are annotated to '{s}'".format(s=symbol))
        return []

    @staticmethod
    def find_correct_spellings(symbol, symbol_db):
        """Find correct names for a gene symbol from a symbol database"""
        hgnc_ids = GenenamesTools.find_hgnc_ids(symbol, symbol_db)
        if hgnc_ids:
            correct_symbols = [
                symbol_db[hgnc_id]["approved_symbol"] for hgnc_id in hgnc_ids
            ]
            if len(correct_symbols) > 1:
                GenenamesTools._log.debug(
                    "{n} possible names found for '{s}'".format(
                        n=len(correct_symbols), s=symbol
                    )
                )
            return sorted(correct_symbols)

        GenenamesTools._log.warning("passed '{s}' symbol as is.".format(s=symbol))
        return [symbol.upper()]

    @staticmethod
    def load_gene_names(filename=""):
        """Load gene information dictionary"""
        if filename:
            with open(filename, "r") as fh:
                gene_symbols = json.load(fh)
                for hgnc_id in gene_symbols.keys():
                    gene_symbols[hgnc_id]["synonyms"] = gene_symbols[hgnc_id].get(
                        "synonyms", []
                    )
                    gene_symbols[hgnc_id]["previous"] = gene_symbols[hgnc_id].get(
                        "previous", []
                    )
                return gene_symbols
        else:
            gene_symbols = GenenamesTools.download_gene_synonyms(raw=False)
            return gene_symbols

    @staticmethod
    def parse_content(content):
        """Parse response content"""

        _log = logging.getLogger(__name__)

        genenames_data = dict()
        lines = content.splitlines()
        header_line, text_body = lines[0], lines[1:]

        header_mapping = {
            "HGNC ID": "id",
            "Approved symbol": "approved_symbol",
            "Alias symbols": "synonyms",
            "Previous symbols": "previous",
            "Status": "withdrawn",
            "Approved name": "new_id",
            "Chromosome": "chromosome",
        }
        header = [header_mapping[k.strip()] for k in header_line.split("\t")]

        for record in text_body:
            if not record or len(record.strip()) < 1:
                continue
            raw_parts = [p.strip() for p in record.split("\t")]
            genename = GenenamesTools.GeneName(**dict(zip(header, raw_parts)))
            genenames_data[str(genename.id)] = genename.dict()
            genenames_data[str(genename.id)].pop("id")

            _log.debug(f"adding 'HGNC:{genename.id}' to gene dictionary")

        return genenames_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Download gene synonyms from " + geneBaseUrl
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--hgnc",
        action="store_true",
        default=False,
        help="Print HGNC ID and symbol only",
    )
    group.add_argument(
        "--json",
        action="store_true",
        default=False,
        help="Print search result in json format",
    )
    group.add_argument(
        "--raw",
        action="store_true",
        default=False,
        help="Print flat file with _all_ symbols",
    )
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        dest="verbosity",
        help="Increase verbosity [-v=INFO, -vv=DEBUG]",
    )
    parser.add_argument(
        "symbols",
        action="store",
        default=None,
        nargs="?",
        help="file with gene symbols (ignored if `--raw` output is requested)",
    )

    args = parser.parse_args()

    if args.verbosity > 1:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbosity > 0:
        logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    response = GenenamesTools.download_gene_synonyms(raw=True)
    response_parsed = GenenamesTools.parse_content(response.content.decode())
    response_parsed_eff = response_parsed

    symbol_dict = {k: v["approved_symbol"] for (k, v) in response_parsed.items()}

    if args.symbols:
        hgnc_set = set()
        response_parsed_eff = dict()
        symbol_dict = dict()
        with open(args.symbols, "r") as fh:
            for s in fh:
                hgnc_set = sorted(
                    set(GenenamesTools.find_hgnc_ids(s.strip(), response_parsed))
                )
                if len(hgnc_set) > 1:
                    log.warning("reporting latest HGNC ID only")
                response_parsed_eff.update(
                    {k: v for (k, v) in response_parsed.items() if k in hgnc_set}
                )
                symbol_dict.update({s.strip(): k for k in hgnc_set})

    if args.raw:
        print(response.content)
    if args.json:
        print(json.dumps(response_parsed_eff, indent=4))
    if args.hgnc:
        for s, k in symbol_dict.items():
            print("%s\t%s" % (s, k))
