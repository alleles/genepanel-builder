import hashlib
import logging
import secrets
from datetime import datetime

from itsdangerous import TimestampSigner
from itsdangerous.exc import SignatureExpired
from sqlitedict import SqliteDict

from gpbuilder.oauth import settings

log = logging.getLogger(__name__)

SESSION_TTL = 60 * 60 * 6  # 6 hours


class SessionStoreSingleton:
    filename: str | None = None

    def __new__(cls, filename):
        if not hasattr(cls, "instance"):
            cls.instance = super(SessionStoreSingleton, cls).__new__(cls)
        return cls.instance

    def __init__(self, filename: str):
        if not self.filename:
            self.filename = filename
        self._store = SqliteDict(self.filename, outer_stack=True, autocommit=True)
        self._signer = TimestampSigner(
            secret_key=settings.SECRET_KEY,
            salt=settings.SALT,
            key_derivation="hmac",
            digest_method=hashlib.sha256,
        )

    def _sign(self, session_id: str):
        return (
            self._signer.sign(session_id).decode("utf-8"),
            datetime.utcfromtimestamp(
                self._signer.get_timestamp() + SESSION_TTL
            ).strftime("%a, %d %b %Y %H:%M:%S GMT"),
        )

    def _unsign(self, session_id: bytes):
        return self._signer.unsign(session_id, max_age=SESSION_TTL).decode("utf-8")

    def _validate(self, session_id: str):
        try:
            return self._signer.validate(
                bytes(session_id, "utf-8"), max_age=SESSION_TTL
            )
        except SignatureExpired as e:
            log.error(str(e))

    def expire(self, session_id: str):
        try:
            del self._store[session_id]
            log.debug({"expired_session": session_id})
        except KeyError as e:
            log.error(str(e))
        except RuntimeError as e:
            log.error(str(e))

    def clear_expired(self):
        for session_id, session in self._store.items():
            if not self._validate(session_id):
                self.expire(session_id)

    def fetch(self, session_id: str):
        return self._store.get(session_id, None)

    def create(self, data: dict):
        session_id, expiry = self._sign(secrets.token_urlsafe(32))
        try:
            self._store[session_id] = data
            log.debug({"created_session": session_id})
            return session_id, expiry
        except RuntimeError as e:
            log.error(str(e))
            return None, None

    def store(self, session_id: str, data: dict):
        if self._validate(session_id):
            self._store[session_id] = data
        else:
            raise ValueError("Invalid session id")

    def is_valid(self, session_id: str):
        if not session_id:
            return False
        session = self.fetch(session_id)
        if not session or not session.get(settings.ID_TOKEN):
            return False
        return self._validate(session_id)
