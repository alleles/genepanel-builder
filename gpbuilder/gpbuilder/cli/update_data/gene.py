import logging
from typing import Iterator

from sqlmodel import Session

from gpbuilder.cli.update_data.common import set_next_update_id
from gpbuilder.datamodel.gene import Gene
from gpbuilder.genenames_api.genenames_api import GenenamesTools

logger = logging.getLogger(__name__)


def fetch_genes() -> Iterator[Gene]:
    logger.info("Downloading gene symbols from genenames.org..")
    symbols_dict = GenenamesTools.download_gene_synonyms()
    for hgnc_id, values in symbols_dict.items():  # type: ignore
        gene_dict = dict(
            hgnc_id=hgnc_id,
            symbol=values["approved_symbol"],
            aliases=values["synonyms"],
            previous_symbols=values["previous"],
            withdrawn=values["withdrawn"],
            chromosome=values["chromosome"],
            new_hgnc_id=values["new_id"],
        )
        yield Gene(**gene_dict)


def insert_gene(session: Session, gene: Gene, defer=False) -> Gene:
    if defer:
        gene_dict = gene.dict(exclude={"new_hgnc_id"})
    else:
        gene_dict = gene.dict()

    db_gene = session.get(Gene, gene.hgnc_id)

    if db_gene is None:
        session.add(Gene(**gene_dict))
    else:
        [setattr(db_gene, k, v) for k, v in gene_dict.items()]
    session.flush()
    return db_gene if db_gene else gene


def populate_gene_table(session: Session):
    deferred = []
    update_id = set_next_update_id("gene", session)
    for gene in fetch_genes():
        gene.update_id = update_id
        if gene.new_hgnc_id:
            deferred.append(gene)
        insert_gene(session, gene, defer=True)
    # Insert genes with new_hgnc_id (this will overwrite the previous insert)
    for gene in deferred:
        insert_gene(session, gene, defer=False)
