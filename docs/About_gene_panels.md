# About gene panels

A gene panel is fundamentally a set of genes that should be examined for a specific genetic condition or _phenotype_. In addition to specifying exact genomic regions to be investigated, a gene panel can also pass on parameters important for variant interpretation, including expected mode of inheritance and default transcripts for each gene. A gene panel may also include genomic regions not typically considered part of a gene, such as enhancer or suppressor regions.

By restricting analyses to relevant genes and regions, gene panels ensure consistent analyses for patients both within and across different patient groups and significantly reduce the number of variants that need to be interpreted. Avoiding analyses of genes unlikely to be involved in a given disease also increases the a priori likelihood of positive findings, while minimizing the risk of incidental findings.

## Included genes

When creating a gene panel, a medically trained person typically starts off with one or more related conditions, which are then used to find a set of genes through known gene-phenotype relationships. These relationships are described in medical research literature and public data sources such as OMIM and supplemented by local knowledge. All of these sources are constantly evolving and do not always agree, so it can be desirable to adopt crowd-sourcing solutions such as PanelApp, including a ranking system and expert reviewers. However, as not all possible conditions and phenotypes are maintained by these crowd-sourcing solutions, it is sometimes necessary to create new or supplement existing panels with locally curated genes and regions.

## Default transcripts

Most genes have multiple alternative transcripts, "mixing and matching" copies of different parts (exons) of the genomic gene sequence through alternative splicing in the final transcripts. These vary in expression in different tissues and can therefore also have varying significance for different conditions. A particular genetic variant can also have very different consequences across alternative transcripts, such as residing in the protein-coding region in one transcript but in a non-coding or non-transcribed region of another. In this example, the reported variant name (traditionally in terms of transcript reference) will also differ.

Therefore, the choice of transcript can have considerable implications. To ensure consistent investigation and reporting within and across labs, one (or, in rare cases, two) of the alternative transcripts must be chosen as the default transcript for the gene. Traditionally, the transcript covering the longest stretches of a gene or protein-coding sequence (and therefore including most clinically relevant variants in the gene) was chosen. However, in later years this "brute-force" approach has been replaced by more nuanced methods, such as the expert curation performed in the MANE Select project, where a transcript's biological relevance for disease plays a more prominent role. An advantage with the MANE Select dataset is also that it ensures consistency between the transcript reference sequences reported by U.S. (NCBI) and European (Ensembl) entities, as well as the genomic reference sequence.

## Mode of inheritance

The mode of inheritance is the manner in which a genetic disorder is passed from one generation to the next. Importantly, dominant versus recessive modes signify whether one or both copies of a gene, respectively, must be affected to cause a disorder. This in turn has importance for the interpretation of variants on several levels:

- A single, heterozygous variant cannot alone cause disease in a recessive gene; thus, either the variant must be found in a homozygous/hemizygous state, or in combination with another pathogenic variant (compound heterozygous).
- The expected population frequency for benign variants is much lower for dominant genes than for recessive ones, allowing lower frequency cut-offs and more efficient variant filtering.
- Follow-up of family members when a pathogenic variant is found is very different for recessive and dominant diseases.

At any rate, the mode of inheritance for a given gene is not always clear-cut and may differ depending on the particular disease context. OMIM and PanelApp are both sources for this information, but the data quality varies. In such, unfortunately quite common cases, the mode of inheritance is set as indeterminate due to uncertainty. To maximize the efficiency and precision of analyses in relevant gene panels, it is therefore often necessary to perform a manual curation depending on the condition of interest.
