from pydantic import BaseModel, Field, validator

from gpbuilder.datamodel.utils.validators import chromosome_conversion_table


class RegionRecord(BaseModel):
    chromosome: str | None = Field(default=None, alias="#chromosome")
    start: int | None = Field(default=None, alias="read start")
    end: int | None = Field(default=None, alias="read end")
    name: str | None = Field(default=None, alias="name")
    score: float = Field(default=0.0, alias="score")
    strand: str = Field(default="+", alias="strand")

    @property
    def _chromosome_number(self):
        return chromosome_conversion_table.get(self.chromosome).get("number")

    @property
    def _sorter_tuple(self):
        return (self._chromosome_number, *(dict(self).values()))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return hash(self) == hash(other)
        return False

    def __hash__(self):
        return hash(repr(self))

    def __lt__(self, other):
        return self._sorter_tuple < other._sorter_tuple

    def __repr__(self):
        return "\t".join(str(v) for v in dict(self).values())

    def __str__(self):
        return repr(self)

    class Config:
        allow_population_by_field_name = True


class TranscriptRecord(BaseModel):
    chromosome: str = Field(alias="#chromosome")
    start: int = Field(alias="read start")
    end: int = Field(alias="read end")
    name: str = Field(alias="name")
    score: float = Field(default=0.0, alias="score")
    strand: str = Field(default="+", alias="strand")
    tags: str | None = Field(default=None, alias="tags")
    hgnc_id: int | None = Field(default=None, alias="HGNC id")
    hgnc_symbol: str | None = Field(default=None, alias="HGNC symbol")
    inheritance: str | None = Field(alias="inheritance")
    coding_start: int | None = Field(default=None, alias="coding start")
    coding_end: int | None = Field(default=None, alias="coding end")
    exon_starts: str | None = Field(default=None, alias="exon starts")
    exon_ends: str | None = Field(default=None, alias="exon ends")
    metadata: str | None = Field(default=None, alias="metadata")

    @property
    def _chromosome_number(self):
        return chromosome_conversion_table.get(self.chromosome).get("number")

    @property
    def _sorter_tuple(self):
        return (
            self._chromosome_number,
            self.start,
            self.end,
            *(dict(self).values()),
        )

    @validator("*", pre=True)
    def _empty_string_to_none(cls, v):
        if v == "":
            return None
        return v

    @validator("exon_starts", "exon_ends", "tags", pre=True)
    def _validate_list_and_turn_it_to_csv(cls, v):
        if isinstance(v, list):
            return ",".join(map(str, v))
        return v

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return hash(self) == hash(other)
        return False

    def __hash__(self):
        return hash(repr(self))

    def __lt__(self, other):
        return self._sorter_tuple < other._sorter_tuple

    def __repr__(self):
        return "\t".join(str(v) for v in dict(self).values())

    def __str__(self):
        return repr(self)

    class Config:
        allow_population_by_field_name = True
