from typing import TYPE_CHECKING, List, Optional

from sqlalchemy.dialects import postgresql
from sqlmodel import Column, Field, Integer, Session, Text, col, select

from gpbuilder.datamodel.base import SQLModelBase

if TYPE_CHECKING:
    # Need to be imported here to avoid cyclic imports
    # Will only run when type checking (not at runtime)
    from gpbuilder.datamodel.transcript import Transcript


class Gene(SQLModelBase, table=True):
    hgnc_id: int = Field(
        unique=True, sa_column=Column(Integer, primary_key=True, autoincrement=False)
    )
    symbol: str
    aliases: List[str] = Field(
        default_factory=list, sa_column=Column(postgresql.ARRAY(Text()))
    )
    previous_symbols: List[str] = Field(
        default_factory=list, sa_column=Column(postgresql.ARRAY(Text()))
    )
    withdrawn: bool = Field(default=False)
    chromosome: str = Field(default="")
    new_hgnc_id: Optional[int] = Field(default=None, foreign_key="gene.hgnc_id")
    update_id: Optional[int] = Field(default=None, foreign_key="update.id")

    @classmethod
    def from_hgnc_ids(
        cls, session: Session, hgnc_ids: list[int], ensure_all=True
    ) -> list["Gene"]:
        res = session.query(cls).filter(col(cls.hgnc_id).in_(hgnc_ids)).all()
        if ensure_all and len(res) != len(set(hgnc_ids)):
            raise ValueError(
                f"Not all HGNC IDs found: {', '.join(map(str, set(hgnc_ids) - set([g.hgnc_id for g in res])))}"
            )
        return res

    @classmethod
    def symbol_by_hgnc_ids(
        cls, session: Session, include_withdrawn: bool = False
    ) -> dict[int, str]:
        return {
            hgnc_id: symbol
            for hgnc_id, symbol in session.exec(
                select(cls.hgnc_id, cls.symbol).where(
                    col(cls.withdrawn).is_(False) if not include_withdrawn else True
                )
            ).all()
        }

    @staticmethod
    def get_default_transcripts_by_hgnc_ids(
        session: Session, hgnc_ids: list[int]
    ) -> dict[int, list["Transcript"]]:
        """
        Get the default transcripts for each gene
        Order of precedence:
        1. Gene defaults (if use_gene_defaults is True)
        2. MANE Select
        3. Longest transcript
        4. Fallback (empty list)
        """

        # Need to be imported here to avoid cyclic imports
        from gpbuilder.datamodel.genedefaults import GeneDefaults
        from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript

        # Note: we use a set to keep track of which HGNC IDs have been processed,
        # to avoid time-consuming queries for HGNC IDs that have already been processed.
        remaining_hgnc_ids = set(hgnc_ids)
        gene_default_transcripts = GeneDefaults.get_transcripts_by_hgnc_ids(
            session, list(remaining_hgnc_ids)
        )

        remaining_hgnc_ids -= set(gene_default_transcripts.keys())
        mane_select_transcripts = ManeSelectTranscript.get_transcripts_by_hgnc_ids(
            session, list(remaining_hgnc_ids)
        )
        remaining_hgnc_ids -= set(mane_select_transcripts.keys())
        longest_transcripts = Transcript.get_longest_transcripts_by_hgnc_ids(
            session, list(remaining_hgnc_ids)
        )
        remaining_hgnc_ids -= set(longest_transcripts.keys())
        fallback_transcripts = {hgnc_id: [] for hgnc_id in remaining_hgnc_ids}

        return {
            **fallback_transcripts,
            **longest_transcripts,
            **mane_select_transcripts,
            **gene_default_transcripts,
        }

    @staticmethod
    def get_chromosomes_by_hgnc_ids(
        session: Session, hgnc_ids: list[int]
    ) -> dict[int, str]:
        return {
            hgnc_id: chromosome
            for hgnc_id, chromosome in session.query(Gene.hgnc_id, Gene.chromosome)
            .filter(Gene.hgnc_id.in_(hgnc_ids))
            .all()
        }

    @staticmethod
    def get_default_inheritance_by_hgnc_ids(
        session: Session, hgnc_ids: list[int]
    ) -> dict[int, str]:
        """
        Get the default inheritance for each gene
        Order of precedence:
        1. Gene defaults (if use_gene_defaults is True)
        2. Phenotype genes
        3. Fallback ("N/A")
        """
        # Need to be imported here to avoid cyclic imports
        from gpbuilder.datamodel.genedefaults import GeneDefaults
        from gpbuilder.datamodel.phenotype import PhenotypeGene

        # Note: we use a set to keep track of which HGNC IDs have been processed,
        # to avoid time-consuming queries for HGNC IDs that have already been processed.
        remaining_hgnc_ids = set(hgnc_ids)
        gene_defaults_inheritance = GeneDefaults.get_inheritance_by_hgnc_ids(
            session, list(remaining_hgnc_ids)
        )

        remaining_hgnc_ids -= set(gene_defaults_inheritance.keys())
        phenotype_genes_inheritance = PhenotypeGene.get_inheritance_by_hgnc_ids(
            session, list(remaining_hgnc_ids)
        )
        remaining_hgnc_ids -= set(phenotype_genes_inheritance.keys())

        fallback_inheritance = {}

        for hgnc_id in remaining_hgnc_ids:
            fallback_inheritance[hgnc_id] = "N/A"

        return {
            **fallback_inheritance,
            **phenotype_genes_inheritance,
            **gene_defaults_inheritance,
        }

    @staticmethod
    def get_approved_gene_by_hgnc_id(
        session: Session, hgnc_id: int
    ) -> Optional["Gene"]:
        def recursive_get_gene(hgnc_id) -> Gene | None:
            if hgnc_id is None:
                return None
            gene = session.get(Gene, hgnc_id)
            if gene is None:
                return None
            elif not gene.withdrawn:
                return gene
            elif gene.withdrawn and gene.new_hgnc_id:
                return recursive_get_gene(gene.new_hgnc_id)

        return recursive_get_gene(hgnc_id)

    @classmethod
    def get_approved_gene_by_symbol(
        cls, session: Session, symbol: str
    ) -> Optional["Gene"]:
        gene = session.exec(select(Gene).where(col(Gene.symbol) == symbol)).all()
        if len(gene) == 0:
            return None
        elif len(gene) > 1:
            raise ValueError(f"Multiple genes found for symbol {symbol}")
        elif not gene[0].withdrawn:
            return gene[0]
        else:
            return Gene.get_approved_gene_by_hgnc_id(session, gene[0].hgnc_id)
