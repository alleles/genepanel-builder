-- Testing:

-- Do testing on some hgnc_ids:
-- hgnc_id 1135 has new_hgnc_id = None
-- hgnc_id 1134 has new_hgnc_id = 1135
-- hgnc_id 1136 has new_hgnc_id = None

-- Check the relevant gene records
select * from gene where hgnc_id in (1134,1135,1136);

-- Check how the relevant gene records are used as foreign keys
SELECT g.hgnc_id as gene_hgnc_id,
	   g.symbol,
	   g.withdrawn,
	   g.previous_symbols,
	   g.new_hgnc_id as gene_new_hgnc_id,
	   t.hgnc_id as transcript_hgnc_id,
	   p2.gene_id as penotype_gene_id,
	   p1.hgnc_id as panelappgene_hgnc_id,
	   g3.hgnc_id as genepanelgene_hgnc_id,
	   g2.hgnc_id as excluded_hgnc_id
	   from gene as g
left join transcript as t on t.hgnc_id = g.hgnc_id 
left join genepanelexcludedgene g2 on g2.hgnc_id = g.hgnc_id
left join genepanelgene g3 on g3.hgnc_id = g.hgnc_id
left join panelapppanelgene p1 on p1.hgnc_id = g.hgnc_id
left join phenotypegene p2 on p2.gene_id = g.hgnc_id
where g.hgnc_id in (1134, 1135, 1136);

-- Run 'trig_update_hgnc_ids' when new_hgnc_id is inserted or updated
-- Insert new_hgnc_id = 1136 for hgnc_id=1135
-- Update new_hgnc_id = 1136 for hgnc_id=1134
UPDATE gene SET new_hgnc_id = 1136 where hgnc_id = 1135 or new_hgnc_id = 1135;

-- Check the relevant gene records and foreign keys and compare to the status before the update