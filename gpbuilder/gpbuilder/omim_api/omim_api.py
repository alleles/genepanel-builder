import logging
import os
from typing import Iterator

import requests
from pydantic import BaseModel, validator

logger = logging.getLogger(__name__)


class PhenotypeMap(BaseModel):
    phenotype: str
    phenotypeInheritance: str | None
    phenotypeMimNumber: int | None

    class Config:
        extra = "ignore"


class OmimGeneMap(BaseModel):
    mimNumber: int
    approvedGeneSymbols: str | None
    phenotypeMapList: list[PhenotypeMap] = []

    @validator("phenotypeMapList", pre=True)
    def unnest_phenotype_map_list(cls, v):
        return [item["phenotypeMap"] for item in v]

    class Config:
        extra = "ignore"


class SearchParams(BaseModel):
    apiKey: str
    sequenceID: int
    limit: int
    format: str = "json"


class OmimApi(object):
    API_KEY = os.getenv("OMIM_API_KEY", "")

    class RequestException(Exception):
        """Dummy request exception class"""

    @staticmethod
    def get_data(start=1, batch_size=100) -> Iterator[OmimGeneMap]:
        """Get data from OMIM API"""
        if not OmimApi.API_KEY:
            logger.error("No API key provided, yielding from empty list")
            return  # yield from []

        base_url = "https://api.omim.org/api/geneMap"
        params = SearchParams(apiKey=OmimApi.API_KEY, sequenceID=start, limit=0)
        response = requests.get(base_url, params=params.dict())

        if response.status_code != 200:
            raise OmimApi.RequestException("Request failed")
        total_count = response.json()["omim"]["listResponse"]["totalResults"]
        params.limit = batch_size
        while params.sequenceID <= total_count:
            response = requests.get(
                base_url, params=params.dict(), headers={"Accept-Encoding": "gzip"}
            )
            response_objects = [
                OmimGeneMap(**obj["geneMap"])
                for obj in response.json()["omim"]["listResponse"]["geneMapList"]
            ]
            yield from response_objects
            params.sequenceID += params.limit


if __name__ == "__main__":
    api = OmimApi()

    for omim_gene_map in api.get_data():
        if omim_gene_map.phenotypeMapList:
            print(omim_gene_map)
