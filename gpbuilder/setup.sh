#!/usr/bin/env bash
#Setup script to give python access to the panel builder's modules
#NOTE: BASH_SOURCE is only available in Bash 3.0 or higher

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

NEW_PYTHONPATH="${SOURCE_DIR}/gpbuilder"
if [[ -n "${PYTHONPATH-}" ]]; then
    NEW_PYTHONPATH="${NEW_PYTHONPATH}:${PYTHONPATH}";
fi

PYTHONPATH="${NEW_PYTHONPATH}"
export PYTHONPATH
