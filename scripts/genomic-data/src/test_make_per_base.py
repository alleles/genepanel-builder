from pathlib import Path

from make_per_base import has_valid_suffix


def test_file_has_valid_suffix_valid_input():
    file_path = Path("valid_file.bam")
    ending = [".bam"]
    assert has_valid_suffix(file_path, ending) is True


def test_file_has_valid_suffix_invalid_input():
    file_path = Path("invalid_file.txt")
    ending = [".cram", ".bam"]
    assert has_valid_suffix(file_path, ending) is False
