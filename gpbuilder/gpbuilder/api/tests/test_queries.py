import pytest
from sqlalchemy.exc import DataError as SQLAlchemyDataError

from gpbuilder.api.queries import (
    get_genepanel_by_id,
    get_genepanels,
    get_genepanels_containing_gene,
    get_genepanels_explicitely_containing_gene,
    get_genepanels_implicitely_containing_gene,
    get_panel_app_panels_including_gene,
    get_panelapp_genes,
)
from gpbuilder.api.tests.mock_data import (
    create_genes,
    create_panelapp_panels,
    create_panels,
    link_genepanel_excluded_gene,
    link_genepanel_gene,
    link_genepanel_panelapp_panel,
    link_panelapp_panel_gene,
)


def test_get_genepanels(session):
    panels = create_panels(2, session)
    session.commit()

    statement = get_genepanels()
    result = session.exec(statement).all()
    assert len(result) == 2

    assert result[0].id == 1
    assert result[0].name == panels[0].name
    assert result[0].description == panels[0].description
    assert result[0].version == panels[0].version
    assert result[0].updated is None

    assert result[1].id == 2
    assert result[1].name == panels[1].name
    assert result[1].description == panels[1].description
    assert result[1].version == panels[1].version
    assert result[1].updated is None


def test_get_genepanel_by_id_not_found(session):
    statement = get_genepanel_by_id(1)
    result = session.exec(statement).all()
    assert len(result) == 0


def test_get_genepanel_by_id_invalid_id(session):
    statement = get_genepanel_by_id("invalid")  # type: ignore
    with pytest.raises(SQLAlchemyDataError):
        session.exec(statement).all()


@pytest.mark.parametrize("input_id", [1, "1", 1.000])
def test_get_genepanel_by_id(session, input_id):
    create_panels(1, session)
    session.commit()
    statement = get_genepanel_by_id(input_id)
    result = session.exec(statement).all()
    assert len(result) == 1
    assert result[0].id == 1


def test_get_genes_by_panelapppanel(session):
    panelapp_panels = create_panelapp_panels(2, session)
    genes = create_genes(2, session)
    session.flush()
    link_panelapp_panel_gene(genes, panelapp_panels[0], 1, session)
    session.commit()

    statement = get_panelapp_genes([panelapp_panels[0].id])
    result = session.exec(statement).all()

    assert len(result) == 2
    assert result[0].hgnc_id == genes[0].hgnc_id
    assert result[0].symbol == genes[0].symbol
    assert result[1].hgnc_id == genes[1].hgnc_id
    assert result[1].symbol == genes[1].symbol

    statement = get_panelapp_genes([panelapp_panels[1].id])
    assert len(session.exec(statement).all()) == 0


def test_get_genes_by_multiple_panelapppanels(session):
    panelapp_panels = create_panelapp_panels(2, session)
    genes = create_genes(5, session)
    session.flush()
    link_panelapp_panel_gene(genes[:2], panelapp_panels[0], 1, session)
    link_panelapp_panel_gene(genes[2:], panelapp_panels[1], 1, session)
    session.commit()

    statement = get_panelapp_genes([panelapp_panels[0].id])
    result = session.exec(statement).all()

    assert len(result) == 2
    assert result[0].hgnc_id == genes[0].hgnc_id
    assert result[0].symbol == genes[0].symbol
    assert result[1].hgnc_id == genes[1].hgnc_id
    assert result[1].symbol == genes[1].symbol

    statement = get_panelapp_genes([panelapp_panels[1].id])
    result = session.exec(statement).all()

    assert len(result) == 3
    assert result[0].hgnc_id == genes[2].hgnc_id
    assert result[0].symbol == genes[2].symbol
    assert result[1].hgnc_id == genes[3].hgnc_id
    assert result[1].symbol == genes[3].symbol
    assert result[2].hgnc_id == genes[4].hgnc_id
    assert result[2].symbol == genes[4].symbol


def test_get_genes_by_panelapppanel_invalid_id(session):
    statement = get_panelapp_genes(["invalid_id"])  # type: ignore
    with pytest.raises(SQLAlchemyDataError):
        session.exec(statement).all()


def test_get_genes_from_panelapppanel_not_found(session):
    statement = get_panelapp_genes([1])
    result = session.exec(statement).all()

    assert len(result) == 0


def test_get_panelapppanel_by_gene(session):
    panelapp_panels = create_panelapp_panels(3, session)
    genes = create_genes(3, session)
    session.flush()
    link_panelapp_panel_gene(genes[:1], panelapp_panels[0], 1, session)
    link_panelapp_panel_gene(genes[:2], panelapp_panels[1], 1, session)
    link_panelapp_panel_gene(genes[:3], panelapp_panels[2], 1, session)
    session.commit()

    statement = get_panel_app_panels_including_gene(genes[1].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 2
    assert result[0].id == panelapp_panels[1].id
    assert result[1].id == panelapp_panels[2].id


def test_get_genepanel_by_gene(session):
    genepanels = create_panels(3, session)
    genes = create_genes(2, session)
    session.flush()
    link_genepanel_gene(genes[0], genepanels[0], session)
    link_genepanel_gene(genes[1], genepanels[1], session)
    link_genepanel_gene(genes[1], genepanels[2], session)
    session.commit()

    statement = get_genepanels_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 1
    assert result[0].id == 1


def test_get_genepanels_by_gene(session):
    genepanels = create_panels(3, session)
    genes = create_genes(2, session)
    session.flush()
    link_genepanel_gene(genes[0], genepanels[0], session)
    link_genepanel_gene(genes[0], genepanels[1], session)
    link_genepanel_gene(genes[0], genepanels[2], session)
    session.commit()

    statement = get_genepanels_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 3
    assert result[0].id == genepanels[0].id
    assert result[1].id == genepanels[1].id
    assert result[2].id == genepanels[2].id


def test_get_genepanels_explicitely_containing_gene(session):
    # [0] A panel does not have the gene, is not selected
    # [1] A panel only has that gene, it is selected
    # [2] A panel has multiple genes, among those the desired gene, it is selected
    # [3] A panel has the gene implicitely, it is not selected
    genepanels = create_panels(4, session)
    panelapp_panels = create_panelapp_panels(1, session)
    genes = create_genes(3, session)
    session.flush()
    link_genepanel_gene(genes[0], genepanels[1], session)  # Panel id 2
    for gene in genes:
        link_genepanel_gene(gene, genepanels[2], session)  # Panel id 3
    link_panelapp_panel_gene([genes[0]], panelapp_panels[0], 1, session)  # Panel id 4
    link_genepanel_panelapp_panel([panelapp_panels[0]], genepanels[3], session)
    session.commit()

    statement = get_genepanels_explicitely_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 2
    assert result[0].id == 2
    assert result[1].id == 3


def test_get_genepanels_implicitely_containing_gene(session):
    # A gene panel has a panelapp panel that has the gene, it is selected
    # A gene panel has a panelapp panel that does not have the gene, it is not selected
    # A gene panel has gene explicitely, it is not selected
    genepanels = create_panels(3, session)
    panelapp_panels = create_panelapp_panels(2, session)
    genes = create_genes(1, session)
    session.flush()

    link_genepanel_panelapp_panel([panelapp_panels[0]], genepanels[0], session)
    link_panelapp_panel_gene([genes[0]], panelapp_panels[0], 1, session)
    link_genepanel_gene(genes[0], genepanels[2], session)
    session.commit()

    statement = get_genepanels_implicitely_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 1
    assert result[0].id == 1


def test_get_genepanel_with_explicitely_containing_gene_but_excluded(session):
    # A genepanel contains directly a gene, but the gene is also excluded,
    # then the genepanel should not be selected
    genepanels = create_panels(1, session)
    genes = create_genes(1, session)
    session.flush()
    link_genepanel_gene(genes[0], genepanels[0], session)
    link_genepanel_excluded_gene([genes[0]], genepanels[0], session)
    session.commit()

    statement = get_genepanels_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 0


def test_get_genepanel_with_implicitely_containing_gene_but_excluded(session):
    # A genepanel has a panelapp panel that contains the gene,
    # but the gene is excluded from the gene panel,
    # then the gene panel should not selected
    genepanels = create_panels(1, session)
    panelapp_panels = create_panelapp_panels(1, session)
    genes = create_genes(1, session)
    session.flush()

    link_genepanel_panelapp_panel([panelapp_panels[0]], genepanels[0], session)
    link_panelapp_panel_gene([genes[0]], panelapp_panels[0], 1, session)
    link_genepanel_excluded_gene([genes[0]], genepanels[0], session)
    session.commit()

    statement = get_genepanels_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 0


def test_get_genepanels_with_panelapps_containing_genes(session):
    # A genepanel has a panelapp panel that contains the gene,
    # but the gene is excluded from the gene panel,
    # then the gene panel should not selected
    genepanels = create_panels(2, session)
    panelapp_panels = create_panelapp_panels(2, session)
    genes = create_genes(3, session)
    session.flush()

    link_genepanel_panelapp_panel([panelapp_panels[0]], genepanels[0], session)
    link_genepanel_panelapp_panel([panelapp_panels[1]], genepanels[1], session)
    link_panelapp_panel_gene(genes[:1], panelapp_panels[0], 1, session)
    link_panelapp_panel_gene(genes, panelapp_panels[1], 1, session)
    session.commit()

    statement = get_genepanels_containing_gene(genes[0].hgnc_id)
    result = session.exec(statement).all()
    assert len(result) == 2
    assert result[0].id == 1
    assert result[1].id == 2
