-- Remove previous definitions of the 'update_hgnc_ids' function
-- DROP FUNCTION update_hgnc_ids; 

-- Function to replace foreign keys 'hgnc_id' by 'new_hgnc_id'
CREATE OR REPLACE FUNCTION update_hgnc_ids()
RETURNS TRIGGER
AS $fn_trig_update_hgnc_ids$
DECLARE 
	-- Query will return rows from tables of type RECORD
	t_row RECORD;
BEGIN
	-- Built-in variable 'old' contains column instances of pre-insert/updated row
    -- Built-in variable 'new' contains column instances of post-instert/updated row
	raise notice 'old: %, new: %', old,new;
    -- Loop over all RECORDs with a column containing a 'hgnc_id' from table 'gene' as a foreign key 
	FOR t_row IN 
	    SELECT 
    	    attrelid::regclass, 
    	    attname
	    FROM pg_attribute JOIN (
        SELECT 
            conname, 
            conrelid::regclass AS referenced, 
            unnest(conkey) AS num,
            unnest(confkey) AS attnum_gene
        FROM pg_constraint 
        WHERE contype='f' 
        AND connamespace = 'public'::regnamespace
        AND confrelid=(SELECT oid FROM pg_class WHERE relname = 'gene')
	    ) AS constr 
	    ON constr.referenced=attrelid 
	    AND constr.num=attnum 
	    AND constr.attnum_gene=(
            SELECT attnum 
            FROM pg_attribute 
            WHERE attrelid=(
                SELECT oid 
                FROM pg_class 
                WHERE relname = 'gene'
            ) 
            AND attname='hgnc_id'
	    )
	LOOP
        -- Note: the 'old.hgnc_id' is identical to 'new.hgnc_id'
        -- But if 'new.new_hgnc_id' is not NULL we have to make sure that all foreign keys 'hgnc_id' are now 'new_hgnc_id'
		raise notice 'update: t: %, c: %, hgnc_id: % -> %',t_row.attrelid,t_row.attname,new.hgnc_id,new.new_hgnc_id;
        -- Replace 'hgnc_id' that points to the current t_row with 'new.new_hgnc_id'
		execute format(
		'update %s set %s = %s where %s = %s',
		t_row.attrelid, t_row.attname, new.new_hgnc_id, t_row.attname, new.hgnc_id
		);
	
	END LOOP;
	RETURN NULL;
END;
$fn_trig_update_hgnc_ids$ language plpgsql;

-- Remove previous definitions of the 'trig_update_hgnc_ids' trigger
-- DROP TRIGGER trig_update_hgnc_ids;

-- Replace all foreign keys 'hgnc_id' in database by 'new_hgnc_id' 
-- after update of 'new_hgnc_id' in a 'gene' record
CREATE OR REPLACE TRIGGER trig_update_hgnc_ids AFTER UPDATE OF
new_hgnc_id ON gene
FOR EACH ROW EXECUTE procedure update_hgnc_ids();
