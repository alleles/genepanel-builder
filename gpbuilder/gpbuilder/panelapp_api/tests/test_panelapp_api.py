# Test Genomics England PanelApp interface

import logging

import pytest

from gpbuilder import panelapp_api

# pytest-mock module install a 'mocker' fixture (see https://github.com/pytest-dev/pytest-mock/)


@pytest.fixture
def query():
    return [111, "test panel"]


@pytest.fixture
def full_query():
    return [111, "test panel", 2]


def test_query(query, caplog):
    caplog.set_level(logging.DEBUG)
    paquery = panelapp_api.PanelAppQuery(*query)
    assert (
        paquery.id == 111
        and paquery.name == "test panel"
        and paquery.confidence_level == 3
    )


def test_full_query(full_query, caplog):
    caplog.set_level(logging.DEBUG)
    paquery = panelapp_api.PanelAppQuery(*full_query)
    assert (
        paquery.id == 111
        and paquery.name == "test panel"
        and paquery.confidence_level == 2
    )
