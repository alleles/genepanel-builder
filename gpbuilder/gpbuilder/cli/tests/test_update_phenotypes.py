from sqlmodel import select

from gpbuilder.api.tests.mock_data import create_genes
from gpbuilder.cli.update_data.phenotype import insert_phenotype
from gpbuilder.conftest import unique_index
from gpbuilder.datamodel.phenotype import Phenotype, PhenotypeGene


def test_insert_phenotype(session):
    idx = next(unique_index())
    (gene1, gene2) = create_genes(2, session)
    phenotype = Phenotype(
        mim_number=idx,
        titles={"Title #1"},
        update_id=1,
    )

    phenotype_gene = PhenotypeGene(
        gene_id=gene1.hgnc_id,
        phenotype_id=idx,
        inheritance_descr="Test inheritance",
        gene_mim_number=123456,
    )

    insert_phenotype(session, phenotype, phenotype_gene)

    phenotype = Phenotype(
        mim_number=idx,
        titles={"Title #2"},
        update_id=1,
    )

    phenotype_gene = PhenotypeGene(
        gene_id=gene2.hgnc_id,
        phenotype_id=idx,
        inheritance_descr="Test inheritance 2",
        gene_mim_number=234567,
    )

    insert_phenotype(session, phenotype, phenotype_gene)

    db_phenotype: Phenotype = session.get(Phenotype, idx)
    assert set(db_phenotype.titles) == {"Title #1", "Title #2"}

    db_phenotype_genes = session.exec(
        select(PhenotypeGene).where(PhenotypeGene.phenotype_id == idx)
    ).all()
    assert len(db_phenotype_genes) == 2


def test_update_phenotype(session):
    idx = next(unique_index())
    (gene1, gene2) = create_genes(2, session)
    phenotype = Phenotype(
        mim_number=idx,
        titles={"Title #1"},
        update_id=1,
    )

    phenotype_gene = PhenotypeGene(
        gene_id=gene1.hgnc_id,
        phenotype_id=idx,
        inheritance_descr="Test inheritance",
        gene_mim_number=123456,
    )

    insert_phenotype(session, phenotype, phenotype_gene)

    phenotype = Phenotype(
        mim_number=idx,
        titles={"Title #2"},
        update_id=1,
    )

    phenotype_gene = PhenotypeGene(
        gene_id=gene1.hgnc_id,
        phenotype_id=idx,
        inheritance_descr="Test inheritance 2",
        gene_mim_number=234567,
    )

    insert_phenotype(session, phenotype, phenotype_gene)

    db_phenotype: Phenotype = session.get(Phenotype, idx)
    assert set(db_phenotype.titles) == {"Title #1", "Title #2"}

    db_phenotype_genes = session.exec(
        select(PhenotypeGene).where(PhenotypeGene.phenotype_id == idx)
    ).all()
    assert len(db_phenotype_genes) == 2
    assert set([pg.inheritance_descr for pg in db_phenotype_genes]) == {
        "Test inheritance",
        "Test inheritance 2",
    }
    assert set([pg.gene_mim_number for pg in db_phenotype_genes]) == {123456, 234567}
