from behave import when


@when("I submit the gene panel")
@when('I submit the gene panel with version "{version}"')
def submit_panel(context, version=None):
    context.execute_steps(
        """
        When I press "Submit Gene Panel"
        """
    )

    if version:
        context.execute_steps(
            f"""
            When I press the element with xpath "//button[contains(., '{version}')]"
        """
        )

    context.execute_steps(
        """
        Then I should see an element with id "confirm-button" within 15 seconds
        When I press the element with xpath "//button[@id='confirm-button']"
        """
    )
