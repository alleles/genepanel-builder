from typing import Optional

from pydantic import root_validator
from sqlalchemy.sql.schema import UniqueConstraint
from sqlmodel import Column, Enum, Field, Identity, Integer

from gpbuilder.datamodel.base import GenomicFeature
from gpbuilder.datamodel.utils.types import CodingStrand


class Region(GenomicFeature, table=True):
    id: Optional[int] = Field(
        default=None, sa_column=Column(Integer, Identity(), primary_key=True)
    )
    name: Optional[str] = Field(default="")
    strand: CodingStrand = Field(
        default="+", sa_column=Column(Enum("+", "-", name="strand"), nullable=False)
    )

    __table_args__ = (UniqueConstraint(*GenomicFeature.__fields__.keys()),)

    @root_validator
    def validate_name(cls, values):
        if not values.get("name"):
            values["name"] = "CR_{chrom}:{start}-{end}".format(
                chrom=values["chromosome"], start=values["start"], end=values["end"]
            )
        return values

    def intersect(self, regions, return_all=False):
        """Intersect with a list of other regions"""
        for _r in regions:
            if self.chromosome != _r.chromosome or self.ref_genome != _r.ref_genome:
                continue
            start = max(self.start, _r.start)
            end = min(self.end, _r.end)
            if start < end or return_all:
                yield Region.construct(
                    chromosome=self.chromosome,
                    start=start,
                    end=end,
                    ref_genome=self.ref_genome,
                    name=self.name,
                    strand=self.strand,
                )

    def slop(self, upstream, downstream):
        """Extend region up- and downstream"""
        slop_start = self.start - upstream
        slop_end = self.end + downstream
        if slop_start < 0:
            slop_start = 0
        return Region.construct(
            chromosome=self.chromosome,
            start=slop_start,
            end=slop_end,
            ref_genome=self.ref_genome,
            name=self.name,
            strand=self.strand,
        )
