import subprocess
import sys


def run_command(cmd: list[str], error_message: str, output = None) -> subprocess.CompletedProcess[str]:
    """Run command line command"""
    print(f"Running \"{' '.join(cmd)}\"")
    if output is None:
        kwargs = {"stdout": subprocess.PIPE}
    else:
        kwargs = {"stdout": output}
    try:
        s:subprocess.CompletedProcess[str] = subprocess.run(cmd, check=True, encoding="utf-8", **kwargs)
    except subprocess.CalledProcessError as e:
        print(f"{error_message}\n{e.stderr}")
        sys.exit(-1)
    return s
