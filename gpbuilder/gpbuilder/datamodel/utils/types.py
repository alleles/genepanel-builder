from typing import Literal, Union

Chromosome = Union[
    Literal["1"],
    Literal["2"],
    Literal["3"],
    Literal["4"],
    Literal["5"],
    Literal["6"],
    Literal["7"],
    Literal["8"],
    Literal["9"],
    Literal["10"],
    Literal["11"],
    Literal["12"],
    Literal["13"],
    Literal["14"],
    Literal["15"],
    Literal["16"],
    Literal["17"],
    Literal["18"],
    Literal["19"],
    Literal["20"],
    Literal["21"],
    Literal["22"],
    Literal["X"],
    Literal["Y"],
    Literal["XY"],
    Literal["MT"],
]

CodingStrand = Union[Literal["+"], Literal["-"]]

ReferenceGenome = Union[Literal["GRCh37"], Literal["GRCh38"]]
