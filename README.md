&nbsp;
&nbsp;

<div align="center">
    <img
        width="350px"
        style="border: 0;"
        src="./docs/img/GPB_logo_vert_blue.svg"
        alt="Gene panel builder"
    />
</div>

&nbsp;
&nbsp;

[![Latest Release](https://gitlab.com/alleles/genepanel-builder/-/badges/release.svg)](https://gitlab.com/alleles/genepanel-builder/-/releases)

Gene panel builder (GPB) is a companion utility for genetic variants interpretation software such as [ELLA](https://allel.es) to build virtual gene panels with transcript information and inheritance modes. Coverage information can be used as an aid in the process.

# Documentation

Built docs available at [DPIPE Documentation](https://dpipe.gitlab.io/docs/docs-pages/genepanel-builder/). See also our [GitLab repository](https://gitlab.com/alleles/genepanel-builder).

## User manual and release notes

- [User manual](./docs/User_manual.md): How-to for GPB end users.
- [Release notes](./docs/Release_notes.md): Versions and changes.

## Background

- [About gene panels](./docs/About_gene_panels.md): Why do we need gene panels and a tool such as GPB?
- [Technical implementation](./docs/Technical_implementation.md): A brief high-level overview of GPB's technical implementation

## Set up for production

- [Production deployment](./docs/Production_deployment.md): How to set up GPB in a production environment.
- [Authentication and authorization](./docs/Authentication_and_Authorization.md): Explanation of how GPB handles authentication, including how to disable it in the production environment, if necessary.
- [Coverage and segmental duplications](./docs/Coverage_segdup.md): How to create files for coverage and segmental duplications, and configure how these are displayed in the frontend.
- [Command line interface](./docs/Command_line_interface.md): CLI for common tasks.

## Set up for development

- [Development](./docs/Development.md): Set up GPB in a development environment.
- [Demo deployment](./docs/Demo_deployment.md): Set up a public demo instance of GPB.

## Contact

For support and suggestions, please contact [ella-support](ma&#105;lt&#111;&#58;&#101;%6&#67;la&#37;2&#68;s&#117;pport&#64;m&#101;&#100;i&#115;&#105;&#110;&#46;%75i%&#54;F&#46;n%&#54;F).

## License and copyright

Gene panel builder (GPB)
Copyright (C) 2022-2024 GPB contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses>.
