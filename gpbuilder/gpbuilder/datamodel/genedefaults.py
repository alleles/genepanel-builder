"""
Class for gene defaults

This should override inheritance and transcripts for a gene, if available for a given hgnc id
"""

import logging
from typing import Optional

from pydantic import ValidationError, validator
from pydantic.error_wrappers import ErrorWrapper
from sqlmodel import Field, Relationship, Session, col, select

from gpbuilder.datamodel.base import SQLModelBase
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.transcript import Transcript

logger = logging.getLogger(__name__)


class GeneDefaultsTranscript(SQLModelBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    transcript_id: int = Field(foreign_key="transcript.id")
    transcript: Transcript = Relationship(sa_relationship_kwargs={"viewonly": True})
    gene_defaults_id: Optional[int] = Field(foreign_key="genedefaults.id")


class GeneDefaults(SQLModelBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    hgnc_id: int = Field(foreign_key="gene.hgnc_id", unique=True)
    gene: Gene = Relationship()
    inheritance: Optional[str] = Field(default=None)
    transcripts: list[Transcript] = Relationship(
        link_model=GeneDefaultsTranscript, sa_relationship_kwargs={"lazy": "joined"}
    )

    _allowed_inheritance_values = [
        "AD",
        "AR",
        "AD/AR",
        "XD",
        "XR",
        "XD/XR",
        "SMu",
        "N/A",
    ]

    def __init__(self, **data):
        super().__init__(**data)
        # We want to check this after the object has been created, and there's currently
        # no other way to do this (until pydantic v2):
        # https://github.com/pydantic/pydantic/issues/1729
        _errors = []
        try:
            self._check_sanity()
        except ValueError as e:
            _errors.append(ErrorWrapper(e, "inheritance | transcripts"))

        try:
            self._check_transcripts()
        except ValueError as e:
            _errors.append(ErrorWrapper(e, "transcripts"))

        if _errors:
            raise ValidationError(_errors, self.__class__)

    @validator("inheritance")
    def inheritance_must_be_valid(cls, v):
        match v:  # noqa: E999
            case "" | None:
                return None
            case "X":
                v = "XD/XR"
            case "AD;AR":
                v = "AD/AR"
            case "XD;XR":
                v = "XD/XR"
            case _:
                if v not in cls._allowed_inheritance_values:
                    logger.warning(f"Invalid inheritance: {v} - setting to 'None'")
                    return None

        if v not in cls._allowed_inheritance_values:
            raise ValueError(
                f"Inheritance must be one of {cls._allowed_inheritance_values}. Got {v}."
            )
        return v

    # Post-validator (see __init__)
    def _check_sanity(self):
        # Entries without both inheritance and transcripts makes no sense
        if self.inheritance is None and not self.transcripts:
            raise ValueError(
                f"Defaults for gene {self.hgnc_id} has no inheritance or transcripts"
            )

    # Post-validator (see __init__)
    def _check_transcripts(self):
        # Check that all transcripts belong to the gene
        for tx in self.transcripts:
            if not isinstance(tx, Transcript):
                raise ValueError(
                    f"Transcript {tx} is not an instance of Transcript (type: {type(tx)})"
                )
            if tx.hgnc_id != self.hgnc_id:
                raise ValueError(
                    f"Transcript {tx.name} does not belong to gene {self.hgnc_id} (belongs to {tx.hgnc_id})"
                )

    @staticmethod
    def _get_gene_defaults(
        session: Session, hgnc_ids: list[int]
    ) -> list["GeneDefaults"]:
        return (
            session.exec(
                select(GeneDefaults).where(col(GeneDefaults.hgnc_id).in_(hgnc_ids))
            )
            .unique()
            .all()
        )

    @classmethod
    def get_inheritance_by_hgnc_ids(
        cls, session: Session, hgnc_ids: list[int]
    ) -> dict[int, str]:
        return {
            gd.hgnc_id: gd.inheritance  # type: ignore
            for gd in GeneDefaults._get_gene_defaults(session, hgnc_ids)
            if gd.inheritance
        }

    @classmethod
    def get_transcripts_by_hgnc_ids(
        cls, session: Session, hgnc_ids: list[int]
    ) -> dict[int, list[Transcript]]:
        return {
            gd.hgnc_id: gd.transcripts
            for gd in GeneDefaults._get_gene_defaults(session, hgnc_ids)
            if gd.transcripts
        }

    # To allow setting transcripts
    class Config:
        extra = "ignore"
