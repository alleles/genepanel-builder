# Test Genenames.org API

import logging

import pytest

from gpbuilder.genenames_api import GenenamesTools


@pytest.fixture
def getresponse(mocker):
    r = mocker.Mock()
    r.encoding = "ascii"
    r.content = b"""\
HGNC ID\tApproved symbol\tApproved name\tStatus\tAlias symbols\tPrevious symbols\tChromosome
1096\tBPNT1\t\"3'(2'), 5'-bisphosphate nucleotidase 1\"\tApproved\t\t\t1q41
13508\tBPY2\t\"basic charge Y-linked 2\"\tApproved\tBPY2A, VCY2A\tVCY2\tYq11.223
38794\tBPY2DP\t\"basic charge Y-linked 2D, pseudogene\"\tApproved\t\t\tYp11.2
18615\tBRAFP1\t\"BRAF pseudogene 1\"\tApproved\tBRAF2\tBRAFPS2\tXq13.3
6\tA1S9T\t\"symbol withdrawn, see [HGNC:12469](/data/gene-symbol-report/#!/hgnc_id/HGNC:12469)\"\tSymbol Withdrawn\t\t\t
"""
    return r


@pytest.fixture
def getresponse_with_rearranged_header(mocker):
    r = mocker.Mock()
    r.encoding = "ascii"
    r.content = b"""\
HGNC ID\tAlias symbols\tApproved symbol\tPrevious symbols\tApproved name\tStatus\tChromosome
13508\tBPY2A, VCY2A\tBPY2\tVCY2\t\"basic charge Y-linked 2\"\tApproved\tYq11.223
    """
    return r


def test_normal(getresponse, mocker, caplog):
    caplog.set_level(logging.DEBUG)
    mocker.patch(
        "gpbuilder.genenames_api.genenames_api.requests"
    ).get.return_value = getresponse

    res = GenenamesTools.download_gene_synonyms()

    assert "1096" in res
    assert "1098" not in res
    assert "13508" in res

    assert res["13508"]["approved_symbol"] == "BPY2"
    assert res["13508"]["new_id"] is None
    assert res["13508"]["withdrawn"] is False
    assert res["13508"]["chromosome"] == "Yq11.223"
    assert sorted(res["13508"]["synonyms"]) == sorted(["BPY2A", "VCY2A"])
    assert sorted(res["13508"]["previous"]) == sorted(["VCY2"])


def test_withdrawn(getresponse, mocker, caplog):
    caplog.set_level(logging.DEBUG)
    mocker.patch(
        "gpbuilder.genenames_api.genenames_api.requests"
    ).get.return_value = getresponse

    res = GenenamesTools.download_gene_synonyms()
    assert "6" in res
    assert "1096" in res
    assert res["6"]["withdrawn"] is True
    assert res["6"]["new_id"] == 12469
    assert res["1096"]["withdrawn"] is False
    assert res["1096"]["new_id"] is None


def test_rearranged(getresponse_with_rearranged_header, mocker, caplog):
    caplog.set_level(logging.DEBUG)
    mocker.patch(
        "gpbuilder.genenames_api.genenames_api.requests"
    ).get.return_value = getresponse_with_rearranged_header

    res_other = GenenamesTools.download_gene_synonyms()
    assert "13508" in res_other
    assert sorted(res_other["13508"]["synonyms"]) == sorted(["BPY2A", "VCY2A"])


def test_find_hgnc_ids(getresponse, mocker, caplog):
    caplog.set_level(logging.DEBUG)
    mocker.patch(
        "gpbuilder.genenames_api.genenames_api.requests"
    ).get.return_value = getresponse

    res = GenenamesTools.download_gene_synonyms()
    assert GenenamesTools.find_hgnc_ids("BPY2A", res).pop() == "13508"
