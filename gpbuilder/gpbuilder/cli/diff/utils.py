"""
Compute diff between two gene panels and export it to a file.

Takes either paths to a exported folders or a gene panel names+versions as input, or a combination
"""
from collections import defaultdict
from enum import Enum
from pathlib import Path

from pydantic import BaseModel, Field, validator
from pydantic.error_wrappers import ValidationError

from gpbuilder.datamodel.utils.records import TranscriptRecord


class ChangeType(str, Enum):
    """Type of change"""

    added = "Gene: NEW"
    removed = "Gene: REMOVED"
    transcript = "New transcript"
    inheritance = "New inheritance"
    symbol = "New gene symbol"


class DiffException(Exception):
    pass


class DiffableTranscriptRecord(BaseModel):
    name: str
    hgnc_id: str
    hgnc_symbol: str
    inheritance: str

    @validator("name", "hgnc_id", "hgnc_symbol", "inheritance", pre=True)
    def none_to_empty(cls, v):
        return "" if v is None else v

    class Config:
        extra = "ignore"
        frozen = True  # Make hashable (for dict keys)


class DiffRowRecord(BaseModel):
    hgnc_id: str = Field(alias="HGNC ID")
    gene_symbol_new: str = Field("", alias="Gene Symbol (new)")
    gene_symbol_old: str = Field("", alias="Gene Symbol (old)")
    transcript_new: str = Field("", alias="Transcript (new)")
    transcript_old: str = Field("", alias="Transcript (old)")
    inheritance_new: str = Field("", alias="Inheritance (new)")
    inheritance_old: str = Field("", alias="Inheritance (old)")
    change_type: str = Field("", alias="Change type")

    class Config:
        allow_population_by_field_name = True

    @classmethod
    def header_to_tsv(cls):
        return "\t".join(f.alias for f in cls.__fields__.values())

    def to_tsv(self):
        return "\t".join(self.dict().values())


def extract_transcript_records(folder: Path) -> set[TranscriptRecord]:
    """Extract TranscriptRecords from an exported gene panel"""
    transcripts = set()
    file = folder / f"{folder.name}_genes_transcripts.tsv"
    assert file.exists()
    with open(file) as f:
        for line in f:
            if line.startswith("#"):
                header = line.strip().split("\t")
                continue
            try:
                tx_record = TranscriptRecord.parse_obj(
                    dict(zip(header, line.strip().split("\t")))
                )
            except:
                print(line)
                raise
            transcripts.add(tx_record)

    return transcripts


def _merge_diffable_records(diffable_records):
    """Merge diffable records, by concatenating unique names and inheritances"""
    merged_records = set()

    # Group by hgnc_id + hgnc_symbol
    records_by_hgnc = defaultdict(set)
    for record in diffable_records:
        records_by_hgnc[(record.hgnc_id, record.hgnc_symbol)].add(record)

    for (hgnc_id, symbol), records in records_by_hgnc.items():
        name = " | ".join(sorted({r.name for r in records}))
        inheritance = " | ".join(sorted({r.inheritance for r in records}))
        record = DiffableTranscriptRecord(
            name=name, hgnc_id=hgnc_id, hgnc_symbol=symbol, inheritance=inheritance
        )

        merged_records.add(record)
    return merged_records


def _to_diffable_records(tx_records: set[TranscriptRecord]) -> set[TranscriptRecord]:
    """Convert TranscriptRecords to a format that can be diffed"""
    diffable_records = set()
    for tx_record in tx_records:
        try:
            tx_record = DiffableTranscriptRecord(**tx_record.dict())
        except ValidationError as e:
            raise DiffException(
                f"Record <{tx_record.dict()}> can't be compared to a `Transcript` [{e}]"
            )
        diffable_records.add(tx_record)
    return _merge_diffable_records(diffable_records)


def compute_diff(
    tx_records_a: set[TranscriptRecord], tx_records_b: set[TranscriptRecord]
) -> tuple[
    dict[DiffableTranscriptRecord, set[tuple[ChangeType, str | None]]],
    set[DiffableTranscriptRecord],
]:
    """
    Compute diff between two exported gene panels

    Sort diff into categories:
    1. Genes in A but not in B - these are the ones that are removed
    2. Genes in B but not in A - these are the ones that are added
    3. Genes in both but with different transcripts
    4. Genes in both but with different inheritance modes
    5. Genes in both but with different gene symbols

    tx_records_a represents the old gene panel, tx_records_b represents the new gene panel
    (corresponding to added/removed genes)

    Return a tuple of (diff, non_diff), where diff is a dict of DiffableTranscriptRecords,
    and non_diff is a set of DiffableTranscriptRecords that are identical between a and b
    """

    diff: dict[TranscriptRecord, set[ChangeType]] = defaultdict(set)

    try:
        tx_records_a = _to_diffable_records(tx_records_a)
        tx_records_b = _to_diffable_records(tx_records_b)
    except ValidationError:
        raise

    hgnc_ids_a = {gene.hgnc_id for gene in tx_records_a}
    hgnc_ids_b = {gene.hgnc_id for gene in tx_records_b}
    inheritance_a = {gene.hgnc_id: gene.inheritance for gene in tx_records_a}
    inheritance_b = {gene.hgnc_id: gene.inheritance for gene in tx_records_b}
    symbols_a = {gene.hgnc_id: gene.hgnc_symbol for gene in tx_records_a}
    symbols_b = {gene.hgnc_id: gene.hgnc_symbol for gene in tx_records_b}
    transcripts_a = {gene.hgnc_id: gene.name for gene in tx_records_a}
    transcripts_b = {gene.hgnc_id: gene.name for gene in tx_records_b}

    for gene in {gene for gene in tx_records_a if gene.hgnc_id not in hgnc_ids_b}:
        # The gene is removed
        diff[gene].add((ChangeType.removed, None))

    for gene in tx_records_b:
        if gene.hgnc_id not in hgnc_ids_a:
            # The gene is new
            diff[gene].add((ChangeType.added, None))
            continue

        if inheritance_a[gene.hgnc_id] != inheritance_b[gene.hgnc_id]:
            # The inheritance is changed
            diff[gene].add((ChangeType.inheritance, inheritance_a[gene.hgnc_id]))

        if symbols_a[gene.hgnc_id] != symbols_b[gene.hgnc_id]:
            # The gene symbol is changed
            diff[gene].add((ChangeType.symbol, symbols_a[gene.hgnc_id]))

        if transcripts_a[gene.hgnc_id] != transcripts_b[gene.hgnc_id]:
            # The transcript is changed
            diff[gene].add(
                (
                    ChangeType.transcript,
                    transcripts_a[gene.hgnc_id],
                )
            )

    non_diff = tx_records_a & tx_records_b
    return dict(diff), non_diff


def diff_to_rows(
    diff: dict[DiffableTranscriptRecord, set[tuple[ChangeType, str | None]]],
) -> list[DiffRowRecord]:
    """Write diff to tsv format"""

    rows = []
    for gene, changes in sorted(diff.items(), key=lambda x: x[0].hgnc_id):
        row = DiffRowRecord(hgnc_id=gene.hgnc_id)  # type: ignore[call-arg]

        for change_type, from_value in sorted(changes):
            match change_type:
                case ChangeType.added:
                    row.gene_symbol_new = gene.hgnc_symbol
                    row.transcript_new = gene.name
                    row.inheritance_new = gene.inheritance
                case ChangeType.removed:
                    row.gene_symbol_old = gene.hgnc_symbol
                    row.transcript_old = gene.name
                    row.inheritance_old = gene.inheritance
                case ChangeType.inheritance:
                    row.gene_symbol_new = gene.hgnc_symbol
                    row.inheritance_new = gene.inheritance
                    row.inheritance_old = from_value
                case ChangeType.symbol:
                    row.gene_symbol_new = gene.hgnc_symbol
                    row.gene_symbol_old = from_value
                case ChangeType.transcript:
                    row.gene_symbol_new = gene.hgnc_symbol
                    row.transcript_new = gene.name
                    row.transcript_old = from_value
                case _:
                    raise ValueError(f"Unknown change type: {change_type}")

            if not row.change_type:
                row.change_type = change_type.value
            else:
                row.change_type += f" | {change_type.value}"
        rows.append(row)

    return rows
