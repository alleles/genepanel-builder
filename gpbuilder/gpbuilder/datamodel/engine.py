import os
from pathlib import Path

import sqlmodel
from sqlalchemy.pool import NullPool
from sqlalchemy_utils import create_database, database_exists

from gpbuilder.datamodel.gene import *  # noqa: F401,F403
from gpbuilder.datamodel.genedefaults import *  # noqa: F401,F403
from gpbuilder.datamodel.genepanel import *  # noqa: F401,F403
from gpbuilder.datamodel.panelapp_panel import *  # noqa: F401,F403
from gpbuilder.datamodel.region import *  # noqa: F401,F403
from gpbuilder.datamodel.transcript import *  # noqa: F401,F403
from gpbuilder.datamodel.update import *  # noqa: F401,F403

DB_URL = os.environ["DB_URL"]

if not database_exists(DB_URL):
    create_database(DB_URL)

if os.environ.get("DEVELOPMENT") == "true":
    engine = sqlmodel.create_engine(DB_URL, echo=False, poolclass=NullPool)
else:
    engine = sqlmodel.create_engine(DB_URL, echo=False, pool_pre_ping=True)


def get_session():
    return sqlmodel.Session(engine, expire_on_commit=False)


def create_db_and_tables():
    with engine.connect() as connection:
        connection.execute(sqlmodel.text("CREATE SCHEMA IF NOT EXISTS public;"))
        connection.execute(
            sqlmodel.text("CREATE EXTENSION IF NOT EXISTS semver WITH SCHEMA public;")
        )
        connection.commit()
    sqlmodel.SQLModel.metadata.create_all(engine)

    # Add trigger
    trigger_file = (
        Path(__file__).parent.parent.parent
        / "resources/sql/trigger_update_hgnc_ids.sql"
    )
    execute_sql_script(trigger_file)


def execute_sql_script(sql_script: Path):
    with open(sql_script, "r") as fo:
        trigger = sqlmodel.text(fo.read())
    with engine.connect() as connection:
        connection.execute(trigger)
        connection.commit()


def copy_to_initial_db():
    """
    Copies the current database to a new database with the suffix _initial.
    """
    db_name = os.environ["PG_DB"]
    initial_db_name = f"{db_name}_initial"
    with engine.connect() as connection:
        # Commit at the beginning, to step out of transaction. Otherwise, the drop command will fail.
        connection.execute(sqlmodel.text("COMMIT"))

        connection.execute(
            sqlmodel.text(
                "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid();"
            )
        )
        connection.execute(
            sqlmodel.text(f"DROP DATABASE IF EXISTS {initial_db_name} WITH (FORCE)")
        )
        connection.execute(
            sqlmodel.text(f"CREATE DATABASE {initial_db_name} WITH TEMPLATE {db_name}")
        )


def drop_all():
    with engine.connect() as connection:
        connection.execute(sqlmodel.text("DROP SCHEMA IF EXISTS public CASCADE"))
        connection.commit()
