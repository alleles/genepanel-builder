"""Module for reading and doing stats on D4 files.

Calculates the ratio of regions that have a value above the threshold.
Chromosome names must use the same notation as in the D4 file.
The start and end positions must be zero-based, half-open.
"""

import gc
import logging
from pathlib import Path

import numpy as np
import numpy.typing as npt
from pyd4 import D4File  # type: ignore
from pydantic import BaseModel, NonNegativeFloat

logger = logging.getLogger(__name__)


class ReturnedFraction(BaseModel):
    """Main returned result from D4NumpyReader"""

    fraction_across_regions: NonNegativeFloat
    fraction_per_region: list[NonNegativeFloat]


class D4NumpyReader:
    """Holds quick D4 numpy objects and calculates stats on given regions.

    d4_path : path to D4 file
    d4_file : Has values for each base per chromsome
    chromosomes_d4np: A dict of numpy arrays made from D4 per chromsome
    """

    def __init__(self, path: Path | str, threshold: int):
        self.d4_path = Path(path)
        self.d4_file = D4File(str(self.d4_path))
        self._load_thread = None

        # Runs the threshold setter
        self.threshold = threshold

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, value: int):
        # Ensure that the load thread is finished before changing the threshold
        # Otherwise, we might get values for old threshold in the chromosome_d4np cache

        # Disabled for now, as the reload functionality in FastAPI causes this to trigger on every reload
        # We should really have a separate endpoint for reloading the D4 file

        # if self._load_thread and self._load_thread.is_alive():
        #     self._load_thread.join()
        self._threshold = value
        self.chromosome_d4np: dict[str, npt.NDArray[np.uint8]] = dict()
        # self._load_thread = threading.Thread(target=self._populate_chromosome_d4np)
        # self._load_thread.start()

    @property
    def _memory_footprint(self) -> int:
        """Returns the memory footprint (in bytes) of the numpy arrays in"""
        return sum(
            [
                self.chromosome_d4np[chromosome].nbytes
                for chromosome in self.chromosome_d4np
            ]
        )

    def populate_chromosome_d4np(self):
        """Populate chromosome_d4np with all chromosomes in d4_file."""
        for chromosome in self.d4_file.chrom_names():
            self.__get_numpy_array(chromosome)

    def get_values_for_region(
        self, chrom: str, start_pos: int, end_pos: int
    ) -> npt.NDArray[np.uint8]:
        """Returns values above/below threshold within a genomic region."""
        assert start_pos < end_pos

        # Find out which bytes we need to read
        start_byte = start_pos // 8
        end_byte = end_pos // 8 + 1

        # Unpack the bits into bytes, so it's easier to read them
        unpacked_array = np.unpackbits(
            self.__get_numpy_array(chrom)[start_byte:end_byte]
        )

        # Find actual start and end positions within the byte
        unpacked_start_pos = start_pos % 8
        unpacked_end_pos = (end_byte - start_byte - 1) * 8 + end_pos % 8

        # Return slice of unpacked array
        return unpacked_array[unpacked_start_pos:unpacked_end_pos]

    def get_number_ge_threshold(self, chrom: str, start_pos: int, end_pos: int) -> int:
        """Returns number of values above or equal to threshold within a genomic region."""
        return (self.get_values_for_region(chrom, start_pos, end_pos)).sum()

    def fraction_ge_threshold(
        self, regions: list[tuple[str, int, int]]
    ) -> ReturnedFraction:
        """Calculates fraction of D4 values equal to or greater than threshold.

        Returns the fraction across all regions as well as fraction for each individual region.
        """
        assert len(regions)
        sum_above_threshold: int = 0
        sum_length: int = 0
        fraction_per_region: list[float] = list()
        for chrom, start_pos, end_pos in regions:
            above_threshold = self.get_number_ge_threshold(chrom, start_pos, end_pos)
            region_length = end_pos - start_pos

            sum_above_threshold += above_threshold
            sum_length += region_length
            fraction_per_region.append(above_threshold / region_length)
        fraction_across_regions = sum_above_threshold / sum_length
        return ReturnedFraction(
            fraction_across_regions=fraction_across_regions,
            fraction_per_region=fraction_per_region,
        )

    def __get_numpy_array(self, chromosome: str) -> npt.NDArray[np.uint8]:
        """Helper to lazily load numpy array for a chromosome.

        Returns a an array of the chromosome (uint8), where each bit represents a base
        (so each byte represents 8 bases)
        A value of 1 means above threshold, 0 means below threshold.

        Raises: KeyError if chromosome not in d4_file"""
        if chromosome not in self.chromosome_d4np:
            logger.info(f"Loading chromosome {chromosome} from {self.d4_path}.")
            try:
                chrom_arr: npt.NDArray[np.uint8] = self.d4_file.load_to_np(chromosome)  # type: ignore
                self.chromosome_d4np[chromosome] = np.packbits(
                    np.array(chrom_arr >= self.threshold, dtype=np.bool8)
                )
                gc.collect()
            except KeyError:
                logger.error(
                    f"Cannot find {chromosome} in {self.d4_path}, returning empty array."
                )
                self.chromosome_d4np[chromosome] = np.packbits(
                    np.empty(0, dtype=np.int32)
                )
        return self.chromosome_d4np[chromosome]
