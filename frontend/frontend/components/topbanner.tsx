import Link from 'components/link'
import { useSession } from 'components/sessioncontext'
import { useEffect, useState } from 'react'
import API from 'api'
import Image from 'next/image'

export default function TopBanner() {
  const { userinfo, logout } = useSession()
  const [version, setVersion] = useState('')

  useEffect(() => {
    API.SettingsGetVersion.get().then((r) => setVersion(r.data))
  }, [])

  return (
    <div className="sticky top-0 z-30 grid w-full grid-cols-3 bg-ellablue-darkest py-2 text-sm font-medium tracking-wide text-ellablue-lighter">
      <div className="ml-8">
        <nav className="flex gap-6">
          <Link href="/">Panels</Link>
          {/* Pending functionality: Add links*/}
          <span className="text-ellablue-darker">Genes</span>
          <span className="text-ellablue-darker">Review</span>
        </nav>
      </div>
      <div className="text-center">
        <Image
          src="/GPB_logo_horiz_white.svg"
          alt="Gene panel builder logo"
          className="mb-1 inline-block"
          width={180}
          height={16}
        />
      </div>
      <div className="mr-9 flex flex-nowrap gap-6 place-self-end text-center">
        <Link href="https://dpipe.gitlab.io/docs/docs-pages/genepanel-builder/docs/User_manual/">
          Documentation
        </Link>
        <Link href="https://dpipe.gitlab.io/docs/docs-pages/genepanel-builder/docs/Release_notes/">
          {` v${version}`}
        </Link>
        <span className="whitespace-nowrap">
          <button
            onClick={logout}
            title={`Logged in as: ${userinfo.name}`}
            className="ml-1"
          >
            Log out
          </button>
        </span>
      </div>
    </div>
  )
}
