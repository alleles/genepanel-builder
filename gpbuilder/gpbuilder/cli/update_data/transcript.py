import logging

from sqlmodel import Session, col, func, select

from gpbuilder.cli.update_data.common import (
    UpdateStatus,
    set_next_update_id,
)
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.ncbi_api.find_gffs import download_refseq_gff_files
from gpbuilder.ncbi_api.parse_ncbi_refseq_gff import parse_ncbi_refseq_gff

logger = logging.getLogger(__name__)


def insert_transcript(
    session: Session, transcript: Transcript, approved_hgnc_ids: set[int]
) -> UpdateStatus:
    if transcript.hgnc_id not in approved_hgnc_ids:
        approved_gene = Gene.get_approved_gene_by_hgnc_id(session, transcript.hgnc_id)
        if approved_gene is None:
            logger.warning(
                f"Unmappable HGNC ID {transcript.hgnc_id} ({transcript.name})"
            )
            return UpdateStatus.IGNORED
        transcript.hgnc_id = approved_gene.hgnc_id

    db_transcript = session.exec(
        select(Transcript).where(
            Transcript.name == transcript.name,
            Transcript.ref_genome == transcript.ref_genome,
        )
    ).one_or_none()

    if db_transcript is None:
        session.add(transcript)
        return UpdateStatus.CREATED

    # Do not update a X chromosome transcript with a Y chromosome transcript
    if transcript.chromosome == "Y" and db_transcript.chromosome == "X":
        logger.debug(
            f"Rejecting Y-remapped transcript '{transcript.name}' "
            f"({transcript.ref_genome}_{transcript.chromosome}:{transcript.start}-{transcript.end})"
        )
        return UpdateStatus.REFUSED

    if (
        transcript.chromosome != "X"
        and db_transcript.chromosome != "Y"
        and db_transcript.chromosome != transcript.chromosome
    ):
        logger.warning(
            f"Existing transcript '{transcript.name}' was remapped from "
            f"chromosome {db_transcript.chromosome} to chromosome {transcript.chromosome}"
        )

    # Update transcript
    for k, v in transcript.dict(exclude={"id"}).items():
        setattr(db_transcript, k, v)
    session.add(db_transcript)
    return UpdateStatus.UPDATED


def update_transcripts_table(session: Session, ref_genome):
    logger.info("Finding transcript files (GFF) from NCBI")

    max_release = session.exec(select(func.max(Transcript.source))).one()  # type: ignore

    gff_files = download_refseq_gff_files(ref_genome)
    if max_release is not None:
        gff_files = [
            (gff_file, release)
            for gff_file, release in gff_files
            if f"NCBI {release}" > max_release
        ]

    if not gff_files:
        logger.info("No new transcript files found from NCBI - no transcripts updated")
        return

    logger.info(
        f"Updating transcripts from NCBI with releases {[x[1] for x in gff_files]}"
    )

    approved_hgnc_ids = set(
        session.exec(select(Gene.hgnc_id).where(col(Gene.withdrawn).is_(False))).all()
    )
    counts = UpdateStatus.get_counter()
    update_id = set_next_update_id("ncbi_transcript", session)
    for gff_file, release in gff_files:
        logger.info(f"Parsing '{gff_file}'")
        release = f"NCBI {release}"
        for transcript in parse_ncbi_refseq_gff(gff_file, release, ref_genome):
            logger.debug(f"Reviewing transcript {transcript.name}")
            transcript.update_id = update_id
            update_status = insert_transcript(session, transcript, approved_hgnc_ids)
            counts[update_status] += 1

        logger.info(f"{release} sourced transcripts: {counts}")
