#!/usr/bin/env python3

"""
Calculate average sequencing coverage from a set of sample files.

INPUT: Sample files in bed.gz format.
OUTPUT: Average depth per base in D4 format.

"""

import argparse
from asyncio import subprocess
from subprocess import Popen
import sys
from pathlib import Path
from statistics import mean

from lib.commands import run_command


def calculate_averages(archived_beds: list[Path], output_path: Path) -> Path:
    """Calculate averages: .bed --> .bedgraph withoud bedtools
    
    - Avoid decompressing bed files
    - Do not save merged bed file
    - Calculate the average of the fly

    """
    print("\nCalculating averages...")
    
    if len(archived_beds) == 0:
        sys.exit("No bed files specified")

    avg_file_path = Path(output_path / "averaged.bedgraph")

    if len(archived_beds) == 1:
        cmd: list[str] = ["zcat", str(archived_beds[0])]
    else:
        cmd = ["bedtools", "unionbedg", "-i"]
        cmd.extend(map(str, archived_beds))

    print(f"Running: {' '.join(cmd)}")
    with Popen(cmd, stdout=subprocess.PIPE, text=True) as process:
        with open(avg_file_path, "w") as avg_file:
            chr: str = ""
            while True:
                line: str = process.stdout.readline()
                if line == "" and process.poll() is not None:
                    break
                if line:
                    # print(f"line: {line.strip()}")
                    fields = line.split()
                    
                    if chr != fields[0]:
                        print(f"---- Processing chromosome: {fields[0]}")
                        chr = fields[0]

                    result: str = "\t".join(fields[:3])
                    average: float = round(mean(map(int, fields[3:])),2)
                    avg: str = result + "\t" + str(average) + "\n"
                    # avg: str = get_region_average(line.strip())
                    avg_file.write(avg)

    print(f"Created averaged bedgraph file: {avg_file_path}.\n")
    return avg_file_path


def convert_bedgraph_to_d4(
    average_bedgraph_file: Path, genome_file: Path, output_location: Path
) -> Path:
    """Convert: .bedgraph --> .d4"""

    output_file = Path(output_location / "coverage.d4")

    cmd: list[str] = [
        "d4tools",
        "create",
        "--denominator", "100", # Get two decimal places
        "--dict-range", "0-8192", # Store coverages up to 81.92 (8192/100) in primary (fast access) table
        "-z",
        "-g",
        str(genome_file),
        str(average_bedgraph_file),
        str(output_file),
    ]
    run_command(cmd, "Failed to run d4tools to convert bedgraph file to d4 format.\n")
    return output_file


def make_d4(
    archived_bed_files: list[Path], genome_sizes: Path, output_location: Path
) -> None:
    print("\nCalculating coverage for the set of bed.gz files: \n")
    print(*archived_bed_files, sep='\n')

    average_bedgraph_file: Path = calculate_averages(archived_bed_files, output_location)

    convert_bedgraph_to_d4(average_bedgraph_file, genome_sizes, output_location)

    print("Finished.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Calculate depth average from a set of sample files."
    )
    parser.add_argument(
        "--samples",
        metavar="<file_name.bed.gz>",
        type=Path,
        nargs="+",
        required=True,
        help="One or more space separated file names",
    )
    parser.add_argument(
        "-g",
        "--genome",
        metavar="<genome_file.genome>",
        type=Path,
        required=True,
        help="""The genome description file (Used by BED inputs). The file has the
        length of each chromosome, one per line. Needed for d4tools create.""",
    )
    parser.add_argument(
        "--output",
        metavar="<output_file.d4>",
        type=Path,
        required=True,
        help="Path to the output d4 file.",
    )
    args = parser.parse_args()

    make_d4(args.samples, args.genome, args.output)
