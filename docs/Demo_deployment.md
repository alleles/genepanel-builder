# Demo deployment

The demo service runs on digital ocean. It is deployed using docker-compose.

## Updating the demo

Ssh into the server and switch to the `gpbuilder` user:

```bash
    ssh root@164.90.186.111
    sudo su - gpbuilder
```

Update the repository:

```bash
    cd gpbuilder
    git pull
```

Rebuild the docker images:

```bash
    docker compose -f docker-compose-prod.yml build
```

Restart the service:

```bash
    docker compose down
    docker compose -f docker-compose-prod.yml up -d
```
