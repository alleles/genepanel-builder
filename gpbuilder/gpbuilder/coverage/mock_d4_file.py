import gc
import sys
from pathlib import Path

import numpy as np
import numpy.typing as npt
import pyd4


class MockD4File(object):
    """
    Utility class for mocking a D4 file, with functions to add data to it.
    """

    CHROM_SIZE = 100_000

    def __init__(self, filename):
        self.path = Path(filename).absolute()
        self.output_file = pyd4.D4Builder(filename)
        self.output_file.for_sparse_data()
        self.output_file.generate_index()

    def _add_chromosome(self, chromosome):
        try:
            self.output_file.add_chrom(chromosome, self.CHROM_SIZE)
        except KeyError as e:
            assert str(e) == "'Sequence is already defined'"

    def add_data(self, data: list[int], chromosome: str, start: int):
        """
        Add data to the D4 file.

        Takes an array of data and adds it to the D4 file, starting at the given chromosome:start position.
        """
        assert (
            start + len(data) < self.CHROM_SIZE
        ), f"Data exceeds chromosome size {self.CHROM_SIZE}"

        self._add_chromosome(chromosome)

        try:
            reader = pyd4.D4File(str(self.path))
            existing: npt.NDArray[np.int32] = reader.load_to_np(chromosome)  # type: ignore
        except OSError:
            existing = np.zeros(self.CHROM_SIZE, dtype=np.int32)
        assert existing[start : start + len(data)].max() == 0  # type: ignore
        existing[start : start + len(data)] = data

        writer = self.output_file.get_writer()
        writer.write_np_array(chromosome, 0, existing)
        writer.close()


def create_mock_d4_file(
    filename: Path, low=0, high=12, low_fraction=0.1, clustering=0.99, seed=0
):
    """Create a mock D4 file with low and high values, and a given fraction of low values."""
    chrom_sizes = {
        "1": 249250621,
        "2": 243199373,
        "3": 198022430,
        "4": 191154276,
        "5": 180915260,
        "6": 171115067,
        "7": 159138663,
        "8": 146364022,
        "9": 141213431,
        "10": 135534747,
        "11": 135006516,
        "12": 133851895,
        "13": 115169878,
        "14": 107349540,
        "15": 102531392,
        "16": 90354753,
        "17": 81195210,
        "18": 78077248,
        "19": 59128983,
        "20": 63025520,
        "21": 48129895,
        "22": 51304566,
        "X": 155270560,
        "Y": 59373566,
        "MT": 16569,
    }

    # Fix seed, so that we get the same data every time
    np.random.seed(seed)
    output_file = pyd4.D4Builder(str(filename))
    output_file.add_chroms(chrom_sizes.items())
    output_file.for_sparse_data()
    output_file.generate_index()
    writer = output_file.get_writer()
    for chrom, size in chrom_sizes.items():
        data = np.ones(size, dtype=np.int32) * high
        num_clusters = max(int((1 - clustering) * size * 2 * low_fraction), 1)
        cluster_sizes = np.random.randint(
            1, size // int((1 - clustering) * size), int(num_clusters)
        )
        cluster_starts = np.random.randint(0, size, int(num_clusters))
        cluster_starts.sort()
        cluster_ends = cluster_starts + cluster_sizes
        cluster_ends[-1] = max(cluster_ends[-1], size)
        for start, end in zip(cluster_starts, cluster_ends):
            data[start:end] = low
        print("Adding data for chromosome", chrom, file=sys.stderr)
        writer.write_np_array(chrom, 0, data)
        gc.collect()
    writer.close()


if __name__ == "__main__":
    # Create mock d4 file

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--low", type=int, default=0)
    parser.add_argument("--high", type=int, default=12)
    parser.add_argument("--low-fraction", type=float, default=0.1)
    parser.add_argument("--clustering", type=float, default=0.99)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--output-file", type=Path, default=Path("test.d4"))
    parser.add_argument("--to-stdout", action="store_true")
    parser.add_argument("-f", "--force", action="store_true")

    args = parser.parse_args()

    if args.output_file.exists():
        if not args.force:
            print(
                f"Output file {args.output_file} already exists. Use -f to overwrite."
            )
            exit(1)
        else:
            args.output_file.unlink()

    create_mock_d4_file(
        args.output_file,
        args.low,
        args.high,
        args.low_fraction,
        args.clustering,
        args.seed,
    )
