"""
Module to fetch and parse the MANE Select summary file from NCBI
"""

import ftplib
import gzip
import io
from typing import Iterator, Literal

from pydantic import BaseModel


class ManeSelectTranscriptNCBI(BaseModel):
    transcript_name: str
    status: Literal["MANE Select"] | Literal["MANE Plus Clinical"]


def fetch_mane_select() -> Iterator[ManeSelectTranscriptNCBI]:
    """
    Fetch the MANE Select summary file from NCBI and yield ManeSelectTranscript objects

    Only GRCh38 is provided by NCBI, so this is the only available resource. There are no
    coordinates involved, so we don't need to worry about the genome build.
    """

    fc = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
    fc.login()

    # https://ftp.ncbi.nlm.nih.gov/refseq/MANE/MANE_human/current/MANE.GRCh38.v1.0.summary.txt.gz
    fc.cwd("/refseq/MANE/MANE_human/current/")
    with io.BytesIO() as f:
        fc.retrbinary("RETR README_versions.txt", f.write)
        f.seek(0)
        lines = f.read().decode().split("\n")
    version = next(l for l in lines if l.startswith("MANE Version")).split("\t")[1]

    with io.BytesIO() as f:
        fc.retrbinary(f"RETR MANE.GRCh38.v{version}.summary.txt.gz", f.write)
        f.seek(0)
        with gzip.open(f) as content:
            for lb in content.readlines():
                l = lb.decode().strip("\n")
                if l.startswith("#"):
                    header = l.strip("#").split("\t")
                    continue
                data = dict(zip(header, l.split("\t")))  # type: ignore

                yield ManeSelectTranscriptNCBI(
                    transcript_name=data["RefSeq_nuc"],
                    status=data["MANE_status"],  # type: ignore
                )
