import React, { InputHTMLAttributes, forwardRef } from 'react'

interface Props
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'> {
  onChange: (value: string) => void
}

const Input = forwardRef<HTMLInputElement, Props>(
  (
    { id, name, placeholder, onChange, onBlur, value, ...props }: Props,
    ref
  ) => {
    const onInputChange = (e) => {
      if (onChange) {
        onChange(e.target.value)
      }
    }

    const onInputBlur = (e) => {
      if (onBlur) {
        onBlur(e.target.value)
      }
    }

    return (
      <div>
        <input
          id={id}
          name={name}
          onChange={onInputChange}
          onBlur={onInputBlur}
          className="relative w-full rounded border border-reactgray py-1 pt-2 pl-2 placeholder:text-reactgray-darkest hover:border-reactgray-dark [&:not(:focus):not(:placeholder-shown)]:invalid:border-red-500"
          value={value}
          placeholder={placeholder}
          ref={ref}
          {...props}
        />
      </div>
    )
  }
)

Input.displayName = 'Input'

export default Input
