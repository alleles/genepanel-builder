#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path

from lib.commands import run_command


def get_prefix(alignment_file: Path, location: Path | None = None) -> Path:
    """Custom function to return the file name along with the path"""
    sample_prefix: Path = alignment_file

    if alignment_file.suffix:
        sample_prefix = alignment_file.with_suffix("")

    if location:
        sample_prefix: Path = location / sample_prefix.name

    return sample_prefix


def convert_to_per_base_bed(
    alignment_file: Path,
    genome_reference: Path,
    mapq: int,
    output_path: Path | None = None,
) -> Path:
    """Convert single alignment file (.bam or .cram) to per base bed.gz file"""

    sample_prefix: Path = get_prefix(alignment_file, output_path)

    cmd: list[str] = [
        "mosdepth",
        "--fast-mode",
        "--fasta",
        str(genome_reference),
        "--flag",
        "0",
        "--mapq",
        str(mapq),
        str(sample_prefix),
        str(alignment_file),
    ]
    run_command(cmd, "Failed to run mosdepth to convert BAM files to GZipped BED")
    return Path(str(sample_prefix) + ".bed.gz")


def make_per_base(
    alignment_files: list[Path],
    genome_reference: Path,
    mapq: int = 60,
    output_location: Path = ".",
) -> list[Path]:
    """
    Convert alignment files to compressed per base bed files:
    (.bam | .cram ) --> .bed.gz.

    INPUT:
        files: list of pathlib Path objects pointing to BAM or CRAM files.
        output_location: Optional path to the directory to save output files in.
                         Defaults to the directory the program is launched from.
    OUTPUT:
        compressed bed.gz files
    """

    per_base_files: list[Path] = []

    for alignment_file in alignment_files:
        per_base_file: Path = (
            output_location / alignment_file.with_suffix(".per-base.bed.gz").name
        )
        if not per_base_file.exists():
            print(f"-- Creating {per_base_file}")
            convert_to_per_base_bed(
                alignment_file, genome_reference, mapq, output_location
            )
        else:
            print(f"   File {per_base_file} ready")

        per_base_files.append(per_base_file)

    return per_base_files


if __name__ == "__main__":
    parser = ArgumentParser(
        description=(
            "Convert alignment files to compressed per base bed files: .bam or .cram to .bed.gz"
        )
    )
    parser.add_argument(
        "--output",
        metavar="<output_path>",
        type=Path,
        required=True,
        help="Path to the directory to save the output files in",
    )
    parser.add_argument(
        "--reference",
        metavar="<reference_path>",
        type=Path,
        required=True,
        help="Path to the genome reference FASTA file",
    )
    parser.add_argument(
        "samples",
        metavar="<.bam|.cram>",
        type=Path,
        nargs="+",
        help="One or more space separated BAM or CRAM file names",
    )

    args = parser.parse_args()

    if args.samples:
        make_per_base(args.samples, args.reference, output_location=args.output)
