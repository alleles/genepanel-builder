from gpbuilder.datamodel.region import Region


class TestRegion:
    def test_intersect(self):
        rx = Region(chromosome=1, start=111, end=1111, ref_genome="GRCh38")
        rx_list = [
            Region(chromosome=11, start=111, end=1111, ref_genome="GRCh38"),
            Region(chromosome=1, start=111, end=1111, ref_genome="GRCh37"),
        ]
        assert list(rx.intersect(rx_list)) == []
        rx_list = [
            Region(chromosome=1, start=11, end=55, ref_genome="GRCh38"),
            Region(chromosome=1, start=1121, end=1221, ref_genome="GRCh38"),
        ]
        assert list(rx.intersect(rx_list)) == []
        rx_list = [
            Region(chromosome=1, start=11, end=111, ref_genome="GRCh38"),
            Region(chromosome=1, start=1111, end=1121, ref_genome="GRCh38"),
        ]
        assert list(rx.intersect(rx_list)) == []
        rx_list = [
            Region(chromosome=1, start=11, end=112, ref_genome="GRCh38"),
            Region(chromosome=1, start=1110, end=1121, ref_genome="GRCh38"),
        ]
        assert list(rx.intersect(rx_list)) == [
            Region(chromosome=1, start=111, end=112, ref_genome="GRCh38", name=rx.name),
            Region(
                chromosome=1, start=1110, end=1111, ref_genome="GRCh38", name=rx.name
            ),
        ]
        rx_list = [
            Region(chromosome=1, start=121, end=1010, ref_genome="GRCh38"),
        ]
        assert list(rx.intersect(rx_list)) == [
            Region(
                chromosome=1, start=121, end=1010, ref_genome="GRCh38", name=rx.name
            ),
        ]

    def test_slop(self):
        rx = Region(chromosome=1, start=111, end=1111, ref_genome="GRCh38")
        assert rx.slop(11, 1) == Region(
            chromosome=1, start=100, end=1112, ref_genome="GRCh38", name=rx.name
        )
