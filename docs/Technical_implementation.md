# Technical implementation

This document provides a high-level overview of the technical implementation of the Gene Panel Builder (GPB).

## Libraries and dependencies

GPB is a web-based application that uses [React.js](https://reactjs.org/) as front end framework and [FASTAPI](fastapi.tiangolo.com/) as backend API framework. The backend uses a [PostgreSQL](https://www.postgresql.org/) database which it accesses via the [SQLModel](https://sqlmodel.tiangolo.com/) library.

## Data sources and database

A database is used to store panel designs, as well as the necessary reference data. The reference data include:

- Gene data from HGNC (genenames.org)
- Transcripts from NCBI RefSeq (including RefSeq and MANE Select status as in GRCh38)
- Inheritance and phenotype data from OMIM
- Panel definitions from Genomics England's PanelApp

The panels are stored as relationships between PanelApp panels, genes, excluded genes, and custom regions. The excluded genes are genes that should not be part of an exported panel, these genes are typically part of a PanelApp panel used when creating the panel, and/or on a wait-and-see "watchlist".

The database design can be found in the SQLModel-inheriting python classes under `gpbuilder/gpbuilder/datamodel/`.

![Database schema](img/db_schema.png)

## Gene panel states

A panel is first created from scratch. After the user enters a name/description and clicks `Next`, the panel gets a draft version and is persisted to the DB. The panel can then later be re-opened for further editing. When the user is happy with the panel and clicks `Submit`, the version is bumped and the draft status is removed.

## Communication between frontend and backend

### Creating a panel

The diagram below shows the communication between front end and backend when a user creates a new gene panel. The endpoint paths actually used are all prefixed with `/api/v1/`.

```mermaid
sequenceDiagram
actor User
participant fe as frontend
participant be as backend
link fe: Link to api.tsx @ https://gitlab.com/alleles/genepanel-builder/-/blob/dev/frontend/frontend/api.tsx
link be: Link to genepanels.py @ https://gitlab.com/alleles/genepanel-builder/-/blob/dev/gpbuilder/gpbuilder/api/genepanels.py

User->>fe: Navigate to front page
fe->>be: GET genepanels/
be-->>fe: list of all genepanels (last version)

User->>fe: Click "New Gene panel"
fe->>fe: GET configure/name.js
Note over User: Enter name, shortname and description
User->>fe: Click "Next (Select)"
fe->>be: Create a new panel....
be-->>fe: Panel ID
fe->>fe: GET configure/select.js
Note over fe: (Not shown: The Select page calls panelapp, gene, and region endpoints)

User->>fe: Click "Next (Edit)"

fe->>be: POST genepanels/select/ (PApanels, genes, regions)
be-->>fe: table data (genes, regions, coverage, etc)
fe->>fe: GET configure/edit.js
Note over User: View/edit table
Note over fe: (Not shown: The Edit page can call coverage and segdup endpoints)

opt User saves before submitting
User->>fe: Click "Save"
fe->>be: PUT genepanels/configure/
end

User->>fe: Click "Submit"
fe->>be: POST genepanels/{id}/submit/
be-->>fe: Front page (index.js)

```

The API docs are available at <http://genepanelbuilder.local:8080/docs>. In brief:

- When pressing `New Gene panel`, the browser loads _name.js_ for entering name/description and thereafter _select.js_ where selection is done.
- When the user selects Panelapp panels, genes and regions for a panel and presses `Next (Edit)`, the _genepanels/select endpoint_ is called with these data and returns genes with inheritance, transcripts (complete with coverage and segmental duplication statistics), as well as regions (also complete with coverage and segmental duplication statistics).
- When viewing and editing the resulting data table of genes/transcripts/regions etc. and clicking `Submit`, the _genepanels/{id}/submit/_ endpoint is called.
- On clicking `Save` or navigating between the sections, the gene panel is saved (_genepanels/configure_)

## Gene panel representation

The GPB app stores gene panels as designs rather than actual collections of genomic regions. In GPB a gene panel consists of:

- Genomics England PanelApp panels
- Custom individual genes
- Custom genomic regions

In addition, users can modify the default gene attributes (e.g., transcript, inheritance, etc.) for each gene in the panel or exclude genes entirely. These modifications are also stored as part of the panel design.

For example, a gene panel design may consist of:

- PanelApp panels
  - ["Brain channelopathy" (ID=90)](https://panelapp.genomicsengland.co.uk/panels/90/)
  - ["Intracerebral calcification disorders" (ID=315)](https://panelapp.genomicsengland.co.uk/panels/315/)
- Individual genes
  - [CACNA1C (HGNC:1390)](https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:1390)
- Genomic regions
  - [chr11:123456-1234567](https://www.ncbi.nlm.nih.gov/genome/gdv/browser/genome/?id=GCF_000001405.25)
- Modifications such as:
  - A gene exclusion
    - [GLRA1 (HGNC:4326)](https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/HGNC:4326)
  - A custom transcript for gene ATN1
    - [NM_001007026.2](https://www.ncbi.nlm.nih.gov/nuccore/NM_001007026.2)
  - A custom mode of inheritance (AD) for gene SPR

The advantage of using this representation instead of storing a collection of genomic regions directly, is that it allows us to automatically accommodate updates of the underlying data sources (e.g., PanelApp panels, HGNC approved gene names, OMIM modes of inheritance).

For instance, if a gene is added to a PanelApp panel, the gene panel builder will automatically update the gene panel to include the new gene. Similarly, if a gene is removed from a PanelApp panel, the gene panel builder will automatically update the gene panel to exclude the gene. Note that any modifications performed by the user to a gene's attributes will be upheld.

### From design to data

In order to enable the full translation of the gene panel design into genomic regions, the gene panel tables are linked to external data source tables coinhabiting the same database (see [database schema](Technical_implementation.md#data-sources-and-database)). When a gene panel is exported, the gene panel builder will query the database to retrieve the genomic regions corresponding to the gene panel design. The genomic regions are then exported in standard formats (BED, TSV).
