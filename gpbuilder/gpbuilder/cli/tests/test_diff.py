from gpbuilder.cli.diff.utils import (
    ChangeType,
    DiffableTranscriptRecord,
    DiffRowRecord,
    _to_diffable_records,
    compute_diff,
    diff_to_rows,
)
from gpbuilder.datamodel.utils.records import TranscriptRecord


def make_tx_record(**overrides):
    defaults = {
        "chromosome": 1,
        "start": 1,
        "end": 1,
        "name": "TX_NAME",
        "score": 0.0,
        "strand": "+",
        "tags": None,
        "hgnc_id": None,
        "hgnc_symbol": None,
        "inheritance": None,
        "coding_start": None,
        "coding_end": None,
        "exon_starts": None,
        "exon_ends": None,
        "metadata": None,
    }
    defaults.update(overrides)
    return TranscriptRecord(**defaults)


def test_extract_diffable_records():
    "Test that diffable records are extracted and merged correctly from TranscriptRecords"
    tx_records = {
        # Should merge these two with name = "NM_001 | NM_002"
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001"),
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_002"),
        # Should merge these two with inheritance = "AD | AR"
        make_tx_record(hgnc_id=2, hgnc_symbol="B", name="NM_003", inheritance="AD"),
        make_tx_record(hgnc_id=2, hgnc_symbol="B", name="NM_003", inheritance="AR"),
        # Should not merge these two because of different hgnc_id
        make_tx_record(hgnc_id=3, hgnc_symbol="C", name="foo", inheritance=""),
        make_tx_record(hgnc_id=4, hgnc_symbol="C", name="bar", inheritance=""),
        # This is the case for transcript records coming from custom regions
        make_tx_record(hgnc_id=None, hgnc_symbol=None, name="asd", inheritance="AD"),
    }
    diffable_records = _to_diffable_records(tx_records)
    assert len(diffable_records) == 5

    assert (
        DiffableTranscriptRecord(
            hgnc_id=1, hgnc_symbol="A", name="NM_001 | NM_002", inheritance=""
        )
        in diffable_records
    )

    assert (
        DiffableTranscriptRecord(
            hgnc_id=2, hgnc_symbol="B", name="NM_003", inheritance="AD | AR"
        )
        in diffable_records
    )

    assert (
        DiffableTranscriptRecord(hgnc_id=3, hgnc_symbol="C", name="foo", inheritance="")
        in diffable_records
    )

    assert (
        DiffableTranscriptRecord(hgnc_id=4, hgnc_symbol="C", name="bar", inheritance="")
        in diffable_records
    )

    assert (
        DiffableTranscriptRecord(
            hgnc_id=None, hgnc_symbol=None, name="asd", inheritance="AD"
        )
        in diffable_records
    )


def test_no_diff():
    "Test that no diff is registered when there is no difference"
    tx_a = {make_tx_record(hgnc_id=1, hgnc_symbol="A")}
    tx_b = {make_tx_record(hgnc_id=1, hgnc_symbol="A")}

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert not diff
    assert no_diff == {DiffableTranscriptRecord.parse_obj(tx) for tx in tx_a}

    rows = diff_to_rows(diff)
    assert not rows


def test_diff_added():
    "Test that added genes are registered in diff"
    common_tx = {make_tx_record(hgnc_id=1, hgnc_symbol="A")}
    added_tx = {
        make_tx_record(hgnc_id=2, hgnc_symbol="B", name="NM_001", inheritance="AD")
    }
    tx_a = common_tx
    tx_b = common_tx | added_tx

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert diff == {
        DiffableTranscriptRecord.parse_obj(set(added_tx).pop()): {
            (ChangeType.added, None)
        }
    }
    assert no_diff == {DiffableTranscriptRecord.parse_obj(set(common_tx).pop())}

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="2",
            gene_symbol_new="B",
            gene_symbol_old="",
            transcript_new="NM_001",
            transcript_old="",
            inheritance_new="AD",
            inheritance_old="",
            change_type=ChangeType.added.value,
        )
    ]


def test_diff_removed():
    "Test that removed genes are registered in diff"
    common_tx = {make_tx_record(hgnc_id=1, hgnc_symbol="A")}
    removed_tx = {
        make_tx_record(hgnc_id=2, hgnc_symbol="B", name="NM_001", inheritance="AD")
    }
    tx_a = common_tx | removed_tx
    tx_b = common_tx

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert diff == {
        DiffableTranscriptRecord.parse_obj(set(removed_tx).pop()): {
            (ChangeType.removed, None)
        }
    }
    assert no_diff == {DiffableTranscriptRecord.parse_obj(set(common_tx).pop())}

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="2",
            gene_symbol_new="",
            gene_symbol_old="B",
            transcript_new="",
            transcript_old="NM_001",
            inheritance_new="",
            inheritance_old="AD",
            change_type=ChangeType.removed.value,
        )
    ]


def test_diff_inheritance():
    "Test change of inheritance"
    tx_a = {make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001", inheritance="AD")}
    tx_b = {make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001", inheritance="AR")}

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert diff == {
        DiffableTranscriptRecord.parse_obj(set(tx_b).pop()): {
            (ChangeType.inheritance, "AD")
        }
    }
    assert not no_diff

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="1",
            gene_symbol_new="A",
            gene_symbol_old="",
            transcript_new="",
            transcript_old="",
            inheritance_new="AR",
            inheritance_old="AD",
            change_type=ChangeType.inheritance.value,
        )
    ]


def test_diff_transcript():
    "Test change of transcript"
    tx_a = {make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001", inheritance="AD")}
    tx_b = {make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_002", inheritance="AD")}

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert diff == {
        DiffableTranscriptRecord.parse_obj(set(tx_b).pop()): {
            (ChangeType.transcript, "NM_001")
        }
    }
    assert not no_diff

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="1",
            gene_symbol_new="A",
            gene_symbol_old="",
            transcript_new="NM_002",
            transcript_old="NM_001",
            inheritance_new="",
            inheritance_old="",
            change_type=ChangeType.transcript.value,
        )
    ]


def test_diff_multiple_transcripts():
    "Test that change of one of multiple transcripts is registered in one row"
    tx_a = {
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001", inheritance="AD"),
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_002", inheritance="AD"),
    }
    tx_b = {
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_002", inheritance="AD"),
        make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_003", inheritance="AD"),
    }

    diff, no_diff = compute_diff(tx_a, tx_b)
    assert diff == {
        DiffableTranscriptRecord(
            hgnc_id=1, hgnc_symbol="A", name="NM_002 | NM_003", inheritance="AD"
        ): {(ChangeType.transcript, "NM_001 | NM_002")}
    }
    assert not no_diff

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="1",
            gene_symbol_new="A",
            gene_symbol_old="",
            transcript_new="NM_002 | NM_003",
            transcript_old="NM_001 | NM_002",
            inheritance_new="",
            inheritance_old="",
            change_type=ChangeType.transcript.value,
        )
    ]


def test_diff_multiple_changes():
    "Test that multiple changes are merged into one diff row"
    tx_a = {make_tx_record(hgnc_id=1, hgnc_symbol="A", name="NM_001", inheritance="AD")}
    tx_b = {
        make_tx_record(hgnc_id=1, hgnc_symbol="B", name="NM_002", inheritance="AR"),
        make_tx_record(hgnc_id=1, hgnc_symbol="B", name="NM_003", inheritance="AR"),
    }

    diff, no_diff = compute_diff(tx_a, tx_b)

    assert diff == {
        DiffableTranscriptRecord(
            hgnc_id=1, hgnc_symbol="B", name="NM_002 | NM_003", inheritance="AR"
        ): {
            (ChangeType.symbol, "A"),
            (ChangeType.transcript, "NM_001"),
            (ChangeType.inheritance, "AD"),
        }
    }
    assert not no_diff

    rows = diff_to_rows(diff)
    assert rows == [
        DiffRowRecord(
            hgnc_id="1",
            gene_symbol_new="B",
            gene_symbol_old="A",
            transcript_new="NM_002 | NM_003",
            transcript_old="NM_001",
            inheritance_new="AR",
            inheritance_old="AD",
            change_type=f"{ChangeType.symbol.value} | {ChangeType.inheritance.value} | {ChangeType.transcript.value}",
        )
    ]
