# 1. Create a GeneDefaults table with columns for hgnc id, inheritance and transcript
# 2. Populate the GeneDefaults table using a file of gene defaults
# 3. Add to CLI

import logging
from pathlib import Path
from typing import Iterator

from sqlmodel import Session, col, delete, func, select, text

from gpbuilder.datamodel.engine import get_session
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.genedefaults import GeneDefaults, GeneDefaultsTranscript
from gpbuilder.datamodel.transcript import Transcript

logger = logging.getLogger(__name__)


def read_gene_defaults(
    session: Session, filename: Path, include_no_defaults: bool = False
) -> Iterator[GeneDefaults]:
    """Read gene defaults from a file

    Expected format:
    <hgnc_id>\t<symbol (not used)>\t<inheritance>\t<transcript1>,<transcript2>,...

    If inheritance and transcripts are empty, the gene will be ignored, unless include_no_defaults
    is set to True.
    """
    with filename.open() as f:
        for line in f:
            hgnc_id, _, inheritance, tx_names = line.strip("\n").split("\t")[:4]
            tx_names = [x.strip() for x in tx_names.split(",") if x]
            transcripts = session.exec(
                select(Transcript).where(col(Transcript.name).in_(tx_names))
            ).all()
            if len(tx_names) != len(transcripts):
                logger.warning(
                    f"Not all transcripts found for gene {hgnc_id}: "
                    f"Ignoring transcript(s) {set(tx_names) - set([x.name for x in transcripts])}."
                )
            if not inheritance and not transcripts:
                if not include_no_defaults:
                    continue
                # Use construct to avoid validation which would otherwise fail because both
                # inheritance and transcripts are empty
                yield GeneDefaults.construct(
                    hgnc_id=int(hgnc_id),
                    transcripts=[],
                    inheritance=None,
                )
            else:
                yield GeneDefaults(
                    hgnc_id=int(hgnc_id),
                    inheritance=inheritance,
                    transcripts=transcripts,
                )


def update_gene_defaults(session: Session, filename: Path) -> None:
    """Update gene defaults from a file"""
    statement = text("TRUNCATE TABLE genedefaults, genedefaultstranscript")
    session.connection().execute(statement)

    for gene_defaults in read_gene_defaults(session, filename):
        session.add(gene_defaults)

    session.flush()

    count = session.exec(select(func.count(GeneDefaults.id))).one()  # type: ignore
    logger.info(f"Updated gene defaults with default values for {count} genes")


def clean_gene_defaults_file(filename: Path, output_filename: Path) -> None:
    """
    Clean gene defaults file

    Will remove inheritance and transcripts if they match the fallback inheritance and transcripts,
    and write new file to output_filename
    """

    with get_session() as session, output_filename.open("w") as f:
        all_hgnc_ids = session.exec(select(Gene.hgnc_id)).all()
        symbols_dict = Gene.symbol_by_hgnc_ids(session)

        # Delete existing gene defaults (in session only, will be rolled back)
        # We only want to check against what is provided in the _absence_ of custom gene defaults
        session.exec(delete(GeneDefaultsTranscript))
        session.exec(delete(GeneDefaults))

        default_transcripts = Gene.get_default_transcripts_by_hgnc_ids(
            session, all_hgnc_ids
        )

        default_inheritance = Gene.get_default_inheritance_by_hgnc_ids(
            session, all_hgnc_ids
        )

        for gene_defaults in read_gene_defaults(
            session, filename, include_no_defaults=True
        ):
            try:
                gene_symbol = symbols_dict[gene_defaults.hgnc_id]
            except KeyError:
                logger.warning(
                    f"Gene {gene_defaults.hgnc_id} not found in database, ignoring"
                )
                continue

            if gene_defaults.inheritance:
                if (
                    default_inheritance[gene_defaults.hgnc_id]
                    == gene_defaults.inheritance
                ):
                    gene_defaults.inheritance = None
                    logger.info(
                        f"The inheritance for gene {gene_symbol} [HGNC:{gene_defaults.hgnc_id}]"
                        " matches its fallback inheritance, so it will be ignored"
                    )

            if gene_defaults.transcripts:
                fallback_transcript_ids = set(
                    [tx.id for tx in default_transcripts[gene_defaults.hgnc_id]]
                )
                transcript_ids = set([tx.id for tx in gene_defaults.transcripts])
                if fallback_transcript_ids == transcript_ids:
                    gene_defaults.transcripts = []
                    logger.info(
                        f"The transcripts for gene {gene_symbol} [HGNC:{gene_defaults.hgnc_id}]"
                        " match its fallback transcripts, so they will be ignored"
                    )

            # Only write to file if inheritance or transcripts are set
            if gene_defaults.inheritance or gene_defaults.transcripts:
                tx_name_output = ",".join([x.name for x in gene_defaults.transcripts])
                inheritance_output = gene_defaults.inheritance or ""
                default_record = [
                    gene_defaults.hgnc_id,
                    gene_symbol,
                    inheritance_output,
                    tx_name_output,
                ]
                f.write("\t".join(default_record))
            else:
                logger.info(
                    f"Gene {gene_symbol} [HGNC:{gene_defaults.hgnc_id}] has no defaults, ignoring"
                )

        session.rollback()
