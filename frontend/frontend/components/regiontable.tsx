import {
  CellContext,
  ColumnDef,
  createColumnHelper,
  flexRender,
  Row,
} from '@tanstack/react-table'
import React from 'react'

import Select from 'components/select'
import Table, { fuzzySort } from 'components/table'
import TextArea from 'components/textarea'
import { GenePanelConfig, Inheritance } from 'types'

type RegionConfig = GenePanelConfig['regions'][number]

export interface RegionTableItem extends RegionConfig {
  coverageWGS: number
  coverageWES: number
  segDup: number
  region: string
}

export default function RegionTable({
  regions,
  onRegionsUpdated,
  loading = false,
  readOnly = false,
  disabled = false,
  setDirty = () => {},
}: {
  regions: RegionTableItem[]
  onRegionsUpdated: (row: number, column: string, value: any) => void
  loading?: boolean
  readOnly?: boolean
  disabled?: boolean
  setDirty?: () => void
}): any {
  const InheritanceRenderer = function (
    info: CellContext<RegionTableItem, Inheritance>
  ) {
    const current = info.getValue()
    const onChange = (value: string) => {
      onRegionsUpdated(info.row.index, info.column.id, value)
      setDirty()
    }

    if (readOnly) {
      return <span>{current}</span>
    }
    return (
      <Select
        name={`inheritance-region-${info.row.index}`}
        multiple={false}
        disabled={disabled}
        options={Object.values(Inheritance).map((inheritance_mode) => {
          return {
            name: inheritance_mode,
            value: inheritance_mode,
            label: inheritance_mode,
            selected: current === inheritance_mode,
          }
        })}
        onChange={onChange}
      />
    )
  }

  const TextFieldRenderer = function (
    info: CellContext<RegionTableItem, string>,
    namePrefix: string = ''
  ) {
    const initialValue = info.getValue()
    const [value, setValue] = React.useState(initialValue)

    if (readOnly) {
      return <span>{value}</span>
    }

    const onBlur = () => {
      onRegionsUpdated(info.row.index, info.column.id, value)
      setDirty()
    }

    const onChange = (v: string) => {
      setValue(v)
      setDirty()
    }

    return (
      <TextArea
        name={`${namePrefix}-${info.row.index}`}
        disabled={disabled}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        maxRows={10}
      />
    )
  }

  const columnHelper = createColumnHelper<RegionTableItem>()
  const columns = React.useMemo<ColumnDef<RegionTableItem>[]>(
    () => [
      columnHelper.accessor('name', {
        header: 'Name',
        filterFn: 'fuzzy',
        sortingFn: fuzzySort,
        cell: (info) => TextFieldRenderer(info, 'region-name'),
        size: 300,
      }),
      columnHelper.accessor('region', {
        header: 'Region',
        filterFn: 'fuzzy',
        size: 240,
      }),
      columnHelper.accessor('inheritance', {
        header: 'Inheritance',
        cell: (info) =>
          InheritanceRenderer(
            info as CellContext<RegionTableItem, Inheritance>
          ),
        enableSorting: true,
        enableColumnFilter: true,
        size: 120,
      }),
      columnHelper.accessor('coverageWGS', {
        cell: (info) => info.getValue().toPrecision(3),
        header: 'Coverage WGS',
        size: 120,
      }),
      columnHelper.accessor('coverageWES', {
        cell: (info) => info.getValue().toPrecision(3),
        header: 'Coverage WES',
        size: 120,
      }),
      columnHelper.accessor('segDup', {
        cell: (info) => info.getValue().toPrecision(3),
        header: 'SegDup',
        size: 120,
      }),
      columnHelper.accessor('comment', {
        header: 'Comment',
        cell: (info) => TextFieldRenderer(info, 'region-comment'),
        size: 390,
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [regions, disabled, readOnly] // data is sufficient, because all renderers only depend on this
  )

  const rowRenderer = (row: Row<RegionTableItem>) => {
    return (
      <tr key={row.id}>
        {row.getVisibleCells().map((cell) => (
          <td
            className={`px-2 ${readOnly ? 'pb-3 align-top' : ''} text-sm`}
            key={cell.id}
          >
            {flexRender(cell.column.columnDef.cell, cell.getContext())}
          </td>
        ))}
      </tr>
    )
  }

  return (
    <Table
      columns={columns}
      rows={regions}
      rowRenderer={rowRenderer}
      loading={loading}
    ></Table>
  )
}
