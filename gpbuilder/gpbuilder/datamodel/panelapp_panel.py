from __future__ import annotations

from datetime import datetime
from typing import List, Optional

from sqlalchemy import PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import object_session
from sqlalchemy.sql.schema import Column
from sqlmodel import DateTime, Field, Integer, Relationship, Session, Text, col, select
from sqlmodel.sql.expression import SelectOfScalar

from gpbuilder.datamodel.base import SQLModelBase
from gpbuilder.datamodel.crud.models import ConfidenceLevel
from gpbuilder.datamodel.gene import Gene
from gpbuilder.datamodel.region import Region


class PanelAppPanelGene(SQLModelBase, table=True):
    hgnc_id: int = Field(foreign_key="gene.hgnc_id")
    panelapp_panel_id: int = Field(foreign_key="panelapppanel.id")
    confidence_level: int
    gene: Gene = Relationship(
        sa_relationship_kwargs={"lazy": "joined", "viewonly": True}
    )

    __table_args__ = (
        PrimaryKeyConstraint("hgnc_id", "panelapp_panel_id", name="pk_pagenes"),
    )


class PanelAppPanelRegion(SQLModelBase, table=True):
    region_id: int = Field(foreign_key="region.id")
    panelapp_panel_id: int = Field(foreign_key="panelapppanel.id")
    confidence_level: int

    __table_args__ = (PrimaryKeyConstraint("region_id", "panelapp_panel_id"),)


class PanelAppPanel(SQLModelBase, table=True):
    id: int = Field(sa_column=Column(Integer, primary_key=True, autoincrement=False))
    name: str
    version: str
    version_created: datetime = Field(sa_column=Column(DateTime(timezone=True)))
    disease_group: str
    disease_sub_group: str
    relevant_disorders: List[str] = Field(
        default=None, sa_column=Column(postgresql.ARRAY(Text()))
    )
    update_id: Optional[int] = Field(default=None, foreign_key="update.id")

    genes: List["Gene"] = Relationship(
        link_model=PanelAppPanelGene, sa_relationship_kwargs={"lazy": "subquery"}
    )
    regions: List["Region"] = Relationship(
        link_model=PanelAppPanelRegion, sa_relationship_kwargs={"lazy": "subquery"}
    )

    __table_args__ = (UniqueConstraint("id", "version", name="panelapp_id_version"),)

    @classmethod
    def from_panelapp_ids(
        cls, session: Session, panelapp_ids: List[int], ensure_all: bool = True
    ) -> List["PanelAppPanel"]:
        res = session.query(cls).filter(col(cls.id).in_(panelapp_ids)).all()
        if ensure_all and len(res) != len(panelapp_ids):
            raise ValueError(
                f"Could not find all panelapp panels in database: {panelapp_ids}"
            )
        return res

    def _genes_with_confidence_level_query(
        self, confidence_level: ConfidenceLevel
    ) -> SelectOfScalar[PanelAppPanelGene]:
        return select(PanelAppPanelGene).where(
            PanelAppPanelGene.panelapp_panel_id == self.id,
            PanelAppPanelGene.confidence_level == confidence_level,
        )

    def get_genes_from_confidence_levels(
        self, session: Session, confidence_levels: dict[ConfidenceLevel, bool]
    ) -> list[PanelAppPanelGene]:
        genes = []
        if confidence_levels.get(ConfidenceLevel.green):
            genes.extend(self.green_genes)
        if confidence_levels.get(ConfidenceLevel.amber):
            genes.extend(self.amber_genes)
        if confidence_levels.get(ConfidenceLevel.red):
            genes.extend(self.red_genes)
        return genes

    @property
    def red_genes(self) -> list[PanelAppPanelGene]:
        session = object_session(self)
        assert type(session) is Session
        return session.exec(
            self._genes_with_confidence_level_query(ConfidenceLevel.red)
        ).all()

    @property
    def green_genes(self) -> list[PanelAppPanelGene]:
        session = object_session(self)
        assert type(session) is Session
        return session.exec(
            self._genes_with_confidence_level_query(ConfidenceLevel.green)
        ).all()

    @property
    def amber_genes(self) -> list[PanelAppPanelGene]:
        session = object_session(self)
        assert type(session) is Session
        return session.exec(
            self._genes_with_confidence_level_query(ConfidenceLevel.amber)
        ).all()
