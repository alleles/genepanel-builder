import Button from 'components/button'
import useGenePanel from 'components/genepanelcontext'
import Input from 'components/input'
import Layout from 'components/layouts/configure'
import DefaultLayout from 'components/layouts/default'
import Spinner from 'components/spinner'
import TextArea from 'components/textarea'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react'
import { GenePanelConfig } from 'types'

export default function NamePanel() {
  const router = useRouter()
  const { genePanelConfig, savePanel, setDirty } = useGenePanel()
  const [tempName, setTempName] = useState('')
  const [tempDescription, setTempDescription] = useState('')
  const [tempShortname, setTempShortname] = useState('')
  const [error, setError] = useState('')
  const nameInputRef = useRef<HTMLInputElement>(null)
  const shortnameInputRef = useRef<HTMLInputElement>(null)

  const { id } = router.query
  const loading = id && !genePanelConfig.id

  useEffect(() => {
    setTempName(genePanelConfig.name)
  }, [genePanelConfig.name])

  useEffect(() => {
    setTempShortname(genePanelConfig.shortname)
  }, [genePanelConfig.shortname])

  useEffect(() => {
    setTempDescription(genePanelConfig.description)
  }, [genePanelConfig.description])

  const onSubmit = (ev) => {
    ev.preventDefault()

    if (nameInputRef.current.validity.patternMismatch) {
      nameInputRef.current.reportValidity()
      nameInputRef.current.focus()
      return
    } else {
      nameInputRef.current.setCustomValidity('')
    }
    const updatedGenePanel: Partial<GenePanelConfig> = {
      name: tempName,
      shortname: tempShortname,
      description: tempDescription,
    }
    savePanel(updatedGenePanel)
      .then((id) => {
        router.push(id ? `/configure/select?id=${id}` : '/configure/select')
      })
      .catch((err) => {
        console.error(err)
        if (err.response && err.response.status === 409) {
          const detail = err.response.data.detail
          if (detail === 'shortname-already-exists') {
            shortnameInputRef.current.setCustomValidity(
              'A panel with this short name already exists. Please choose another short name.'
            )
            shortnameInputRef.current.reportValidity()
            shortnameInputRef.current.focus()
          }
        } else {
          setError('An error occurred while creating the panel.')
        }
      })
  }

  useEffect(() => {
    if (nameInputRef.current) {
      nameInputRef.current.focus()
    }
  }, [])

  function handleNameChange(value: string): void {
    setTempName(value)
    setDirty()
  }

  function handleShortnameChange(value: string): void {
    setTempShortname(value)
    setDirty()
    if (shortnameInputRef.current.validity.patternMismatch) {
      shortnameInputRef.current.setCustomValidity(
        'Short name can only contain 2-12 latin letters, no spaces.'
      )
      shortnameInputRef.current.reportValidity()
      shortnameInputRef.current.focus()
    } else {
      shortnameInputRef.current.setCustomValidity('')
    }
  }

  function handleDescriptionChange(value: string): void {
    setTempDescription(value)
    setDirty()
  }

  return (
    <form className="m-auto h-screen w-full" onSubmit={onSubmit}>
      <div className="sticky top-0 z-30 flex w-full flex-row items-center justify-between bg-ellablue-lighter py-2 px-8">
        <h2 className="text-lg font-medium text-gray-700">
          Create or modify a gene panel
        </h2>
        <div className="text-sm text-gray-500">
          Provide a name and short name for the panel, and optionally a brief
          description.
        </div>

        <div className="flex gap-x-2">
          <div className="w-40">
            <Button onClick={() => router.push('/')}>Cancel</Button>
          </div>

          <div className="ml-auto w-40 justify-end">
            <Button disabled={loading} color="confirm" type="submit">
              Continue (Select)
            </Button>
          </div>
        </div>
      </div>
      <div className="h-[calc(100vh-89px)] overflow-x-hidden overflow-y-scroll">
        {(loading && (
          <div className="flex h-full pt-28">
            <Spinner />
          </div>
        )) || (
          <div className="m-auto space-y-6 pb-8 pl-8 pr-7 2xl:w-1/2">
            <div className="mt-12">
              {error ? <span className=" text-red-500">{error}</span> : null}
            </div>
            <div className="grid grid-cols-7 items-start gap-4">
              <label
                htmlFor="name"
                className="col-span-2 block pt-2 font-medium text-gray-700"
              >
                Panel name <span style={{ fontWeight: '300' }}>(required)</span>
              </label>
              <div className="col-span-5 w-full">
                <Input
                  name="name"
                  placeholder="Enter name"
                  ref={nameInputRef}
                  value={tempName}
                  onChange={handleNameChange}
                  required
                />
              </div>
            </div>

            <div className="grid grid-cols-7 items-start gap-4 pt-5">
              <label
                htmlFor="shortname"
                className="col-span-2 block pt-2 font-medium text-gray-700 "
              >
                Short name <span style={{ fontWeight: '300' }}>(required)</span>
              </label>
              <div className="col-span-5 w-full">
                <Input
                  name="short-name"
                  ref={shortnameInputRef}
                  value={tempShortname}
                  placeholder="Enter short name"
                  onChange={handleShortnameChange}
                  maxLength={12}
                  pattern="[a-zA-Z0-9]+"
                  required
                />
              </div>
            </div>

            <div className="grid grid-cols-7 items-start gap-4 pt-5">
              <label
                htmlFor="description"
                className="col-span-2 block pt-2 font-medium text-gray-700"
              >
                Panel description
              </label>
              <div className="col-span-5 w-full">
                <TextArea
                  name="description"
                  placeholder="Enter description"
                  value={tempDescription}
                  onChange={handleDescriptionChange}
                  rows={5}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </form>
  )
}

NamePanel.getLayout = (page) => (
  <DefaultLayout>
    <Layout>{page}</Layout>
  </DefaultLayout>
)
