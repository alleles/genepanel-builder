import pytest
import semver
from sqlmodel import select, update

from gpbuilder.api.tests.mock_data import (
    create_genes,
    create_panelapp_panels,
    create_panels,
    create_regions,
    link_genepanel_excluded_gene,
    link_genepanel_gene,
    link_genepanel_panelapp_panel,
    link_genepanel_region,
    link_panelapp_panel_gene,
)
from gpbuilder.datamodel.crud.genepanel import (
    CreatePanelException,
    DeletePanelException,
    ShortnameExistsException,
    VersionException,
    create_from_panel,
    create_panel,
    delete_panel,
    release_panel,
    update_panel,
)
from gpbuilder.datamodel.crud.models import (
    ConfidenceLevel,
    GenePanelConfig,
    GenePanelConfigGene,
    PanelAppInput,
)
from gpbuilder.datamodel.genepanel import GenePanel, GenePanelGene
from gpbuilder.datamodel.panelapp_panel import PanelAppPanel


def test_create_panel(session):
    # Test that a new panel is created
    genepanel = create_panel("name", "shortname", "description")
    assert genepanel.name == "name"
    assert genepanel.shortname == "shortname"
    assert genepanel.description == "description"
    assert genepanel.version == "0.0.1-draft"

    # Test that shortname is not already used works
    session.add(genepanel)
    session.commit()
    with pytest.raises(ShortnameExistsException):
        create_panel("name", "shortname", "description")


def test_create_from_panel(session):
    # Wrong ID
    with pytest.raises(CreatePanelException):
        create_from_panel(session, -1)

    (genepanel,) = create_panels(1, session)
    (panelapp_panel,) = create_panelapp_panels(1, session)
    (gene_included, gene_excluded) = create_genes(2, session)
    (region,) = create_regions(1, session)

    assert genepanel.version.endswith("draft")

    session.add_all([genepanel, panelapp_panel, gene_included, gene_excluded, region])
    session.flush()
    (genepanel_panelapp_panel,) = link_genepanel_panelapp_panel(
        [panelapp_panel], genepanel, session
    )
    (genepanel_excluded_gene,) = link_genepanel_excluded_gene(
        [gene_excluded], genepanel, session
    )
    genepanel_included_gene = link_genepanel_gene(gene_included, genepanel, session)
    link_genepanel_region(region, genepanel, session)
    session.commit()

    # Update to non-draft, and try again
    session.exec(
        update(GenePanel).where(GenePanel.id == genepanel.id).values(version="1.0.0")
    )
    session.commit()

    # Check that everything is copied
    copied_genepanel = create_from_panel(session, genepanel.id)  # type: ignore
    assert copied_genepanel.name == genepanel.name
    assert copied_genepanel.shortname == genepanel.shortname
    assert copied_genepanel.description == genepanel.description
    assert copied_genepanel.version == "1.0.1-draft"

    # Test that genes, regions, excluded genes, panelapp panels are copied
    assert len(copied_genepanel.genes) == 1
    assert len(copied_genepanel.excluded_genes) == 1
    assert len(copied_genepanel.regions) == 1
    assert len(copied_genepanel.panelapp_panels) == 1

    # TODO: Test properties of the linked objects (e.g. confidence_level, comment etc)
    copied_genepanel_gene = session.exec(
        select(GenePanelGene).where(GenePanelGene.genepanel_id == copied_genepanel.id)
    ).all()
    assert len(copied_genepanel_gene) == 1
    assert copied_genepanel_gene[0].hgnc_id == genepanel_included_gene.hgnc_id
    assert copied_genepanel_gene[0].comment == genepanel_included_gene.comment
    assert copied_genepanel_gene[0].transcripts == genepanel_included_gene.transcripts


def test_delete_panel(session):
    (genepanel,) = create_panels(1, session)
    session.add(genepanel)
    session.commit()
    assert genepanel.id is not None
    delete_panel(session, genepanel.id)
    # Check that the panel is deleted
    assert session.get(GenePanel, genepanel.id) is None, "Panel was not deleted"

    # Check that function fails if panel id doesn't exist
    with pytest.raises(DeletePanelException) as e:
        delete_panel(session, genepanel.id)
        assert str(e) == "Panel does not exist"

    # Check that function fails if panel is not a draft
    (genepanel,) = create_panels(1, session)
    genepanel.version = "1.0.0"
    session.add(genepanel)
    session.commit()
    assert genepanel.id is not None
    with pytest.raises(DeletePanelException) as e:
        delete_panel(session, genepanel.id)
        assert str(e) == "Panel is not a draft"


@pytest.mark.parametrize(
    "from_version,to_version",
    [
        ("0.0.1-draft", "1.0.0"),
        ("0.0.1-draft", "0.1.0"),
        ("1.0.1-draft", "1.1.0"),
        ("1.0.5-draft", "2.0.0"),
    ],
)
def test_release_panel(session, from_version, to_version):
    # Create a panel and a draft panel
    # Release the panel, and check that the draft panel is deleted
    (genepanel, genepanel_draft) = create_panels(2, session)
    genepanel_draft.shortname = genepanel.shortname
    genepanel_draft.version = semver.bump_patch(from_version) + "-draft"
    genepanel.version = from_version
    session.add(genepanel)
    session.add(genepanel_draft)
    session.flush()

    # Release the panel
    assert genepanel.id is not None
    release_panel(session, genepanel, to_version)
    session.commit()

    # Check that the panel is released
    genepanel_after = session.get(GenePanel, genepanel.id)
    assert genepanel_after.version == to_version

    # Check that the draft panel is deleted
    assert session.get(GenePanel, genepanel_draft.id) is None


@pytest.mark.parametrize(
    "from_version,to_version",
    [
        ("0.0.1-draft", "0.0.1"),
        ("1.0.0", "1.1.0"),
        ("1.0.1-draft", "1.0.0"),
        ("1.0.5-draft", "3.0.0"),
        ("1.0.5-draft", "1.0.5-draft"),
        ("2.0.1-draft", "1.0.0"),
        ("2.0.1-draft", "2.1"),
    ],
)
def test_release_panel_fails(session, from_version, to_version):
    (genepanel,) = create_panels(1, session)
    genepanel.version = from_version
    session.add(genepanel)
    session.flush()
    with pytest.raises(VersionException):
        release_panel(session, genepanel, to_version)


@pytest.fixture(scope="function")
def genepanel(session) -> GenePanel:
    (genepanel,) = create_panels(1, session)
    session.add(genepanel)
    session.flush()

    return genepanel


@pytest.fixture(scope="function")
def panelapp_panel(session) -> PanelAppPanel:
    (panelapp_panel,) = create_panelapp_panels(1, session)
    (gene_green, gene_amber, gene_red) = create_genes(3, session)
    (panelapp_panel,) = create_panelapp_panels(1, session)
    link_panelapp_panel_gene(
        [gene_green], panelapp_panel, ConfidenceLevel.green, session
    )
    link_panelapp_panel_gene(
        [gene_amber], panelapp_panel, ConfidenceLevel.amber, session
    )
    link_panelapp_panel_gene([gene_red], panelapp_panel, ConfidenceLevel.red, session)
    session.add(panelapp_panel)
    session.flush()
    assert panelapp_panel.id is not None

    return panelapp_panel


def test_update_genepanel_diff_cleaned(session, genepanel: GenePanel):
    assert genepanel.id is not None
    (gene1, gene2, gene3) = create_genes(3, session)
    # Diff, but for genes that are not part of the panel - these should be ignored
    genepanel_config = GenePanelConfig(
        **{
            **genepanel.as_config().dict(),
            "diff": {
                gene1.hgnc_id: GenePanelConfigGene(comment="foo"),
                gene2.hgnc_id: GenePanelConfigGene(exclusion_reasons=["other"]),
            },
        }
    )
    updated_panel = update_panel(session, genepanel, genepanel_config)
    assert updated_panel.id == genepanel.id
    assert updated_panel.genes == []
    hgnc_ids = GenePanel.hgnc_ids_by_panel_id(session, [genepanel.id])[updated_panel.id]
    assert not hgnc_ids


@pytest.mark.parametrize(
    "confidence_levels",
    [
        {
            ConfidenceLevel.red: True,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.green: False,
        },
    ],
)
def test_update_genepanel_pa_panel(
    session,
    genepanel: GenePanel,
    panelapp_panel: PanelAppPanel,
    confidence_levels: dict[ConfidenceLevel, bool],
):
    assert genepanel.id is not None
    # Added PanelApp panel, but no diff
    genepanel_config = GenePanelConfig(
        **{
            **genepanel.as_config().dict(),
            "panelapp_panels": [
                PanelAppInput(
                    panelapp_id=panelapp_panel.id,
                    confidence_levels=confidence_levels,
                )
            ],
        }
    )
    updated_panel = update_panel(session, genepanel, genepanel_config)
    assert updated_panel.id == genepanel.id
    assert updated_panel.genes == []  # No diffs
    hgnc_ids = GenePanel.hgnc_ids_by_panel_id(session, [genepanel.id])[genepanel.id]
    expected_hgnc_ids = set()
    if confidence_levels[ConfidenceLevel.green]:
        expected_hgnc_ids.update(gene.hgnc_id for gene in panelapp_panel.green_genes)
    if confidence_levels[ConfidenceLevel.amber]:
        expected_hgnc_ids.update(gene.hgnc_id for gene in panelapp_panel.amber_genes)
    if confidence_levels[ConfidenceLevel.red]:
        expected_hgnc_ids.update(gene.hgnc_id for gene in panelapp_panel.red_genes)
    assert hgnc_ids == expected_hgnc_ids


@pytest.mark.parametrize(
    "confidence_levels",
    [
        {
            ConfidenceLevel.red: True,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: True,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.green: True,
        },
        {
            ConfidenceLevel.red: False,
            ConfidenceLevel.amber: False,
            ConfidenceLevel.green: False,
        },
    ],
)
def test_update_genepanel_pa_panel_with_diff(
    session,
    genepanel: GenePanel,
    panelapp_panel: PanelAppPanel,
    confidence_levels: dict[ConfidenceLevel, bool],
):
    assert genepanel.id is not None
    gene_green = panelapp_panel.green_genes[0]
    gene_amber = panelapp_panel.amber_genes[0]
    gene_red = panelapp_panel.red_genes[0]

    # Same, but with diff
    genepanel_config = GenePanelConfig(
        **{
            **genepanel.as_config().dict(),
            "panelapp_panels": [
                PanelAppInput(
                    panelapp_id=panelapp_panel.id,
                    confidence_levels=confidence_levels,
                )
            ],
            "diff": {
                gene_green.hgnc_id: GenePanelConfigGene(comment="foo"),
                gene_amber.hgnc_id: GenePanelConfigGene(exclusion_reasons=["other"]),
                gene_red.hgnc_id: GenePanelConfigGene(),  # Should never be included as GenePanelGene
            },
        }
    )
    updated_panel = update_panel(session, genepanel, genepanel_config)
    assert updated_panel.id == genepanel.id
    hgnc_ids = GenePanel.hgnc_ids_by_panel_id(session, [genepanel.id])[genepanel.id]
    expected_hgnc_ids = set()
    expected_genepanel_gene_hgnc_ids = set()
    expected_excluded_gene_hgnc_ids = set()
    if confidence_levels[ConfidenceLevel.green]:
        expected_hgnc_ids.add(gene_green.hgnc_id)
        expected_genepanel_gene_hgnc_ids.add(gene_green.hgnc_id)
    if confidence_levels[ConfidenceLevel.amber]:
        expected_excluded_gene_hgnc_ids.add(gene_amber.hgnc_id)
    if confidence_levels[ConfidenceLevel.red]:
        expected_hgnc_ids.add(gene_red.hgnc_id)

    assert hgnc_ids == expected_hgnc_ids
    assert (
        set([g.hgnc_id for g in updated_panel.genes])
        == expected_genepanel_gene_hgnc_ids
    )
    assert (
        set([g.hgnc_id for g in updated_panel.excluded_genes])
        == expected_excluded_gene_hgnc_ids
    )
