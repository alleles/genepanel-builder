"""
Module to parse NCBI RefSeq GFF files

RefSeq GFF files are available at:
https://ftp.ncbi.nlm.nih.gov//genomes/all/annotation_releases/9606/

Validated for release 105.20220307 against UCSC
https://api.genome.ucsc.edu/getData/track?genome=hg19;track=ncbiRefSeqCurated
"""

import gzip
import logging
import re
from pathlib import Path
from typing import Iterator

from pydantic import BaseModel, ValidationError

from gpbuilder.datamodel.transcript import Transcript
from gpbuilder.datamodel.utils.validators import chromosome_validator

logger = logging.getLogger(__name__)


class LineData(BaseModel):
    """Data model for a line in a GFF file"""

    seqid: str
    source: str
    type: str
    start: int
    end: int
    score: str
    strand: str
    phase: str
    attributes: dict


def parse_ncbi_refseq_gff(
    gff_file: Path, source: str | None, ref_genome: str, curated_only: bool = True
) -> Iterator[Transcript]:
    """Parse a NCBI RefSeq GFF file and yield transcripts"""

    if source is None:
        source = gff_file.name

    def split_attributes(attributes):
        "Split attributes column from GFF into a dict"
        x = dict([a.split("=") for a in attributes.split(";")])
        if "Dbxref" in x:
            x["Dbxref"] = dict(a.split(":", 1) for a in x["Dbxref"].split(","))
        return x

    def flush_transcripts(transcripts):
        if transcripts:
            current_chromosome = next(v for v in transcripts.values()).chromosome
            logger.info(
                f"Flushing {len(transcripts)} transcripts for chromosome {current_chromosome}"
            )
            for tx in transcripts.values():
                assert tx.chromosome == current_chromosome
                tx.exon_starts = sorted(tx.exon_starts)
                tx.exon_ends = sorted(tx.exon_ends)
                yield tx

    def extract_line_data(line):
        data = dict(zip(LineData.__fields__.keys(), line.rstrip("\n").split("\t")))
        data["attributes"] = split_attributes(data["attributes"])
        return LineData(**data)

    transcripts = {}

    with gzip.open(gff_file, "rt") as f:
        for l in f:
            # If we start a new chromosome, we flush the transcripts from the current chromosome
            # before continuing
            if l.startswith("#"):
                chromosome = ""
                m = re.match(r"##sequence-region ([^\s]+) ", l)
                if m:
                    logger.info(f"New sequence region detected: {m[1]}")
                    yield from flush_transcripts(transcripts)
                    transcripts = {}
                continue

            line_data = extract_line_data(l)
            if not line_data.seqid.startswith("NC_"):
                logger.debug(
                    f"Sequence ID {line_data.seqid} is not a reference sequence chromosome, skipping"
                )
                continue

            # Build transcript objects by looking at the third column in the GFF file (type)
            match line_data.type:  # noqa: E999
                case "region":
                    # Skip blocks that are not on a valid chromosome
                    try:
                        chromosome_str = line_data.attributes.get("Name", "")
                        chromosome = chromosome_validator(chromosome_str)
                    except ValueError:
                        logger.debug(f"'{chromosome_str}' is not a valid chromosome")
                    finally:
                        continue
                case "exon":
                    # Add exon to transcript
                    transcript_key = line_data.attributes["Parent"]
                    try:
                        exon_start, exon_end = line_data.start - 1, line_data.end
                        transcripts[transcript_key].exon_starts.append(exon_start)
                        transcripts[transcript_key].exon_ends.append(exon_end)
                    except KeyError:
                        logger.debug(f"Transcript {transcript_key} not found for exon")
                    continue
                case "CDS":
                    # Add CDS to transcript
                    transcript_key = line_data.attributes["Parent"]
                    try:
                        transcripts[transcript_key].coding_start = min(
                            transcripts[transcript_key].coding_start or float("inf"),
                            line_data.start - 1,
                        )
                        transcripts[transcript_key].coding_end = max(
                            transcripts[transcript_key].coding_end or 0,
                            line_data.end,
                        )
                    except KeyError:
                        logger.debug(f"Transcript {transcript_key} not found for CDS")
                    continue
                case _ as t:
                    # Try creating a Transcript object from the line
                    # There are different line types (e.g. gene, miRNA, str, enhancer, silencer).
                    # We will try creating a Transcript object from all of them, but we make sure
                    # we only create actual transcripts here. We will use the GFF IDs as transcript
                    # keys for to be able to add exons and CDS later.

                    # Mitochondrial genes do not have transcripts like the nuclear ones.
                    # We will instead create a transcript object for each gene.
                    if chromosome == "MT" and t == "gene":
                        hgnc_id = line_data.attributes["Dbxref"]["HGNC"].split(":")[1]
                        transcript_key = line_data.attributes["Name"]
                        is_coding = (
                            line_data.attributes["gene_biotype"] == "protein_coding"
                        )
                        tx = Transcript(
                            id=None,
                            name=transcript_key,
                            chromosome=chromosome,
                            strand=line_data.strand,  # type: ignore
                            hgnc_id=hgnc_id,  # type: ignore
                            source=source,
                            ref_genome=ref_genome,  # type: ignore
                            coding_start=line_data.start if is_coding else None,
                            coding_end=line_data.end if is_coding else None,
                            exon_starts=[line_data.start],
                            exon_ends=[line_data.end],
                            start=line_data.start,
                            end=line_data.end,
                        )
                        transcripts[transcript_key] = tx
                        logger.debug(f"Transcript {transcript_key} added")
                        continue

                    # Ignore transcripts without a name (probably not transcripts at all)
                    try:
                        transcript_name = line_data.attributes["Name"]
                    except KeyError:
                        logger.debug("No available 'Name' to create transcript")
                        continue

                    # Ignore transcripts without an ID (used as key in the transcript dictionary)
                    try:
                        transcript_key = line_data.attributes["ID"]
                    except KeyError:
                        logger.debug("No available 'ID' to index transcript")
                        continue

                    # Ignore anything without the correct prefix
                    prefix = transcript_name[:3]
                    curated_prefixes = ["NM_", "NR_"]
                    predicted_prefixes = ["XM_", "XR_"]

                    if (
                        curated_only
                        and prefix not in curated_prefixes
                        or (
                            not curated_only
                            and prefix not in curated_prefixes + predicted_prefixes
                        )
                    ):
                        logger.debug(f"Ignoring {transcript_name}")
                        continue

                    hgnc_id = (
                        line_data.attributes.get("Dbxref", {})
                        .get("HGNC", ":")
                        .split(":")[1]
                    )
                    if not hgnc_id:
                        hgnc_id = None

                    try:
                        tx = Transcript(
                            id=None,
                            ref_genome=ref_genome,
                            chromosome=chromosome,
                            start=line_data.start - 1,
                            end=line_data.end,
                            coding_start=None,
                            coding_end=None,
                            exon_starts=[],
                            exon_ends=[],
                            hgnc_id=hgnc_id,  # type: ignore
                            name=transcript_name,
                            strand=line_data.strand,  # type: ignore
                            source=source,
                        )
                    except ValidationError:
                        logger.debug(
                            f"Failed to add as transcript: type: {t}, name: {transcript_name}"
                        )
                        continue
                    transcripts[transcript_key] = tx
                    logger.debug(f"Transcript {transcript_key} added")
                    continue

    yield from flush_transcripts(transcripts)
    transcripts = {}
