import logging
from collections import defaultdict
from typing import Iterator

from sqlmodel import Session, func, select

from gpbuilder.cli.update_data.common import (
    UpdateStatus,
    set_next_update_id,
)
from gpbuilder.datamodel.transcript import ManeSelectTranscript, Transcript
from gpbuilder.ncbi_api.fetch_mane_select import fetch_mane_select

logger = logging.getLogger(__name__)


def insert_mane_transcript(session, mane_tx):
    session.add(mane_tx)


def fetch_mane_select_transcripts(
    db_transcripts_by_name,
) -> Iterator[ManeSelectTranscript]:
    for mane_info in fetch_mane_select():
        if mane_info.transcript_name not in db_transcripts_by_name:
            logger.warning(
                f"Transcript {mane_info.transcript_name} not in database. Ignoring."
            )
        else:
            # There could be multiple transcripts with the same name (different reference genomes)
            for db_tx in db_transcripts_by_name[mane_info.transcript_name]:
                yield ManeSelectTranscript(
                    status=mane_info.status,
                    transcript_id=db_tx.id,
                    update_id=None,  # type: ignore
                )


def update_mane_transcripts(session: Session):
    # Delete all existing MANE entries
    pre_existing_mane_tx = session.exec(select(ManeSelectTranscript)).all()
    for mane_tx in pre_existing_mane_tx:
        session.delete(mane_tx)
    session.flush()
    pre_existing_mane_tx = set(
        [(mane_tx.transcript_id, mane_tx.status) for mane_tx in pre_existing_mane_tx]
    )

    # Add new entries
    new_mane_tx = set()
    db_transcripts_by_name: dict[str, list[Transcript]] = defaultdict(list)
    for tx in session.exec(select(Transcript)).all():
        db_transcripts_by_name[tx.name].append(tx)

    update_id = set_next_update_id("mane_transcript", session)
    for mane_tx in fetch_mane_select_transcripts(db_transcripts_by_name):
        mane_tx.update_id = update_id
        insert_mane_transcript(session, mane_tx)
        new_mane_tx.add((mane_tx.transcript_id, mane_tx.status))

    # Compare new entries with pre-existing (now deleted) entries to see if any were updated

    counter = UpdateStatus.get_counter()
    counter[UpdateStatus.CREATED] = len(new_mane_tx - pre_existing_mane_tx)  # type: ignore
    counter[UpdateStatus.DELETED] = len(pre_existing_mane_tx - new_mane_tx)  # type: ignore
    counter[UpdateStatus.IDENTICAL] = len(new_mane_tx & pre_existing_mane_tx)  # type: ignore

    total_count = session.exec(select(func.count(ManeSelectTranscript.id))).one()  # type: ignore

    logger.info(f"Update counts: {counter}")
    logger.info(f"Total number of MANE entries: {total_count}")
