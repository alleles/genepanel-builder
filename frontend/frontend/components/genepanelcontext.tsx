import React, {
  Context,
  useContext,
  useEffect,
  useCallback,
  useMemo,
  useState,
} from 'react'
import {
  ConfidenceLevels,
  Gene,
  GeneExclusionReasons,
  GenePanelConfig,
  Inheritance,
  PanelAppPanel,
  Region,
} from '../types'

import API from 'api'
import { useRouter } from 'next/router'

const fetchPanelAppPanels = async ({
  panelapp_panels,
}: {
  panelapp_panels: GenePanelConfig['panelapp_panels']
}): Promise<PanelAppPanel[]> => {
  const confidenceLevels = panelapp_panels.reduce(
    (acc, p) => ({
      ...acc,
      [p.panelapp_id]: p.confidence_levels,
    }),
    {}
  )
  return await API.PanelAppPanels.post(
    panelapp_panels.map((p) => p.panelapp_id)
  ).then((result) =>
    result.data.map((p) => ({
      ...p,
      confidence_levels: confidenceLevels[p.id],
    }))
  )
}

const fetchGenes = async ({
  hgnc_ids,
}: {
  hgnc_ids: number[]
}): Promise<Gene[]> => {
  return await API.Genes.post(hgnc_ids).then((result) =>
    result.data.map((g) => ({
      ...g,
      aliases: g.aliases ?? [],
      previous_symbols: g.previous_symbols ?? [],
    }))
  )
}

interface GenePanelContext {
  genePanelConfig: GenePanelConfig
  genes: Gene[]
  addGenes: (genesToAdd: Gene[]) => void
  removeGene: (hgncId: number) => void
  panelAppPanels: PanelAppPanel[]
  addPanelAppPanel: (panels: PanelAppPanel) => void
  removePanelAppPanel: (panelappId: number) => void
  setPanelAppPanelLevels: (panelappId: number, levels: ConfidenceLevels) => void
  regions: Region[]
  addRegion: (region: Region) => void
  removeRegion: (region: Region) => void
  savePanel: (config?: Partial<GenePanelConfig>) => Promise<number>
  fetchGeneRegionDetails: () => Promise<any>
  dirty: boolean
  setDirty: () => void
}
const genePanelContext: Context<GenePanelContext | undefined> =
  React.createContext(undefined)

export function GenePanelProvider({ children }: { children: React.ReactNode }) {
  const [genePanelConfig, setGenePanelConfig] = useState<GenePanelConfig>({
    id: null,
    name: '',
    shortname: '',
    description: '',
    version: '',
    hgnc_ids: [],
    panelapp_panels: [],
    regions: [],
    diff: {},
  })

  const [genes, setGenes] = useState<Gene[]>([])
  const [panelAppPanels, setPanelAppPanels] = useState<PanelAppPanel[]>([])
  const [regions, setRegions] = useState<Region[]>([])
  const [dirty, setDirty] = useState(false)

  // Load config if id is set in URL
  const router = useRouter()
  const { id } = router.query

  useEffect(() => {
    const fetch = async (id) => {
      const gpc = await API.GenePanelConfig.get(id).then(
        (response) => response.data
      )
      setGenePanelConfig(gpc)
      setPanelAppPanels(await fetchPanelAppPanels(gpc))
      setGenes(await fetchGenes(gpc))
      setRegions(
        gpc.regions.map((r) => ({
          ...r,
          comment: r.comment ?? '',
          inheritance: r.inheritance as Inheritance, // Need to cast to enum
        }))
      )
    }
    if (id) {
      fetch(id)
    }
  }, [id])

  // PanelApp functions
  const addPanelAppPanel = useCallback(
    (panel: PanelAppPanel) => {
      setPanelAppPanels([...panelAppPanels, panel])
      const configPanel: GenePanelConfig['panelapp_panels'][number] = {
        panelapp_id: panel.id,
        confidence_levels: panel.confidence_levels,
      }
      setGenePanelConfig({
        ...genePanelConfig,
        panelapp_panels: [...genePanelConfig.panelapp_panels, configPanel],
      })
      setDirty(true)
    },
    [genePanelConfig, panelAppPanels]
  )

  const removePanelAppPanel = useCallback(
    (panelapp_id: number) => {
      setPanelAppPanels(
        panelAppPanels.filter((panel) => panel.id !== panelapp_id)
      )
      setGenePanelConfig({
        ...genePanelConfig,
        panelapp_panels: genePanelConfig.panelapp_panels.filter(
          (panel) => panel.panelapp_id !== panelapp_id
        ),
      })
      setDirty(true)
    },
    [genePanelConfig, panelAppPanels]
  )

  const setPanelAppPanelLevels = useCallback(
    (panelapp_id: number, levels: ConfidenceLevels) => {
      const panel = panelAppPanels.find((panel) => panel.id === panelapp_id)
      panel.confidence_levels = levels
      setPanelAppPanels([...panelAppPanels])
      const configPanel = genePanelConfig.panelapp_panels.find(
        (panel) => panel.panelapp_id === panelapp_id
      )
      configPanel.confidence_levels = levels
      setGenePanelConfig({ ...genePanelConfig })
      setDirty(true)
    },
    [genePanelConfig, panelAppPanels]
  )

  // Gene functions
  const addGenes = useCallback(
    (genesToAdd: Gene[]) => {
      setGenes([...genes, ...genesToAdd])
      setGenePanelConfig({
        ...genePanelConfig,
        hgnc_ids: [
          ...genePanelConfig.hgnc_ids,
          ...genesToAdd.map((g) => g.hgnc_id),
        ],
      })
      setDirty(true)
    },
    [genePanelConfig, genes]
  )

  const removeGene = useCallback(
    (hgnc_id: number) => {
      setGenes(genes.filter((gene) => gene.hgnc_id !== hgnc_id))
      setGenePanelConfig({
        ...genePanelConfig,
        hgnc_ids: genePanelConfig.hgnc_ids.filter((id) => id !== hgnc_id),
      })
      setDirty(true)
    },
    [genePanelConfig, genes]
  )

  // Region functions
  const addRegion = useCallback(
    (region: Region) => {
      const configRegion: GenePanelConfig['regions'][number] = {
        name: region.name,
        chromosome: region.chromosome,
        start: region.start,
        end: region.end,
        ref_genome: 'GRCh37',
        comment: region.comment,
      }

      setRegions([...regions, region])
      setGenePanelConfig({
        ...genePanelConfig,
        regions: [...genePanelConfig.regions, configRegion],
      })
      setDirty(true)
    },
    [genePanelConfig, regions]
  )

  const removeRegion = useCallback(
    (region: Region) => {
      const regionEquals = (r1, r2) => {
        return (
          r1.chromosome === r2.chromosome &&
          r1.start === r2.start &&
          r1.end === r2.end &&
          r1.ref_genome === r2.ref_genome
        )
      }
      setRegions(regions.filter((r) => !regionEquals(r, region)))
      setGenePanelConfig({
        ...genePanelConfig,
        regions: genePanelConfig.regions.filter(
          (r) => !regionEquals(r, region)
        ),
      })
      setDirty(true)
    },
    [genePanelConfig, regions]
  )

  const fetchPanel = useCallback(async (id) => {
    const gpc = await API.GenePanelConfig.get(id).then(
      (response) => response.data
    )
    setGenePanelConfig(gpc)
    setPanelAppPanels(await fetchPanelAppPanels(gpc))
    setGenes(await fetchGenes(gpc))
    setRegions(
      gpc.regions.map((r) => ({
        ...r,
        comment: r.comment ?? '',
        inheritance: r.inheritance as Inheritance, // Need to cast to enum
      }))
    )
  }, [])

  const savePanel = useCallback(
    async (updatedGenePanel: Partial<GenePanelConfig> = {}) => {
      // No point in saving if dirty is false (nothing to save)
      if (!dirty) {
        return genePanelConfig.id
      }
      return API.Configure.put({
        ...genePanelConfig,
        ...updatedGenePanel,
        version: '',
      }).then((response) => {
        const id = response.data.id
        setDirty(false)
        return fetchPanel(id).then(() => {
          return id
        })
      })
    },
    [fetchPanel, genePanelConfig, dirty]
  )

  const select_endpoint_payload = useMemo(
    () => ({
      hgnc_ids: genePanelConfig.hgnc_ids,
      panelapp_panels: genePanelConfig.panelapp_panels,
      regions: genePanelConfig.regions.map((r) => ({
        id: r.id,
        chromosome: r.chromosome,
        start: r.start,
        end: r.end,
        ref_genome: r.ref_genome,
      })),
    }),
    [
      genePanelConfig.hgnc_ids,
      genePanelConfig.panelapp_panels,
      genePanelConfig.regions,
    ]
  )

  const fetchGeneRegionDetails = useCallback(
    async function () {
      // No need to fetch anything if there are no genes, regions or PanelApp panels
      if (
        select_endpoint_payload.hgnc_ids.length === 0 &&
        select_endpoint_payload.regions.length === 0 &&
        select_endpoint_payload.panelapp_panels.length === 0
      ) {
        return [[], []]
      }
      return await API.Select.post(select_endpoint_payload).then((response) => {
        const geneData = response.data.genes.map((gene) => {
          const hasDiff = gene.hgnc_id in genePanelConfig.diff
          const diffContent = genePanelConfig.diff[gene.hgnc_id]
          return {
            hgnc_id: gene.hgnc_id.toString(),
            symbol: gene.symbol,
            inheritance:
              hasDiff && diffContent?.inheritance
                ? (diffContent.inheritance as Inheritance)
                : (gene.inheritance as Inheritance),
            transcripts: {
              current:
                hasDiff && diffContent?.transcripts?.length
                  ? diffContent.transcripts
                  : gene.default_transcripts,
              defaults: gene.default_transcripts,
              available: gene.available_transcripts,
              origin: gene.transcript_origin,
            },
            coverageWGS: gene.coverage.coverageWGS,
            coverageWES: gene.coverage.coverageWES,
            segDup: gene.segdup,
            panelAppPanels: gene.origin.map((origin) => ({
              id: origin.panelapp_id,
              confidence: origin.conf_level,
            })),
            exclusion_reasons:
              hasDiff && diffContent?.exclusion_reasons
                ? diffContent.exclusion_reasons.map(
                    (r) => r as GeneExclusionReasons
                  )
                : [],
            comment: hasDiff && diffContent.comment ? diffContent.comment : '',
          }
        })
        const regionData = response.data.regions.map((region) => {
          const configRegion = genePanelConfig.regions.find(
            (r) =>
              r.chromosome === region.region.chromosome &&
              r.start === region.region.start &&
              r.end === region.region.end &&
              r.ref_genome === region.region.ref_genome
          )
          if (!configRegion) {
            throw new Error('Region not found in config')
          }

          return {
            chromosome: region.region.chromosome,
            start: region.region.start,
            end: region.region.end,
            region: `${region.region.chromosome}:${region.region.start}-${region.region.end}`,
            inheritance: (configRegion.inheritance || 'N/A') as Inheritance,
            name: configRegion.name,
            comment: configRegion.comment,
            coverageWGS: region.coverage?.coverageWGS || -1,
            coverageWES: region.coverage?.coverageWES || -1,
            segDup: region.segdup,
          }
        })
        return [geneData, regionData]
      })
    },
    [genePanelConfig.diff, genePanelConfig.regions, select_endpoint_payload]
  )

  return (
    <genePanelContext.Provider
      value={{
        genes,
        genePanelConfig,
        panelAppPanels,
        addPanelAppPanel,
        removePanelAppPanel,
        setPanelAppPanelLevels,
        addGenes,
        removeGene,
        regions,
        addRegion,
        removeRegion,
        savePanel,
        fetchGeneRegionDetails,
        dirty,
        setDirty: () => setDirty(true), // Only allow setting dirty=true
      }}
    >
      {children}
    </genePanelContext.Provider>
  )
}

export default function useGenePanel(): GenePanelContext {
  return useContext(genePanelContext)
}
