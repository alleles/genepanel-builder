from pathlib import Path

import numpy as np
import numpy.typing as npt
import pytest

from gpbuilder.coverage.D4Reader import D4NumpyReader


@pytest.fixture
def d4r():
    return D4NumpyReader(Path(__file__).resolve().parent / "test.d4", 0)


def test_d4_from_bedgraph_uses_zero_halfopen_interval(d4r: D4NumpyReader):
    """D4 from bedgraph uses zero-based half-open intervals

    https://genome-blog.soe.ucsc.edu/blog/2016/12/12/the-ucsc-genome-browser-coordinate-counting-systems/
    """
    chr1: npt.NDArray[np.int32] = d4r.d4_file["1"]  # type: ignore
    assert list(chr1[8:10]) == [0, 0]
    assert list(chr1[10:11]) == [4]
    assert list(chr1[13:15]) == [4, 4]
    assert list(chr1[13:16]) == [4, 4, 5]


def test_gets_values_for_a_region(d4r: D4NumpyReader):
    # Values at [10:12] are [4, 4]
    d4r.threshold = 5
    assert list(d4r.get_values_for_region("1", 10, 12)) == [0, 0]  # 4 < 5
    d4r.threshold = 3
    assert list(d4r.get_values_for_region("1", 10, 12)) == [1, 1]  # 4 >= 3

    # Values at [14:18] are [4, 5, 5, 88]
    d4r.threshold = 89
    assert list(d4r.get_values_for_region("1", 14, 18)) == [0, 0, 0, 0]
    d4r.threshold = 6
    assert list(d4r.get_values_for_region("1", 14, 18)) == [0, 0, 0, 1]
    d4r.threshold = 5
    assert list(d4r.get_values_for_region("1", 14, 18)) == [0, 1, 1, 1]
    d4r.threshold = 3
    assert list(d4r.get_values_for_region("1", 14, 18)) == [1, 1, 1, 1]


def test_returns_empty_array_if_chromosome_not_in_file(
    d4r: D4NumpyReader, caplog: pytest.LogCaptureFixture
):
    assert d4r.get_values_for_region("wrongchr", 5, 10).size == 0
    assert "wrongchr" in caplog.text, "Writes logging error for failed lookup"


def test_indexing_out_of_bounds_is_pythonic(d4r: D4NumpyReader):
    assert list(d4r.get_values_for_region("1", 9999, 99999)) == []
    # But does not make sense to allow start_pos > stop_pos
    with pytest.raises(AssertionError):
        d4r.get_values_for_region("1", 10, 5)


def test_gets_number_of_values_above_or_equal_to_threshold(d4r: D4NumpyReader):
    d4r.threshold = 5
    assert d4r.get_number_ge_threshold("1", 10, 20) == 5
    d4r.threshold = 79
    assert d4r.get_number_ge_threshold("1", 0, 600) == 49 + 3
    d4r.threshold = 999
    assert d4r.get_number_ge_threshold("1", 0, 600) == 0


def test_calculates_fraction_ge_threshold(d4r: D4NumpyReader):
    """Given a threshold and list of regions,
    calculates fraction of each region and overall having values greater or equal to threshold.
    """
    # [10:12] is [4, 4], [14:18] is [4, 5, 5, 88]
    regions = [("1", 10, 12), ("1", 14, 18)]
    d4r.threshold = 5
    fractions = d4r.fraction_ge_threshold(regions)
    assert fractions.fraction_across_regions == 0.5
    assert fractions.fraction_per_region == [0.00, 0.75]
