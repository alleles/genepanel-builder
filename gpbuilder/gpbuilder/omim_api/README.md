OMIM API access
===========

Intro
--------
API to get phenotype information from gene symbols.
More info: https://omim.org/api and https://omim.org/help/api

API key
------
The key is valid **one** year, it belongs to Erik Severinsen (monoburn@gmail.com)

Sample API response
-------------
 Create API sample response for use in testing:
 `wget -q -O-  "https://api.omim.org/api/geneMap/search?apiKey=<OMIM_API_KEY>&format=json&search=gene_symbol:<some gene symbol>"  | json_pp > some_gene_info.json`
