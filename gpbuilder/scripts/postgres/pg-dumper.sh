#!/bin/bash

set -euf
set -o pipefail

APP_NAME="gpbuilder"

PG_HOST=${PG_HOST:-postgres}
PG_PORT=${PG_PORT:-5432}
PG_USER=${PG_USER:-postgres}
PG_PWD=${PG_PWD:-postgres}
PG_DB=${PG_DB:-postgres}

DUMP_FILE="pg-dump.sql.gz"
BUCKET="s3://genepanel-builder/database-dumps"
BUCKET_PATH="$BUCKET/$DUMP_FILE"

create_db() {
    PGPASSWORD="$PG_PWD" createdb -h "$PG_HOST" -U "$PG_USER" "$PG_DB"
}

drop_db() {
    PGPASSWORD="$PG_PWD" dropdb -h "$PG_HOST" -U "$PG_USER" "$PG_DB" --force --if-exists
}

dump() {
    if [ -z "${SPACES_KEY:-}" ] || [ -z "${SPACES_SECRET:-}" ]; then
        printf "No DO tokens provided\n"
        exit 1
    fi
    printf "Dumping $APP_NAME database to gzip archive\n"
    PGPASSWORD="$PG_PWD" pg_dump -h "$PG_HOST" -U "$PG_USER" -d "$PG_DB"  > /tmp/dump.psql
    gzip -9 /tmp/dump.psql
    s3cmd \
        --access_key=${SPACES_KEY} \
        --secret_key=${SPACES_SECRET} \
        --host fra1.digitaloceanspaces.com \
        --host-bucket=%\(bucket\)s.fra1.digitaloceanspaces.com \
        put /tmp/dump.psql.gz "$BUCKET_PATH"
    s3cmd \
        --access_key=${SPACES_KEY} \
        --secret_key=${SPACES_SECRET} \
        --host fra1.digitaloceanspaces.com \
        --host-bucket=%\(bucket\)s.fra1.digitaloceanspaces.com \
        setacl "$BUCKET_PATH" --acl-public
    printf "$APP_NAME database successfully dumped\n"
}

restore() {
    if [ -z "$DUMP_FILE" ]; then
        printf "No db dump file provided"
    else
        printf "Restoring $APP_NAME database from gzip archive: $DUMP_FILE\n"
        drop_db
        create_db
        PGPASSWORD="$PG_PWD" psql \
            --host="$PG_HOST" \
            --username="$PG_USER" \
            --dbname="$PG_DB" \
            -c "CREATE EXTENSION IF NOT EXISTS semver;"
        curl -s "https://genepanel-builder.fra1.digitaloceanspaces.com/database-dumps/$DUMP_FILE" \
            | gunzip -c \
            | PGPASSWORD="$PG_PWD" psql \
                --host="$PG_HOST" \
                --username="$PG_USER" \
                --dbname="$PG_DB"

        # Backup database to "initial" template, to be used in tests
        PGPASSWORD="$PG_PWD" dropdb \
            --host="$PG_HOST" \
            --username="$PG_USER" \
            --force --if-exists \
            "${PG_DB}"_initial
        PGPASSWORD="$PG_PWD" createdb \
            --host="$PG_HOST" \
            --username="$PG_USER" \
            --template="$PG_DB" \
            "${PG_DB}"_initial
        printf "\n$APP_NAME database successfully restored.\n"
    fi
}

dflag=
rflag=
while getopts dr action; do
    case $action in
    d)
        dflag=1
        ;;
    r)
        rflag=1
        ;;
    ?)
        printf "Usage: %s: [-d(ump)] [-r(estore)]\n" $0
        exit 2
        ;;
    esac
done

if [ ! -z "$dflag" ]; then
    dump
fi
if [ ! -z "$rflag" ]; then
    restore
fi
