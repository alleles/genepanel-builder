from fastapi import APIRouter

from gpbuilder.oauth import api

auth_router = APIRouter()
auth_router.include_router(api.router)
