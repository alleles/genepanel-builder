from sqlmodel import select

from gpbuilder.api.tests.mock_data import create_genes
from gpbuilder.cli.update_data.common import UpdateStatus
from gpbuilder.cli.update_data.transcript import insert_transcript
from gpbuilder.datamodel.transcript import Transcript


def get_tx(overrides=None):
    if overrides is None:
        overrides = {}
    return Transcript(
        **{
            "id": None,
            "name": "NM_000000.1",
            "chromosome": "1",
            "strand": "+",
            "start": 1,
            "end": 100,
            "coding_start": 1,
            "coding_end": 100,
            "exon_starts": [1, 50],
            "exon_ends": [49, 100],
            "hgnc_id": None,  # type: ignore
            "ref_genome": "GRCh37",
            "source": "test",
            **overrides,
        }
    )


def test_insert_transcript(session):
    (gene,) = create_genes(1, session)
    tx = get_tx({"hgnc_id": gene.hgnc_id})

    assert insert_transcript(session, tx, {gene.hgnc_id}) == UpdateStatus.CREATED
    session.flush()
    db_tx = session.exec(select(Transcript)).one()
    assert db_tx.dict(exclude={"id"}) == tx.dict(exclude={"id"})


def test_insert_transcript_ignored(session):
    """Test that we can't insert a transcript with a non-existing HGNC ID"""
    tx = get_tx({"hgnc_id": 1})
    assert insert_transcript(session, tx, set()) == UpdateStatus.IGNORED
    session.flush()


def test_insert_transcript_new_hgnc_id(session):
    """Test that we can update the HGNC ID of a transcript"""
    (withdrawn_gene, new_gene) = create_genes(2, session)
    withdrawn_gene.withdrawn = True
    withdrawn_gene.new_hgnc_id = new_gene.hgnc_id
    tx = get_tx({"hgnc_id": withdrawn_gene.hgnc_id})

    assert insert_transcript(session, tx, {new_gene.hgnc_id}) == UpdateStatus.CREATED

    db_tx = session.exec(select(Transcript)).one()
    assert db_tx.hgnc_id == new_gene.hgnc_id


def test_insert_transcript_sex_chromosomes(session):
    """
    Test that update is refused if trying to insert a Y-chromosome transcript,
    when there already exists and X-chromosome transcript with the same name and ref-genome

    Test also that the other way around should update
    """
    (gene,) = create_genes(1, session)
    tx1 = get_tx({"hgnc_id": gene.hgnc_id, "chromosome": "X"})
    tx2 = get_tx({"hgnc_id": gene.hgnc_id, "chromosome": "Y"})

    assert insert_transcript(session, tx1, {gene.hgnc_id}) == UpdateStatus.CREATED
    assert insert_transcript(session, tx2, {gene.hgnc_id}) == UpdateStatus.REFUSED

    db_tx = session.exec(select(Transcript)).one()
    assert db_tx.chromosome == "X"
    assert db_tx.dict(exclude={"id"}) == tx1.dict(exclude={"id"})

    tx1 = get_tx({"name": "B", "hgnc_id": gene.hgnc_id, "chromosome": "Y"})
    tx2 = get_tx({"name": "B", "hgnc_id": gene.hgnc_id, "chromosome": "X"})

    # Now try the other way around
    assert insert_transcript(session, tx1, {gene.hgnc_id}) == UpdateStatus.CREATED
    assert insert_transcript(session, tx2, {gene.hgnc_id}) == UpdateStatus.UPDATED

    db_tx = session.exec(select(Transcript).where(Transcript.name == "B")).one()
    assert db_tx.chromosome == "X"
    assert db_tx.dict(exclude={"id"}) == tx2.dict(exclude={"id"})


def test_insert_transcript_update(session):
    (gene,) = create_genes(1, session)
    tx1 = get_tx({"hgnc_id": gene.hgnc_id, "source": "UPDATE ME"})
    assert insert_transcript(session, tx1, {gene.hgnc_id}) == UpdateStatus.CREATED

    tx2 = get_tx(
        {"hgnc_id": gene.hgnc_id, "start": tx1.start + 1, "source": "SOME OTHER SOURCE"}
    )
    assert insert_transcript(session, tx2, {gene.hgnc_id}) == UpdateStatus.UPDATED
    db_tx = session.exec(select(Transcript)).one()
    assert db_tx.dict(exclude={"id"}) == tx2.dict(exclude={"id"})
