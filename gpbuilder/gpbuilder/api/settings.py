from fastapi import APIRouter

import gpbuilder

router = APIRouter()


# Endpoints


@router.get("/version", tags=["Settings"])
async def get_version():
    return gpbuilder.__version__
