#!/usr/bin/env python
"Genomics England PanelApp interface"

import json
import logging
import sys

import requests

__all__ = ["PanelAppQuery", "PanelAppApi"]


BASE_URL = {
    "AUS": "https://panelapp.agha.umccr.org/api/v1",
    "ENG": "https://panelapp.genomicsengland.co.uk/api/v1",
}


class PanelAppQuery(object):
    """
    Generic PanelApp query container
    """

    class FormatException(Exception):
        pass

    arg_list = ("_pid", "_pname", "_pconf", "_site")

    def __init__(
        self, pid, pname="", pconf=3, site=sorted(BASE_URL.keys()).pop(), **attributes
    ):
        self._pid = int(pid)
        self._pname = str(pname)
        self._pconf = int(pconf)
        self._site = str(site) or sorted(BASE_URL.keys()).pop()
        self._meta = attributes
        if not isinstance(self._pid, int):
            raise PanelAppQuery.FormatException("PanelApp identifier: integer expected")
        if not isinstance(self._pname, str):
            raise PanelAppQuery.FormatException(
                "PanelApp name: character string expected"
            )
        if self._pconf not in [1, 2, 3]:
            raise PanelAppQuery.FormatException(
                "PanelApp confidence level: one of 1,2,3 expected"
            )
        if self._site not in BASE_URL.keys():
            raise PanelAppQuery.FormatException(
                "PanelApp site: one of %s expected, got %s"
                % (str(BASE_URL.keys()), self._site)
            )

    @property
    def id(self):
        return self._pid

    @property
    def name(self):
        return self._pname

    @property
    def confidence_level(self):
        return self._pconf

    @property
    def site(self):
        return self._site

    @property
    def metadata(self):
        return self._meta


class PanelAppApi(object):
    """
    Queries Genomics England PanelApp for genes belonging in specific gene panels.

    See https://panelapp.genomicsengland.co.uk for more information
    """

    def __init__(self, *query, panelapp_info_path=""):
        self._log = logging.getLogger(__name__)
        self._panelapp_info_path = panelapp_info_path
        self._query = None
        if query:
            if len(query) == 1 and type(query[0]) is PanelAppQuery:
                self._log.debug("detected standard PanelApp query")
                self._query = query[0]
            elif query:
                self._log.debug("detected manual PanelApp query")
                self._query = PanelAppQuery(*query)
            else:
                raise PanelAppQuery.FormatException(
                    "'%s' doesn't look like a valid PanelApp query" % str(query)
                )
            self._log.debug(
                (
                    "panelapp query:\n"
                    "  id={pid}\n"
                    "  name={name}\n"
                    "  conf={conf}\n"
                    "  site={site}\n"
                    "  meta={meta}\n"
                ).format(
                    pid=self._query.id,
                    name=self._query.name,
                    conf=self._query.confidence_level,
                    site=self._query.site,
                    meta=self._query.metadata,
                )
            )

    @staticmethod
    def get_all_panels(site=sorted(BASE_URL.keys()).pop(), db_dump=""):
        """Return dict of all panels from the /panels endpoint."""
        panels = {}
        _log = logging.getLogger(__name__)
        if db_dump:
            with open(db_dump, "r") as fh:
                return json.load(fh)
        try:
            requestString = "/".join((BASE_URL[site], "panels"))
        except KeyError:
            _log.error("unknown PanelApp site: {}".format(site))
            return panels
        _log.info("requesting {}".format(requestString))
        sys.stdout.flush()
        while requestString:
            r = requests.get(requestString)
            response = json.loads(r.text)
            for panel in response["results"]:
                _log.debug("adding panel with id '{}'".format(str(panel["id"])))
                assert panel["name"] not in panels
                panels[panel["name"]] = panel
            requestString = response["next"]
        return panels

    @staticmethod
    def get_panel(
        panelId, panelVersion="", site=sorted(BASE_URL.keys()).pop(), db_dump=""
    ):
        """Return panel data for a given panelId from /panels/{id}."""
        response = {}
        _log = logging.getLogger(__name__)
        if db_dump:
            response = PanelAppApi.parse_panelapp_dump(db_dump, panelId, panelVersion)
        if not response:
            try:
                requestString = "/".join((BASE_URL[site], "panels", str(panelId)))
            except KeyError:
                _log.error("unknown PanelApp site: {}".format(site))
                return response
            if panelVersion:
                requestString = requestString + "?version=%s" % panelVersion
            _log.info("requesting {}".format(requestString))
            r = requests.get(requestString)
            response = json.loads(r.text)
            _log.info(
                "downloaded panel %d [%s] -- version %s"
                % (panelId, response["name"], response["version"])
            )
        return response

    @staticmethod
    def parse_panelapp_dump(db_dump, pid, pversion=""):
        """Parse panelapp JSON dump for a given panel id[version]."""
        pdict = {}
        _log = logging.getLogger(__name__)
        with open(db_dump, "r") as fh:
            panelappDict = json.load(fh)
            for name, content in panelappDict.items():
                if content["id"] == pid:
                    if pversion == "" or content["version"] == pversion:
                        pdict.update(content)
        if not pdict:
            _log.warning(
                "panel %d v%s is not available in PanelApp dump '%s'"
                % (pid, pversion, db_dump)
            )
        return pdict
