# Deploying to production

## Prerequisites

- A Linux server with a working installation of [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/install/).
- Git

## Setup

Switch to the user you want to be running the service as. Then clone the repository:

```sh
git clone https://gitlab.com/alleles/genepanel-builder.git
cd genepanel-builder
```

## Quick guide

Generate coverage and segmental duplications D4 files (see [Coverage and Segmental Duplications](./Coverage_segdup.md)). The parameters determining how to compute the coverage and segmental duplications statistics shown in the UI are provided through a JSON file stored in the `GENERAL_SETTINGS` environment variable (see `gpbuilder/resources/example_settings.json` for an example).

See [Coverage and Segmental Duplications](./Coverage_segdup.md) for further details.

Copy the `docs/deployment` directory to the desired location (e.g. `/home/gpbuilder/deployment`), modify the paths in `.env` and `docker-compose.volumes.yaml`, and put the D4 files `wes.d4`, `wgs.d4`, and `segdup.d4` in the subdirectory called `d4-files`.

From the `genepanel-builder` root directory, run

```sh
docker compose \
    --env-file /home/gpbuilder/deployment/.env \
    -f docker-compose-prod.yml \
    -f /home/gpbuilder/deployment/docker-compose.volumes.yml \
    up --build --detach \
    gpbuilder \
    frontend \
    nginx \
    postgres
```

!!! Note

    This will launch GPB without authentication

## Configuring authentication & authorization

Set `USE_OAUTH` to `True` and provide the configuration for authorization by passing the following environment variables to the `gpbuilder` service:

```sh
SESSION_FILE =  <LOCATION_OF_SESSION_STORE_DB_FILE>
CALLBACK_ROUTE = <CALLBACK_ENDPOINT_REGISTERED_WITH_PROVIDER>
OIDC_CLIENT_ID = <OIDC_CLIENT_ID>
OIDC_CLIENT_SECRET = <OIDC_CLIENT_SECRET>
CONF_URL = <OIDC_WELL_KNOWN_CONFIGURATION_URL>
SCOPE = <SCOPES>
DEFAULT_SCOPE = <DEFAULT_SCOPES>
LOGIN_REDIRECT_URL = <URL_WHERE_USER_LOGS_IN>
```

See also [Authentication and authorization](./Authentication_and_Authorization.md).

## Using an external postgres

Remove `postgres` from the commands above, and supply URI in the form of environment variables (see `.env`).

## Mounting additional volumes

By default, the data mounted into the container are very limited. You might want to mount in e.g. D4-files. This can be done by supplying an additional docker compose file (see <https://docs.docker.com/compose/how-tos/multiple-compose-files/extends>).

## Populating the database with external data

After startup, it is necessary to populate the database with external data. This is done through the cli:

```sh
docker compose exec -it gpbuilder gpb update all-external
```

See also [Command line interface](./Command_line_interface.md).

To be able to download OMIM data, an API key is required. [Request access here](https://omim.org/api). OMIM information is not required for the GPB to function, however, without it all modes of inheritance will be set to default values.

## Exposing the service through a web proxy

You should expose the service through a web proxy. If you use nginx, this is a good starting point (assuming your domain is `gpb.your.doma.in`):

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name gpb.your.doma.in;

    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_pass http://127.0.0.1:3000;
    }

    location ~* ^/(api/auth|api/v1|docs|openapi.json) {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://127.0.0.1:8000;
    }
}
```
